---
id: "683742055681136"
title: Jürgen Kaube (FAZ) zum 250. Geburtstag Hegels
start: 2020-11-12 18:30
locationName: Universität Ulm
address: Helmholtzstraße 16, 89081 Ulm
link: https://www.facebook.com/events/683742055681136/
image: 120200496_3741360652565469_1945405391473251934_n.jpg
teaser: "Dem 250. Geburtstag von Georg Wilhelm Friedrich Hegel in diesem Jahr gedenkt
  das Humboldt-Studienzentrum mit zwei Hegelvorträgen:   02.11.2020: Prof."
isCrawled: true
---
Dem 250. Geburtstag von Georg Wilhelm Friedrich Hegel in diesem Jahr gedenkt das Humboldt-Studienzentrum mit zwei Hegelvorträgen: 

02.11.2020: Prof. Dr. Markus Gabriel, Universität Bonn, Stadthaus Ulm, 20:00 Uhr.

12.11.2020: Jürgen Kaube, Mitherausgeber der FAZ, in Zusammenarbeit mit der Goethe-Gesellschaft, Universität Ost N27, Multimediaraum, 18:30 Uhr.

Für beide Vorträge ist begrenzt Publikum zugelassen, im Stadthaus bis zu 57 Personen, im Mulitmediaraum 30 Personen. Zudem werden die Vorträge live gestreamt über webex-event. Ein entsprechender Link dazu wird eine Woche vorher auf der Homepage des HSZ bekannt gegeben.