---
id: "2915601951840687"
title: Ulmer Denkanstöße 2020 "Intensität"
start: 2020-03-11 00:00
end: 2020-03-14 00:00
locationName: Stadthaus Ulm
address: Münsterplatz 50, 89073 Ulm
link: https://www.facebook.com/events/2915601951840687/
image: 87734571_3532691056773366_8865686937105596416_o.jpg
teaser: Intensität ist das Versprechen der Moderne. Das unstillbare Verlangen des
  Menschen nach Glück artikuliert sich in der Moderne als andauernde Steigerun
isCrawled: true
---
Intensität ist das Versprechen der Moderne. Das unstillbare Verlangen des Menschen nach Glück artikuliert sich in der Moderne als andauernde Steigerung der Lebensintensität. Nicht nur immer mehr erfreuliche Erfahrungen soll das gute Leben bringen, es soll auch immer intensiver sein. Dabei ist das Streben nach Intensität keineswegs nur negativ, artikuliert sich darin doch eine lebenbejahende Haltung, die alle Bereiche durchdringt: Intensivierung der Wahrnehmung, etwa in Kino, Cyberspace oder der Küche, Intensivierung des Körpergefühls und der Fitness, Intensivierung von Achtsamkeit im Umgang mit sich selbst oder mit anderen, Steigerung der emotionalen Intensität und vieles mehr.
 
Zugleich aber birgt das Streben nach Intensität große Risiken: alles muss perfekter, beschleunigter, produktiver, maximaler und optimierter werden. Die eiserne Logik maßloser Steigerung treibt das Versprechen der Moderne an eine Grenze: Überforderung, Erschöpfung, ja Zerstörung von Mensch und Natur, greifbar etwa in Phänomenen wie Freizeitstress und Burnout. Ausbeutung oder Umweltkrise sind die Folge.

Eine gemeinsame Veranstaltung des Humboldt Studienzentrums der Universität Ulm, der Stadt Ulm und der Sparda-Bank.

Informationen zu den einzelnen Veranstaltungen: http://www.ulmer-denkanstoesse.de/