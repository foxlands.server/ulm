---
id: "188864195733408"
title: Landeskonferenz 2020
start: 2020-04-18 13:00
end: 2020-04-18 18:00
address: Ulm
link: https://www.facebook.com/events/188864195733408/
image: 87644855_3058444077521967_1063996886802759680_o.jpg
teaser: Am Samstag, 18.04.2020, findet die nächste reguläre Landeskonferenz der Falken
  Baden-Württemberg in Ulm statt. Der Beginn wird um 13:00 Uhr sein.  Die
isCrawled: true
---
Am Samstag, 18.04.2020, findet die nächste reguläre Landeskonferenz der Falken Baden-Württemberg in Ulm statt. Der Beginn wird um 13:00 Uhr sein.

Die Landeskonferenz ist das höchste Organ unseres Verbandes und besteht aus den 39 Delegierten der Ortsgruppen. Die Landeskonferenz nimmt die Berichte des Vorstandes sowie des Büros entgegen, entscheidet über satzungsändernde, -ausführende, oder anderweitige Anträge und wählt den Vorstand für die nächsten 2 Jahre (wer genaueres wissen will, kann in unserer Satzung nachschauen: https://www.sjd-falkenbw.de/dokumente/).

Wir freuen uns über Kandidaturen aus den Gliederungen für den Vorstand. Bitte schickt uns dazu euren Bewerbungstext mit Namen, entsendender Gliederung sowie den Posten, für den ihr kandidieren wollt (SJ-Ring Beisitz/Vorsitz, F-Ring Beisitz/Vorsitz, oder Vorsitzende*r) bis zum 23.03.2020 an: buero@sjd-falkenbw.de
Spätere oder spontane Bewerbungen auf der Konferenz sind selbstverständlich auch möglich, diese können dann allerdings nicht in die Delegiertenunterlagen aufgenommen werden und sind den Gliederungen vor der Konferenz somit nicht zugänglich.

Ebenfalls sind Anträge von den Gliederungen an die Konferenz möglich. Die Antragsfrist beträgt 3 Wochen. Sie müssen also bis spätestens zum 28.03.2020 beim Landesbüro vorliegen. Auch hierfür schreibt einfach eine Email an: buero@sjd-falkenbw.de
Initiativanträge sind auch außerhalb der Antragsfrist möglich. Diese müssen sich auf aktuelle Ereignisse beziehen.

Wir freuen uns, mit unserer Konferenz dieses Jahr beim Ortsverband Ulm zu Gast sein zu dürfen. Die Konferenz beginnt Samstag Mittag um 13:00 Uhr. Abends möchten wir dann zusammen im Ulmer Falken-Keller feiern. Für Übernachtungsplätze in der Jugendherberge ist gesorgt! Die Fahrtkosten für Delegierte werden selbstverständlich erstattet. Genauere Informationen zu Ablauf und Format werden wir in den nächsten Wochen zur Verfügung stellen.

Bereits jetzt bitten wir um eure Anmeldung für die Landeskonferenz, um unsere Planung zu erleichtern. Das ist ganz einfach über unsere Homepage möglich: https://www.sjd-falkenbw.de/event/landeskonferenz/