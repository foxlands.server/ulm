---
id: "829394440831708"
title: Flohmarkt im Falkenkeller
start: 2020-02-14 18:00
locationName: SJD - Die Falken Ulm
address: Ziegelländeweg 3 (Oberer Donauturm), Ulm
link: https://www.facebook.com/events/829394440831708/
teaser: Spendenflohmarkt im Falkenkeller!  Trotz Kritik an Konsumkritik, hat nicht
  jede*r die Möglichkeit sich neue Klamotten zu kaufen. Um auch eine Alternat
isCrawled: true
---
Spendenflohmarkt im Falkenkeller!

Trotz Kritik an Konsumkritik, hat nicht jede*r die Möglichkeit sich neue Klamotten zu kaufen. Um auch eine Alternative zu Secondhandläden zu bieten, verantstalten wir einen Flohmarkt mit Kleidung, die nicht mehr getragen wird, aber durchaus noch getragen werden kann.

Alle Sachen können gegen eine Spende mitgenommen werden!

Unser Ziel ist es mit den erhaltenen Spenden des Flohmarkts eine neue Anlage für unseren Raum zu finanzieren, da wir wieder Punkkonzerte und DnB/Hitech-Parties für euch veranstalten wollen!

Zusätzlich findet eine Küfa (Küche für Alle) statt. Es gibt veganes Essen gegen Spende.

Kommt rum! Wir freuen uns auf euch :)


PS.: Falls ihr noch Sachen (in gutem Zustand und gewaschen) loswerden wollt; bringt sie doch am Donnerstag zwischen 18 und 22 Uhr, oder am Freitag ab 16 Uhr vorbei!