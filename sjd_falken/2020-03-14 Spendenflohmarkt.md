---
id: "201700487577868"
title: Spendenflohmarkt
start: 2020-03-14 16:00
locationName: SJD - Die Falken Ulm
address: Ziegelländeweg 3 (Oberer Donauturm), Ulm
link: https://www.facebook.com/events/201700487577868/
image: 87681642_2952480818151727_6528060096159678464_n.jpg
teaser: Und nochmal von vorn; Flohmarkt!  Wie schon letzten Monat veranstaltet der
  Falkenkeller am 14.03.2020 nochmals einen Spendenflohmarkt!   Trotz Kritik
isCrawled: true
---
Und nochmal von vorn; Flohmarkt!

Wie schon letzten Monat veranstaltet der Falkenkeller am 14.03.2020 nochmals einen Spendenflohmarkt! 

Trotz Kritik an Konsumkritik, hat nicht jede*r die Möglichkeit sich neue Klamotten zu kaufen. Um auch eine Alternative zu Secondhandläden zu bieten, verantstalten wir einen Flohmarkt mit Kleidung, die nicht mehr getragen wird, aber durchaus noch getragen werden kann.

Alle Sachen können gegen eine Spende mitgenommen werden!

Unser Ziel ist es mit den erhaltenen Spenden des Flohmarkts eine neue Anlage für unseren Raum zu finanzieren, da wir wieder Punkkonzerte und DnB/Hitech-Parties für euch veranstalten wollen!

Kommt rum! Wir freuen uns auf euch :)


PS.: Falls ihr noch Sachen (in gutem Zustand und gewaschen) loswerden wollt; bringt sie doch auch gern vorbei!