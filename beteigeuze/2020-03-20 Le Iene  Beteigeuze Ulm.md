---
id: "186898149268354"
title: Fällt aus Le Iene + Banda Bastardi Fällt aus
start: 2020-03-20 20:00
end: 2020-03-21 01:00
locationName: Beteigeuze
address: Pfaffenäcker 1, 89075 Ulm
link: https://www.facebook.com/events/186898149268354/
image: 89636504_2892055077555041_6090228939572117504_n.jpg
teaser: Le Iene' is a project bringing together five people from very different
  musical backgrounds. The result is a dynamic, full sounding ska.  https://www.
isCrawled: true
---
Le Iene' is a project bringing together five people from very different musical backgrounds. The result is a dynamic, full sounding ska.

https://www.facebook.com/LEIENEskapunk/
 
Born as a group in Ferrara (IT) in the fall of 2015, they publish their first self-produced EP "Fai la tua Mossa" in February 2017 bringing their energetic music to many venues all over Italy. In this time they've catched the eye of many groups with which they shared the stage, such as: Los Fastidios, Talco, Maleducazione Alcolica, Argies, Iesse, Gasmac Gilmore and many more.
 
Thanks also to the collaboration with Ahoi-Concerts & more the band has performed in numerous venues in Germany, including festivals and open air concerts.
 
While promoting the EP both in Italy and abroad, some tracks from "Fai la tua Mossa" were included in compilations, such as: "What do you know about skapunk? Vol.2" (USA); "Skannibal Party Vol.15" (GER) "Italian Ska Vol.2" (ITA)
 
On July 2018, Le Iene enter officially in the KOB RECORDS roster.

Und als lokalen Support dazu gibt es Banda Bastardi

 Avanti! BandaBastardi gibt Gas und haut euch italienischen Ska-Punk um die Ohren! Die Italienisch-deutsche Mischung vereint schnelle Rhythmen und melodiöse Zutaten. Schmeckt wie bei Mamma!

BandaBastardi wurde 2009 im süddeutschen Nersingen von den Brüdern Giuseppe „Gui“ und Fabio Concialdi gegründet. Schlagzeug, Gitarre und Gesang waren somit vorhanden. Als sich Evi dann mit dem Horn anschloss, entwickelte sich die eigenständige sizilianisch-bayrische Mischung. Mit Pino am Bass wird das Fahrzeug tiefergelegt.

https://de-de.facebook.com/BandaBastardi

bandabastardi.wordpress.com 