---
name: BUND Donau-Iller, Ulm
website: http://www.bund-ulm.de
email: bund.ulm@bund.net
scrape:
  source: facebook
  options:
    page_id: BUNDUlm
---
Bund für Umwelt und Naturschutz Deutschland (BUND) – BUND-Regionalverband Donau-Iller und BUND-Kreisverband Ulm
