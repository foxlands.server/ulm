---
name: Canapé Café
website: http://www.agwest.de/
email: m.gmeiner@agwest.de
scrape:
  source: facebook
  options:
    page_id: Canape-Cafe-403423499744536
---
Im Canapé Café, dem Wohnzimmer der Weststadt fühlt man sich wilkommen. Das gemütliche Ambiente, freundliche Menschen, das köstliche Angebot an selbst hergestellten Kuchen, Suppen, Salaten, vegetarischen Gerichten und Getränken zu ungewöhnlich günstigen Preisen ermöglichen es jedem, sich hier wohl zu fühlen. Möglich ist dies Dank einer bunten Vielfalt an ehrenamtlichen Mitarbeitern. Im Vordergrund stehen persönlicher Kontakt, Begegnung und gegenseitiger Austausch. Canapé Kulturreihe: jeden Mittwoch 19:30, freier Eintritt
