---
name: Medinetz
website: http://www.medinetz-ulm.de
email: kontakt@medinetz-ulm.de
scrape:
  source: facebook
  options:
    page_id: medinetzulm
---
Medizinische Hilfe für Menschen ohne Papiere und/oder ohne Krankenversicherung.
