---
id: "197165451503288"
title: Shiva Dance Export Allgäu
start: 2020-03-13 20:00
end: 2020-03-13 23:00
locationName: Yoga Rauhenstein
address: Rauhenstein, 87487 Wiggensbach
link: https://www.facebook.com/events/197165451503288/
image: 85242035_1052193441781153_8261846738952781824_n.jpg
teaser: Shiva Dance (DJ Klaus) kommt ins Allgäu, um alle begeisterten
  Barfußtänzer*innen der Region mit seiner downbeatigen Ethnotechnomukke zum
  Powackeln, Hü
isCrawled: true
---
Shiva Dance (DJ Klaus) kommt ins Allgäu, um alle begeisterten Barfußtänzer*innen der Region mit seiner downbeatigen Ethnotechnomukke zum Powackeln, Hüftkreisen, Ausdruckstanzen zu verführen...
Wir starten gemeinsam zu einer tänzerischen Reise zu uns selbst..., kommt also pünktlich...:-)