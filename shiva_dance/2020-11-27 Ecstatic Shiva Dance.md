---
id: "1760044730824634"
title: Ecstatic Shiva Dance
start: 2020-11-27 19:30
end: 2020-11-27 22:00
link: https://www.facebook.com/events/1760044730824634/
image: 126748661_1265269640473531_9134538887949437095_n.jpg
teaser: Shiva Dance kommt zu dir per Stream nach Hause. Wer teilnehmen möchte,
  schreibt mir einfach eine kurze Nachricht in den Kommentaren oder an
  k.fumy@gmx
isCrawled: true
---
Shiva Dance kommt zu dir per Stream nach Hause. Wer teilnehmen möchte, schreibt mir einfach eine kurze Nachricht in den Kommentaren oder an k.fumy@gmx.de. Dann bekommst du den Link zum Radio Live Stream. Ich werde ein langes durchgängiges Shiva Dance Set auflegen und freu mich, wenn du reinhörst und mittanzt. 

