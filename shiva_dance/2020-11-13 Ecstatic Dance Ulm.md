---
id: "3604841152906301"
title: Ecstatic Shiva Dance
start: 2020-11-13 19:30
end: 2020-11-13 22:00
link: https://www.facebook.com/events/3604841152906301/
image: 123653942_1254450424888786_665465357630838094_n.jpg
teaser: Ecstatic Shiva Dance kommt zu dir per Stream nach Hause. Wer teilnehmen
  möchte, schreibt mir einfach eine kurze Nachricht an k.fumy@gmx.de. Dann bekom
isCrawled: true
---
Ecstatic Shiva Dance kommt zu dir per Stream nach Hause. Wer teilnehmen möchte, schreibt mir einfach eine kurze Nachricht an k.fumy@gmx.de. Dann bekommst du den Link zum Stream. Ich werde ein langes durchgängiges Shiva Dance Set auflegen und freu mich, wenn du reinhörst und mittanzt. 

