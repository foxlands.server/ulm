---
id: "613538246045235"
title: "Shiva Dance #3/2020"
start: 2020-03-20 21:00
end: 2020-03-21 01:00
locationName: Shiva Dance
address: Frauenstraße 124, 89073 Ulm
link: https://www.facebook.com/events/613538246045235/
image: 87633473_1058578447809319_2284958464448921600_n.jpg
teaser: "Shiva Dance #3/2020. Ich  lade wieder herzlich zu einer neuen Runde barfuß
  tanzen ins AYI ein. Wer organische Beats, Downtempo, melodiösen Techno und"
isCrawled: true
---
Shiva Dance #3/2020. Ich  lade wieder herzlich zu einer neuen Runde barfuß tanzen ins AYI ein. Wer organische Beats, Downtempo, melodiösen Techno und Ethnogrooves mag, ist willkommen, sich tänzerisch bei Kerzenschein im Yogastudio auszutoben. 
Wir freuen uns auf dich. Wenn möglich komm pünktlich, damit wir gemeinsam beginnen können.
Wie immer ohne Eintritt, Schuhe, Alkohol und Drogen. 
Spenden für unsere Unkosten sind willkommen...