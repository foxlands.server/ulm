---
id: "980098675785234"
title: Seltsame Liebe - Live Hörspiel mit Hanna Münch & Clemens Grote
start: 2020-08-06 20:00
end: 2020-08-06 21:15
link: https://www.facebook.com/events/980098675785234/
teaser: Liebe Freunde der Wortkunst,  vor zwei Jahren musste unsere Vorstellung auf
  der Wilhelmsburg ausfallen, jetzt holen wir es innerhalb des "Kultur-Casin
isCrawled: true
---
Liebe Freunde der Wortkunst,

vor zwei Jahren musste unsere Vorstellung auf der Wilhelmsburg ausfallen, jetzt holen wir es innerhalb des "Kultur-Casino" nach:

Am 06.08. 2020 spielen wir

"Seltsame Liebe"
ein Dialog als Live-Hörspiel
in der Caponniere 4 in Neu-Ulm
um 20.00 Uhr   
Eintritt frei / Spende 
begrenzte Platzzahl     

Es spielen: Hanna Münch und Clemes Grote
am Mischpult: Klaus Fumy
Stimme vom Band: Katja Kaufmann

