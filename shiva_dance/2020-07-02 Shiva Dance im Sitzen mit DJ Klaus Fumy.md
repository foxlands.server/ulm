---
id: "272814653797652"
title: Shiva Dance im Sitzen mit DJ Klaus Fumy
start: 2020-07-02 20:00
end: 2020-07-03 00:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/272814653797652/
image: 105971695_155059279478581_2139468616951836910_o.jpg
teaser: Downbeatiger Ethno Techno von feinsten.
isCrawled: true
---
Downbeatiger Ethno Techno von feinsten.