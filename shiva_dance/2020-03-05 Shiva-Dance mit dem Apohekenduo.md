---
id: "808272066339073"
title: Shiva-Dance mit dem Apohekenduo
start: 2020-03-05 21:00
end: 2020-03-06 01:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/808272066339073/
teaser: "Kultwirt Stefan (Café Jedermann) & DJ Klaus (Shiva Dance) servieren euch
  wieder einen entspannten Abend unter dem Motto: panta rhei - alles fließt  La"
isCrawled: true
---
Kultwirt Stefan (Café Jedermann) & DJ Klaus (Shiva Dance) servieren euch wieder einen entspannten Abend unter dem
Motto: panta rhei - alles fließt

Lasst euch treiben, bei guten Gesprächen, guten Getränken, guter Musik. Und wenn euch der feine Musikmix aus Downbeat, Ethno, melodiösem Techno zur Selbstbewegung animiert, spricht nichts gegen ein flottes Apothekentänzchen.

Eintritt frei!

Wir freuen uns auf euch! :)