---
id: "263203521448174"
title: ROXY Sound Garten mit DJ Klaus Fumy (Shiva Dance)
start: 2020-06-25 17:00
end: 2020-06-26 00:00
link: https://www.facebook.com/events/263203521448174/
image: 105576185_10157670040287756_3630324236662265028_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00 H): DJ Klaus Fumy (Shiva Dance)
DJ Klaus beschallt euch mit Ethnogrooves, Downbeat, Afrohouse und Unerwartetem, damit trotz Tanzverbot, die Füße wippen, der Popo wackelt und der Kopf frei wird.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de