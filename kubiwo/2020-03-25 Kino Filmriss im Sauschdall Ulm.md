---
id: "226242955187150"
title: Kino Filmriss im Sauschdall Ulm (ABGESAGT!)
start: 2020-03-25 19:00
end: 2020-03-25 23:00
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/226242955187150/
image: 88360878_869567723505325_8782796864316506112_n.jpg
teaser: '***ABGESAGT!***  Kino Filmriss zeigt: "Aus dem Nichts" (Drama,
  2017)   https://www.zeit.de/2017/48/aus-dem-nichts-diane-kruger-fatih-akin-film  Wieter'
isCrawled: true
---
***ABGESAGT!***

Kino Filmriss zeigt: "Aus dem Nichts" (Drama, 2017) 

https://www.zeit.de/2017/48/aus-dem-nichts-diane-kruger-fatih-akin-film

Wietere Infos folgen... :)