---
name: arthauskinosulm
website: https://www.cineplex.de/ulm
email: kartenservice@dietrich-theater.de
scrape:
  source: facebook
  options:
    page_id: ulmerarthauskinos
---
Zu den arthauskinosulm zählen die drei Standorte Obscura, Mephisto & Lichtburg. In 5 Sälen mit insgesamt über 800 Sitzplätzen bieten wir beste Kinounterhaltung in 4K Auflösung und atemberaubendem Sound.
