---
id: "316564789352548"
title: Klimawandel, Rassismus & die Privilegien des reichen Westens
start: 2020-07-13 19:00
end: 2020-07-13 20:30
locationName: Festival Contre Le Racisme Ulm
address: Ulm
link: https://www.facebook.com/events/316564789352548/
image: 106423851_2398849100414204_4787479719491553572_o.jpg
teaser: "[DE]: Der Klimawandel betrifft uns alle – allerdings trifft er gerade die,
  die am wenigsten dazu beigetragen haben am schwersten. Die ärmsten Länder d"
isCrawled: true
---
[DE]: Der Klimawandel betrifft uns alle – allerdings trifft er gerade die, die am wenigsten dazu beigetragen haben am schwersten. Die ärmsten Länder der Welt sind für weniger als 1% der Emissionen verantwortlich, werden aber am verletzlichsten für die Folgen des Klimawandels sein. Für Klimagerechtigkeitsbewegungen wie Extinction Rebellion ist der Klimawandel deshalb nicht nur ein Umweltproblem, sondern auch eine Frage sozialer Gerechtigkeit. Aber was bedeutet das für unser Vorgehen? Was genau fordern wir, wenn wir Klimagerechtigkeit fordern? Wie wollen wir das erreichen? Und wieso sind soziale Ungleichheit, Rassismus und Klassendiskriminierung untrennbar mit dem Klimawandel verbunden? Auf diese und weitere Fragen wollen wir mit euch bei unserem Workshop zu Klimagerechtigkeit Antworten finden. 

Die Veranstaltung findet in Kooperation mit Extinction Rebellion Ulm statt.

Konferenzraum zugänglich unter: https://conference.sixtopia.net/b/tim-o5h-jvq. Schaltet euch einfach ab 19 Uhr dazu!

[EN]: Climate change affects us all - but especially those who haven´t been contibuting to it are affected most.  The world's poorest countries are responsible for less than 1% of emissions, but will be most vulnerable to the effects of climate change. For climate justice movements like Extinction Rebellion, climate change is therefore not only an environmental problem but also a question of social justice. But what does this mean for our actions? What exactly do we demand when we call for climate justice? How do we want to achieve this? And why are social inequality, racism and class discrimination inextricably linked to climate change? We want to find answers to these and other questions with you at our workshop about climate justice. Against racism and inhuman attitudes.

The event takes place in cooperation with Extinction Rebellion Ulm.

Link for participation:  https://conference.sixtopia.net/b/tim-o5h-jvq. Yust log yourself in at 19 o clock!