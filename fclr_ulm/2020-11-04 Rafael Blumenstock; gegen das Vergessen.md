---
id: "676243996611377"
title: Rafael Blumenstock; gegen das Vergessen
start: 2020-11-04 18:00
end: 2020-11-04 19:00
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/676243996611377/
image: 123299709_3414070171961846_1812853801631450027_n.jpg
teaser: Am 04.11.20 ist der 30. Jahrestag des Mordes an Rafael Blumenstock. Er wurde
  brutal auf dem Münsterplatz ermordet von mehreren bis heute unbekannten T
isCrawled: true
---
Am 04.11.20 ist der 30. Jahrestag des Mordes an Rafael Blumenstock. Er wurde brutal auf dem Münsterplatz ermordet von mehreren bis heute unbekannten Tätern. Das Motiv konnte nicht aufgeklärt werden doch es gibt deutliche Hinweise, dass es sich um einen schwulenfeindlichen und rechten Mord handelt. Wir rufen zu einem Gedenken auf:

Am 04.11. um 18 Uhr auf dem Münsterplatz.

Wir wollen neben dem Gedenken auch Forderungen stark machen:

   - Ein neues Mahnmal für Rafael Blumenstock. Das alte ist kaum sichtbar und beschädigt. Es steht viel mehr für das Vergessen des Mordes als für eine Mahnung.

   - Veröffentlichung von Informationen von damaligen Ermittlung zu Nazistrukturen

   - Eine Untersuchung zu personellen und strukturellen Kontinuität (sowohl die Kontinuität von der NS-Zeit bis 1990, sowie 1990 bis jetzt)

   - Einstufung des Mordes als Verdachtsfall rechter Gewalt

Doch was hat der Mord mit Nazis zu tun und warum überhaupt dieses Thema derart politisieren?

Rafael Blumenstock wurde getötet weil er anders war.

Rafael schminkte sich gelegentlich, trug "Frauenkleider", war links und sprach fremde Leute nach dem Namen oder der Telefonnummer an. Viele hielten ihn wohl deswegen für schwul. Doch das heißt auf keinen Fall, dass Rafael Blumenstock auch nur einen Funken an Mitschuld an dem Mord trägt!

Neben der Außenwahrnehmung von Rafael Blumenstock spricht auch der besonders brutale Mord für ein Hassverbrechen und es gab Hinweise auf Nazis am Tatort. Zudem gab es zu der Zeit in Ulm eine große Präsenz von Nazis, sowie viele Gewalttaten, die dokumentiert sind. Auch gab es viele Angriffe auf vermeintlich Schwule in ganz Deutschland sowie in Ulm. Hier herrschte eine schwulenfeindliche Stimmung.

Belege und eine Ausführliche Recherche über die damaligen Verhältnisse finden sich hier: https://kollektiv26.blackblogs.org/2020/10/13/gegen-das-vergessen-der-mord-an-rafael-blumenstock-in-ulm/ (Ersatzlink, falls der Blog down ist: https://de.indymedia.org/node/112126)
Video: https://youtu.be/VyrMpbGFkn8

Die Forderungen werden gestellt von: Kollektiv.26 - Autonome Gruppe Ulm, Young and Queer Ulm e.V., Jusos Ulm, Grüne Jugend Ulm, Neu-Ulm, Alb-Donau, DIE LINKE Ulm / Alb-Donau, Mein "ICH" gegen Rassismus, Festival contre le racisme Ulm

Bitte Kommt mit Mund- und Nasenschutz und haltet Abstand. Bitte keine Parteifahnen.