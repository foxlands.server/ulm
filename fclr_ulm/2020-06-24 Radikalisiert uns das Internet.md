---
id: "584013775862142"
title: Radikalisiert uns das Internet?
start: 2020-06-24 18:00
end: 2020-06-24 19:00
locationName: Festival Contre Le Racisme Ulm
address: Ulm
link: https://www.facebook.com/events/584013775862142/
image: 105040775_2390888107876970_5097905954682108524_n.jpg
teaser: "Wir starten unser Festival mit einem Spannenden Vortrag über unser aller
  täglichen Begleiter - das Internet: Dr. Cornelia Sindermann von der Universit"
isCrawled: true
---
Wir starten unser Festival mit einem Spannenden Vortrag über unser aller täglichen Begleiter - das Internet: Dr. Cornelia Sindermann von der Universität Ulm beleuchtet die Chancen und Risiken des digitalen Nachrichtenkonsums.

Das Internet bietet zahlreiche Möglichkeiten, sich über aktuelle Geschehnisse zu informieren: Podcasts, Nachrichtenwebsites und die Newsfeeds sozialer Medien sind nur einige Beispiele. 

In Einklang mit der Vielfalt an Möglichkeiten tendieren gerade junge Personen immer mehr dazu, Nachrichten online zu konsumieren. Während Manche die Chance der Verbreitung von Nachrichten online sehen, fürchten Andere die Probleme: Beispielsweise Filterblasen, Echokammern, Informationskokons. 

All diese Phänomene hängen mit einer einseitigen Informationsaufnahme zusammen. Das bedeutet, dass Personen (gewollt oder ungewollt) nur noch Informationen konsumieren könnten, die bestimmten Ansichten entsprechen.
Hinzu kommt, dass einzelne Parteien online und über soziale Medien wesentlich aktiver Informationen verbreiten, als andere Parteien. Dies könnte zusätzlich zu einem Ungleichgewicht in den konsumierten Inhalten durch die Nutzer führen. Ein weiteres Phänomen, welches sich gerade online auf sozialen Medien verbreitet, sind Fake News, also Falschnachrichten. Diese scheinen häufig genutzt zu werden, um Wähler zu beeinflussen, also: Mehr Wähler für eine Partei zu gewinnen.

All diese Phänomene – so wird gefürchtet – könnten eine Extremisierung von Meinungen zur Folge haben und letztendlich zur Radikalisierung von Personen führen. In dem Vortrag wird erörtert, inwieweit online Nachrichtenkonsum tatsächlich mit einer Extremisierung zusammenhängt. Zudem wird darauf eingegangen, wie sich jeder Einzelne vor potenziellen Gefahren wie Echokammern, Filterblasen und Fake News schützen kann.

Der Vortrag findet live über das Meetingportal Webex statt. Ihr könnt euch ab 17:55 Uhr dazuschalten. 

Teilnahmelink: https://uni-ulm.webex.com/webappng/sites/uni-ulm/meeting/download/38243e85f57c4428b444fdb5b2edc8bb?siteurl=uni-ulm&MTID=ma7507714c611bf3491dbe6dd48a9c398

Alle aktuellen Informationen dazu findet ihr auch auf unserer Website: www.fclr-ulm.de