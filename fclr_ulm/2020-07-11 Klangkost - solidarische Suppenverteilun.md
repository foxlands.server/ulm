---
id: "270299387724084"
title: Klangkost - solidarische Suppenverteilung
start: 2020-07-11 09:00
end: 2020-07-11 13:00
locationName: Hirschstraße
address: Hirschstr., 89075 Ulm
link: https://www.facebook.com/events/270299387724084/
image: 106508221_2399457680353346_9105463458073074139_o.jpg
teaser: "*english version below*   Unser diesjähriges Klangkost kann leider nicht wie
  geplant stattfinden, aber wir haben uns dafür ein neues Konzept ausgedach"
isCrawled: true
---
*english version below* 

Unser diesjähriges Klangkost kann leider nicht wie geplant stattfinden, aber wir haben uns dafür ein neues Konzept ausgedacht und überlegt, wie wir auch in Zeiten von Corona ein schönes Klangkostfestival mit euch zusammen erleben können.

Auch dieses Jahr wird es deshalb leckere Suppen geben, die dich dazu einladen sollen, mit uns ins Gespräch zu kommen! Hierfür arbeiten wir, wie auch schon in den letzten Jahren, mit den Ulmer Initiativen zusammen. Jede Initiative, die mitmacht, kocht ihre eigene Suppe, die Suppe wird in ausgekochte Gläser abgefüllt und so gibt es am Ende eine bunte Mischung aus den verschiedensten selbstgekochten Suppen. 

Diese Suppen erhältst du kostenlos am 11. und am 18. Juli von 9 - 14 Uhr in der Ulmer Innenstadt in der Hirschstraße! Komm also gerne vorbei, lern Neues über die verschiedensten Ulmer Initiativen, quatsch eine Runde mit uns und nimm dir leckere Suppe mit nach Hause :) 

Wir freuen uns auf dich!

Weitere Veranstaltungen des diesjährigen Klangkosts sind die Klangdemos, die im Anschluss an die Suppenaktion am 11. & 18. Juli stattfinden. Weitere Informationen findet ihr in hier: 

https://www.facebook.com/events/873239133168299/
https://www.facebook.com/events/739401020138180/


Unfortunately, this year's Klangkost - Festival cannot take place as planned, but we have come up with a new concept and have been thinking about how we can experience a Klangkost-Festival together with you even in times of Corona.

So this year there will be delicious soups again, which should invite you to come and talk to us! As we have done in the past  years, we are working together with the Ulmer Initiatives. Every initiative that takes part cooks its own soup, the soup is filled into boiled glasses and so in the end there will be a colourful mixture of different home-made soups. 

You can get these soups for free on July 11th and 18th from 9am - 1pm at the weekly market on the Münsterplatz! So come along, learn more about the different Ulm initiatives, talk to us and take some delicious soup home with you :) 

We are looking forward to see you there! 

Further events of this year's Klangkost-Festival are the sound demonstrations, which will take place after the soup action on 11 & 18 July. You can find more information here: 

https://www.facebook.com/events/873239133168299/
https://www.facebook.com/events/739401020138180/