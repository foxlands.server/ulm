---
name: Festival Contre Le Racisme Ulm, Neu-Ulm
website: https://fclr-ulm.de
email: stuve.fclr@uni-ulm.de
scrape:
  source: facebook
  options:
    page_id: fclrulm
---
Für Courage, gegen Rassismus und andere menschenverachtende Einstellungen!
