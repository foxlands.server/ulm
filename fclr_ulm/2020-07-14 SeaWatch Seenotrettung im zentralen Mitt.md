---
id: "335537384108307"
title: "SeaWatch: Seenotrettung im zentralen Mittelmeer"
start: 2020-07-14 19:00
end: 2020-07-14 20:30
link: https://www.facebook.com/events/335537384108307/
image: 107915881_2407355809563533_7555704405566527555_n.jpg
teaser: "[DE]: Friedhold Ulonska aus Rottenburg hat seit 2016 an zehn
  Rettungsmissionen im zentralen Mittelmeer teilgenommen. Er war Kapitän oder
  Erster Offizi"
isCrawled: true
---
[DE]: Friedhold Ulonska aus Rottenburg hat seit 2016 an zehn Rettungsmissionen im zentralen Mittelmeer teilgenommen. Er war Kapitän oder Erster Offizier auf Schiffen von Sea-Eye, Mission Lifeline, Resqship und Sea-Watch. Damit direkt beteiligt an der Rettung von mehr als 3.000 schiffbrüchigen Flüchtenden. Sein Vortrag gibt einen Überblick über die Entwicklung der zivilen Seenotrettung und den Bordalltag und den typischen Ablauf einer Rettungsmission. Besonderes Augenmerk gilt den zunehmend phantasievollen Manövern der EU-Regierungen, mit denen zivile Seenotrettung be- / verhindert und kriminalisiert wird - auch durch deutsche Politiker*innen. Aber auch der starken zivilgesellschaftlichen Gegenbewegung, z.B. durch die „Sicheren Häfen“ der Aktion Seebrücke. 

Die Veranstaltung wird in Kooperation mit Seawatch e.V. durchgeführt.

Teilnahmelink: https ://uni-ulm.webex.com/uni-ulm-de/onstage/g.php?MTID=e9b4990f4c8e4c20ffa5531923a575060. Schaltet euch einfach ab 19 Uhr dazu. 

[EN]: Friedhold Ulonska from Rottenburg has participated in ten rescue missions in the central Mediterranean since 2016. He was captain or first officer on ships of Sea-Eye, Mission Lifeline, Resqship and Sea-Watch. He was thus directly involved in the rescue of more than 3,000 shipwrecked refugees. His lecture will give an overview of the development of civil sea rescue, the everyday life on board and the typical course of a rescue mission. Special attention will be paid to the increasingly imaginative manoeuvres of EU governments to prevent and criminalise civil sea rescue - also by German politicians. But also the strong counter-movement of civil society, e.g. through the "safe havens" of the "Seebrücke" campaign will be focused.

The event is organized in cooperation with Seawatch e.V.

Link for participation: https://uni-ulm.webex.com/uni-ulm-de/onstage/g.php?MTID=e9b4990f4c8e4c20ffa5531923a575060.   Just sign yourself in at 19 o' clock. We are looking forward to see you! 