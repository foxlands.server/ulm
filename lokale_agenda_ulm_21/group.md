---
name: Lokale Agenda Ulm 21
website: http://www.agenda21.ulm.de/
scrape:
  source: facebook
  options:
    page_id: Lokale-Agenda-Ulm-21-225687344207129
---
100 Freiwillige engagieren sich in der lokalenagenda ulm 21 für Nachhaltigkeit in Ulm. Interessierte sind jederzeit herzlich eingeladen, sich an den Arbeitstreffen und an einer der vielfältigen Veranstaltungen zu beteiligen.
