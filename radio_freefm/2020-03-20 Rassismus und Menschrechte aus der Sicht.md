---
id: "2597128910507146"
title: Rassismus und Menschrechte aus der Sicht eines Betroffenen
start: 2020-03-20 19:00
end: 2020-03-20 21:00
address: Münsterplatz 25, 89073 Ulm
link: https://www.facebook.com/events/2597128910507146/
image: 87481342_1073837009669727_3340793796697784320_n.jpg
teaser: Im ersten Teil des Vortrags erzählt Herr Bassène seine persönliche Geschichte
  und wie er konkret mit Rassismus bei der Arbeit konfrontiert wurde als e
isCrawled: true
---
Im ersten Teil des Vortrags erzählt Herr Bassène seine persönliche Geschichte und wie er konkret mit Rassismus bei der Arbeit konfrontiert wurde als er 2010 nach Deutschland kam.
Im zweiten Teil berichtet er über die negativen Erlebnisse von Einwanderer mit ihren Arbeitskollegen und der damit verbundenen mangelnden Achtung der Menschenrechte.
Anschließend steht der Referent für Fragen zur Verfügung.
Referent: Eric Bassène, Journalist
EINTRITT FREI!
