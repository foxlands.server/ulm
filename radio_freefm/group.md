---
name: Radio FreeFM Ulm
website: http://www.freefm.de
email: radio@freefm.de
scrape:
  source: facebook
  options:
    page_id: radiofreefmulm
---
Freies Radio für Ulm, Neu-Ulm und Umgebung auf UKW 102,6 MHz, im Kabel auf 97,7 MHz & 93,45 MHz und weltweit im Livestream über freefm.de - tune in!
