---
id: "918984441924480"
title: "Online-Workshop: Grundkurs Studiotechnik"
start: 2020-10-02 19:00
end: 2020-10-02 21:00
link: https://www.facebook.com/events/918984441924480/
teaser: "Online-Workshop: Grundkurs Studiotechnik In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspi"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik
In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung.
Referent: Julius Taubert
Online-Link:
https://meeting.bz-bm.de/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder!