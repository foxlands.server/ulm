---
id: "255226612130345"
title: "Workshop :: Homepageworkshop für Beginner"
start: 2020-02-12 18:00
end: 2020-02-12 20:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/255226612130345/
teaser: "Bei diesem Workshop bekommst du eine Einführung in die neue free FM
  Webseite.  Workshopinhalte sind unter anderem:  - Anlegen von Usern - Tagging
  von"
isCrawled: true
---
Bei diesem Workshop bekommst du eine Einführung in die neue free FM Webseite.

Workshopinhalte sind unter anderem:

- Anlegen von Usern
- Tagging von Inhalten
- Erstellung von Audios, Artikeln und Playlisten

Referent: Dominic Köstler