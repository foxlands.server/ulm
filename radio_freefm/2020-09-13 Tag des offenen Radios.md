---
id: "1307914559417723"
title: Tag des offenen Radios
start: 2020-09-13 13:00
end: 2020-09-13 17:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/1307914559417723/
image: 87166446_10158004207286668_4595701692044935168_o.jpg
teaser: "#25JahrefreeFM Einst ab dem 16. Jahrhundert als Waffenarsenal genutzt, werden
  im Büchsenstadel heute vor allem verbale Geschütze aufgefahren. Seinen e"
isCrawled: true
---
#25JahrefreeFM
Einst ab dem 16. Jahrhundert als Waffenarsenal genutzt, werden im
Büchsenstadel heute vor allem verbale Geschütze aufgefahren. Seinen ehemals militärischen Zweck gibt das Gebäude bei genauem Hinsehen aber bis heute preis. An der Nordseite des spätgotischen Baus erkennt der geschulte Blick eine eingemauerte Kanonenkugel. Diese ist bei weitem nicht die einzige, die in den Mauern des 1485 erbauten Hauses verarbeitet wurde. Der Büchsenstadel ist heute kein militärischer Ort mehr. Im Erdgeschoss beherbergt er ein Jugendhaus, direkt unterm Dach tüfteln die
Mitglieder des Hackerspace-Ulm e.V. und im 2.OG entfaltet sich das nicht kommerzielle Radio free FM. Das Haus ist so heute ein Ort der Begegnung verschiedener sozialer Gruppen. Radio free FM sendet mit diesem Gedanken rund um die Uhr auf UKW 102,6 MHz sowie auf Kabel 93,45 & 97,70 MHz und im Stream unter www.freefm.de . Die Radiomacher*innen öffnen zum Tag des offenen Denkmals von 13 bis 17 Uhr die Türen des ehemaligen Zeughauses.