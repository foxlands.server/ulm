---
id: "1160932230906632"
title: 'Podcast: "Empowerment & Widerstand schwarzer Menschen"'
start: 2020-07-01 15:00
end: 2020-07-01 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/1160932230906632/
image: 104370247_1160164964370264_225798539575461163_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Josephine Jackson vom adis e.V.. Sie berichtet über Empowerment & Widerstand schwarzer Menschen. Es geht um die Widerstandsgeschichte schwarzer Menschen in Deutschland, Gewalterfahrung und aktuelle Diskriminierungserfahrungen in Deutschland. Wie erreichen wir es, dass die Expertise und das Wissen schwarzer Menschen anerkannt wird? Wie kann unsere Mehrheitsgesellschaft davon profitieren?
Live zu hören bei Radio free FM, 102.6 MHz, am 01.07.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de