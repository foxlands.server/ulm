---
id: "504120323549274"
title: "Online-Workshop :: Redaktion und Recherche"
start: 2020-07-25 11:00
end: 2020-07-25 13:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/504120323549274/
image: 95985324_10158269664421668_814572901675040768_o.jpg
teaser: "Online-Workshop :: Redaktion und Recherche  Wie recherchiere ich Themen im
  Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was m"
isCrawled: true
---
Online-Workshop :: Redaktion und Recherche

Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf? 

Referent: K.W.

Online-Link: 
https://meeting.bz-bm.de/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de