---
id: "694506514662372"
title: Podcast "Entwicklungen in jihadistischer Radikalisierung"
start: 2020-06-24 15:00
end: 2020-06-24 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/694506514662372/
image: 104812443_1163213180732109_6339854042383651199_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Micha Bröckling vom Inside out e.V. Er ist Politik- und Islamwissenschaftler und berichtet über aktuelle Trends und Entwicklungen der jihadistischen Radikalisierung. Es geht um die Hintergründe und Entstehung von Al Kaida und dem IS. Wie sieht das Feld der jihadistischen Medien aus? Wie und in welcher Medienlandschaft verteilen sie sich? Wie ist die aktuelle Entwicklung und der Wandel bei Al Kaida und dem IS zu erklären?
Live zu hören bei Radio free FM, 102.6 MHz, am 24.06.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de