---
id: "303137133910099"
title: "Workshop :: Moderation und Interview"
start: 2020-03-18 17:00
end: 2020-03-18 19:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/303137133910099/
image: 79236289_10157827339841668_3693562443088265216_o.jpg
teaser: Regeln für die Vorbereitung und für die Fragestellung eines Interviews;
  Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische
isCrawled: true
---
Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)

Referent: Clemens Grote

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung: ausbildung[at]freefm.de