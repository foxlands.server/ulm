---
id: "319230502566689"
title: "Alternative Autokino: Ich war noch niemals in New York"
start: 2020-07-31 20:15
end: 2020-08-01 00:00
address: Bodelschwinghweg 1, 89160 Dornstadt
link: https://www.facebook.com/events/319230502566689/
teaser: "#25JahrefreeFM Radio free FM und der Obstwiesenfestival e.V.
  präsentieren:   Alternative Autokino: Ich war noch niemals in New York Für
  Lisa Wartberg"
isCrawled: true
---
#25JahrefreeFM
Radio free FM und der Obstwiesenfestival e.V. präsentieren: 

Alternative Autokino: Ich war noch niemals in New York
Für Lisa Wartberg (Heike Makatsch), erfolgsverwöhnte Fernsehmoderatorin und Single, steht ihre Show an erster Stelle. Doch dann verliert ihre Mutter Maria (Katharina Thalbach) nach einem Unfall ihr Gedächtnis, kommt ins Krankenhaus und kann sich nur noch an eines erinnern: Sie war noch niemals in New York! Kurzentschlossen flieht Maria und schmuggelt sich als blinder Passagier an Bord eines luxuriösen Kreuzfahrtschiffes.
Gemeinsam mit ihrem Maskenbildner Fred (Michael Ostrowski) macht sich Lisa auf die Suche nach ihrer Mutter und spürt sie tatsächlich auf der "MS Maximiliane" auf. Doch bevor die beiden Maria wieder von Bord bringen können, legt der Ozeandampfer auch schon ab und die dreifinden sich auf einer unfreiwilligen Reise über den Atlantik wieder. 
Lisa lernt an Bord Axel Staudach (Moritz Bleibtreu) und dessen Sohn Florian (Marlon Schramm) kennen. Axel ist so gar nicht Lisas Typ, doch durch eine Reihe unglücklicher Missgeschicke kommen sich die beiden schließlich näher... Mutter Maria trifft auf Eintänzer Otto (Uwe Ochsenknecht), der behauptet, eine gemeinsame Vergangenheit mit ihr zu haben - was Maria mangels Gedächtnis natürlich nicht überprüfen kann. Und Fred verliebt sich Hals über Kopf in den griechischen Bordzauberer Costa (Pasquale Aleardi). 
So verläuft die turbulente Schiffsreise - mit mehrmaligem Finden und Verlieren der Liebe und jeder Menge Überraschungen - nach New York.

 Eine Abendkasse ist eingerichtet! 

Spielregeln:
Um einen reibungslosen Ablauf zu garantieren benötigen wir eure Unterstützung und bitten um die Einhaltung unserer Spielregeln.
Schon jetzt vielen Dank für eure Unterstützung.

1. Pro Fahrzeug sind maximal 2 Personen zugelassen

2. Die Teilnahme ist in jedem Fahrzeug und auch mit dem Fahrrad, Campingstuhl und Picknickdecke möglich!

3. Die Altersfreigabe der Filme ist zu beachten und wird bei der Einfahrt kontrolliert

4. Die Tonübertragung erfolgt über Radio free FM. Bitte schaltet kurz vor dem Film die Frequenz von free FM 102,6 ein, dort informieren wir euch über die Frequenz für die Tonübertragung

5. Bitte schaltet während des Filmes die Beleuchtung eurer Fahrzeuge aus

6. Das Verlassen des Geländes während der Vorstellung ist nicht gestattet

7. Das Verlassen des Fahrzeuges ist nur für Toilettengänge sowie den Erwerb von Getränken und Snacks gestattet. Wir empfehlen euch bereits mit dem Ticket Getränke und Snacks zu erwerben. Die aktuell gültigen Hygieneregeln sind dabei einzuhalten. Bitte haltet mind. 1,5 m Abstand und tragt beim Verlassen des Fahrzeuges unbedingt eine Mund- und Nasenmaske

8. An- und Abfahrt
Die Zu- und Abfahrt erfolgt über den Hubertusweg. Bei Ankunft werdet ihr und euer Fahrzeug auf den Parkplatz geleitet und dort durch unsere Ordner auf die Parkfläche eingewiesen. Die Einfahrt der Fahrzeuge ist für ein Zeitfenster von 20:15 Uhr - 21:15 Uhr geplant. Die Abfahrt des Platzes erfolgt reihenweise nach Anweisung durch das Ordnerpersonal.

9. Für eventuelle Pannen (in erster Linie leere Autobatterien) ist ein KFZ-Techniker vor Ort

10. Was passiert, wenn es regnet?
Das Gute ist, wir können den Film auch bei Regen spielen. Tritt während der Veranstaltung Starkregen oder Sturm auf, werden wir die Vorstellung abbrechen oder bei entsprechender Vorhersage rechtzeitig absagen. Wenn der Film bereits begonnen hat, erfolgt keine Erstattung. Sollte der Film vorsorglich abgesagt werden müssen, erhaltet ihr den Eintrittspreis zurück, da es keinen Ersatztermin geben kann. Cool wäre es natürlich, wenn ihr das Tickets dann an Radio free FM spenden würdet. 
