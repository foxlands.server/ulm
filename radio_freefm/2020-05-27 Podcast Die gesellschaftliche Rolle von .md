---
id: "323001965357751"
title: Podcast "Die gesellschaftliche Rolle von migrantischen Vereinen"
start: 2020-05-27 15:00
end: 2020-05-27 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/323001965357751/
image: 98024857_1134604030259691_2722700783932932096_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist Jens Ostwaldt von der Fachstelle PREvent!on. Er referiert über "Die gesellschaftliche Rolle von migrantischen Vereinen". Wie sind die migrantischen Vereine überhaupt organisiert, welche Strukturen haben sie? In den aktuellen Zeiten, welche Rolle können die Vereine/Verbände einnehmen?
Live zu hören bei Radio free FM, 102.6 MHz, am 27.05.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de