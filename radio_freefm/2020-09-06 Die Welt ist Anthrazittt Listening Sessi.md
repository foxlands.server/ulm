---
id: "408085596826217"
title: Die Welt ist Anthrazittt Listening Session - 25 Jahre free FM
start: 2020-09-06 19:00
end: 2020-09-06 22:00
locationName: Hungry Turtle
address: Schaffnerstraße 2, 89073 Ulm
link: https://www.facebook.com/events/408085596826217/
teaser: '-AUSVERKAUFT- live auf der 102.4 mhz  Radio free FM präsentiert: Zum Album
  "Die Welt ist Anthrazittt" eine Listening Session mit Anthrazittt (Ulm)  Di'
isCrawled: true
---
-AUSVERKAUFT-
live auf der 102.4 mhz

Radio free FM präsentiert:
Zum Album "Die Welt ist Anthrazittt" eine Listening Session mit
Anthrazittt (Ulm)

Die bekannte Hip Hop Band Anthrazittt hat ihr neues Album" Die Welt ist Anthrazitt" im Kasten und lädt zur Listening Session ein.
Gemeinsam mit dem Produzenten Joe Styppa haben die Jungs die letzten zwei Jahre daran gearbeitet und nun ist es endlich fertig. Auf den Zehn Songs beschreiben Anthrazittt die letzten Jahre ihres Lebens. Der tägliche Struggle für die Musik, Stolz auf Freundschaft und Liebe, sowie die damit verbundenen Probleme sind Themen, die die beiden tagtäglich begleiten und sich somit auf ihrem Album widerspiegeln. Der leichte Hang zur Melancholie, der in der DNA von Anhtrazittt tief verankert ist, lässt sich unschwer erkennen und wurde in zwei Jahren Studioarbeit in ein frisches New-School-Hiphop-Gewand gekleidet, welches perfekt den Zeitgeist trifft und ohne dabei an schon Bekanntes zu erinnern. 

KidKun von der Step Up Show wird nach der Listening Session ein Heizungskeller Set im Behind Closed Door Shop im Hungry Turtle spielen. Dies wird Liveübertragen auf Radio free FM 102,6 Mhz und per Video Stream auf Facebook. 


-------------------------
Radio free FM gem. GmbH
Platzgasse 18
89077 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.