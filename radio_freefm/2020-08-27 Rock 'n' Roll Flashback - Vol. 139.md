---
id: "3029558367166291"
title: Rock 'n' Roll Flashback - Vol. 139
start: 2020-08-27 20:00
end: 2020-08-27 22:00
link: https://www.facebook.com/events/3029558367166291/
teaser: Rock 'n' Roll Flashback - The Music of the 50s & 60s - Jeden zweiten
  Donnerstag von 20:00 bis 22:00 Uhr, presented by your "Daddio of the Raddio".
  Nur
isCrawled: true
---
Rock 'n' Roll Flashback - The Music of the 50s & 60s - Jeden zweiten Donnerstag von 20:00 bis 22:00 Uhr, presented by your "Daddio of the Raddio". Nur echt auf UKW 102.6MHz oder www.freefm.de

In meiner 14-tägigen Radioshow Rock 'n' Roll Flashback könnt ihr zwei Stunden lang die Musik der 50s & 60s neu entdecken und erleben, natürlich alles direkt von echten Schallplatten aus meiner eigenen Sammlung. Egal, ob große Hits oder B-Seiten - hier werden die Songs gespielt, die ihr sonst nirgendwo im Radio hören könnt. Außerdem präsentiere ich heute für euch das "Warm Up" zum heutigen Film im Radio free FM Autokino in Dornstadt und versorge euch mit allen nötigen Infos. Schaltet also pünktlich um 20:00 Uhr ein und begleitet mich auf einer musikalischen Zeitreise in die goldene Ära des Rock 'n' Roll!