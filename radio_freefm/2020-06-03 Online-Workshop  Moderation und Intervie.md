---
id: "613895629223975"
title: "Online-Workshop :: Moderation und Interview"
start: 2020-06-03 17:00
end: 2020-06-03 19:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/613895629223975/
image: 101162690_10158357926316668_6047419273956032512_o.jpg
teaser: "Online-Workshop :: Moderation und Interview  Regeln für die Vorbereitung und
  für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio"
isCrawled: true
---
Online-Workshop :: Moderation und Interview

Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)

Referent: Clemens Grote

Online-Link: https://meet.jit.si/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder

Anmeldung: ausbildung[at]freefm.de