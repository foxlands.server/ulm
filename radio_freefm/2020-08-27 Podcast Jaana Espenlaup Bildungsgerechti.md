---
id: "1087571345032624"
title: Podcast Jaana Espenlaup "Bildungsgerechtigkeit in Deutschland"
start: 2020-09-16 15:00
end: 2020-09-16 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/1087571345032624/
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast ist Jaana Espenlaup. Sie ist die Bundeslandkoordinatorin für BW von arbeiterkind.de. Gibt es in Deutschland Bildungsgerechtigkeit? Was macht eigentlich Arbeiterkind.de? Wie kann man die Organisation unterstützen und wie erhöhen wir die Chancen für alle in unserem Land? 

Live zu hören bei Radio free FM, 102.6 MHz, am 16.09.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 