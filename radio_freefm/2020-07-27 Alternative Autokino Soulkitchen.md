---
id: "266999161272984"
title: "Alternative Autokino: Soulkitchen"
start: 2020-07-27 20:15
end: 2020-07-28 00:00
address: Bodelschwinghweg 1, 89160 Dornstadt
link: https://www.facebook.com/events/266999161272984/
teaser: "#25JahrefreeFM Radio free FM und der Obstwiesenfestival e.V.
  präsentieren:  Alternative Autokino: Soulkitchen Kneipenbesitzer Zinos ist vom
  Pech verfo"
isCrawled: true
---
#25JahrefreeFM
Radio free FM und der Obstwiesenfestival e.V. präsentieren:

Alternative Autokino: Soulkitchen
Kneipenbesitzer Zinos ist vom Pech verfolgt: erst zieht seine Freundin Nadine für einen neuen Job nach Shanghai, dann erleidet er einen Bandscheibenvorfall. Als er in seiner Not den exzentrischen Spitzenkoch Shayn engagiert, bleiben auf einmal auch noch die ohnehin schon wenigen Stammgäste aus. Und als wäre das nicht schon genug, taucht auch noch sein leicht krimineller Bruder Ilias auf und bittet ihn um Hilfe. Während Zinos noch überlegt, wie er den Laden los wird, um Nadine nach China folgen zu können, locken Musik und die ausgefallene Speisekarte immer mehr Szenepublikum an. Das „Soul Kitchen“ rockt und boomt wie nie zuvor. Doch dann überschlagen sich die Ereignisse. 
Spielregeln:
Um einen reibungslosen Ablauf zu garantieren benötigen wir eure Unterstützung und bitten um die Einhaltung unserer Spielregeln.
Schon jetzt vielen Dank für eure Unterstützung.

Eine Abendkasse ist eingerichtet! 

Spielregeln:
 Um einen reibungslosen Ablauf zu garantieren benötigen wir eure Unterstützung und bitten um die Einhaltung unserer Spielregeln.
Schon jetzt vielen Dank für eure Unterstützung.

1. Pro Fahrzeug sind maximal 2 Personen zugelassen

2. Die Teilnahme ist in jedem Fahrzeug und auch mit dem Fahrrad, Campingstuhl und Picknickdecke möglich!

3. Die Altersfreigabe der Filme ist zu beachten und wird bei der Einfahrt kontrolliert

4. Die Tonübertragung erfolgt über Radio free FM. Bitte schaltet kurz vor dem Film die Frequenz von free FM 102,6 ein, dort informieren wir euch über die Frequenz für die Tonübertragung

5. Bitte schaltet während des Filmes die Beleuchtung eurer Fahrzeuge aus

6. Das Verlassen des Geländes während der Vorstellung ist nicht gestattet

7. Das Verlassen des Fahrzeuges ist nur für Toilettengänge sowie den Erwerb von Getränken und Snacks gestattet. Wir empfehlen euch bereits mit dem Ticket Getränke und Snacks zu erwerben. Die aktuell gültigen Hygieneregeln sind dabei einzuhalten. Bitte haltet mind. 1,5 m Abstand und tragt beim Verlassen des Fahrzeuges unbedingt eine Mund- und Nasenmaske

8. An- und Abfahrt
Die Zu- und Abfahrt erfolgt über den Hubertusweg. Bei Ankunft werdet ihr und euer Fahrzeug auf den Parkplatz geleitet und dort durch unsere Ordner auf die Parkfläche eingewiesen. Die Einfahrt der Fahrzeuge ist für ein Zeitfenster von 20:15 Uhr - 21:15 Uhr geplant. Die Abfahrt des Platzes erfolgt reihenweise nach Anweisung durch das Ordnerpersonal.

9. Für eventuelle Pannen (in erster Linie leere Autobatterien) ist ein KFZ-Techniker vor Ort

10. Was passiert, wenn es regnet?
Das Gute ist, wir können den Film auch bei Regen spielen. Tritt während der Veranstaltung Starkregen oder Sturm auf, werden wir die Vorstellung abbrechen oder bei entsprechender Vorhersage rechtzeitig absagen. Wenn der Film bereits begonnen hat, erfolgt keine Erstattung. Sollte der Film vorsorglich abgesagt werden müssen, erhaltet ihr den Eintrittspreis zurück, da es keinen Ersatztermin geben kann. Cool wäre es natürlich, wenn ihr das Tickets dann an Radio free FM spenden würdet. 