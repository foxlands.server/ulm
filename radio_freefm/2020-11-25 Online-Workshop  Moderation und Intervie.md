---
id: "328937234924514"
title: "Online-Workshop :: Moderation und Interview"
start: 2020-11-25 19:00
end: 2020-11-25 21:00
link: https://www.facebook.com/events/328937234924514/
image: 118509396_10158630059786668_3803089559255671284_o.jpg
teaser: "Online-Workshop :: Moderation und Interview Regeln für die Vorbereitung und
  für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-"
isCrawled: true
---
Online-Workshop :: Moderation und Interview
Regeln für die Vorbereitung und für die Fragestellung eines Interviews; Technik (Umgang mit Mikro; Studio-Technik für Telefon-Interview). Theoretische Vorbereitung auf die moderierte Sendung: Aufteilung, Jingles, Musikeinsatz, Präsentation und Nachrichten. "Bei Radio free FM beobachtete Fehler" mit Hörbeispielen. (Nicht: Moderationsschulung im Studio.)
Referent: Clemens Grote
Online-Link: https://meeting.bz-bm.de/radiobaukastenfreefm
Pflicht für Neueinsteiger, kostenlos für Mitglieder
Anmeldung: ausbildung[at]freefm.de