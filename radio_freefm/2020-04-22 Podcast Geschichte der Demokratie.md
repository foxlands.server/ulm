---
id: "223555695573435"
title: Podcast "Geschichte der Demokratie"
start: 2020-04-22 15:00
end: 2020-04-22 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/223555695573435/
image: 94035977_1115874908799270_1977985138780274688_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die neue Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Den Auftakt macht Frau Prof. Dr. Barbara Zehnpfennig von der Universität Passau mit der "Geschichte der Demokratie". Live zu hören bei Radio freeFM am 22.04.20 um 15.00 Uhr oder danach als Podcast bei www.freefm.de