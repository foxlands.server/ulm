---
id: "868466263680202"
title: Fabio Battista + Twiceasmad -25 Jahre free FM
start: 2020-09-19 19:00
end: 2020-09-19 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/868466263680202/
teaser: "Fabio Battista (ex Furasoul,Biberach)  Support: Twiceasmad (München)  ----
  Fabio Battista ist in der Region Ulm-Biberach als Frontsänger der Band Fura"
isCrawled: true
---
Fabio Battista (ex Furasoul,Biberach)

Support: Twiceasmad (München)

----
Fabio Battista ist in der Region Ulm-Biberach als Frontsänger der Band Furasoul bekannt geworden. Trotz verlockender Angebote aus der Plattenindustrie, löste sich die Band 2013 auf und Fabio entschloss sich nach Köln zu ziehen um seinen Weg als Solo-Künstler weiter zu bestreiten. In Köln arbeitete er als Produzent und Songwriter, was ihm den nötigen Abstand verschaffte und ihn vor allem als Künstler weiter wachsen ließ.
Sowohl live als auch auf Platte glänzt er mit seiner vielseitigen Musik. Seine einzigartige rauchige Stimme macht den Rest und bleibt im Kopf.

----
“Wenn der Tag den Ostbahnhof erreicht, kotz’ ich den Abend in den Raucherbereich”, singen Twiceasmad in ihrem gleichnamigen Song. Die beiden machen super schöne, zeitgenössische Musik mit einem einzigartigen Mix aus Ukulele, E-Gitarre und alten Synthies aus der frühen 90ern! 