---
id: "270077027666341"
title: Podcast mit Olivia Wenzel "1000 Serpentinen Angst"
start: 2020-07-15 15:00
end: 2020-07-15 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/270077027666341/
image: 104998947_1163260220727405_8623200409613859549_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus.
Heute zu Gast ist die Autorin Olivia Wenzel. Sie hat ihr aufsehenerregendes erstes Buch "1000 Serpentinen Angst" geschrieben. Mit ihr reden wir über die Reaktionen, wie das Buch wahrgenommen wird. Sie berichtet aus der Praxis, als sie mit dem Buch unterwegs war und über den Entstehungsprozess. Ein weiteres Thema wird die Black Lives Matter-Bewegung sein.
Live zu hören bei Radio free FM, 102.6 MHz, am 15.07.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de
Autor: Olivia Wenzel. Foto: Juliane Werner