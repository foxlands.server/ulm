---
id: "836920016804163"
title: "Online-Workshop: Grundkurs Studiotechnik"
start: 2020-05-29 17:00
end: 2020-05-29 19:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/836920016804163/
image: 95343736_10158269688746668_1139143240931344384_o.jpg
teaser: "Online-Workshop: Grundkurs Studiotechnik  In diesem Baustein lernt ihr die
  Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattensp"
isCrawled: true
---
Online-Workshop: Grundkurs Studiotechnik

In diesem Baustein lernt ihr die Technik im Sendestudio kennen und bedienen. Mischpult, CD-Player, Plattenspieler, Tapedeck, Mikrofone, Durchführung von Telefoninterviews, Mitschnitte der Sendung. 

Referent: Julius Taubert

Online-Link: https://meet.jit.si/radiobaukastenfreefm

Pflicht für Neueinsteiger, kostenlos für Mitglieder!