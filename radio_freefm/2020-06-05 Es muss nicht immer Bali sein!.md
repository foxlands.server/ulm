---
id: "627035311482993"
title: Es muss nicht immer Bali sein!
start: 2020-07-05 21:30
end: 2020-07-05 23:30
address: Internationales Donaufest / Bühne Neu-Ulm
link: https://www.facebook.com/events/627035311482993/
image: 87421619_10158020905551668_620887881128345600_o.jpg
teaser: "#25JahrefreeFM  Die großen Abenteuer beginnen direkt vor der Haustür.
  Kulturkrieger Philipp Zey ist 3 Jahre mit dem Fahrrad um die Welt geradelt. Im
  G"
isCrawled: true
---
#25JahrefreeFM 
Die großen Abenteuer beginnen direkt vor der Haustür. Kulturkrieger Philipp Zey ist 3 Jahre mit dem Fahrrad um die Welt geradelt. Im Gepäck eine Posaune, ein Golfschläger und ein Spätzlesbrett. Über seine Abenteuer produzierte er von unterwegs die Sendung "fillyroundtheworld", welche regelmäßig auf Radio free FM ausgestrahlt wurde. 

Am Anfang der Reise war die Donau. Entlang dieses Flusses und durch den Balkan ergaben sich grandiose  Abenteuer.
"Filly" stellt das erstes Kapitel seiner Weltreise in einem Audio-visuellen Vortrag dar. Der Titel lässt ahnen, dass man die großen Abenteuer nicht zwangsweise in der Ferne liegen, sondern direkt an der Haustür vorbei fließen.