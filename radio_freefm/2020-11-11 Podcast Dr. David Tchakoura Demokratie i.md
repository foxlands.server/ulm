---
id: "368785077699146"
title: Podcast Dr. David Tchakoura "Demokratie im sub-Sahara Afrika"
start: 2020-11-18 15:00
end: 2020-11-18 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/368785077699146/
image: 123644882_1273464239707002_3517552594578448004_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast ist Dr. David Tchakoura. Er berichtet über die Demokratie in Afrika. Wie ist der aktuelle Stand? Welche Herausforderungen und Lösungsansätze gibt es?
Live zu hören bei Radio free FM, 102.6 MHz, am18.11.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de 