---
id: "601265520484945"
title: Podcast "Upskirting"
start: 2020-06-10 15:00
end: 2020-06-10 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/601265520484945/
image: 100873422_1144967345890026_2315688994106507264_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. Heute zu Gast ist die Regisseurin Hanna Seidel. Mit ihr reden wir über Upskirting und sexualisierte Gewalt. Sie berichtet von ihren persönlichen Erfahrungen. Und natürlich über ihre Petition an den Bundestag. Wie wird die Istanbuler Konvention in Deutschland umgesetzt? Ist sexualisierte Gewalt in Deutschland ein strukturelles Problem?
Live zu hören bei Radio free FM, 102.6 MHz, am 10.06.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de