---
id: "255704065730211"
title: Passagen . Werk für Walter Benjamin - 25 Jahre free FM
start: 2020-08-30 21:00
end: 2020-08-30 23:00
locationName: Stadthaus Ulm
address: Ulm
link: https://www.facebook.com/events/255704065730211/
image: 105586590_10158439351731668_3697931013888698320_o.jpg
teaser: "#25JahrefreeFM heißt auch 25 Jahre Entartet  Die Redaktion Entartet und Radio
  free FM präsentieren:  Passagen . Werk für Walter Benjamin Eine radiopho"
isCrawled: true
---
#25JahrefreeFM heißt auch 25 Jahre Entartet

Die Redaktion Entartet und Radio free FM präsentieren:

Passagen . Werk für Walter Benjamin
Eine radiophone Komposition von Gerald Fiebig

---------------------------------------

Live im Stadthaus Ulm und On Air auf 102,6 Mhz und www.freefm.de 
Am Sonntag 30.08.2020
Um 21 Uhr (pünktlich!)

------------------------------------

Achtung! Ausgebbucht!
Der Eintritt zum Konzert vor Ort ist frei, wir bitten um Platzreservierung https://stadthaus.ulm.de/reservierungen?eventid=
Die Kapazität ist Beschränkt auf 50 Personen. Vor Ort gelten selbstredend alle bekannten Corona-Verhaltens- und Abstandsregeln. 

------------------------------------

Die Uraufführung einer neuen radiophonen Komposition "Passagen. Werk für Walter Benjamin".
Das Stück wird speziell für diese Sendung entwickelt und erinnert anlässlich seines bevorstehenden 80. Todestags an Walter Benjamin - scharfsinniger Kulturtheoretiker und virtuoser Prosaautor, polemischer Literaturkritiker und visionärer Geschichtsphilosoph, messianischer Kommunist und antifaschistischer Kämpfer und nicht zuletzt medienbewusster Radiomacher. 
Das Stück verwebt Zitate aus Benjamins Schriften mit unterschiedlichen Klang-Szenen. Gerald Fiebig hat sich in verschiedenen Texten immer wieder auf Walter Benjamin bezogen, aber noch nie in einer akustischen Arbeit. Die Bezüge, die er in diesem Stück zwischen Benjamins Texten und seinen Sounds herstellt, sind teilweise inspiriert von dem Aufsatz "Exploding the Atmosphere: Realizing the Revolutionary Potential of 'the Last Street Song'", in dem der neuseeländische Noise-Musiker Bruce Russell experimentelle Sound-Praxis mit Bezug auf Benjamin und die Situationistische Internationale in einen politischen Kontext stellt.

------------------------------------

Gerald Fiebig ::: audio art | Text. Klang
Schwerpunkte der akustischen Arbeiten des Augsburger Audiokünstlers Gerald Fiebig sind radiophone Kompositionen, elektroakustische Live-Improvisation sowie Installations- und Performancekonzepte für spezifische Orte und Anlässe. Seit 2006 werden seine akustischen Arbeiten auf Festivals im In- und Ausland aufgeführt und ausgestellt, darunter „Synthèses“ (Bourges/Frankreich), „lab.30“ (Augsburg), „Hörkunstfestival“ (Erlangen), „Zeppelin“ (Barcelona/Spanien), „Puente Sonoro“ (Manizales/Kolumbien), „Musica viva“ (Lissabon/Portugal), „Diagonale“ (Bielefeld), „Lichtnächte“ (Eichstätt) und „Experimentelle Musik“ (München).
Für seine Audioarbeiten erhielt Fiebig unter anderem ein TONSPUR-Artist-in-Residence-Stipendium vom quartier21/MuseumsQuartier in Wien sowie einen Kunstförderpreis der Stadt Augsburg. Sein Stück “Identification de substances”, komponiert zusammen mit Gerhard Zander, wurde 2007 in die Vorauswahl für den Preis des Institut de Musique Electroacoustique de Bourges aufgenommen.
Als Solokünstler veröffentlicht und performt Gerald Fiebig unter seinem eigenen Namen und als Sustained Development. Außerdem ist er ein Mitglied des Lo-Fi-Pop-Trios Jesus Jackson & die grenzlandreiter. Daneben fanden Veröffentlichungen und Auftritte mit den Projekten Audiovisuelles Kollektiv, Klonk, Knark Esion, Laptobler Combo, Poembeat Live und Zander/Fiebig statt.
Er veröffentlichte Alben und EPs in den Formaten CD, Kassette und Download auf den Labels Brainhall, Dhyana Records, Enough Records, Fieldmuzick, gebrauchtemusik, Gruenrekorder, Kulturterrorismus, Mandarangan Recordings, Tim Timm Tonträger, Treetrunk Records und vor allem attenuation circuit, einem Label für experimentelle Musik, bei dem er auch mitarbeitet.
Gerald Fiebig ist Mitglied der Deutschen Gesellschaft für Elektroakustische Musik (DEGEM), der Association Presque Rien autour des amis de Luc Ferrari und von jetzt:musik!, der Augsburger Gesellschaft für Neue Musik.

------------------------------------

Veranstalter:
Radio free FM gem. GmbH
Platzgasse 18
89073 Ulm

In Kooperation mit
Stadthaus Ulm – Stadt Ulm
Münsterplatz 50
89073 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.