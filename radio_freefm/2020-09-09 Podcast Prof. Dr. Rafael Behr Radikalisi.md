---
id: "322021589018853"
title: Podcast Prof. Dr. Rafael Behr "Radikalisierung in der Polizei"
start: 2020-09-09 15:00
end: 2020-09-09 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/322021589018853/
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 
Heute zu Gast ist Prof. Dr. Rafael Behr. Er ist Professor für Polizeiwissenschaft an der Akademie der Polizei Hamburg.
Mit ihm reden wir über racial profiling und Radikalisierung bei der Polizei. Gibt es Anzeichen für einen strukturellen Rassismus bei der Polizei? Haben Gewalt und illegale Gewaltanwendung der Polizei zugenommen?
Live zu hören bei Radio free FM, 102.6 MHz, am 09.09.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de