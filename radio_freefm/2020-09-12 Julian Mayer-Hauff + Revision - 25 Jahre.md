---
id: "330464218149087"
title: "Live Techno: Julian Maier-Hauff + Revision 25 Jahre free FM"
start: 2020-09-12 19:00
end: 2020-09-12 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/330464218149087/
teaser: "Radio free FM präsentiert:  - Julian Maier-Hauff - Revision  Julian
  Maier-Hauff, analog, elektronisch, verspielt - akustisch und vor allem eins:
  LIVE."
isCrawled: true
---
Radio free FM präsentiert:

- Julian Maier-Hauff
- Revision

Julian Maier-Hauff, analog, elektronisch, verspielt - akustisch und vor allem eins: LIVE. Als einer der wenigen in der elektronischen Szene extemporiert der Musiker während seines Auftritts und produziert somit seinen Sound direkt, in der Anwesenheit seines Publikums. Abgerundet wird das Ganze dann noch mit einem Mix aus diversen Instrumenten. Trompete, Saxophon, Posaune und Rhodes bilden den Kontrast zur analogen Elektronik und fügen sich charmant in den Gesamtsound ein. Julian Maier-Hauff spielt mit der Tiefe des fiktiven Raums, kombiniert flächige Elemente mit animierenden Grooves, kristalline Geräusche mit ruhigen Melodien. Ohne Unterbrechung bewegt sich sein Klangapparat sobald er einmal angeschoben wurde von einem Motiv zum nächsten. 

Überzeugt euch selbst von seiner Kunst im Liederkranz am 12.09.2020, der Eintritt ist frei! 



-------------------------
Radio free FM gem. GmbH
Platzgasse 18
89077 Ulm

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.