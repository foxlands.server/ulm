---
id: "1187294471632764"
title: Alkapulco Roy & Hans Francisco 25 Jahre freeFM
start: 2020-09-20 20:00
end: 2020-09-20 22:00
address: Livestream
link: https://www.facebook.com/events/1187294471632764/
teaser: Alkapulco Roy & Hans Francisco spielen ein Liveset aus alten Klassikern, Mash
  Ups, eigenen Songs und Liveraps. Old Kidz In the Block sind heute in dei
isCrawled: true
---
Alkapulco Roy & Hans Francisco spielen ein Liveset aus alten Klassikern, Mash Ups, eigenen Songs und Liveraps. Old Kidz In the Block sind heute in deinem Radio!