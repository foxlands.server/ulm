---
id: "2252254188412331"
title: Live Radio auf dem Internationalen Donaufest / Weindorfbühne
start: 2020-07-09 13:00
end: 2020-07-09 23:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/2252254188412331/
image: 87359336_10158004203776668_3201228761399820288_o.jpg
teaser: "#25JahrefreeFM  Mehr Infos folgen in Kürze in voller Länge."
isCrawled: true
---
#25JahrefreeFM

Mehr Infos folgen in Kürze in voller Länge.