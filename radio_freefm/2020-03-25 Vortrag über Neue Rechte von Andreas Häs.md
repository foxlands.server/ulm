---
id: "988190428249081"
title: Vortrag über "Neue Rechte" von Andreas Hässler
start: 2020-03-25 19:00
end: 2020-03-25 20:30
address: Münsterplatz 25, 89073 Ulm
link: https://www.facebook.com/events/988190428249081/
image: 86189405_1062897507430344_6069760770411855872_n.jpg
teaser: AUFGRUND DER AKTUELLEN GESUNDHEITSLAGE WIRD DIESE VERANSTALTUNG ABGESAGT.  Im
  Rahmen der INTERNATIONALEN WOCHEN GEGEN RASSISMUS findet ein Vortrag von
isCrawled: true
---
AUFGRUND DER AKTUELLEN GESUNDHEITSLAGE WIRD DIESE VERANSTALTUNG ABGESAGT.

Im Rahmen der INTERNATIONALEN WOCHEN GEGEN RASSISMUS findet ein Vortrag von Andreas Hässler von der Fachstelle Mobile Beratung gegen rechts, statt. 
Die sogenannte „Neue Rechte“ hat in den letzten Jahren zur Modernisierung des extrem rechten Spektrums erheblich beigetragen. Nicht mehr der klare Bezug auf den historischen Nationalsozialismus wird propagiert, sondern eine Verpackung des Themas in eine unscheinbare Hülle mit ebenso völkischen Bezugspunkten. Das betrifft auch das Thema Rassismus, welches unter dem Schlagwort des Ethnopluralismus als vielfältig und bunt verkauft wird.
EINTRITT IST FREI