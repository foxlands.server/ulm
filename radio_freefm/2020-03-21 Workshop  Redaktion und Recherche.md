---
id: "504120330215940"
title: "Workshop :: Redaktion und Recherche"
start: 2020-03-21 11:00
end: 2020-03-21 13:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/504120330215940/
image: 80315147_10157827348076668_2690482576388259840_o.jpg
teaser: 'Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich
  spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie berei'
isCrawled: true
---
Wie recherchiere ich Themen im Radio und texte sie verständlich? Wo finde ich spannende Geschichten? Was macht gute "Radiosprache" aus? Und: Wie bereite ich meine Inhalte auf, bringe sie auf den Punkt und entwickle sie weiter? Wie baue ich ein Themen-Netzwerk auf? 

Referent: K.W.
Pflicht für Neueinsteiger, kostenlos für Mitglieder!

Anmeldung: ausbildung[at]freefm.de