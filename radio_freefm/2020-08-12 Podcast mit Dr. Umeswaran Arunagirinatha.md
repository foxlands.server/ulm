---
id: "312653263191494"
title: Podcast mit Dr. Umeswaran Arunagirinathan
start: 2020-08-12 15:00
end: 2020-08-12 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/312653263191494/
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 

Heute zu Gast ist der Herzchirurg und Autor Dr. Umeswaran Arunagirinathan. Mit ihm reden wir über Flucht, Integration und Rassismus.
 
Live zu hören bei Radio free FM, 102.6 MHz, am 12.08.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de