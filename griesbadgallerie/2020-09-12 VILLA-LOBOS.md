---
id: "3188221841273677"
title: VILLA-LOBOS
start: 2020-09-12 20:30
end: 2020-09-12 21:00
locationName: Stiege
address: Zum Rosengarten 1 (Herdbrücke), 89073 Ulm
link: https://www.facebook.com/events/3188221841273677/
teaser: VILLA-LOBOS Stiege Ulm, 12.09.2020 I 20:30h Eintritt frei  Das letzte Konzert
  in der Reihe „this sounds good (and looks good)“ ist als einzige Veranst
isCrawled: true
---
VILLA-LOBOS
Stiege Ulm, 12.09.2020 I 20:30h
Eintritt frei

Das letzte Konzert in der Reihe „this sounds good (and looks good)“ ist als einzige Veranstaltung klassisch gehalten, wobei eine besonders außergewöhnlich Besetzung mit brasilianischer Musik zu hören sein wird. Auf dem Programm stehen die Bachianas Brasileiras Nr. 1 & 5 von Villa-Lobos für acht Celli und eine Sopranistin. Die melancholisch anmutende Musik an der Donau stellt die Ulmer Schachtel in einen neuen Kontext.

Die Künstler:

Maria Chabounia, Sopran

Cellisten:
Mathis Merkle
Oliver Léonard
Joel Blido
Giovanni Crivelli
Florian Schmidt-Bartha
Andreas Schmalhofer
Stanislas Kim
David Bühl

Weitere Informationen finden Sie unter ponte-ulm.com/programm/villa-lobos/