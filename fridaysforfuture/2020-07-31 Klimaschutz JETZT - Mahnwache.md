---
id: "2715675562009191"
title: Klimaschutz JETZT - Mahnwache
start: 2020-07-31 17:30
end: 2020-07-31 18:30
address: Münsterplatz Ulm
link: https://www.facebook.com/events/2715675562009191/
teaser: "#KlimaschutzJetzt  Mahnwache am Freitag, 31.07.2020 um 17.30 Uhr
  Münsterplatz  Die Klimakrise schreitet beängstigend rasant voran, auch wenn in
  den Me"
isCrawled: true
---
#KlimaschutzJetzt

Mahnwache am Freitag, 31.07.2020 um 17.30 Uhr
Münsterplatz

Die Klimakrise schreitet beängstigend rasant voran, auch wenn in den Medien wenig darüber berichtet wird: Rekordtemperaturen in der Arktis und in Sibirien, schmelzendes arktisches Eis, verheerende Waldbrände in Sibirien, Trockenheit und Baumsterben in weiten Teilen Deutschlands, Jahrhunderthochwasser in China, und so weiter.

Die Politik in Europa, in Deutschland, in Baden-Württemberg und auch lokal hier in Ulm hat immer noch keine passenden Lösungen für die große Menschheitsaufgabe des Klimaschutzes: Auf dem EU Gipfeltreffen werden Mittel, die für Klimaschutzmaßnahmen vorgesehen waren, gestrichen, die Bundesregierung beschließt ein üppiges Unterstützungsprogramm für Kohlekonzerne und erlaubt den Abbau des dreckigsten Energieträgers Braunkohle bis 2038, in Baden-Württemberg ist der Ausbau der Windenergie fast zum Erliegen gekommen und in Ulm gibt es immer noch keine Anzeichen für eine Verkehrswende, die ihren Namen verdient und den CO2-Ausstoß im Verkehrssektor deutlich senken würde. Zudem haben die Corona-Ausbrüche in den Schlachthöfen ein grelles Licht auf die unhaltbaren Zustände in der Fleischindustrie geworfen, die sowohl Menschen- und Tierrechte mit Füßen tritt als auch einen riesigen Beitrag zur Treibhausgas-Emission leistet.

Wir fordern eine politische Wende auf allen Ebenen, damit endlich mit ernsthafter Klimaschutzpolitik begonnen wird: 

- Ausstieg aus der Kohleverbrennung bis 2030 
- Konsequente Förderung von erneuerbaren Energien
- Verbot von Inlandsflügen
- Ende der Massentierhaltung
- Vorfahrt für umweltfreundliche Verkehrsmittel sowie autofreie Innenstädte!

Zeigen wir als Bürgerinnen und Bürger von Ulm, dass wir nicht mit einer zerstörerischen Politik einverstanden sind!

Unterstützt unsere Forderungen bei der Kundgebung am Freitag, den 31. Juli von 17.30 bis 18.30 Uhr auf dem Münsterplatz.

Bitte mindestens 1,50 m Abstand untereinander halten und 5 m zu PassantInnen. Bringt nach Möglichkeit Eure Fahrräder mit, als Signal für nachhaltige Mobilität und um das Abstandhalten zu erleichtern.
Personen, die sich krank fühlen oder krank sind, bleiben bitte zuhause.
Das Tragen einer Mund-Nasen-Maske wird empfohlen.


Organisation: Extinction Rebellion Ulm, Parents for Future Ulm/Neu-Ulm/Alb-Donau-Kreis

Unterstützer: ADFC Ulm/Alb-Donau, BUND Donau-Iller, Ulm, Bündnis für eine agrogentechnikfreie Region Ulm, divest Ulm, Fridays For Future Ulm, Gemeinwohl-Ökonomie Ulm,  Greenpeace Ulm/Neu-Ulm, Lokale Agenda Ulm 21, Umweltgewerkschaft Gruppe Ulm/Neu-Ulm, Solidarische Landwirtschaft Ulm/Neu-Ulm.