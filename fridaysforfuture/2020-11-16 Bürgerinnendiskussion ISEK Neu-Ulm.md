---
id: "1722395734595009"
title: Bürger:innendiskussion ISEK Neu-Ulm
start: 2020-11-16 18:30
link: https://www.facebook.com/events/1722395734595009/
teaser: Gestalte deine Stadt! Die Stadtverwaltung Neu-Ulm hat einen Prozess
  angestoßen, in dem die städtebauliche Entwicklung für die nächsten 10 Jahre
  geplan
isCrawled: true
---
Gestalte deine Stadt!
Die Stadtverwaltung Neu-Ulm hat einen Prozess angestoßen, in dem die städtebauliche Entwicklung für die nächsten 10 Jahre geplant wird - gemeinsam mit den Neu-Ulmer Bürger:innen möchte die Stadtverwaltung ein Integriertes Stadtentwicklungskonzept (ISEK) erarbeiten. 

Mit blick auf individuelles Wohnen und das gesamte Stadtbild, in Bezug auf Nahversorgung un dsoziale Infrastruktur, Bildunf, Mobilität, Freizeit und Kultur, Natur, Klima oder Energie. 

Eure Meinung und Ideen sind gefragt!!

Bis zur Onlinediskussion können über folgenden Link Ideen und Meinungen eingebracht werden: 

https://nu.neu-ulm.de/stadt-politik/stadtentwicklung/isek-2030/

Die Teilnahme an der Online Live Bürger:innendiskission ist über folgenden Link möglich: 

https://live.tonecdn.com/?stream=neuulm

Wir freuen uns auf euch! :) 

