---
name: Fridays For Future Ulm
website: http://www.fridaysforfuture.de/
email: ulm@fridaysforfuture.de
scrape:
  source: facebook
  options:
    page_id: ulm.fridaysforfuture
---
Wir sind Schülerinnen und Schüler die für mehr Klimaschutz streiken. Wir sind damit Teil der weltweiten Bewegung Fridays for Future.
