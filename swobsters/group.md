---
name: SWOBSTER'S
website: http://www.swobsters.de
email: info@swobsters.de
scrape:
  source: facebook
  options:
    page_id: swobsters
---
Ulms' first & only Tiki Cafe & 50s bar / good rockin' music / special events / Livemusic
