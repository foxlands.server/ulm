---
id: "1321421122111278"
title: Frauenkreis / Women Circle
start: 2020-09-23 18:00
end: 2020-09-23 19:30
locationName: www
image: womencircle.jpg
teaser: "Join Verena for a women's circle and share your truth, from the heart and without judgement."
isCrawled: false
---
Join Verena for a women's circle and share your truth, from the heart and without judgement.

Women have been gathering together for centuries, from biblical ‘Red Tents’ where the women in a tribe would live together during the days of menstruation, right up to today. The purpose of our circles is to create a place for women to empower and build stronger relationships with each other.

To hold the integrity of our circle, the following rules are set:

- Once a woman talks, we provide a space to listen. This means that no direct responses are given, or judgement about what was said. This is the foundational rule in order to create a safe space where one can feel heard and held
- Everything shared in the circles are confidential and held in that container
- We aim to bring everyone into the same space, which means the circle is meant for every woman
- We're an international group and everyone is welcome to share in their own language. However as most of us are Germany based, the circles will be guided in English and/or German
- We absolutely do not provide space for bullying, shaming or judgment. If this happens, you may be removed from the group

When: Wednesday 23th September 18:00
Where: online via Jitsi
Duration: approx 90 mins
Energy exchange: To keep our circles accessible, our circles are on donation, between 5-15€. This leaves it up to you to decide the value in relation to your financial situation. If you're in financial hardship right now, you are very welcome to join us with an energy exchange of some kind - just drop us a message

Just email me for further information :)
womencircle@posteo.de@posteo.de
Looking forward to sharing space with you! :)
