---
name: CAT
website: http://www.cat-ulm.de
email: info@cat-ulm.de
scrape:
  source: facebook
  options:
    page_id: CAT.Ulm
---
Tanz- und Nachtclub, Veranstaltungsort für Live-Musik
