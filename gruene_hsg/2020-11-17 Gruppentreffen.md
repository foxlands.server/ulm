---
id: "360514278547065"
title: Gruppentreffen
start: 2020-11-17 19:00
end: 2020-11-17 20:30
link: https://www.facebook.com/events/360514278547065/
image: 125254951_859455394858304_7171484229093597637_n.jpg
teaser: Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Dienstag, den 17. November um 19:00 Uhr. Den Link zu u
isCrawled: true
---
Wir laden Euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Dienstag, den 17. November um 19:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.
Herzlich Einladung auch an alle Erstis. Ihr könnt einfach vorbeikommen, reinschnuppern und Euch ein Eindruck von unserer Arbeit als politische Hochschulgruppe machen.
Wir freuen uns auf Euch!  