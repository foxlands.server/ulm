---
id: "320892812279508"
title: Gruppentreffen
start: 2020-07-21 20:00
end: 2020-07-21 22:00
link: https://www.facebook.com/events/320892812279508/
image: 107900993_766560830814428_7241973930081236414_n.jpg
teaser: Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Dienstag, den 21. Juli um 20:00 Uhr. Den Link zu unser
isCrawled: true
---
Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Dienstag, den 21. Juli um 20:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.
Wir freuen uns über neue und alte Gesichter!