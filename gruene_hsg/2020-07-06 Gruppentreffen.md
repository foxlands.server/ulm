---
id: "292841515174152"
title: Gruppentreffen
start: 2020-07-06 20:00
end: 2020-07-06 22:00
link: https://www.facebook.com/events/292841515174152/
image: 106528145_759454498191728_2676085527486756765_n.jpg
teaser: Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Dienstag, den 23. Juni um 20:00 Uhr. Den Link zu unser
isCrawled: true
---
Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Dienstag, den 23. Juni um 20:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.
Wir freuen uns über neue und alte Gesichter! 