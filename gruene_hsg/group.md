---
name: Grüne Hochschulgruppe Uni Ulm
website: https://ghgulm.wordpress.com/
scrape:
  source: facebook
  options:
    page_id: GHGUlm
---
Wir verstehen uns als eine parteiunabhängige Hochschulgruppe, die den Grünen Nahe steht. Themen: Ökologie und Nachhaltigkeit sowie soziale Gerechtigkeit.
