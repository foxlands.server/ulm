---
id: "988384524964933"
title: Gruppentreffen
start: 2020-06-23 20:00
end: 2020-06-23 22:00
link: https://www.facebook.com/events/988384524964933/
image: 104131710_746874039449774_3078565537603953301_n.jpg
teaser: Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe.
  Wir treffen uns am Dienstag, den 23. Juni um 20:00 Uhr. Den Link zu unser
isCrawled: true
---
Wir laden euch herzlich ein zum nächsten Treffen der Grünen Hochschulgruppe. Wir treffen uns am Dienstag, den 23. Juni um 20:00 Uhr. Den Link zu unserem Online-Treffen erhaltet ihr auf Anfrage.
Wir freuen uns über neue und alte Gesichter! 