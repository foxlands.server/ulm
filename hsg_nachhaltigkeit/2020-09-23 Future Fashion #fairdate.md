---
id: "324191285555073"
title: "Future Fashion #fairdate"
start: 2020-09-23 08:00
end: 2020-09-23 11:00
link: https://www.facebook.com/events/324191285555073/
teaser: "Wir laden euch nächste Woche auf einen #fairdate ein, um gemeinsam die Faire
  Woche 2020 zu feiern!   Ihr wollt die nachhaltige Modeszene in Stuttgart"
isCrawled: true
---

Wir laden euch nächste Woche auf einen #fairdate ein, um gemeinsam die Faire Woche 2020 zu feiern! 

Ihr wollt die nachhaltige Modeszene in Stuttgart und Ulm entdecken und Future Fashion näher kennenlernen? Das ist eure Chance! 

Macht einfach bei unseren Digiwalk mit und kommt im Anschluss darauf im Future Fashion Store (9-12 Uhr) oder in der Secontique Ulm (10-12 Uhr) bei unseren Treffen. Bei einem leckeren, fairen Getränk erzählen wir euch gerne alles, was ihr schon immer über unsere Bewegung und über #futurefashion wissen wolltet.

Wie genau funktioniert der Digiwalk?

Die Digiwalk-App führt euch zu nachhaltigen Stores, die hochwertige Upcycling-Kleidung, Second Hand Fashion, vegane und öko-faire Mode anbieten. Kurze Audio- und Videobotschaften stellen euch die Konzepte der Stores vor. 

DigiWalk Stuttgart: https://www.digiwalk.de/walks/future-fashion-tour-stuttgart

DigiWalk Ulm: https://www.digiwalk.de/walks/future-fashion-tour-ulm

Wir freuen uns sehr, unsere #futurefashioncommunity live kennenzulernen und von euren Digiwalk Erfahrungen zu hören. 

Bring gerne auch eure Freunde mit!

See you soon! 