---
id: "2975857519142618"
title: "Digitale Future Fashion Tour per App #FashionRevolution"
start: 2020-04-21 08:00
end: 2020-04-26 23:00
address: Stuttgart, Ulm & Tübingen
link: https://www.facebook.com/events/2975857519142618/
image: 93439119_10163323138110282_3477938476190531584_o.jpg
teaser: Wo kann ich stylische öko-faire Klamotten kaufen? Und was kann ich mit
  Kleidung machen, die ich nicht mehr trage? An jedem Stopp der Future Fashion
  To
isCrawled: true
---
Wo kann ich stylische öko-faire Klamotten kaufen? Und was kann ich mit Kleidung machen, die ich nicht mehr trage? An jedem Stopp der Future Fashion Tour in Stuttgart und Ulm erfährst Du Wissenswertes über nachhaltige Mode und Textilien und lernst in kurzen Video- und Audiobeiträgen neue Labels und Läden kennen.

Im Zuge der Fashion Revolution Week stellen wir unsere Tour auf digitale Beine!  Fashion Revolution Deutschland

Lade dir die App DigiWalk herunter und wähle unter "Weiteres", "Touranbieter wechseln" Future Fashion aus.

Egal ob in Stuttgart, in Ulm oder von zuhause aus: Beginne mit der Station "Begrüßung", danach werden alle Stationen freigeschaltet, die du in beliebiger Reihenfolge ansehen- und hören kannst. Am schönsten ist es natürlich, wenn du die Route tatsächlich abläufst und die Beiträge an den dafür passenden Stationen anhörst.

Link zur Tour in Ulm: https://www.digiwalk.de/walks/future-fashion-tour-ulm/de
Link zur Tour in Stuttgart: https://www.digiwalk.de/walks/future-fashion-tour-stuttgart/de
Link zur Tour in Tübingen: https://actionbound.com/bound/modeaufderspur

Diese Veranstaltung ist Teil der Bewegung Future Fashion. Gemeinsam mit der Stiftung Entwicklungszusammenarbeit BW, dem Dachverband Entwicklungspolitik Baden-Württemberg e.V. - DEAB und vielen weiteren Akteuren organisiert die Jugendinitiative der Nachhaltigkeitsstrategie Baden-Württemberg Wir ernten was wir säen Kleidertauschpartys, Upcycling-Events, Vorträge und Stadtführungen.

Alle Infos unter https://www.futurefashion.de/future-fashion-on-tour/