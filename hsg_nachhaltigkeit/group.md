---
name: Hochschulgruppe für Nachhaltigkeit
website: https://www.uni-ulm.de/misc/hg-nachhaltigkeit/home
email: hg-nachhaltigkeit@uni-ulm.de
scrape:
  source: facebook
  options:
    page_id: HSGNachhaltigkeit
---
Wir sind eine Gruppe von Studenten, die sich mit verschiedenen Aspekten der Nachhaltigkeit auseinander setzt und versucht diese voran zu bringen.
Wir veranstalten zum Beispiel einmal im Jahr die Hochschultage in Zusammenarbeit mit dem FAW/n, bei denen Vorträge, Podiumsdiskussion und Workshops besucht werden können.
Außerdem zeigen wir bei der Kinoreihe watch.think.act pro Semester 3-4 Filme mit anschließender Diskussion im Kino Mephisto.
Weitere Aktionen finden meist während des Semester statt und werden natürlich hier bekannt gegeben.
