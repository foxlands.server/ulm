---
id: "296015351658949"
title: "Summer in the City / Rockabend: Groove Bang + Next Boy"
start: 2020-09-16 19:00
end: 2020-09-16 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/296015351658949/
teaser: Groovige Rockmusik mit Funk-Elementen? Yeah! Songs, die bewegen und in die
  Beine gehen? Yeah! Songs, die zu wenig bekannt sind, um interpretiert zu we
isCrawled: true
---
Groovige Rockmusik mit Funk-Elementen? Yeah!
Songs, die bewegen und in die Beine gehen? Yeah!
Songs, die zu wenig bekannt sind, um interpretiert zu werden.?
Songs, die zu gut sind, um es nicht trotzdem zu tun? Yeah, yeah!
Groove Bang sind:
Tobias Saur: Gesang
Steffen Seitter: Gitarre
Christoph Bölke: Bass
Carsten Stocker: Schlagzeug

Den Abend eröffnen werden Next Boy mit einem modernen Mix aus Pop, Rock & Rap.

Eintritt frei!

Wir danken dem Ministerium für Forschung, Wissenschaft und Kunst Baden-Württemberg für die Unterstützung

