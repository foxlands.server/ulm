---
name: Kunstwerk e.V. Ulm
website: http://www.kunstwerk-ulm.de/
scrape:
  source: facebook
  options:
    page_id: KunstwerkUlm
---
Wir sind ein kleines Team von rund einem Dutzend Menschen, die Lust haben, Kulturveranstaltungen ehrenamtlich zu organisieren. Es gibt immer viel zu tun, daher freuen wir uns über jede Unterstützung und Hilfe, und neue Mitarbeiter begrüßen wir sehr, sehr gern. Meldet euch einfach!
