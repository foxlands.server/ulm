---
id: "418206965868623"
title: "Dem Künstler über die Schulter schauen: Jörg Eberwein"
start: 2020-12-19 13:00
end: 2020-12-19 19:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/418206965868623/
image: 123760395_197186328593540_859690453184236384_n.jpg
teaser: Heute druckt Jörg Eberwein in der Galerie. Für beste Unterhaltung ist gesorgt.
  Der Künstler ist anwesend und tritt mit den Anwesenden in einen produkt
isCrawled: true
---
Heute druckt Jörg Eberwein in der Galerie. Für beste Unterhaltung ist gesorgt. Der Künstler ist anwesend und tritt mit den Anwesenden in einen produktiven Austausch. Lassen Sie sich porträtieren, und Sie erhalten einen Druckabzug in Form eines kolorierten gedruckten Exemplars.
(Die Ausstellung „Drucken ist ein Abenteuer“ ist in dieser Zeit geöffnet; Eintritt frei. Im Anschluss findet ab 19 Uhr die Grafik-Auktion „Kunstsammeln für Arbeitnehmer statt)
