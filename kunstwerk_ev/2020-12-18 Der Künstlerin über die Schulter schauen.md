---
id: "397576968092496"
title: "Der Künstlerin über die Schulter schauen: Adela Knajzl"
start: 2020-12-18 12:00
end: 2020-12-18 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/397576968092496/
image: 123712897_196276798684493_2757866383629057650_o.jpg
teaser: Heute druckt Adela Knajzl in der Galerie ihre Grafikblätter, bedruckt aber
  auch Stofftaschen. Und wer Lust hat, kann sie besuchen, ihr (mit Distanz, v
isCrawled: true
---
Heute druckt Adela Knajzl in der Galerie ihre Grafikblätter, bedruckt aber auch Stofftaschen. Und wer Lust hat, kann sie besuchen, ihr (mit Distanz, versteht sich) zuschauen, sich mit ihr über ihre Arbeit und das Drucken unterhalten.  Kein Angst: Sie beißt nicht!  
(Die Ausstellung ist in dieser Zeit geöffnet; Eintritt frei)
