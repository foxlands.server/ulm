---
id: "841919516546722"
title: "ENTFÄLLT leider wegen Corona Lockdown: B3 (fusion funk)"
start: 2020-11-21 21:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/841919516546722/
image: 121164113_4892963947388315_1183196248889025013_o.jpg
teaser: "ENTFÄLLT LEIDER WEGEN CORONA-LOCKDOWN  Wegen doch recht begrenzter Plätze
  empfehlen wir den Vorverkauf: https://www.ulmtickets.de/produkte/5892-ticket"
isCrawled: true
---
ENTFÄLLT LEIDER WEGEN CORONA-LOCKDOWN

Wegen doch recht begrenzter Plätze empfehlen wir den Vorverkauf:
https://www.ulmtickets.de/produkte/5892-tickets-b3-fusion-funk-ulmer-volkshochschule-ulm-am-21-11-2020

Diese Band überzeugt stets mit ansteckender Spiellaune und elektrisierender Performance. Das Publikum ist mit- und hingerissen, ein Funk-Vierer in Höchstform. Ach was, Funk, da geht´s genauso um Rock oder Jazz, egal, Hauptsache, es geht in die Beine! 
Saustarke Grooves, eingängige Melodien, intelligente Songs: Hier sprühen die Funken im wahrsten Sinne des Wortes mit jedem Ton. Die Band betört Funkaholics, Rocksters und Jazzies in gleicher Weise.
Presse: "Akzentuierter Rock, Funk und etwas Blues - super gut!" (Concerto) * "Ein gelungenes Live-Erlebnis" (GoodTimes) * "Ein schönes Album voller jazziger Emotionen" (Jazz Podium) * „Ins Bein gehende Fusion-Grooves, scharfe Gitarrensoli und muskulöse Drums“ (Jazz thing)

Ron Spielman: Gitarre, Gesang
Andreas Hommelsheim: Hammond B3, keys
Lutz Halfter: Schlagzeug

Eintritt: 15 €, ermäßigt 10 € (mit Ausweis: Mitglieder/Schüler/Studenten/Bufdis/FSJ), Menschen bis 16 Jahre haben freien Eintritt
