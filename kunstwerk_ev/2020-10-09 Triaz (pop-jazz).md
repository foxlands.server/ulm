---
id: "2129170787227533"
title: TRIAZ (pop-jazz) // Ulm Einsteinhaus
start: 2020-10-09 21:00
end: 2020-10-09 23:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/2129170787227533/
image: 117878963_4594897983861581_4627747051182413339_o.jpg
isCrawled: true
---
