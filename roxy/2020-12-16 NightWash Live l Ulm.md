---
id: "2606535012758466"
title: NightWash Live l Ulm
start: 2020-12-16 20:00
end: 2020-12-16 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2606535012758466/
image: 75233368_10156713248617083_8313499729346428928_n.jpg
teaser: "Die Veranstaltung findet wie geplant statt. Bitte beachtet: Unsere Bestuhlung
  ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer G"
isCrawled: true
---
Die Veranstaltung findet wie geplant statt. Bitte beachtet: Unsere Bestuhlung ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer Gruppengröße geeignete Sitzgruppen, ggf. muss sich eure Gruppe für die Zeit der Vorstellung trennen. Stühle dürfen nicht verschoben werden.
Tragt eure eigene Mund-Nasen-Bedeckung, die ihr auf eurem Sitzplatz abnehmen könnt. Wir werden eure Kontaktdaten aufnehmen (Telefonnummer oder Adresse), für vier Wochen verwahren und nur im Falle eines Coronafalls an das Gesundheitsamt aushändigen.
Danke für Geduld und Rücksicht auf andere Gäste.
***
Der NightWash Moderator Thomas Schmidt präsentiert euch Fabio Landert, Falk Schug, Bumillo und Negah Amiri in einer Show, die sich gewaschen hat.

NightWash bringt die frischeste Stand-Up Comedy angesagter Comedians und Newcomer, überraschende Showeinlagen gepaart mit dem allerbesten Publikum auf die Bühne. Jede Show ein Unikat auf höchstem Niveau. Hier bleibt garantiert kein Auge trocken! Comedy der Extraklasse!
 
NightWash Live - Stand-Up Comedy at its best!


Alle aktuellen Infos unter: www.nightwash.de

NightWash auf Instagram:
https://www.instagram.com/nightwash_official/
