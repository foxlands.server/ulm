---
id: "213761703368617"
title: Dame - Zeus Tour 2021 - Roxy Ulm
start: 2021-01-14 19:00
end: 2021-01-14 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/213761703368617/
image: 90565342_2877403559005662_8441639404312723456_o.jpg
teaser: "!!! ERSATZTERMIN !!! (Ticket behält Gültigkeit)  ADVANCED SOCIETY & COLD LIFE
  ENTERTAINMENT PRÄSENTIEREN  DAME - ZEUS TOUR 2021 PLUS SPECIAL GUEST: MA"
isCrawled: true
---
!!! ERSATZTERMIN !!!
(Ticket behält Gültigkeit)

ADVANCED SOCIETY & COLD LIFE ENTERTAINMENT PRÄSENTIEREN

DAME - ZEUS TOUR 2021
PLUS SPECIAL GUEST: MACE

Nach der Unterbrechung durch die Corona-Maßnahmen setzt DAME seine bislang erfolgreichste Tournee zu seinem neuen Album „ZEUS“ 2021 fort.

Spätestens seit sich DAME mit seinen letzten drei Alben die Spitze der heimischen Charts sichern konnte, ist das österreichische Ausnahmetalent längst kein Geheimtipp mehr. Zahlreiche, nahezu ausverkaufte Tourneen, begehrte Positionen auf den Bühnen der renommiertesten Festivals und über 735.000 treu ergebene YouTube-Abonnenten verschaffen einem nur einen kleinen Einblick in seine sagenhafte Karriere.

Nach einer anderthalbjährigen Kreativpause meldete sich DAME am 10. Mai mit der fulminanten Single „Herrscher des Olymps“ und der Ankündigung des neuen Albums „ZEUS“, welches am 06.09.2019 erscheinen wird, aus seiner Hitschmiede zurück. Ohne große Anstrengungen konnte er nach nur 48 Stunden die 200.000er Marke knacken und schoss direkt auf Platz 1 der Amazon Box-Set Charts des deutschsprachigen Raumes und ließ andere große Namen wie RAMMSTEIN, SIDO oder SHINDY hinter sich.

Auf „ZEUS“ zeigt sich DAME von gänzlich unterschiedlichen Seiten und stellt einmal mehr seinen einzigartigen Facettenreichtum unter Beweis. Während „Herrscher des Olymps“ zunächst mehr auf seine Fertigkeiten als Rapper abzuzielen scheint, zeugt schon die zweite Single-Auskopplung „Lichtblick“ von tiefgehender Lyrik und einem äußerst emotionalen Thema - und kommt dennoch als eingängiger Ohrwurm daher. Insgesamt sieben Singles hat er bereits vor der Album-Veröffentlichung preisgegeben - und alle können sich größten Lobes erfreuen. Das alles macht Lust auf mehr! Und das sollen die Fans bekommen!

Im September und Oktober 2019 ist DAME endlich wieder live zu erleben und es zeichnet sich bereits jetzt ab, dass diese Tournee etwas ganz Besonderes werden wird. DAME selbst verspricht eine Show, wie es sie noch nie gegeben hat. Ferner ist klar, dass sie den Erfolg der vorherigen Tournee einmal mehr übertrumpfen wird, denn nahezu alle Shows sind bereits seit einiger Zeit hochverlegt und/oder ausverkauft. Da die Nachfrage jedoch auch kurz vor dem Startschuss nicht abzureißen scheint, entschloss sich DAME kurzerhand, noch einmal nachzulegen – und so verkündet er nun, im Februar und März 2020 in die zweite Runde zu gehen und das gleich mit 19 Shows in Deutschland, Österreich und der Schweiz.

Als Special Guest wird ihn MACE begleiten. Die eisernen DAME-Fans konnte MACE mit Sicherheit bereits mit der ein oder anderen Show von seinem Live-Talent überzeugen, doch spätestens mit seiner Debütsingle „Schubladendenken“ stellte er endgültig klar, dass seine Art von Musik jede potentielle Schublade sprengt. Er versteht es, rhythmisch zu variieren und unter Beweis zu stellen, wie unbefangen moderner Rap aus Österreich klingen kann.

Egal, ob der junge Wahl-Salzburger persönliche Erlebnisse reflektiert („Weißt du noch“), zwischen schnelleren und langsameren Passagen hin und her springt oder kompromisslos mit der Autotune-Keule um sich schlägt („Alle so wie du“) - mit stets sitzender Frisur und brodelnder Stimme brettert er über die Beats, wie eine Dampflok. Mit seinen Erzählungen lädt er den Hörer auf eine spannende Reise durch verschiedenste Stilrichtungen, durch seine persönlichen Erfahrungen, Emotionen und Verrücktheiten ein; eine Reise mit klarem Kurs: nach oben. Und diese Einladung nehmen wir doch dankend an.

Homepage:	www.damestream.at
Facebook:	www.facebook.com/damestream
Twitter:		www.twitter.com/damestream
YouTube:	        www.youtube.com/user/sentinelsbg

