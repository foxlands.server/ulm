---
id: "645216016248367"
title: Open Stage
start: 2020-09-14 20:00
end: 2020-09-14 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/645216016248367/
image: 73533024_10156898434207756_7264035736268046336_o.jpg
teaser: Hier kann man einmal im Monat aufstrebende Talente, charmante Dilettanten und
  unverbrauchte Profis beim Schwitzen, Zittern, Triumphieren, aber auch be
isCrawled: true
---
Hier kann man einmal im Monat aufstrebende Talente, charmante Dilettanten und unverbrauchte Profis beim Schwitzen, Zittern, Triumphieren, aber auch beim Abstürzen erleben — ein Schauplatz kultiger Auftritte von Darstellern aus den Bereichen Kabarett, Comedy, Musik und Tanz, die sich berufen fühlen, sich der Gunst des Publikums zu stellen. Möglich ist (fast) alles! Unterhaltsam und spannend ist es immer!

Moderation: Matthias Matuschik

Mit freundlicher Unterstützung von
EDAG
Transporeon