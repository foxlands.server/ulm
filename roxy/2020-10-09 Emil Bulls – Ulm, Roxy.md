---
id: "639999953215896"
title: Emil Bulls – Ulm, Roxy
start: 2020-10-09 19:00
end: 2020-10-09 23:59
address: ROXY.ulm
link: https://www.facebook.com/events/639999953215896/
image: 91546695_10151335510939996_5841185608805908480_o.jpg
teaser: "Einlass: 19:00 Uhr | Beginn: 20:00 Uhr  Immer wieder aufs Neue beweisen sich
  Emil Bulls als eine der wandlungsfähigsten und im positiven Sinne unberec"
isCrawled: true
---
Einlass: 19:00 Uhr | Beginn: 20:00 Uhr

Immer wieder aufs Neue beweisen sich Emil Bulls als eine der wandlungsfähigsten und im positiven Sinne unberechenbarsten Bands ihres  Genres. Das Jahr 2020 steht bei den Musikern aus Süddeutschland unter dem Motto „Party Hard“. Erfahrungsgemäß teilen Emil Bulls derartige Vorhaben liebend gern mit ihren Fans - vorzugsweise auf großen Bühnen und einer ausgedehnten Tour. Letztere können sich die Fans schon jetzt im Kalender mit einem großen Ausrufezeichen vermerken, denn im Oktober und Dezember rocken Emil Bulls auf ihrer „25 TO LIFE“-Jubiläumstour wieder die Konzerthallen hierzulande. Finaler Höhepunkt des Jubiläumsjahres wird dann am 19. Dezember der Emil Bulls Birthday Bash im Münchner Zenith sein. In diesem Sinne: „HAPPY BIRTHDAY AND FAMILY MEANS FAMILY FOREVER!“.