---
id: "149540756271577"
title: Verschoben - Sulaiman Masomi "Morgen - Land" (Ulm)
start: 2020-05-27 20:00
end: 2020-05-27 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/149540756271577/
image: 89540481_1782764295199713_4527168470860693504_o.jpg
teaser: "*** Der Termin wird aufgrund der Corona Krise auf den 28.02.2021 (Neues
  Programm) verschoben****  Sulaiman Masomis Stücke sind wie trojanische
  Pferde."
isCrawled: true
---
*** Der Termin wird aufgrund der Corona Krise auf den 28.02.2021 (Neues Programm) verschoben****

Sulaiman Masomis Stücke sind wie trojanische Pferde. Sie wirken unterhaltsam und harmlos, aber sind sie erst einmal in die Köpfe der Zuhörer eingedrungen, entfalten sie ihre volle Wirkung und die in ihnen befindliche Botschaft.

Denn egal wie kurzweilig, witzig und nahbar seine Worte wirken: Immer versteckt sich seine ganz eigene Sicht auf die Welt und eine durchdachte Botschaft zwischen den Zeilen.

Schon der Titel seines neuen Programmes „Morgen - Land“ spiegelt seine Art der Bühnenkunst wieder. Es vereint Herkunft und Zukunft mit einem Wort und verortet gleichzeitig seine Sicht auf die heutige Gesellschaft. Und genau darum geht es auch in seinem Programm.

Seine Zuhörer erwartet eine Gratwanderung zwischen klugen Beobachtungen und witzigen Alltagsgeschichten, die mit Sicherheit ihren ganz eigenen Abdruck in den trojanischen Köpfen der Menschen hinterlassen wird. 

Der studierte Literaturwissenschaftler und frisch gekürte Gewinner des Dresdner Satire Preises ist darüber hinaus auch Landesmeister im Poetry Slam, Jurymitglied beim Treffen der jungen Autoren der Berliner Festspiele und immer wieder kultureller Botschafter für das Goethe Institut.

Er tanzt auf allen Hochzeiten und bittet nun Sie zum Tanz.

Und wer ihn einmal erlebt hat, tanzt mit.

"Sulaiman ist der Beste und Schönste und in zehn Jahren ist er auch noch alt und populär, also gucken Sie ihn sich an, so lange Sie es sich noch leisten können." (Helene Bockhorst)

„Der Typ ist ein Genie.“ (Torsten Sträter)

„Wegen Dir sind die Russen einmarschiert“ (Seine Mutter)

"Masomi jongliert mit all jenen Ängsten, Klischees und Vorurteilen, die sich um Migration, um Islam oder Islamismus ranken (…) Seine Botschaft ist glasklar und er gießt sie in Texte, die mal urkomisch, mal schmerzhaft ernst daherkommen. Raffiniert führt er zur zündenden Pointe oder tief hinein ins Absurde spießbürgerlicher Befindlichkeiten. Er beherrscht die feine Ironie ebenso wie den brachialen Kalauer (…) Masomi ist eine Bereicherung fürs Kabarett.“ (Allgemeine Zeitung Mainz 12.10.2018)