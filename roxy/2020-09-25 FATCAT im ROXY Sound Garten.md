---
id: "804546700288110"
title: FATCAT im ROXY Sound Garten (indoor)
start: 2020-09-25 20:00
end: 2020-09-25 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/804546700288110/
teaser: FATCAT - Acht verrückte Vollblutmusiker um den deutsch-amerikanischen Sänger
  Kenny Joyner, die dem Funk einen neuen Anstrich verpassen. Fetter Groove,
isCrawled: true
---
FATCAT - Acht verrückte Vollblutmusiker um den deutsch-amerikanischen Sänger Kenny Joyner, die dem Funk einen neuen Anstrich verpassen. Fetter Groove, schneidende Bläsersätze und eine mitreißende Soulstimme sind das Rezept für ihren modernen, erstaunlich vielschichtigen Sound: Powerfunk! 

Am 29. Mai 2020 erscheint FATCATs neues Album „GOOD.zip“. Es ist die zweite Studio-LP der Band, fast vier Jahre nach ihrem Debüt „Champagne Rush“ (2016) mit chartplatzierten Disco-Funk-Nummern wie „Glitter & Gold“ und „Champagne Rush“. Die neue Platte ist nun ihre erste komplette Eigenproduktion, und die neugewonnene künstlerische Freiheit hört man: Wer ein Schema F sucht, sucht hier vergebens. Stattdessen nimmt das Album mit auf spannende Entdeckungsreise. 

Auf GOOD.zip betten sich Ohrwürmer, wie die bereits veröffentlichte erste Single 

„i_feel_good“ zwischen komplexe und tiefsinnige Nummern ein, die mal wie eine smoothe Prince-Nummer (two_step), mal rau wie James Brown (golden_lines) oder virtuos und mitreißend wie Tower of Power (disco_queen) daherkommen. Dabei verlässt die Band auch gerne mal ihr heimisches Funk-Terrain - das Schlagzeug-Solo im grandiosen Finale „golden_leaves_and_rain“ kulminiert in einer Drum’n’Bass-Ekstase, die man auf Funk-LPs wohl nicht alle Tage hört. 

Den roten Faden durch das Album zieht ein absurd-schräges Interview des Godfather of Funk, James Brown; immer wieder werden Passagen daraus in kurzen humorvollen Skits aufgegriffen und auch der Titel „GOOD.zip“ versteht sich als Anlehnung. Es stellt sich die Frage: Wie menschlich dürfen unsere Idole sein? 

FATCAT jedenfalls zeigt sich sehr menschlich. „Social distancing“ ist für die achtköpfige Truppe eine echte Herausforderung, aus der Not macht sie aber lieber eine Tugend. In kleiner Besetzung spielt die Band regelmäßig Balkonkonzerte in ihrer Heimat Freiburg, lässt liebevoll handgenähten Mundschutz produzieren und spendet den Erlös aus deren Verkauf. Außerdem wurden kurzerhand alle Pläne für die erste Single-Auskopplung über Bord geworfen und stattdessen ca. 120 Video-Einsendungen von heimisolierten Fans und Musiker-Kollegen zum offiziellen Musikvideo von „i_feel_good“ zusammengeschnitten. Der Gute-Laune-Track, erschienen am 10. April, lenkt den Fokus auf das Gute und lässt für einen kurzen Moment die Umstände vergessen. Es ist nach dem Hören schier unmöglich die Welt mit einem Grinsen auf den Lippen und einer Melodie im Ohr nicht ein wenig optimistischer zu sehen. 

Über 350 Shows im In- und Ausland zeugen von der Hingabe, mit der sich die acht Freiburger eine stetig wachsende Fangemeinde erspielen. 

Neben Auftritten auf dem Fusion Festival, dem Baltic Soul Weekender und beim Bundespräsidenten auf Schloss Bellevue hat FATCAT als Support auch schon der Soul-Legende Chaka Khan, den Weißwurstbombern von LaBrassBanda sowie dem Jazz-Virtuosen Jamie Cullum eingeheizt. Letzterer schwärmt nach dem gemeinsamen Auftritt bei der Jazz Open Stuttgart: 

„Wir müssen euch nach England bringen – ihr seid unglaublich!“ 

Ein absolutes Highlight bleibt derweil die Open-Air Headliner-Show beim legendären Montreux Jazz Festival - mittlerweile sogar als LP erhältlich. 

Sänger Kenny: „Eine unbeschreibliche Erfahrung. Wir durften als letzte Band bei Sonnenuntergang mit Blick auf den Genfer See spielen, dort, wo schon so viele unserer Idole standen. Da gingen Kindheitsträume in Erfüllung.“ 

Einlass 19:00 H, Beginn 20:00 H
Eintritt frei