---
id: "351028372887290"
title: Poetry Slam - Frühe Vorstellung
start: 2020-11-14 18:00
end: 2020-11-14 19:15
address: ROXY.ulm
link: https://www.facebook.com/events/351028372887290/
image: 121467736_10157979171777756_558782807264194042_n.jpg
teaser: "Einlass: 17:00 H  Ein Poetry Slam ist der Dichterwettstreit unserer Zeit und
  macht Poesie zum Erlebnis. Gleichzeitig ist ein Poetry Slam aber auch ein"
isCrawled: true
---
Einlass: 17:00 H

Ein Poetry Slam ist der Dichterwettstreit unserer Zeit und macht Poesie zum Erlebnis. Gleichzeitig ist ein Poetry Slam aber auch eine Plattform für Jedermann - man muss sich nur trauen. Das Publikum ist mittendrin statt nur dabei und bestimmt durch seinen Applaus den Ausgang des Wettbewerbs. 

Wer selbst Gedichte, Geschichten oder Rap Texte schreibt, kann sich unter: poetry-slam@roxy.ulm.de anmelden und steht dann mit den Stars der Szene aus dem In- und Ausland auf der Bühne. Wichtig ist nur, dass man eigene Texte vorträgt, der Auftritt nicht länger als 6 Minuten dauert, man nicht auf Hilfsmittel wie Kostüme oder Instrumente angewiesen ist und man auch für den Fall der Fälle gerüstet ist und einen weiteren Text für das Finale dabei hat. 

Schreien, flüstern, jaulen, keuchen, pfeifen, rappen – all das ist jedoch erlaubt, um im Wettstreit gegeneinander anzutreten. Die Waffen: Poesie, Stimme und Körper. 

Zu befürchten hat man nichts, steht doch beim Poetry Slam der Respekt vor dem Poeten im Mittelpunkt.

LineUp:
Clemse Lebemann (Frankfurt/Main) 
Max Osswald (München) 
Maron Fuchs (Nürnberg) 
Marcel Schneuer (München)
Lukas Bühner (Ulm)
tba


***
Die Poetry Slams finden aufgrund der aktuellen Lage mit weniger Slammern und ohne Pause statt. Bitte beachtet: Unsere Bestuhlung ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer Gruppengröße geeignete Sitzgruppen, ggf. muss sich eure Gruppe für die Zeit der Vorstellung trennen. Stühle dürfen nicht verschoben werden. Tragt eure eigene Mund-Nasen-Bedeckung, die ihr auf eurem Sitzplatz abnehmen könnt. Wir werden eure Kontaktdaten aufnehmen (Telefonnummer oder Adresse), für vier Wochen verwahren und nur im Falle eines Coronafalls an das Gesundheitsamt aushändigen. Danke für Geduld und Rücksicht auf andere Gäste.

Mit freundlicher Unterstützung von
scanplus GmbH
ibis budget Ulm City

Medienpartner
swp.de