---
id: "950099115471825"
title: "ENTFÄLLT: Akku Quintet (cineastic minimal jazz)"
start: 2020-11-11 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/950099115471825/
image: 121158800_4892943287390381_2718874638169105091_o.jpg
teaser: Sphärisch – atmosphärisch. Komplex und federleicht rollt, gleitet, hüpft und
  tanzt ihre Musik durch die Takte, schwingt sich auf in luftige Höhen und
isCrawled: true
---
Sphärisch – atmosphärisch. Komplex und federleicht rollt, gleitet, hüpft und tanzt ihre Musik durch die Takte, schwingt sich auf in luftige Höhen und gleitet nieder in erdige Sphären, wo sie mit rollenden Bässen und messerscharfen Beats Anlauf für den nächsten Wachtraum nimmt. 
Die Schweizer Band um den Schlagzeuger Manuel Pasquinelli vereint Elemente aus Jazz, Minimal Music und Rock/Pop zu einem eigenständigen Bandsound.
Auf der aktuellen Tour präsentiert das Quintett sein neues Album «Depart». Dieses vierte Studioalbum der Band ist auf dem Label 7dMedia von Trey Gunn (langjähriges Mitglied von King Crimson) erschienen.

Musik zum Eintauchen mit Live-Visuals.

Manuel Pasquinelli - Drums
Michael Gilsenan - Sax
Maja Nydegger - Piano
Markus Ischer - Guit
Andi Schnellmann  - Bass
Jonas Fehr - Visuals

Vorverkauf: 14,20 €
Abendkasse: 15,00 € / ermäßigt 10,00 € für 
Kunstwerk-Mitglieder/Schüler/Studierende/Bufdis/FSJ)

Menschen bis einschließlich 15 Jahre haben freien Eintritt - bitte an der Abendkasse melden.
