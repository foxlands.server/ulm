---
id: "828613294341203"
title: Infoabend zur Landesgartenschau 2030
start: 2020-09-21 19:30
end: 2020-09-21 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/828613294341203/
teaser: Eine Landesgartenschau ausgerechnet entlang einer Bundesstraße! Verrückte Idee
  oder ein bewusst hoch gestecktes Ziel aus Kalkül? Sicherlich beides!  E
isCrawled: true
---
Eine Landesgartenschau ausgerechnet entlang einer Bundesstraße! Verrückte Idee oder ein bewusst hoch gestecktes Ziel aus Kalkül? Sicherlich beides! 
Ende Januar 2020 trafen sich Stadt-, Verkehrs- und Landschaftsplaner aus ganz Deutschland zu einer mehrtägigen Planungswerkstatt auf der Wilhlemsburg. Der Fokus lag dabei auf dem Bereich zwischen dem Ehinger und dem Blaubeurer Tor, der als der kniffligste Abschnitt gilt. Fantastereien waren nicht gefragt, Blicke über den Tellerrand hinaus aber sehr wohl. Ihre Vorschläge sollten ebenso ambitioniert sein wie tatsächlich umsetzbar.
Die drei interdisziplinär besetzten Teams präsentieren nach der Corona - Zwangspause nun Ihre ausgeheckten Ideen und Visionen und stellen diese zur Diskussion. OB Czisch und Baubürgermeister Tim von Winning werden diesen Abend moderieren.
