---
id: "2498907573754704"
title: Suchtpotenzial Wunschkonzert Vol. 3
start: 2020-04-24 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/2498907573754704/
image: 93721254_3035721079799751_8057054902913859584_n.jpg
teaser: Das bekloppte Wunschkonzert geht in die dritte Runde! In der Zeit des
  Veranstaltungsverbots halten wir uns mit digitaler Straßenmusik über Wasser.
  Wir
isCrawled: true
---
Das bekloppte Wunschkonzert geht in die dritte Runde! In der Zeit des Veranstaltungsverbots halten wir uns mit digitaler Straßenmusik über Wasser. Wir spielen aber nicht unsere eigenen Songs, sondern alles was man gerne von uns hören will. 
EGAL ob Metal, Schlager oder Musical. 
🔥
Wir spielen, ihr könnt dabei bin zuhause mitsingen, mittrinken oder auch uns ausbuhen. Is uns EGAL! 
Bei Gefallen darf man was in den digitalen Hut werfen via Paypal: Suchtpotenzial.music@gmail.com
Oder wer kein Paypal hat: https://secure.affilicon.net/shop?merchant=suchtpotenzial2020&productId=34470&language=de

‼️
Dieses Mal wieder aus der Filiale Süd! 
Das Roxy Ulm, mit dem besten Team, dem Masked Bringer und in der Suchti-Ball Halftime-Show The Müller Sisters, Teenager unter Hausarrest. 
 🎸 
Aufgrund vielfachen Wunsches jetzt am Freitag! 
Arbeitnehmer freundlich 
😆
Getränke gestiftet von Ficken Partyschnaps