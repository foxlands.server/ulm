---
id: "942968866189812"
title: Tim Vantol im ROXY Sound Garten
start: 2020-08-20 20:00
end: 2020-08-20 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/942968866189812/
teaser: Tim Vantols Stimme klingt immer all in. Sie klingt wie der Arm, der einen
  zurück über die Brüstung ins Fenster zieht, aus dem man gefallen ist. Wie de
isCrawled: true
---
Tim Vantols Stimme klingt immer all in. Sie klingt wie der Arm, der einen zurück über die Brüstung ins Fenster zieht, aus dem man gefallen ist. Wie der Rudelführer einer gutartigen Gang oder der letzte verantwortungsvolle Typ in einer verrückten Welt. Sie klingt, als ob der Sozialismus mit Leuten wie ihm funktionieren würde. Rau, aber eben nur so rau wie ein Wollpullover und nicht wie berstendes Schmirgelpapier. Vor allem klingt sie mitreißend – und das wiederum passt zur einer Musik, die nur den Vorwärtsgang kennt.

Der Biergarten ist bereits ab 15:00 H geöffnet.
Eintritt frei