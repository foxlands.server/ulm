---
id: "556905721774224"
title: Mono & Nikitaman * ROXY Ulm
start: 2020-05-30 20:00
end: 2020-05-30 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/556905721774224/
image: 79681571_10157048448787756_6201468240351199232_o.jpg
teaser: Nie so sein wie all die anderen  Mono & Nikitaman haben letztes Jahr ihr
  sechstes Studioalbum mit dem Titel “Guten Morgen es brennt” auf ihrem eigenen
isCrawled: true
---
Nie so sein wie all die anderen

Mono & Nikitaman haben letztes Jahr ihr sechstes Studioalbum mit dem Titel “Guten Morgen es brennt” auf ihrem eigenen Label M&N Records veröffentlicht.
Es stieg auf Platz #13 der deutschen Charts ein, wurde mit einer ausgiebigen Tour und auf knapp 30 Festivals gefeiert und schreit nun nach meeeehr!
Deshalb laden die beiden nun zur zweiten Tourrunde. Faust hoch für Sozialkritik und Mucke mit Inhalt zu der im Kollektiv eskaliert und im Off-Beat getanzt werden darf.
Das Publikum von M&N ist so bunt wie das Plakat, das diese Tour ankündigt. Zwischen Pogopit und Lichtermeer, Abriss und Upliftment ist alles dabei.