---
id: "714263956064254"
title: Atole Loco im ROXY Sound Garten
start: 2020-07-24 20:00
end: 2020-07-24 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/714263956064254/
image: 107372193_10157712757262756_3588727049000153662_o.jpg
teaser: Atole ist ein mexikanisches Heißgetränk aus Mais, das man in Lateinamerika in
  vielen verschiedenen Geschmacksrichtungen an jeder beliebigen Ecke genie
isCrawled: true
---
Atole ist ein mexikanisches Heißgetränk aus Mais, das man in Lateinamerika in vielen verschiedenen Geschmacksrichtungen an jeder beliebigen Ecke genießen kann. Genau diese bunte Vielfalt hat sich die Freiburger Band Atole Loco zum Programm gemacht: Atole Loco bedeutet tanzbare Cumbia, feuriger Ska und Latin Rock. Atole Loco heißt lateinamerikanische Rhythmen und grenzenloser Spaß auf der Bühne! Die Band mit internationalen Wurzeln (Mexiko, Kolumbien, Peru, Spanien und Deutschland) liefert ein explosives Programm, welches unweigerlich zum Tanzen einlädt.

Unser Biergarten hat ab 15:00 H geöffnet.
Eintritt frei.