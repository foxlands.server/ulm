---
id: "383482676153640"
title: Showbuddies . Corona-Duett * ROXY Ulm
start: 2020-11-13 20:00
end: 2020-11-13 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/383482676153640/
image: 122101572_10157997168712756_7510134497679616373_o.jpg
teaser: Die Showbuddies treten wieder an, um sich den Ideen des Abends zu stellen.
  Eure Fantasie ist der Regisseur der Show, der die Spieler*in einen spontane
isCrawled: true
---
Die Showbuddies treten wieder an, um sich den Ideen des Abends zu stellen. Eure Fantasie ist der Regisseur der Show, der die Spieler*in einen spontanen, theatralischen, witzigen und überraschenden Abend zaubern lässt. Ob Western, Drama, Horror, Heimatfilm oder Krimi. Poesie, Rock’n'Roll oder Shakespeare: Interaktives Theater in voller Breite. Im Moment für den Moment.

***
Aufgrund von Corona wird diese Showbuddiesausgabe ein 2-Personen-Impro-Theater.


www.showbuddies.de