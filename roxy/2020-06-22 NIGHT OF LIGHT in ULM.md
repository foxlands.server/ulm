---
id: "320460925622087"
title: NIGHT OF LIGHT in ULM
start: 2020-06-22 22:00
end: 2020-06-22 23:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/320460925622087/
image: 104290747_1418753551669225_6542859199755383082_o.jpg
teaser: Am kommenden Montag, 22.06.2020 lassen wir von 22 - 23 Uhr das Ulmer Münster
  und den Münsterplatz rot erleuchten. Wir, das sind die Unternehmen der Ul
isCrawled: true
---
Am kommenden Montag, 22.06.2020 lassen wir von 22 - 23 Uhr das Ulmer Münster und den Münsterplatz rot erleuchten. Wir, das sind die Unternehmen der Ulmer Veranstaltungsbranche. Wir freuen uns auf euer Kommen. #kulturerhalten #nightoflight #bettertogether