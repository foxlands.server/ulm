---
id: "598295940780425"
title: Jaya The Cat * ROXY Ulm
start: 2020-12-18 20:00
end: 2020-12-18 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/598295940780425/
image: 94394256_10157461356132756_4806410597153374208_o.jpg
teaser: Das Doppelkonzert von Jaya The Cat und The Magic Mumble Jumble vom 30.04.2020
  wird auf zwei Termine gesplittet.  Die Tickets behalten ihre Gültigkeit
isCrawled: true
---
Das Doppelkonzert von Jaya The Cat und The Magic Mumble Jumble vom 30.04.2020 wird auf zwei Termine gesplittet.

Die Tickets behalten ihre Gültigkeit für das Konzert von Jaya The Cat am 18.12.2020. Natürlich können sie auch an den jeweiligen VVK-Stellen zurückgegeben werden.

The Magic Mumble Jumble werden im kommenden Jahr am 30.04.2021 mit einer noch nicht bekannten Band zusammen auftreten. Hierfür sind Tickets demnächst separat erhältlich.
***
Roots-Reggae, Ska, Punkrock: Die drei Hauptzutaten des Jaya The Cat-Cocktails rufen normalerweise eher Bilder von sonnigen Stränden, der coolen Lockerheit Kaliforniens oder zumindest dem schwülen Dunst Floridas vor das geistige Auge. Umso erstaunlicher, dass die Band um Mastermind und Frontmann Geoff Lagadec und Schlagzeuger David Germain ihren Ursprung im klimatisch eher milden Boston hat. Dass Jaya The Cat sich hingegen 2003 die europäische Metropole Amsterdam als neues Hauptquartier aussuchen und ihre cremige Sound Mische von dort aus auf bislang vier Alben an das Partyvolk bringen, passt so gut zusammen wie Piña Colada und karibische Sonne. Dabei sollte man allerdings nicht meinen, dass sich die Band allzu stark von dem zurückgelehnten Flair der holländischen Hauptstadt hat inspirieren lassen. Denn wer eine vergleichbar explosive, wandlungsfähige und dynamische Live Band aus dem Skapunk-Sektor sucht, sollte eine Lupe im Gepäck haben – hunderte von Shows zwischen ausverkauften Headliner-Touren, Support-Slots für Bands wie die Beatsteaks, Less Than Jake oder Sublime und prestigeträchtigen Festivalauftritten auf dem Ruhrpott Rodeo, dem Pukkelpop und dem Lowlands sind Beweis genug für die musikalische Feuerkraft des Quartetts.