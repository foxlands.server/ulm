---
id: "202991914363491"
title: Culcha Candela /// top ten tour /// Ulm /// ROXY
start: 2020-10-13 20:00
end: 2020-10-13 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/202991914363491/
image: 91649180_143121417212022_1656587882707550208_o.jpg
teaser: "Culcha Candela 13.10.2020  Ulm /// ROXY Einlass: 19:00 /// Beginn: 20:00
  Präsentiert von event.Magazin /// kulturnews /// MTV Germany  ///  2020 ist f"
isCrawled: true
---
Culcha Candela
13.10.2020

Ulm /// ROXY
Einlass: 19:00 /// Beginn: 20:00
Präsentiert von event.Magazin /// kulturnews /// MTV Germany

///

2020 ist für Culcha Candela ein besonderes Jahr, die Berliner Band bringt nicht nur ein weiteres Album raus, sondern spielt im Herbst gleich eine zweite Tournee. Nach einer komplett ausverkauften BESTESTE-Tour im Frühjahr ist klar wohin die Reise geht: Über die Hüften und Ohren der Fans direkt in die Charts. TOP TEN soll das neue Album von Culcha Candela heißen, das im Herbst erwartet wird - und der Titel hält, was er verspricht: 10 definitive Top Ten Hits, die kein Bein am Boden lassen. Die erste Single „Rhythmus wie ein Tänzer“ ist eine Kooperation mit Dancefloor-Ikone DJ Antoine. Den magischen Refrain singt Julie Brown, die als Backgroundsängerin der Kombo schon seit Jahren live mit der Band unterwegs ist. In 18 Jahren hat die Band so viele Hits rausgeballert, dass jedes Konzert zu einer kollektiven Feier des Culcha-Sounds wird. Nicht nur Klasskiker wie „Hamma!“, „Monsta“ oder „Berlin City Girl“, sondern neue Tracks wie „No Tengo Problema“ sind Garanten für eine ausgelassene Eskalation.