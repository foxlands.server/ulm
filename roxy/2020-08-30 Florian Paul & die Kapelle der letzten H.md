---
id: "2676223839311694"
title: Florian Paul & die Kapelle der letzten Hoffnung im ROXY
start: 2020-08-30 19:00
end: 2020-08-30 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2676223839311694/
teaser: Florian Paul ist ein Geschichtenerzähler. Er erzählt vom Leben, von der Liebe,
  von der Einsamkeit, von großen Themen in kleinen Ereignissen, mit rauch
isCrawled: true
---
Florian Paul ist ein Geschichtenerzähler. Er erzählt vom Leben, von der Liebe, von der Einsamkeit, von großen Themen in kleinen Ereignissen, mit rauchiger Stimme und großen Gesten, wie ein alter Trinker der vom Tresen aus versucht die Welt zu erklären. Dabei ist er eigentlich erst 24. Nachdem dem Abitur 2013 arbeitete er 2 Jahre als Schauspieler und musikalischer Leiter bei dem renommierten Off- Theaterprojekt „TheaterTotal“ in Bochum. 2015 zog er für sein Filmmusikstudium nach München und etablierte sich dort als Komponist für zahlreiche Theater- und Filmprojekte, darunter auch der Kinofilm „Unheimlich perfekte Freunde“ des bayrischen Kultregiesseurs Marcus H. Rosenmüller.
Rund um die Münchner Musikhochschule und seinen Professor Gerd Baumann lernte er Flurin Mück (Drums) und Nils Wrasse (Saxophon) kennen. Später stießen dann noch der Pianist Giuliano Loli und der Bassist Robin Jermer dazu und die „Kapelle der letzten Hoffnung“ ist seitdem komplett. Loli und Wrasse sind ebenfalls nicht nur Musiker, sondern auch Filmkomponisten und alle vier sind Mitglieder weiterer namhafter Bands und Projekte in- und außerhalb von München, wie unter anderem bei Hannah Weiss, Dreiviertelblut oder Ark Noir. Gemeinsam halfen sie Florian Paul dem einsamen Liedermacherdasein zu entkommen, indem sie seinen Songs einen kraftvollen und einprägsamen Sound verliehen.

Der Biergarten öffnet bereits um 15:00 H
Eintritt frei