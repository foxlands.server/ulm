---
id: "2528354247384895"
title: Dave Hause and the Mermaid – Ulm, Roxy
start: 2020-05-22 21:00
end: 2020-05-22 23:30
address: ROXY.ulm
link: https://www.facebook.com/events/2528354247384895/
image: 89772702_10151322111924996_7362190603863457792_n.jpg
teaser: Anfang des Jahres war Dave Hause hierzulande zu Gast und hat Fans auf seiner
  Akustik-Tour verzaubert. Punk ist noch lange nicht tot, er klingt nur and
isCrawled: true
---
Anfang des Jahres war Dave Hause hierzulande zu Gast und hat Fans auf seiner Akustik-Tour verzaubert. Punk ist noch lange nicht tot, er klingt nur anders und zeigt massig Gefühle. Der Mann kann einfach in allen Sätteln und im Notfall auch ohne reiten. Und natürlich dürfen wir an dieser Stelle keinesfalls vergessen, dass seine Band, The Mermaid, eine wundervolle Möglichkeit bietet, Dave Hause’ Musik live zu genießen. Und gerade, weil Dave Hause so ein unermüdlicher Live-Musiker ist, hat er jetzt bestätigt, dass er im Mai für zwei weitere Auftritte zusammen mit The Mermaid nach Ulm und Erfurt kommt. 

Weiterlesen >> fkpscorpio.com/de/bands-archiv/dave-hause-and-the-mermaid 