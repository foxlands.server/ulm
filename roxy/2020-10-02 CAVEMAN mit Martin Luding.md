---
id: "1081734622164666"
title: CAVEMAN mit Martin Luding
start: 2020-10-02 20:00
end: 2020-10-02 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/1081734622164666/
image: 81289993_10157629647694088_8025666298367180800_n.jpg
teaser: "Die Veranstaltung findet wie geplant statt. Bitte beachtet: Unsere Bestuhlung
  ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer G"
isCrawled: true
---
Die Veranstaltung findet wie geplant statt. Bitte beachtet: Unsere Bestuhlung ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer Gruppengröße geeignete Sitzgruppen, ggf. muss sich eure Gruppe für die Zeit der Vorstellung trennen. Stühle dürfen nicht verschoben werden.
Tragt eure eigene Mund-Nasen-Bedeckung, die ihr auf eurem Sitzplatz abnehmen könnt. Wir werden eure Kontaktdaten aufnehmen (Telefonnummer oder Adresse), für vier Wochen verwahren und nur im Falle eines Coronafalls an das Gesundheitsamt aushändigen.
Danke für Geduld und Rücksicht auf andere Gäste.
***
Zweihunderttausend Jahre – und nochmal ein paar obendrauf: Schon seit Anbeginn der Menschheit versuchen Mann und Frau leidenschaftlich, den gemeinsamen Alltag zu meistern. Oder sogar eine Partnerschaft zu führen. 

Doch erst im aktuellen Jahrtausend liefert CAVEMAN den charmanten Beweis, dass sich die grundlegenden Herausforderungen einer Beziehung auch in Zeiten der 3-Zimmer-Eigentumshöhle und der Jagd auf Smartphones nicht wesentlich geändert haben. 

Eine beziehungsreiche Erkenntnis, die dank des sympathischen Helden Tom jedoch für pures Vergnügen sorgt. Mit ihm gemeinsam erkundet das Publikum die ungleichen Territorien der beiden Spezies. Auf der einen Seite jene klar überschaubare Welt von scharf schießenden Fernbedienungen und rollenden Revieren samt ferngesteuerter Zentralverriegelung. Auf der anderen jenes außerordentlich komplexe Universum aus besten Freundinnen, ungebremster Vorstellungskraft und Körben voller Informationen. So lernen wir etwa, warum ER Fernsehen als Arbeit bewertet wissen will und SIE ein sauberes Badezimmer wünscht, auch ohne eine Operation an der offenen Lunge zu planen. Verliebt, verlobt, verdammt witzig – CAVEMAN wirft eben einen genauso kurzweiligen wie treffenden Blick ins Innere einer jeden Beziehung. 

Bereits die Vorlage von Rob Becker avancierte zum erfolgreichsten Solo-Stück in der Geschichte des Broadway. Und auch hierzulande ist CAVEMAN in der Inszenierung von Esther Schweins und der Übersetzung von Kristian Bader seit dem Jahr 2000 ein Bühnen-Dauerbrenner. Dank seinem pointierten Einfallsreichtum und der liebevollen Aufarbeitung tiefer Wahrheiten nebst amüsanter Klischees ist CAVEMAN mehr denn je ein Muss für alle, die eine Beziehung führen, führten oder führen wollen. Und macht aus der „Mammut-Aufgabe Beziehung“ einen unvergesslich unterhaltsamen Abend.
