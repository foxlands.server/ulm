---
id: "1186500501707544"
title: Shokee & Sands im ROXY Sound Garten
start: 2020-08-27 19:00
end: 2020-08-27 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/1186500501707544/
teaser: Die Kultband Shokee & Sands besteht aus fünf Musikern mit langjähriger Bühnen-
  und Banderfahrung, die in dieser Besetzung zusammenkommen, um die Liebl
isCrawled: true
---
Die Kultband Shokee & Sands besteht aus fünf Musikern mit langjähriger Bühnen- und Banderfahrung, die in dieser Besetzung zusammenkommen, um die Lieblingssongs des Publikums, spontan, improvisiert, mit Herzblut, Leidenschaft und hohem musikalischem Können, auf die Bühne zu bringen. Bei lecker Bierchen im Biergarten können die Zuhörer wieder ihre Wunschsongs notieren, welche die Band anschließend 100% LIVE performen wird! Mal witzig interpretiert, mal mit Parodie, mal theatralisch, mal mit "waghalsigen" Gitarrensoli … maximalst unterhaltsam, authentisch und souverän gespielt, mit Gesangsstimmen, die unter die Haut gehen! Und man weiß nicht welche, Überraschungen an solch einem Abend sonst noch so passieren …

Der Biergarten öffnet bereits um 15:00 H
Eintritt frei