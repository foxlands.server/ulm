---
id: "308846560453187"
title: ROXY Sound Garten mit Variante:Unbekannte
start: 2020-08-13 17:00
end: 2020-08-13 23:25
address: ROXY.ulm
link: https://www.facebook.com/events/308846560453187/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00 H): Musik, abseits der festgetretenen Mainstream-Pfade. Die Variante bleibt eine Unbekannte und das zu Recht: Nur viele Musikfarben ergeben den kompletten Malkasten für offene Gedanken und Inspirationen.
Live zu hören bei Radio free FM, mittwochs von 18 bis 20 Uhr und heute bei uns zu Gast im Sound Garten, ab 17 Uhr.

Eine Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten öffnet um 15:00 H
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de