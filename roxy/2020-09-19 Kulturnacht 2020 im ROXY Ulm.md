---
id: "4369186546455926"
title: Kulturnacht 2020 im ROXY Ulm
start: 2020-09-19 15:00
end: 2020-09-19 23:55
address: ROXY.ulm
link: https://www.facebook.com/events/4369186546455926/
teaser: Wir freuen uns, euch auch an der Kulturnacht 2020 ein abwechslungsreiches
  Programm anbieten zu können!  **15:00 H**  Öffnung Biergarten  ROXY Lockdown
isCrawled: true
---
Wir freuen uns, euch auch an der Kulturnacht 2020 ein abwechslungsreiches Programm anbieten zu können!

**15:00 H**

Öffnung Biergarten

ROXY Lockdown Sessions – Die Foto-Ausstellung
Ausstellung mit Fotos von Diana Mühlberger und Rob Stirner, die während der Arbeiten der Lockdown Sessions entstanden sind.


**16:00 H / 18:00 H**

Schwarzlicht-Yoga mit Yvonne Graf
Schminken je 30 min früher, bitte eigene Matte mitbringen. In angenehm gedimmtem Licht mit Schwarzlichtflutern werden wir einen sanften Vinyasa-Flow entstehen lassen. Um im Dunkeln richtig zur Geltung zu kommen, könnt ihr euch vor der Stunde mit professioneller Farbe Füße, Arme, Hände und Gesicht bemalen. Glow with the Flow und strahlt mit uns. Das Schwarzlicht- Yoga ist für alle Levels geeignet.
Neon-Schminke könnt ihr für für 2,50 € pro Stift direkt vor Ort erwerben! Bitte bringt euren eigenen Pinsel mit, um euch anzumalen.

Um Anmeldung wird gebeten: anmeldung@roxy.ulm.de


**17:00 H / 19:00 H / 22:00 H**

ROXY Hinter den Kulissen
Führungen durchs Haus mit interessanten Infos und Hintergründen

Um Anmeldung wird gebeten: anmeldung@roxy.ulm.de


**19:00 H / 21:00 H / 23:00 H**

ROXY Lockdown Sessions – Die Doku-Premiere
Mit den ROXY Lockdown Sessions wurde ein Projekt realisiert, das diese außergewöhnliche Corona-Phase nutzt, um ein gemeinschaftliches, facettenreiches Zeitdokument zu schaffen. Kooperation über Genre-Grenzen: Die live aufgenommenen Musikstücke dienen als Grundlage für ein künstlerisches Musikvideo mit dem Schwerpunkt Tanz. Bei der Kulturnacht gibt es die Premiere der entstandenen Doku über das Projekt inklusive der Präsentation der 4 Musik-Tanz-Videos zu sehen. (In Kooperation mit dem Obscura.)


**21:00 H**

Konzert im ROXY Sound Garten
Live-Konzert mit mitwirkenden Künstlern der Lockdown Sessions: Lemony Rug, Luke Noa, James Geier + special guests


***
Ab dem 09.09. können Eintrittsbänder für die Kulturnacht direkt beim ROXY erworben werden:
TicketService: mittwochs, 17:00-19:00 H
Während der Biergartenöffnungszeiten am Biergarten-Einlass

10,— / 8,— ermäßigt, Kinder bis einschließlich 12 frei