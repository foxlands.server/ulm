---
id: "2746595958698154"
title: Cavewoman * ROXY Ulm
start: 2021-02-10 20:00
end: 2021-02-10 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2746595958698154/
image: 71511646_10156839418837756_3159197335267835904_n.jpg
teaser: Sex, Lügen und Lippenstifte – CAVEWOMAN Heike nutzt die letzten Stunden vor
  der Trauung, um den peinlicherweise zu früh erschienenen Hochzeitsgästen n
isCrawled: true
---
Sex, Lügen und Lippenstifte – CAVEWOMAN Heike nutzt die letzten Stunden vor der Trauung, um den peinlicherweise zu früh erschienenen Hochzeitsgästen noch einmal einen Schnellkurs in Sachen Mann-Frau-Beziehung zu geben. Und das mit einer gehörigen Portion Wut im Bauch. Oder was würden Sie sagen, wenn Ihr Mann einen Abend vor der Hochzeit verschwindet, nur weil Sie „Hau ab!“ zu ihm gesagt haben?! Doch keine Sorge: CAVEWOMAN ist kein feministischer Großangriff auf die gemeine Spezies Mann – vielmehr ein vergnüglicher Blick auf das (Zusammen-) Leben zweier unterschiedlicher Wesen, die sich einen Planeten, ein Land, eine Stadt und das Schlimmste: EINE WOHNUNG teilen müssen!

Heike, die Zukünftige von Tom, rechnet in dieser fulminanten Solo-Show mit den selbsternannten „Herren der Schöpfung“ ab. Mal mit der groben Steinzeit-Keule, mal mit den spitzen, perfekt gepflegten Nägeln einer modernen Höhlenfrau – aber immer treffend und saukomisch! Ein Theaterabend, der Sie zum Staunen und vor allem zum Lachen bringen wird, denn eigentlich haben wir es ja schon immer gewusst: Wenn Männer so gute Liebhaber wären, wie sie denken, hätten Frauen gar keine Zeit, sich die Haare zu machen!
***
Die Veranstaltung wurde vom 18.04. auf den 24.10.2020 verschoben. Tickets behalten ihre Gültigkeit.