---
id: "194964215067760"
title: Fly High & Scheiter Heiter - Teatro International * ROXY Ulm
start: 2020-09-23 19:30
end: 2020-09-23 21:30
address: ROXY.ulm
link: https://www.facebook.com/events/194964215067760/
image: 89781456_10157302107172756_7598281537330610176_o.jpg
teaser: Verschoben vom 17.05.2020, 16:00 H. *** Jedem Scheitern wohnt ein Träumen inne
  … Migranten sind Experten des Träumens und Scheiterns. Sie brechen auf
isCrawled: true
---
Verschoben vom 17.05.2020, 16:00 H.
***
Jedem Scheitern wohnt ein Träumen inne … Migranten sind Experten des Träumens und Scheiterns. Sie brechen auf mit ihren Zielen und Visionen, stoßen an Grenzen, überwinden sie oder suchen neue Wege und bringen durch ihren Mut und ihre Kreativität bereichernde Perspektiven und Ideen in die Gesellschaft. 
Ausgehend von der Figur Berblingers und seines Traums vom Fliegen beschäftigt sich Teatro International in seinem neuen Stück mit dem Scheitern auf der Probebühne des Lebens. Im Vorfeld des Theaterprojekts hat Teatro International dafür „Scheiter-Geschichten“ von Menschen allen Alters und unterschiedlicher sozialer und kultureller Herkunft gesammelt. Diese werden Teil eines Geschichten-Parcours am 15. und 30. Mai entlang der Donau im Rahmen des Berblinger-Jubiläums. 

Regie: Claudia Schoeppl
Mitwirkende: Ensemble des Teatro International
Im Rahmen des Berblinger-Jubiläums der Stadt Ulm

Kooperationspartner: 
Ulmer Volkshochschule
Buchhandlung Aegis
Kulturabteilung Stadt Ulm
Landesamateurtheaterverband Baden-Württemberg