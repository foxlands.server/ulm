---
id: "299885367916030"
title: ROXY Sound Garten mit Antistatik
start: 2020-08-19 17:00
end: 2020-08-20 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/299885367916030/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00): Antistatik
"Bewegliche Ziele, bewegliche Klänge", so lautet das Motto der wöchentlichen Radiosendung der Herren Edel und Fritz. Und so lautet auch das Motto des heutigen Abends. Gepflegte Unterhaltung für den launigen Sommerfeierabend aus 6 Jahrzehnten und von 7 Kontinenten. Diesmal nicht bei Radio Free FM, sondern im ROXY Biergarten. 

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de