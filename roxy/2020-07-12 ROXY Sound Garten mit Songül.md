---
id: "641866469737653"
title: ROXY Sound Garten mit Songül
start: 2020-07-12 16:00
end: 2020-07-12 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/641866469737653/
image: 106740183_10157703288897756_2591304808680215973_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 16:00 H): Songül
Sie spielt, was ihr gefällt: jazzig, surfig, soulig, african sound und vieles mehr.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de