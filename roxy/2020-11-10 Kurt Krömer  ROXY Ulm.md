---
id: "395623467802753"
title: Kurt Krömer * ROXY Ulm
start: 2020-11-10 20:00
end: 2020-11-10 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/395623467802753/
teaser: Auch im Jahr 2020 begibt sich Kurt Krömer mit seinem aktuellen Programm
  Deutschlandweit bei seinen Auftritten wieder in eine Stresssituation.  Wohnhaf
isCrawled: true
---
Auch im Jahr 2020 begibt sich Kurt Krömer mit seinem aktuellen Programm Deutschlandweit bei seinen Auftritten wieder in eine Stresssituation.

Wohnhaft in Neukölln, zu Hause auf der Bühne: Kurt Krömer ist ein schrulliger, gnadenloser
Kabarettist mit eigenwilligem Modebewusstsein und Berliner Schnauze: ein Punk im Körper eines Sparkassenangestellten.

Stresssituation ist das mittlerweile sechste Stand-up-Comedy Programm, mit dem Krömer seit Anfang 2018 überaus erfolgreich durch Deutschland reist. Eine garantierte Stresssituation für Tränensäcke, Lachmuskeln und ein tatsächlich ungewohnt provokantes Programm, mit dem Krömer selten ein gutes Haar an Politik, Gesellschaft und dem anwesenden Publikum lässt. Hier kommt keiner ungeschoren davon: weder Omas Couchtisch noch Krömer selbst.

http://www.kurtkroemer.de/