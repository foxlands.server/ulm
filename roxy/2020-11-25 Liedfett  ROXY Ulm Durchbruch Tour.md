---
id: "2431840370364320"
title: Liedfett * ROXY Ulm (wird verschoben) * Durchbruch Tour
start: 2020-11-25 20:00
end: 2020-11-25 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2431840370364320/
image: 75402300_10156957302897756_9062967622668124160_o.jpg
teaser: "Goldene Zeiten dank offener Weiten: Die hanseatischen Hallodris von Liedfett
  starten den Durchbruch. Zäune um Seelen werden umgemäht, Mauerwerke vor H"
isCrawled: true
---
Goldene Zeiten dank offener Weiten: Die hanseatischen Hallodris von Liedfett starten den Durchbruch. Zäune um Seelen werden umgemäht, Mauerwerke vor Herzen ausgehebelt. Der Weg nach oben führt durch die Deepness; Lieder der Bewegung voller bestätigter Hoffnung. Soulfood für Ohren. Ausverkaufte Hallen und Charterfolge straight outta Underground, folgt nun der nächste Step. Ihr neues Album steht bereits in den Startlöchern und kanns kaum erwarten. Adrenalin, Hochspannung, Durchbruch. Bald geht’s los. Allerorts ungeduldige Vorfreude, doch Liedfett zählen just den Countdown ein und touren sich mit kraftvollen Esopunk-Hymnen himmelwärts. Ihre „Durchbruch“-Tour beginnt. Die vier Querköpfe stürmen die Bühnen und Melodien strömen in die Atmosphäre. Leuchtende Augen euphorischer Menschen unterstützen ihre neueste These: „Geilgeilgeil“! Live ist Laufenlassen. Zueinander und nach oben. Musik ist ein Lachs auf dem Weg flussaufwärts. Er funkelt, dank Liedfett.

***
Das Konzert wurde vom 25.03. auf den 25.11. verschoben. Tickets behalten ihre Gültigkeit.