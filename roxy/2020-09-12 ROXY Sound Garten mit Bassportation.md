---
id: "650344852267655"
title: ROXY Sound Garten mit Bassportation
start: 2020-09-12 18:00
end: 2020-09-12 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/650344852267655/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00 H): Bassportation
Sommerlich entspannter Sound aus der Techhouse / Techno / Drum'n'Bass Kiste ;) Draußen statt im Gemäuer - wir freuen uns!

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 17:00 H geöffnet.
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de