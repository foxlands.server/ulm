---
id: "298873104691736"
title: ROXY Sound Garten mit deleted user und reminder
start: 2020-08-06 15:00
end: 2020-08-06 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/298873104691736/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 15:00): deleted user und reminder
reminder und deleted user aka un:cut kennt man u.a. von der subt0ne (dubstep) oder aus dem 2005er bis 2012er Drum'n'Bass-Geschehen in Ulm. Da die beiden noch ganz andere Richtungen hören und auflegen, könnt ihr gespannt sein, was an diesem Tag so musikalisch geboten ist. Es kann auch gut sein, dass sie die alten Drum'n'Bass- oder Dubstep-Vinyls auspacken. Better be prepared!

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de