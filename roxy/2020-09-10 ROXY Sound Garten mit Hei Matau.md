---
id: "666058170688634"
title: ROXY Sound Garten mit Hei Matau
start: 2020-09-10 18:30
end: 2020-09-10 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/666058170688634/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00 H): From Friends For Friends mit Hei Matau

House & Deep House Musik
100% Ulmer Wohlfühlmusik, 100% Herzenssache.

Eine Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten öffnet um 17:00 H
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de