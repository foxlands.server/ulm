---
id: "273509200749376"
title: "Summer in the city: Teresa Bergman im ROXY Sound Garten"
start: 2020-08-01 20:00
end: 2020-08-01 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/273509200749376/
image: 107090383_10157712835782756_4929227413751201002_o.jpg
teaser: „Phantastische Stimme, phantastische Songs. Einfach so. Bläst sie einen um.“
  (FAZ) - Teresa Bergman, die Sängerin aus Neuseeland und Wahlberlinerin sc
isCrawled: true
---
„Phantastische Stimme, phantastische Songs. Einfach so. Bläst sie einen um.“ (FAZ) - Teresa Bergman, die Sängerin aus Neuseeland und Wahlberlinerin schlägt mit ihrer Band musikalische Brücken zwischen Folk, Funk und Jazz. Ihr typischer Bergman-Groove, ihr Mut zu stilistischen Kontrasten sowie ihr Bühnen-Mix aus Spaß und Intimität machen Teresas Konzerte zu magischen wie mitreißenden Erlebnissen.

Eintritt frei.
Der Biergarten ist ab 15:00 H geöffnet.

***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.

Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.