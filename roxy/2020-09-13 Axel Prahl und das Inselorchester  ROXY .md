---
id: "1054044788285876"
title: Axel Prahl und das Inselorchester * ROXY Ulm
start: 2020-09-13 19:00
end: 2020-09-13 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/1054044788285876/
image: 87307455_10157248746892756_4753787773079519232_o.jpg
teaser: MEHR — Das Konzert zum neuen Album  Vieles, was die Medien an ihm schätzen,
  aber noch mehr, was das Publikum so an ihm liebt, findet man in seinen Lie
isCrawled: true
---
MEHR — Das Konzert zum neuen Album

Vieles, was die Medien an ihm schätzen, aber noch mehr, was das Publikum so an ihm liebt, findet man in seinen Liedern wieder. Prahl singt Prahl – authentisch, bodenständig, erdig, mit Witz und Lust am Musizieren.   

Für viele war seine Debüt-Album „Blick aufs Mehr“ die Überraschung, für nicht wenige die neue Lieblingsscheibe. Im November 2018 erscheint sein, mit viel Vorfreude erwartetes, zweites Studioalbum „MEHR“. Dazu und natürlich darüber hinaus gibt es – endlich wieder Konzerte.   

Mit Axel Prahl betritt kein singender Schauspieler, sondern ein wunderbarer Musiker und Sänger die deutschen (Musik-)Bühnen mit Songs, die aus der eigenen Feder und dem eigenen Erleben entsprungen sind.  

Seine keineswegs nebenbei gefeierte Band ist eine kleine, handverlesene Truppe von neun Musikern, die in die deutsche Rock-, Jazz- und Klassikszene klangvolle Namen einzubringen haben. Allen voran Danny Dziuk, der mit Songs und Songtexten das Ansehen von Annett Louisan oder jenes von Stoppok befördert hat.

Der Musiker Prahl räsoniert und randaliert, säuselt und seufzt, ist bissig bis blauäugig brav, rührt und verführt. Vor allem aber ist Axel Prahl als Musiker ganz er selbst, sprich „in der Rolle seines Lebens“ wie „ZDF-Aspekte“ etwas genüsslich anmerkte. 

Axel Prahl (voc, Gitarren)
Danny Dziuk (Keys, back-voc, Arrangements und Musikalische Leitung)
Sylvia Eulitz (Cello), Christiane Silber (Viola, back-voc) Rainer Korf (Violine), Nicolai Ziel (Drums, perc.), Johannes Feige (Gitarren, back-voc), Tom Baumgarte (Bässe, back-voc) Tom Keller (Sax, Klarinette, Flöte), Jörg Mischke (Keys, Akk, backvoc)