---
id: "2748096265446171"
title: ROXY Sound Garten mit Schüttel dein Speck
start: 2020-08-29 17:00
end: 2020-08-29 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/2748096265446171/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00): Schüttel dein Speck - mit Abstand
Select.A.Tom und Deejot Roterfreibeuter können nicht länger an sich halten und fordern euch heraus. Unsere Party-Lieblinge, die euch und uns monatlich mit ihrem vogelwilden Musikmix die Schuhe zertanzt haben, spielen für euch auf. Leider herrscht aktuell Tanzverbot. Zumindest Tanzen im Stehen ist nicht möglich. Doch im Sitzen sind keine Grenzen gesetzt. Wer ist der wildeste Kopfnicker? Wer kann Armspeck am groovigsten schütteln? Wir freuen uns auf das schicke Duo, auf euch und eure Oberkörper-Bodywaves.

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 15:00 H geöffnet.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de