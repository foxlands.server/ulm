---
id: "3043567989011076"
title: Christian Steiffen • Roxy, Ulm • 13.05.20
start: 2020-05-13 20:00
end: 2020-05-13 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/3043567989011076/
image: 86348745_10157004643837361_4243164862219288576_o.jpg
teaser: Christian Steiffen GOTT OF SCHLAGER TOUR 2020  Einlass 19:00 I Beginn 20:00
  Eintritt VVK ab 32,95€   CHRISTIAN STEIFFEN - DER „GOTT OF SCHLAGER“ SETZT
isCrawled: true
---
Christian Steiffen
GOTT OF SCHLAGER TOUR 2020

Einlass 19:00 I Beginn 20:00
Eintritt VVK ab 32,95€ 

CHRISTIAN STEIFFEN - DER „GOTT OF SCHLAGER“ SETZT SEINE TOUR DIESES JAHR FORT!

Die Gelegenheit die Legende aus Osnabrück mit seinem dritten Meisterwerk auf der Bühne live zu erleben, solltet ihr euch nicht entgehen lassen – denn wo der Steiffen ist, da ist die Party!

Nüchtern und sachlich, wie immer gewohnt reflektiert. „Gott of Schlager“ - so der Titel des dritten Ergusses - ist ein weiterer Höhepunkt im Steiffen-Oeuvre. Schon in der Wahl des Albumtitels greift Christian auf das von ihm perfekt inszenierte und zelebrierte Stilmittel der Untertreibung zurück und fügt hinzu: „Worte können mich und dieses Album nicht beschreiben. 

Alles dargebracht im wunderbar weichen Timbre von Christians Stimme, das wie immer perfekt korreliert mit den Harmonien und Arrangements seines Freundes und Mitproduzenten Dr. Martin Haseland. Das sind neue große Melodien eines sich stetig selbstübertreffenden Poeten und Entertainers. Die Christianisierung hat wohl noch lange nicht ihren Höhepunkt erreicht, aber dass der Steiffen immer grösser wird, stört den Steiffen nicht: „Ich zwinge ja niemanden, zu meinen Konzerten zu kommen – die kommen alle freiwillig. (…)“. Und so wird er auch mit diesem Album wieder auf große Fahrt gehen.

Mehr Infos und Tickets gibt es auf www.neuland-concerts.com. 