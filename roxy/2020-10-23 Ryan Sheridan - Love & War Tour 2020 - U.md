---
id: "1360024477533572"
title: Ryan Sheridan - Love & War Tour 2020 - Ulm
start: 2020-10-23 19:00
end: 2020-10-23 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1360024477533572/
image: 79755934_2699869666725592_602806296445452288_n.jpg
teaser: Ryan Sheridan, alongside drummer and percussionist Ronan Nolan, bring an
  explosion of energy to the stage. With their dynamic performances, their onst
isCrawled: true
---
Ryan Sheridan, alongside drummer and percussionist Ronan Nolan, bring an explosion of energy to the stage. With their dynamic performances, their onstage connection, singalong melodies and crowd interaction, you certainly do not want to miss this ticket.