---
id: "3223744087850241"
title: KYTES + Special Guest * ROXY Ulm
start: 2020-12-19 21:00
end: 2020-12-19 23:59
address: ROXY.ulm
link: https://www.facebook.com/events/3223744087850241/
image: 101093541_10157577253772756_6266035699187712000_o.jpg
teaser: Die Geschichte von Michael Spieler, Timothy Lush, Kerim Öke und Thomas
  Sedlacek dreht und windet sich bereits seit 2012. Seit der Schülerband machen
  s
isCrawled: true
---
Die Geschichte von Michael Spieler, Timothy Lush, Kerim Öke und Thomas Sedlacek dreht und windet sich bereits seit 2012.
Seit der Schülerband machen sie in verschiedenen Konstelationen zusammen Musik, in dieser Zeit haben die Münchner viel an ihrem Sound und Songwriting gearbeitet und 2015 die ON THE RUN EP unter dem Milky Chance Label Lichtidicht Records released. Es folgte die erste Tour durch die Republik und eine große Festival Runde mit Stationen beim SXSW 2016 in Austin / Texas, Melt! Festival und MS Dockville. Das Debüt Album HEADS AND TALES, im September 2016 veröffentlicht, landete auf #14 der iTunes Alternative Charts, die Songs wurden bis dato mehr als 7 Millionen mal auf Spotify gestreamt.
Kytes begannen im Sommer 2017 die Arbeiten am Nachfolgealbum zusammen mit dem Wiener Produzenten Filous, parallel dazu spielten sie auf weiteren europäischen Festivals u.a. The Great Escape (UK), Sziget (HUN) und FM4 Frequency (AU). Im April diesen Jahres headlineten Kytes die egoFM Feste und eröffneten quer durch Europa den Abend für die Indie-Heros Shout Out Louds und Everything Everything.