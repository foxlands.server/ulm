---
id: "269755700957744"
title: "ROXY Live - Die Late Night Show #2"
start: 2020-07-13 21:00
end: 2020-07-13 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/269755700957744/
image: 106531315_10157698005097756_7997786374743190670_o.jpg
teaser: "Selbstoptimierung - Leben für Profis  Wer sich nichtsahnend durchs Internet
  bewegt wird schnell merken: Leben, das macht man verdammt schnell falsch."
isCrawled: true
---
Selbstoptimierung - Leben für Profis

Wer sich nichtsahnend durchs Internet bewegt wird schnell merken: Leben, das macht man verdammt schnell falsch. Wir nutzen unsere Zeit nicht gut, priorisieren falsch, sind zu dick, zu dünn, lassen uns unter Wert bezahlen und essen immer zu viel oder zu wenig von irgendwas. Und dass es tausendundeinen Life Hack für schnelleres Schuhebinden gibt, zeigt unmissverständlich, dass ohne Ratgeberliteratur und Tutorials eigentlich kaum ein fehlerfreier Handgriff möglich ist. 

In der zweiten Ausgabe von ROXY Live widmen sich Andreas Rebholz und Niklas Ehrentreich dem ewigen Streben nach Verbesserung in allen Lebensbereichen, von Arbeitsmarkt bis Zoom-Date: Geht da wirklich mehr? Ist das „best me“ ein erstrebenswerter Zustand? Und wer sagt eigentlich, dass nur ausgeschöpfte Potenziale Lebensqualität bedeuten?

Zu Gast sind die Ulmer Schauspielerin Leonie Hassfeld und der Musiker „Lampe“. Die Show kann sowohl live in der Werkhalle des ROXY als auch im Live-Stream via YouTube verfolgt werden.
Eintritt frei.

***
"ROXY Live - Die Late Night Show" ist ein Veranstaltungsformat, das sich in jeder Ausgabe intensiv auf ein Schwerpunktthema stürzt. Die beiden Moderatoren Andreas Rebholz und Niklas Ehrentreich, beide bekannt von den Poetry Slam-Bühnen der Republik, verbinden dabei Information mit Entertainment, immer angetrieben von der eigenen Neugier auf die Sache. Zu Gast sind Gesprächspartner*innen aus Kultur, Wissenschaft, Politik und Wirtschaft.

Niklas Ehrentreich steht unter seinem Künstlernamen "Nik Salsflausen" seit 2011 auf der Bühne. Seitdem erreichte er zweimal das Finale der deutschsprachigen Meisterschaften im Poetry Slam und gewann die baden-württembergische Meisterschaft 2014. Der gelernte Gymnasiallehrer lebt in Esslingen und arbeitet unter anderem als Moderator sowie als Trainer für öffentliche Vorträge und Präsentationen.

Andreas Rebholz ist seit 2013 auf Poetry Slams im gesamten deutschsprachigen Raum unterwegs. Seit dem Beginn seiner wissenschaftlichen Tätigkeit im Themenbereich „Nachhaltige Mobilität“ tritt er zudem auf Science Slams auf. Derzeit promoviert der gebürtige Oberschwabe am Insitut für Nachhaltige Unternehmensführung an der Universität Ulm.