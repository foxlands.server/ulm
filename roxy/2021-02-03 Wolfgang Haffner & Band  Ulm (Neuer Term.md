---
id: "409264449755105"
title: Wolfgang Haffner & Band // Ulm (Neuer Termin)
start: 2021-02-03 20:00
end: 2021-02-03 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/409264449755105/
image: 90718176_10157592324804934_203352210245943296_o.jpg
teaser: "👉 Tickets: https://kj.de/qm  Wolfgang Haffner & Band ›Live 2021‹  03.02.21
  Ulm – ROXY.ulm präsentiert von kulturnews & Jazz thing  Einlass: 19:00 Uhr"
isCrawled: true
---
👉 Tickets: https://kj.de/qm

Wolfgang Haffner & Band
›Live 2021‹

03.02.21 Ulm – ROXY.ulm
präsentiert von kulturnews & Jazz thing

Einlass: 19:00 Uhr | Beginn: 22:00 Uhr

Wolfgang Haffner ist ein exzellenter, ja ein begnadeter Schlagzeuger, das weiß man längst nicht nur in der Jazzwelt. Zahlreiche prominente Zusammenarbeiten mit unterschiedlichsten Künstlern und Bands, bei denen der fränkische Musiker seine Spuren hinterlassen hat - national wie international - sprechen für sich und auch das Trophäenregal mit Auszeichnungen und Preisen füllt sich zusehends. Er wird gelobt und verehrt für seine unglaubliche technische Brillanz und seine geniale Musikalität, die besonders bei seinen eigenen Kompositionen und Projekten zur Geltung kommt. 

Mit seinen Alben unter eigenem Namen gehört Haffner heute zu den erfolgreichsten deutschen Jazzmusikern und -bandleadern. Er ist ein ausgesprochener Meister darin, Stimmungen zu erzeugen und weiterzugeben, den Hörer mit auf eine Reise in seine Klangwelten zu nehmen. So darf man es auch bei seinem neuesten Projekt mit dem Titel ›Kind Of Tango‹ (VÖ: 28.02.2020), dem letzten Teil seiner Trilogie mit akustischer Band erwarten. Deren ersten beiden Teile ‚Kind Of Cool‹ (2015) mit 50s Jazz- und Bop-Exkursionen und ›Kind Of Spain‹ (2017), wo traditionelle spanische Musik mit Jazz zusammen trifft, sind bereits mit großem Erfolg auf dem Label ACT erschienen, mit ›Kind Of Spain‹ tourte er außerdem mehr als zwei Jahre durch Europa, Asien und Afrika.

Im Frühjahr 2020 wird Wolfgang Haffner mit seiner neuen Live-Band auf umfangreiche Deutschlandtournee gehen. Mit dabei ist das junge Tasten-Talent Simon Oslender, der bereits seit 2018 regelmäßig in Haffners Besetzung zu bestaunen ist. Erstmalig in seinem Line-Up taucht der Name der Münchener Sängerin Alma Naidu auf. Vibraphonist Christopher Dell, der auch bei den Album-Besetzungen seiner ›Kind Of‹-Trilogie mitwirkte und Bassist Claus Fischer runden das Haffner-Quintett ab.

