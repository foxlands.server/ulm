---
id: "3251330018290575"
title: Kammerchor der Uni Ulm im ROXY Sound Garten
start: 2020-09-25 19:00
end: 2020-09-25 20:00
address: ROXY.ulm
link: https://www.facebook.com/events/3251330018290575/
teaser: Ein spätsommerliches Biergarten-Konzert  Mitglieder vom Kammerchor der
  Universität Ulm haben zusammen mit ihrem Leiter Manuel Sebastian Haupt nach
  Bee
isCrawled: true
---
Ein spätsommerliches Biergarten-Konzert

Mitglieder vom Kammerchor der Universität Ulm haben zusammen mit ihrem Leiter Manuel Sebastian Haupt nach Beendigung des Singverbots ein kleines Programm einstudiert, das sie im öffentlichen Raum schon präsentiert haben.

Für den Auftritt im Roxy Sound Garden wird das Programm ein wenig verändert und angepasst.

Französische Chansons, deutsche Lieder und englische Songs werden in dem etwa 35-minütigen A-Cappella-Programm erklingen.