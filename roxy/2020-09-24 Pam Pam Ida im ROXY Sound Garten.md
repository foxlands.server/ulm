---
id: "656328458575038"
title: Pam Pam Ida im ROXY Sound Garten
start: 2020-09-24 21:00
end: 2020-09-24 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/656328458575038/
teaser: Aufgrund des schlechten Wetters findet das Konzert in der Werkhalle statt.
  Allerdings erst um 21:00 H. Einlass 20:00 H. Eintritt weiterhin frei. *** I
isCrawled: true
---
Aufgrund des schlechten Wetters findet das Konzert in der Werkhalle statt. Allerdings erst um 21:00 H. Einlass 20:00 H. Eintritt weiterhin frei.
***
Im turbulentesten Jahr der Musikindustrie leuchtet ein rettendes Licht vom Sternenhimmel: Pam Pam Ida offenbaren am 25. September ihr neues Album FREI. 

Die dicken Lettern des Titels in schwarz auf rot machen deutlich, diese Scheibe ist heilige Schrift und Geheimdokument zugleich. Frei wird, wer FREI hört. Kryptische Zeichen bergen alte Weisheiten und die Szenerie des Covers beweist Dir: Pam Pam Ida haben in einer anderen Welt bislang Unvorstellbares gefunden. Als Whistleblower des alternativen Pop weihen sie Dich nur zu gerne ein und nehmen dich mit auf eine ohrenöffnende Abenteuerreise. 

Nach Optimist (2017) und Sauber (2019) bereichern Pam Pam Ida die Pop-Szene wieder mit ihrem ausgefallenen Verständnis des Genres. Das bezieht sich nicht nur auf die bairischen Texte – keine Angst: Die Band stellt Übersetzungen zur Verfügung. Hier paart sich ernsthafte, lyrische Tiefe mit verspielten Arrangements zwischen Synthies, Streichquartett und Bongo. Mit Fingerspitzengefühl aus musikalischer Ekstase und intimem Geständnis manövrieren Pam Pam Ida Dich heil durchs Asteroidenfeld. Darüber singt Andreas Eckert so charismatisch wie energetisch vom Altwerden, verpassten Chancen und der Flucht ins Frei sein.  

Zwischen Falco und Flöten, Sentimentalität und Sarkasmus: PAM PAM IDA sind wohl mit das Außergewöhnlichste, was Deutschlands Musiklandschaft in letzter Zeit zu bieten hat. Eine avantgardistische Melange mit bayrischen Untertönen.