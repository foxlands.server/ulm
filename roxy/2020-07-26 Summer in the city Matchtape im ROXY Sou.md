---
id: "605603123669183"
title: "Summer in the city: Matchtape im ROXY Sound Garten"
start: 2020-07-26 20:00
end: 2020-07-26 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/605603123669183/
image: 106536185_10157703720412756_3557852535059946988_o.jpg
teaser: Mit fettem Vintage-Sound zurück in die Zukunft.  Martin Meixner ist ein
  Teamspieler. Er ist nicht schuld daran, dass sein Spiel die Aufmerksamkeit auf
isCrawled: true
---
Mit fettem Vintage-Sound zurück in die Zukunft.

Martin Meixner ist ein Teamspieler. Er ist nicht schuld daran, dass sein Spiel die Aufmerksamkeit auf sich zieht - er kann nur einfach zu viel, als dass es sich vermeiden ließe. Dieser Musiker bleibt nicht an der Oberfläche der Effekte. An den Tasten zu röhren, genügt ihm nicht. Also geht er tiefer, und wenn er spielt, wird das Instrument zum Organismus: er lockt aus ihm Klänge, die sonst nur das Stethoskop den Eingeweiden wilder Tiere ablauscht. Denn eine Orgel die immer nur faucht, ist schal wie ein eingesperrter Tiger, der immer nur brüllt. Und so brüllt und faucht Martin Meixners Orgel nicht nur, sie wimmert und schnurrt auch, spielt Spielchen, macht schöne Augen und packt dich schließlich doch am Hals.
Und wenn die Röhren in der Hammond Orgel zu glühen beginnen und das Horn im Leslie Verstärker seine Pirouetten dreht dann gibt es kein Halten mehr. Das ist nur ein Grund, warum der Stuttgarter Hammond Spieler Martin Meixner seinem neuen Trio den Namen "Matchtape" gegeben hat, was soviel bedeutet wie Zündschnur oder Anzündband. Eine Anzünd-Band? Gitarrist Jörg Teichert und Schlagzeuger Christian Huber sind die weiteren wichtigen Hauptklang Elemente in dieser Formation und vervollständigen das organische Triebwerk um Martin Meixner. Der eigene Sound, der sich in dieser Kombination aus Jazz & Pop, frechen Melodien und erdigen Grooves entwickelt, versprüht richtig gute Laune und sorgt für enorme positive Energie und Schubkraft, die einlädt zum Abheben und Ankommen.

Martin Meixner – Organ & Keys
Jörg Teichert - Guitars
Christian Huber - Drums, FX

Eintritt frei
Der Biergarten öffnet bereits um 15:00 H.
***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.
Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.