---
id: "1191467301235549"
title: Impala Ray im ROXY Sound Garten
start: 2020-09-18 20:00
end: 2020-09-18 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/1191467301235549/
teaser: Freiheit zu vertonen klingt super, bekommt aber nicht jeder hin. Weil dafür
  Studio-Skills allein nicht reichen. Man muss sie erlebt, sein Ding gemacht
isCrawled: true
---
Freiheit zu vertonen klingt super, bekommt aber nicht jeder hin. Weil dafür Studio-Skills allein nicht reichen. Man muss sie erlebt, sein Ding gemacht haben. Ray ist so einer. Einer, der unterwegs ist in der Welt. Einer, der schon lange Musik macht und sie auf Bühnen in ganz Europa gebracht hat.
Seine Songs sind von den Vibes der California Bay Area genauso inspiriert, wie von der Zeit, als er in Afrika gelebt hat. Sein Sound steht für Freiheit, Sorglosigkeit und der unbändigen Liebe am Leben. IMPALA RAY ist dein akustischer Roadtrip mit deinen besten Freunden zu den schönsten Orten der Welt.

Der Biergarten öffnet um 17:00 H.
Eintritt frei.