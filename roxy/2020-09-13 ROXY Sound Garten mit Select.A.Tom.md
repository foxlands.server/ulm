---
id: "961037927709705"
title: ROXY Sound Garten mit Select.A.Tom
start: 2020-09-13 17:00
end: 2020-09-13 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/961037927709705/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00 H): Select.A.Tom
Schöne Sonntagsmatinéemusik

Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten ist ab 17:00 H geöffnet.
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de