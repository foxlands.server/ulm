---
id: "513436449240482"
title: Felix Lobrecht - HYPE - Ulm * ausverkauft
start: 2020-09-22 20:00
end: 2020-09-22 23:00
locationName: ratiopharm arena
address: Europastr. 25, 89231 Neu-Ulm
link: https://www.facebook.com/events/513436449240482/
image: 69516527_2724662240891730_1107943945658171392_o.jpg
teaser: ROXY.ulm goes ratiopharm arena  Die Show wird vom 02.04.2020 auf den
  22.09.2020 verschoben.  Ort, Einlass und Beginn bleiben unverändert.  Die
  Tickets
isCrawled: true
---
ROXY.ulm goes ratiopharm arena

Die Show wird vom 02.04.2020 auf den 22.09.2020 verschoben. 
Ort, Einlass und Beginn bleiben unverändert. 
Die Tickets behalten ihre Gültigkeit oder können alternativ an der entsprechenden Vorverkaufsstelle zurückgegeben werden.

___

Ich hab mal in einer Cocktailbar gearbeitet. Mein Chef Deniz meinte damals „Fäälix, weißt du wie man merkt, dass die Gäste wirklich zufrieden waren? Wenn sie wiederkommen.“ Deniz hat zwar auch mit brennender Fluppe im Mundwinkel die Gaskartusche von dem Bunsenbrenner für die Shishakohle gewechselt, aber trotzdem hat er recht.

Nach „kenn ick.“ sollte es also eigentlich reichen, das neue Programm von Felix Lobrecht mit den Worten „das neue Programm von Felix Lobrecht“ zu bewerben. Zufriedene Leute kommen ja eh wieder. Die Society will aber einen akkurat-verkopften Pressetext von mir, will Formulierungen wie „messerscharfe Alltagsbeobachtungen“, „unangenehm wahr“, „brüllend komisch“ hören. Aber come on, ES IST DAS NEUE PROGRAMM VON FELIX LOBRECHT. Jetzt noch voller mit messerscharfen Alltagsbeobachtungen, noch unangenehm wahrer und noch brüllend komischer.

„Kenn ick.“ war krass, „Hype“ ist krasser. GEHT DA HIN!