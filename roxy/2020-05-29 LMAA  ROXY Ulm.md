---
id: "303228627357509"
title: LMAA * ROXY Ulm (Sound Garten)
start: 2020-05-29 17:00
end: 2020-05-29 23:59
address: ROXY.ulm
link: https://www.facebook.com/events/303228627357509/
image: 101032613_10157577026512756_6962142085600247808_o.jpg
teaser: Liederabend mit Andi und Achim  Den ersten Biergartenabend 2020 mit Programm
  werden die zwei musikalischen Füchse Andi und Achim aka Purple Haze und D
isCrawled: true
---
Liederabend mit Andi und Achim

Den ersten Biergartenabend 2020 mit Programm werden die zwei musikalischen Füchse Andi und Achim aka Purple Haze und Deejot Roterfreibeuter begleiten.
Eintritt frei.

****
Es gelten die aktuellen Anordnungen bzgl. Hygienemaßnahmen, Haushaltzusammenkünfte und Abstandsregeln. Beim Einlass erklären wir euch alles - bitte lasst uns hier zusammenarbeiten. Wir sind fest davon überzeugt, dass das auch unter den gegebenen Umständen eine hervorragende Biergartensaison wird! Wir freuen uns auf auch :D