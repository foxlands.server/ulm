---
id: "2514567605312555"
title: "Summer in the City: Pulsar Trio im ROXY Sound Garten"
start: 2020-07-10 20:00
end: 2020-07-10 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/2514567605312555/
image: 106554790_10157703335812756_4291962959185308509_o.jpg
teaser: (global pop groove fusion) (bei schlechtem Wetter in der Werkhalle) Klavier
  und und die indische Sitar? Und dann noch Schlagzeug und sogar Elektronik
isCrawled: true
---
(global pop groove fusion)
(bei schlechtem Wetter in der Werkhalle)
Klavier und und die indische Sitar? Und dann noch Schlagzeug und sogar Elektronik dazu?
Ja !  In dieser Band funktioniert so etwas, und zwar großartig. Ansteckende Grooves,  hypnotisch strahlende Melodien, grandiose Spannungsbögen. Leichtfüßig kommt diese Musik daher, und gleichzeitig übt sie einen intensiven Sog aus. Weltmusikalische Anklänge treffen auf Break- und House-Beats, und egal ob auf dem psychedlischen Burg-Herzberg-Festival oder beim Glastonbury-Festival: Die Fans werden ständig mehr. Ulm wird da keine Ausnahme sein.
Aaron Christ: Schlagzeug
Beate Wein: Piano
Matyas Wolter: Sitar und Surbahar

Eintritt frei
Der Biergarten ist ab 15:00 H geöffnet.
***
Konzerte in der Reihe "Summer in the city" sind eine Kooperation von Kunstwerk e.V. und ROXY gGmbH.
Wir danken für die Unterstützung durch das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg
