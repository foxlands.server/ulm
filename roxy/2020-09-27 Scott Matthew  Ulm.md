---
id: "453822088659613"
title: Scott Matthew | Ulm
start: 2020-09-27 20:00
end: 2020-09-27 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/453822088659613/
image: 78889535_10162742593090261_8999158693710790656_o.jpg
teaser: "Scott Matthew  27.09.20 ROXY.ulm Einlass: 19 Uhr | Beginn: 20 Uhr  Tickets
  sind ab 22€ zzgl. Geb. online unter www.schoneberg.de und an ausgewählten V"
isCrawled: true
---
Scott Matthew

27.09.20
ROXY.ulm
Einlass: 19 Uhr | Beginn: 20 Uhr

Tickets sind ab 22€ zzgl. Geb. online unter www.schoneberg.de und an ausgewählten VVK-Stellen erhältlich.

► www.scottmatthewmusic.com
► www.bit.ly/2KIZqF1 ♫

Der begnadete Songpoet Scott Matthew hat sich seit seinem selbstbetitelten Debut von 2008 mit seinen bisherigen fünf Solo-Alben höchsten Respekt sowohl bei Kritikern als auch bei seinen Fans erarbeitet. Der gebürtige Australier schreibt und singt vom Herbeisehnen und Verschwinden der Liebe wie kaum jemand sonst – immer zutiefst einfühlsam, berührend arrangierend und in ganz eigenem Stil.
Rechtzeitig zum 1. Advent veröffentlicht Scott Matthew am 29. November nun eine 2-Track Single mit zwei Weihnachtsklassikern, die er gemeinsam mit seiner australischen Kollegin Sia interpretiert und aufgenommen hat. Während das Duett „Baby It’s Cold Outside“ zu seinen absoluten Lieblingen gehört, entstand „Silent Night“ eher zufällig – passt aber als spartanisch instrumentierte Weihnachtsballade mit Gitarre, Ukulele und Glöckchen ganz wunderbar in die besinnliche Adventszeit.
Wer Scott Matthew live erlebt, ist meist seltsam ergriffen. Scott macht Leid zu Lied, er entkleidet sein Inneres vor dem Publikum. Er lässt die Hörer teilhaben, teilnehmen, Teil von etwas Besonderem werden. Scott Matthew berührt zutiefst, die Intensität seines Gesangs ist immer ganz unmittelbar und oft wunderbar.
Fotocredit: Michael Mann