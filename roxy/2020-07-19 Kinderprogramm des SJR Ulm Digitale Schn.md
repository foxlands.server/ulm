---
id: "745687296183893"
title: "Kinderprogramm des SJR Ulm: Digitale Schnitzeljagd"
start: 2020-07-19 15:00
end: 2020-07-19 17:00
address: ROXY.ulm
link: https://www.facebook.com/events/745687296183893/
image: 107159568_10157712792847756_5129444813336349214_o.jpg
teaser: Mit iPads und der App Actionbound geht ihr auf eine spannende Entdeckungsreise
  rund um die Donaubastion. Mit kleinen Rätseln und Kreativaufgaben bleib
isCrawled: true
---
Mit iPads und der App Actionbound geht ihr auf eine spannende Entdeckungsreise rund um die Donaubastion. Mit kleinen Rätseln und Kreativaufgaben bleibt hier gewiss kein Detail unentdeckt.

Maximale Teilnehmerzahl: 10
Ab 8 Jahre
15:00-17:00 H

Das Angebot ist kostenlos (und findet unter der Berücksichtigung der derzeitigen Hygiene- und Sicherheitsmaßnahmen statt). Wer mag, kann sich gerne im Voraus mit Name, Adresse und Telefonnummer unter knehr@sjr-ulm.de anmelden.

Kooperation von Stadtjugendring e.V. und ROXY gGmbH