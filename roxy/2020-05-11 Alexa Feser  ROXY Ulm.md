---
id: "713196562503307"
title: Alexa Feser * ROXY Ulm
start: 2020-05-11 20:00
end: 2020-05-11 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/713196562503307/
image: 78579570_10157029718512756_58537797199331328_o.jpg
teaser: "Alexa Feser 11.05.2019 | ROXY.ulm Einlass: 19:00 H |  Beginn 20:00 H  Tickets
  ab dem 11.12.2020 über Ticketmaster Tickets ab dem 12.12.2020 über Event"
isCrawled: true
---
Alexa Feser
11.05.2019 | ROXY.ulm
Einlass: 19:00 H |  Beginn 20:00 H

Tickets ab dem 11.12.2020 über Ticketmaster
Tickets ab dem 12.12.2020 über Eventim, Reservix und allen bekannten örtlichen VVK-Stellen

Tour: Live Nation GmbH
Local: ROXY gGmbH

Auf ALEXA FESERs letztem Top 10 Album „A!“ ließ die Sängerin tief blicken, so tief, wie man es in der deutschen Pop-Landschaft selten erlebt und erst recht nicht von einer Künstlerin, die man als starke, erfolgreiche Frau kennt. Auch mit „Zwischen den Sekunden“ (2017) erreichte sie Platz 3 der deutschen Album-Charts, ihre Songs wie „Wunderfinder (feat. Curse)“, „Medizin“ und „Wir sind hier“ wurden millionenfach gestreamt, ihre Konzerte sind ausverkauft. Im Mai kommt „Die Wunderfinderin“ nach Bielefeld (05.05., Forum), Düsseldorf (06.05., Zakk), Kiel (07.05., Die Pumpe), Rostock (08.05., Moya), Potsdam (09.05., Waschhaus) sowie Ulm (11.05., Roxy), Stuttgart (12.05., Im Wizemann), Erlangen (13.05., E-Werk) und Cottbus (14.05., Glad House).

Es wäre einfach gewesen, da weiterzumachen, wo sie mit ihren letzten Alben aufgehört hatte – ihre große Gabe, Alltagssituationen in Texten mit kraftvollen Bildern abzufassen, verhalf der Musikerin zu einer Vielzahl an Songs, die das breite Feld der Emotionen als solche thematisieren. Doch ALEXA FESER wollte den Blick von außen nach innen richten. Auf sich selbst. Die Zeit sei reif für Klartext, wie sie sagt.

„Auf A! findet man meine Reise von damals bis heute – mein Leben bis zum jetzigen Zeitpunkt.“

Um sich dem eigenen Lebenslauf schonungslos und künstlerisch zu widmen, braucht es Ehrlichkeit, Reflektion, vor allem aber Mut zur Offenheit. All das besitzt ALEXA FESER und teilt auf „A!“ der ganzen Welt mit, was man - wenn überhaupt - seinem besten Freund anvertrauen würde.

Nach 12 nahegehenden Deutschpop-Songs fragt man sich: Kann man so einer offenen Künstlerin überhaupt noch näherkommen? Man kann: Im Mai 2020 setzt ALEXA FESER mit ihrer Band fort, was im Frühjahr und Herbst letzten Jahres begann: Die A! Tour – eine Tournee, die wie das Album, zahlreiche „!“-Momente bereithält.

„Es gibt noch so viele Sachen bei mir zu entdecken. Irgendwo anzukommen, würde für mich Stillstand bedeuten – und ich habe das Gefühl, ich habe noch eine Mission.“ So auch im Mai 2020, wenn Alexa Feser wieder auf Tour geht. 