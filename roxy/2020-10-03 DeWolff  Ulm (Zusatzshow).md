---
id: "504682526891268"
title: DeWolff | Ulm (Zusatzshow)
start: 2020-10-03 20:00
end: 2020-10-03 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/504682526891268/
image: 90503020_3092978610723203_6723458142937546752_o.jpg
teaser: "Präsentiert von: ROCKS Magazin & Betreutes Proggen  Special Guest: The Grand
  East  Nach den erfolgreichen Festivalauftritten im Sommer dieses Jahres k"
isCrawled: true
---
Präsentiert von: ROCKS Magazin & Betreutes Proggen

Special Guest: The Grand East

Nach den erfolgreichen Festivalauftritten im Sommer dieses Jahres kündigen die niederländischen Blues-Rocker DeWolff im Rahmen der Promotion ihres am 10. Januar 2020 erschienenen siebten Albums „Tascam Tapes“ (Mascot/Rough Trade) eine weitere Europa-Tournee an. 