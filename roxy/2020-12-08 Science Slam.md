---
id: "380615599999326"
title: Science Slam
start: 2021-03-02 20:00
end: 2021-03-02 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/380615599999326/
image: 120118673_10157932345352756_167724778261343252_n.jpg
teaser: Der Salm musste vom 08.12.2020 auf den 02.03.2021 verschoben werden. Tickets
  behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurück
isCrawled: true
---
Der Salm musste vom 08.12.2020 auf den 02.03.2021 verschoben werden. Tickets behalten ihre Gültigkeit oder können an den jeweiligen VVK-Stellen zurückgegeben werden.
***
Das Prinzip ist einfach: Jeder Slammer hat 10 Minuten Zeit, ein an sich wissenschaftliches, komplexes Thema seiner Wahl einem breiten Publikum verständlich zu machen. Egal ob Schüler, Student, Lehrer, Wissenschaftler oder Laie mit speziellen Fachkenntnissen: jeder darf auf die Bühne. Im Anschluss wird der Vortrag vom Publikum bewertet. Das Publikum bildet die Jury und bestimmt, wer am Ende des Abends zum Science-Slam-Sieger gekürt wird.

Moderiert von Dana Hoffmann

Du hast selbst ein wissenschaftliches Thema, das du unserem Publikum näher bringen willst? Dann melde dich an und nutze deine 10 Minuten! scienceslam@roxy.ulm.de

Danke an scanplus für die Unterstützung!