---
id: "4963294073696029"
title: MASAA im ROXY Sound Garten
start: 2020-09-20 19:00
end: 2020-09-20 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/4963294073696029/
teaser: Bei MASAA verweben sich tiefempfundene arabische Verse und zeitgenössischer
  Jazz, gehen Abend- und Morgenland in bisher nicht dagewesener Schlüssigkei
isCrawled: true
---
Bei MASAA verweben sich tiefempfundene arabische Verse und zeitgenössischer Jazz, gehen Abend- und Morgenland in bisher nicht dagewesener Schlüssigkeit eine lyrische Liaison ein. Ihr bahnbrechendes Konzept haben MASAA auf bislang drei Alben umgesetzt: dem Debüt „Freedom Dance“ (2012), dem Nachfolger „Afkar“ (2014) und „Outspoken“ (2017), für das sie mit dem Preis der deutschen Schallplattenkritik ausgezeichnet wurden. Rabih Lahoud wurde zudem für dieses Album für den ECHO Jazz in der Sparte „bester nationaler Sänger“ nominiert. Bereits im Gründungsjahr 2012 gewannen Masaa den Bremer Jazzpreis. In den folgenden Jahren gingen 2 weitere wichtige Weltmusikpreise Deutschlands an sie: Der Publikumspreis des Creole-Wettbewerbs (2013) und die Förder-RUTH des Rudolstadt-Festivals (2015). Von Beginn an ist es der Band ein großes Anliegen gewesen, ihre Arbeit nach außen zu tragen: MASAA haben erfolgreiche Tourneen durch Afrika und Lahouds Heimat Libanon absolviert; 2018 folgte die Einladung zum Salam Orient Festival in Wien und 2019 zum Jazzfest Baku in Aserbaidschan. Im Teamwork mit der israelischen Sängerin Yael Deckelbaum (March of the Mothers) setzen MASAA ein Signal für die Unterstützung des Friedensprozesses zwischen jüdischer und arabischer Kultur.

Im Januar 2019 ist der Gitarrist Reentko Dirks für den Pianisten Clemens Christian Poetzsch in die Band gekommen. Mit ihm entstand in den Berliner Traumton Studios das aktuelle Album „Irade“ (VÖ: 14.02.20). Durch den Wechsel in der Instrumentierung entsteht ein aufregend neuer Masaa-Sound: die Doppelhals-Gitarre changiert mit und ohne Bünden mühelos zwischen der hiesigen und der Viertelton-geprägten Harmonik. Reentko bringt einen noch wärmeren und zugleich impulsiveren Gestus, geprägt von Klassik, Pop und Weltmusik in die Band und lädt Lahoud, Kappenstein und Rust zu einer neuen Art des musikalischen Dialogs.

Besetzung:
RABIH LAHOUD (GER/LIB) vocals
RENTKO DIRKS (GER) guitar
DEMIAN KAPPENSTEIN (GER) drums
MARCUS RUST (GER) trumpet

Der Biergarten öffnet um 17:00 H.
Eintritt frei.