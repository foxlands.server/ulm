---
id: "520099688568384"
title: Enno Bunger – Ulm, Roxy - Neuer Termin!
start: 2020-07-16 21:00
end: 2020-07-16 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/520099688568384/
image: 90511763_10163056833120425_4889100052020068352_o.jpg
teaser: "Enno Bunger Was berührt, das bleibt.  Live 2020 Präsentiert von kulturnews
  und DIFFUS Magazin  Einlass: 20:00 Uhr | Beginn: 21:00 Uhr  Enno Bungers vi"
isCrawled: true
---
Enno Bunger
Was berührt, das bleibt. 
Live 2020
Präsentiert von kulturnews und DIFFUS Magazin

Einlass: 20:00 Uhr | Beginn: 21:00 Uhr

Enno Bungers viertes Album bietet den Soundtrack für ein besseres Leben. Eines, in dem man lieber gute Erinnerungen sammelt als Messenger-Verläufe. Eines, in dem die Qualität des Augenblicks größeren Wert hat als das imaginierte Ziel in der Ferne. „Wir wollen nichts mehr werden / wir wollen nur noch sein.“ Die Konsequenz der Platte liegt darin, dass sie diese Qualität lyrisch wie musikalisch in jedem Ton umsetzt. Mutig öffnen sich Bunger und seine Musiker modernen stilistischen Formen und nutzen sie allesamt zu ihrem Vorteil. 

Nach großer Tour im Herbst 2019, kommt Enno Bunger mit seinem aktuellen Album „Was berührt, das bleibt“ im Frühling 2020 erneut auf Konzertreise. 
Weiterlesen >> https://gastspielreisen.com/enno-bunger