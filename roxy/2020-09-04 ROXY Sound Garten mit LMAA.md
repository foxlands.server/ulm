---
id: "310662526662722"
title: ROXY Sound Garten mit LMAA
start: 2020-09-04 17:00
end: 2020-09-04 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/310662526662722/
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00 H): L.M.A.A. - Liederabend mit Andi und Achim
Diesen Abend werden die zwei musikalischen Füchse Andi und Achim aka Purple Haze und Deejot Roterfreibeuter begleiten.

Eine Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten öffnet um 17:00 H
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de