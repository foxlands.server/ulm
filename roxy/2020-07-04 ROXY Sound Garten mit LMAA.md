---
id: "224322325219922"
title: ROXY Sound Garten mit LMAA
start: 2020-07-04 17:30
end: 2020-07-05 00:00
address: ROXY.ulm
link: https://www.facebook.com/events/224322325219922/
image: 106588347_10157703097822756_921493728308513440_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:30 H): L.M.A.A. - Liedabend mit Andi und Achim
Diesen Abend werden die zwei musikalischen Füchse Andi und Achim aka Purple Haze und Deejot Roterfreibeuter begleiten.

Eine Spende für die musikalische Unterhaltung wird gern entgegen genommen.

Der Biergarten öffnet um 15:00 H
***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de