---
id: "2527827543995857"
title: The Sisters of Mercy – Ulm, Roxy
start: 2020-09-11 19:00
end: 2020-09-11 23:59
address: ROXY.ulm
link: https://www.facebook.com/events/2527827543995857/
image: 90621965_10151331281199996_378587374138949632_o.jpg
teaser: "Verlegt vom 23.03.2020. Alle Tickets behalten ihre Gültigkeit.
  ________________________  Einlass: 19:00 | Beginn: 20:00  Andrew Eldritch,
  Mastermind d"
isCrawled: true
---
Verlegt vom 23.03.2020. Alle Tickets behalten ihre Gültigkeit.
________________________

Einlass: 19:00 | Beginn: 20:00

Andrew Eldritch, Mastermind der The Sisters Of Mercy, zieht es ein weiteres Mal auf deutsche Bühnen: Nachdem die letzte Tour im Oktober komplett ausverkauft war, werden die nicht weniger als legendären The Sisters Of Mercy fünf weitere Konzerte spielen, um noch einmal Hits wie „Under The Gun“, „Walk Away“, „Doctor Jeep“ und natürlich „Temple Of Love“ zu zelebrieren. Obwohl Eldritch seit den späten Neunzigern kein neues Material mehr veröffentlicht hat, klingt der Goth- und Post-Punk-Sound der Band aus Leeds immer noch zeitlos frisch. Im Herbst 2020 kommt The Sisters of Mercy erneut auf Tour durch Deutschland.

Weiterlesen. >> fkpscorpio.com/de/bands-archiv/the-sisters-of-mercy