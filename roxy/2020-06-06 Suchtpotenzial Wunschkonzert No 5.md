---
id: "2583094165123517"
title: Suchtpotenzial Wunschkonzert No 5
start: 2020-06-06 20:00
end: 2020-06-06 23:00
link: https://www.facebook.com/events/2583094165123517/
image: 101038853_3141506862554505_5388790948262051840_n.jpg
teaser: HIGH FIVE! Wunschkonzert Nummer 5, Top40 Edition.  Leute, es geht wieder los!
  Und das Schöne ist, wir werden vor echten Menschen spielen, mit echtem A
isCrawled: true
---
HIGH FIVE! Wunschkonzert Nummer 5, Top40 Edition. 
Leute, es geht wieder los! Und das Schöne ist, wir werden vor echten Menschen spielen, mit echtem Applaus, und noch Arianes Geburtstag nachfeiern.
Der Masked Bringer, die Müller Sisters, alle sind am Start! 
Im Roxy Biergarten ist es super schön, es gibt Essen und Getränke, und man unterstützt mit jedem EGAL Ulms coolsten Kulturschuppen. 
Eintritt frei, Spende erwünscht, wer zuerst kommt,  kriegt nen Platz auf der Bierbank, ab 17 Uhr is offen.
Trotzdem streamen wir natürlich weltweit, is klar!

Und es werden noch Wünsche angenommen. Also haut mal raus, da geht noch was!