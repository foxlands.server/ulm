---
id: "280266626658497"
title: Suchtpotenzial Wunschkonzert VOL 6
start: 2020-07-19 19:00
end: 2020-07-19 22:00
link: https://www.facebook.com/events/280266626658497/
teaser: Wünsch dir was von Suchtpotenzial!  Wir spielen die Lieblingslieder unserer
  Fans. Von Metal bis Schlager, wir spielen alles!  Wir scheitern auch live,
isCrawled: true
---
Wünsch dir was von Suchtpotenzial! 
Wir spielen die Lieblingslieder unserer Fans. Von Metal bis Schlager, wir spielen alles! 
Wir scheitern auch live, aber EGAL!
2 Stimmen, Keyboard, Akustikgitarre...und wenn es hart auf hart kommt auch mal die Müller Sisters als Backings. 
Die beiden Girls werden auch die Sucht Halftime Show rocken, mit Ukulele und guter Laune. 
Wir streamen live aus dem Roxy Ulm. Bei gutem Wetter im Sound Garten, bei schlechtem Wetter dürfen bis zu 250 Leute in der großen Halle zuschauen. 
Und weltweit online auf unserem Youtube Kanal.
Wir freuen uns!

Eintritt ist frei, Spende erbeten. 
Paypal: suchtpotenzial.music@gmail.com