---
id: "207660407096068"
title: Alternative Party mit DJ Red Vicious und Tom Biafra
start: 2020-04-30 23:00
end: 2020-05-01 04:30
address: ROXY.ulm
link: https://www.facebook.com/events/207660407096068/
image: 86853649_10157242646647756_4749547059810402304_o.jpg
teaser: Im Anschluss an die Konzerte von Jaya the Cat & The Magic Mumble Jumble * ROXY
  Ulm tanzen wir gemeinsam mit den DJs Red Vicious und Tom Biafra (Schütt
isCrawled: true
---
Im Anschluss an die Konzerte von Jaya the Cat & The Magic Mumble Jumble * ROXY Ulm tanzen wir gemeinsam mit den DJs Red Vicious und Tom Biafra (Schüttel dein Speck) in den Monat Mai.

Wer Konzerttickets hat, kann einfach bleiben. "Nur-Party-Gänger" zahlen 7,— € an der Abendkasse.