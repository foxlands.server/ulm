---
id: "184591869315494"
title: "Technotheater: The Orange Moon im ROXY"
start: 2020-10-08 20:00
end: 2020-10-08 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/184591869315494/
image: 83296128_2630663273821552_2419845962533961728_o.jpg
teaser: "Technotheater: The Orange Moon Sphärische Synthieklänge, düstere Bässe und
  ein eindringliches Pulsieren. Ein Sog. Die Vereinigung von elektronischen K"
isCrawled: true
---
Technotheater: The Orange Moon
Sphärische Synthieklänge, düstere Bässe und ein eindringliches Pulsieren. Ein Sog. Die Vereinigung von elektronischen Klängen und Spoken Words. In der körperlichen Erfahrung manifestieren sich Inhalte. Die freigesetzten Energien erzeugen Aktivität.

Techno und Theater, geht das? Theater und Party im Club funktioniert diese Kombination? Die Antwort: Ja!
Dramatiker Tobias Frühauf und Regisseur Philipp Wolpert vom Theaterlabel Tacheles und Tarantismus arbeiten an der Entwicklung ihres Technotheaters und versammeln ein explosives künstlerisches Team um sich. Zwei erfahrene Technoproducer, zwei Technonewcomer, ein Allround-Musiker, ein Körperkünstler, und ein fünfköpfiges Schauspielensemble, bilden die Formation der Neuproduktion.
Mit ihrer Eigenproduktion „The Orange Moon“ touren sie durch Deutschland und die Schweiz und bringen Theater in den Clubraum. Kreiert wird ein Grenzgang zwischen Techno, Performace, Party und Theater.

Gespielt wird die Koproduktion mit dem Stadtpalais Stuttgart in renommierten Szenelocations wie dem Stadtpalais Stuttgart, der Kantine am Berghain Berlin, der Roten Fabrik Zürich, dem Tanzhaus West Frankfurt, dem Bahnwärter Thiel München, der Halle 02 Heidelberg, dem Roxy Ulm, dem Dornheim Würzburg, dem Hafen 49 in Mannheim. Entwickelt wird die Produktion im Mobilat Heilbronn. Tobias Frühauf und Philipp Wolpert leiten dort seit 2019 das Theaterlabor Stilbruch.

»The Orange Moon« ist in einer nahen Zukunft verortet. Keine Maschinen, keine Technik, es geht nur um den Menschen und die »schöne neue Welt«, die er sich geschaffen hat. Die Gesellschaft befindet sich in einem Entwicklungsstadium, hin zu einer apokalyptischen Welt, doch dieser Prozess wird nur von den Wenigen als solcher registriert. Blank und ehrlich werden Schicksale und ihr Kampf, um ihren Platz in einer kaputten Umwelt geschildert. Fünf Charaktere die erkennen müssen, dass ihre individuellen Ängste, Komplexe und Probleme, ihr Streben nach Unendlichkeit, Wahrheit, vollkommener Liebe und innerem Frieden, Katalysatoren des »großen Feuers« sind. Es ist unsere Generation. Am Ende bleibt wenig - entblößte Körper vor einem Scherbenhaufen. Eine nach sozialdarwinistischen Mustern handelnde Regierung, vermeintliche Wohlstandprobleme, die große Depression, ethische Zerreißproben, der Kampf um den inneren Frieden und andere, fremde Schicksale. Die Flucht in den Exzess, doch es gibt keinen Zufluchtsort mehr. Eine Parabel auf den Menschen und auf unsere Zeit die in ihrer Aufführung eine Auflösung der geometrischen Figur zur Folge hat. Die Skizzierung löst sich spiralförmig auf und implodiert. Lasst uns selbst Geschichte schreiben an diesem Abend und eine Prophezeiung feiern und voraussehen, die letztlich doch nur jeder Einzelne für sich und in der Solidarisierung mit sich selbst und seinen Mitmenschen aufhalten könnte.

Wir laden in den »Zirkus der aufgehobenen Schwerkraft«, in das »theatrale Panoptikum«. Die Welt ist die Bühne, ist die Manege. Alter Glanz, der allmählich zerfällt. Ein tiefgehender Theaterabend, ein intensives Partyerlebnis und keine Antworten. Oder liegt im Erlebnis und in den Gedanken der Stein des Anstoßes? Tanzt so lange ihr könnt!
Thematisch werden Fragestellungen rund um die Umweltsituation, Veränderungen bzw. alternative Modelle des menschlichen Zusammenlebens und die mögliche Zukunft erörtert. Ein kritischer Beitrag zur Nachhaltigkeit, zum modernen Menschen und zur großen Frage, wie das postkapitalistische Zeitalter aussieht.
Die musikalische Konstruktion bildet das Fundament der Dramaturgie. Der stramme Techno und deepe Techouse wird immer wieder von Retrofusion-Synthiesound durchbrochen. Zitate an die Italo Disco.

Regie: Philipp Wolpert
Text: Tobias Frühauf
Musik: Frank Nova, Helmut Dubnitzky, Marco Tondera, Julian Konsent, Michel Schulze

Mit Varya Popovkina, Paul-Louis Schopf, Carmen Yasemin Zehentmeier, Sebastian Graf, Andreas Posthoff, Tom Feyerabend

Beginn: 20:00 Uhr (Einlass: 19:00 Uhr) 