---
id: "574453736778303"
title: ROXY Sound Garten mit Miendientje
start: 2020-06-21 17:00
end: 2020-06-21 22:00
address: ROXY.ulm
link: https://www.facebook.com/events/574453736778303/
image: 104287918_10157651180512756_3851422372590770212_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 17:00 H): Miendientje
Sommerleichter Feelgood-Indie-Sound mit einer leichten Lieder-Brise aus Hamburg.


Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de