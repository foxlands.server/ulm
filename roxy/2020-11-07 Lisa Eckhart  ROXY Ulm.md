---
id: "1020980001627405"
title: Lisa Eckhart * ROXY Ulm (wird verschoben, Ersatztermin noch nicht bekannt)
start: 2020-11-07 20:00
end: 2020-11-07 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1020980001627405/
teaser: Die Veranstaltung wird verschoben. Sobald ein Ersatztermin steht, wird dieser
  bekannt gegeben. Tickets behalten ihre Gültigkeit! ***  Es war nicht all
isCrawled: true
---
Die Veranstaltung wird verschoben. Sobald ein Ersatztermin steht, wird dieser bekannt gegeben. Tickets behalten ihre Gültigkeit!
***

Es war nicht alles schlecht unter Gott. Gut war zum Beispiel, dass alles schlecht war. Denn alles, was man tat, war Sünde. Wir waren alle gute Christen und hatten einen Heidenspaß. Die Hölle zählte Leistungsgruppen, Ablässe waren das perfekte Last-Minute Geschenk und lasterhaft zu sein noch Kunst. Doch dann starb Gott ganz unerwartet an chronischer Langeweile. Und bei der Testamentsverlesung hieß es, wir wären alle von der Ursünde enterbt. Fortan war kein Mensch mehr schlecht, jedes Laster nunmehr straffrei und die Hölle wegen Renovierungsarbeiten geschlossen. So fand der Spaß ein jähes Ende.

Heute ziehen Eisfirmen, Elektronikgeschäfte und jedes zweite Schlagerlied die sieben Sünden in den Dreck, indem man sie zur heiligen Tugend erklärt. Gott befahl uns zu entsagen, Coca Cola zu genießen. Man hat uns alles erlaubt und somit alles genommen. Polyamorie versaute die Unzucht. All-You-Can-Eat Buffets vergällten die Völlerei. Facebook beschämte die Eitelkeit. Ego-Shooter liquidierten den Jähzorn. Wellnesshotels verweichlichten die Trägheit. Sie alle haben's schlecht gemeint. Doch schlecht gemeint ist bekanntlich das Gegenteil von schlecht. Und kein Zweck heiligt das Mittelmaß.

Darum gilt es, die Sünden neu zu erfinden. 

Wie widersetzt man sich der Spaßgesellschaft ohne den eigenen  Spaß  einzubüßen? 
Wie empört  man  seine  Umwelt  ohne  als  Künstler  verleumdet  zu werden? 
Wie verweigert man sich dem Konsumerismus ohne auf irgendetwas zu verzichten? 
Wie verachtet man die Unterhaltungsindustrie ohne Adorno schmeichelnd ans Gemächt zu fassen? Wie wird man zum Ketzer einer säkularisierten Welt?

Seien Sie neidisch auf andere, doch anstatt ihnen nachzueifern, ziehen Sie sie auf Ihr Niveau. 
Seien Sie träge und zeigen Sie Ihrem Partner, wer in der Beziehung die Windeln anhat.
Seien Sie jähzornig und beschimpfen Sie Werner Herzog.
Seien Sie wollüstig und beschränken Sie sich nicht auf die zwei, drei Abgründe Ihres Körpers. 
Seien Sie eitel und entreißen Sie Ihre Schönheit dem trüben Auge des Betrachters.
Seien Sie geizig und teilen Sie nicht länger brüderlich wie Kain den Schädel seines Bruders. 
Seien Sie maßlos in allem, nur niemals der Mittelmäßigkeit.