---
id: "2440642599356083"
title: Verschoben - Rüdiger Hoffmann - Ulm, 20.01.2021
start: 2021-01-20 20:00
end: 2021-01-20 22:30
address: ROXY.ulm
link: https://www.facebook.com/events/2440642599356083/
image: 70353190_3200079130026824_9213860166057852928_n.jpg
teaser: Der Termin wurde vom 07.05.20 auf den 20.01.21 verschoben. Die Tickets
  behalten ihre Gültigkeit.  Jetzt bietet Rüdiger Hoffmann allen Fans guter
  Comed
isCrawled: true
---
Der Termin wurde vom 07.05.20 auf den 20.01.21 verschoben.
Die Tickets behalten ihre Gültigkeit.

Jetzt bietet Rüdiger Hoffmann allen Fans guter Comedy erstmals die geballte Ladung: mit einem prallvollen BEST OF seines Schaffens. Ab Herbst 2017 präsentiert der „Entdecker der Langsamkeit“ zwei Stunden lang ein legendäres Comedy-Highlight nach dem anderen.