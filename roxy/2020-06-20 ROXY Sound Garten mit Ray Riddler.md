---
id: "286549302393818"
title: ROXY Sound Garten mit Ray Riddler
start: 2020-06-20 17:00
end: 2020-06-21 00:00
address: ROXY.ulm
link: https://www.facebook.com/events/286549302393818/
image: 104366077_10157651167362756_2693999914231103515_o.jpg
teaser: Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem
  regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.
isCrawled: true
---
Wir freuen uns, euch in unserem Sound Garten begrüßen zu dürfen. Neben dem regulären Biergartenbetrieb versüßen euch unterschiedliche DJs die Ohren.

HEUTE (ab 18:00 H): Ray Riddler
Feeel gooood im Sound Garten!
Funk, Disco, Electronica, Soul, 80s Pop & Rock, Synthiepop, Indie Classics. Abseits von Einheitsbrei gibt es Gutes, Grooviges, Seltenes, Kurioses und auch die Hits aus vergangenen Tagen.
Alles kann, nichts muss...


Eintritt frei, Spende für die musikalische Unterhaltung wird gern entgegen genommen.

***
Alle aktuellen Bestimmungen bzgl. Corona findet ihr auf unserer Homepage www.roxy.ulm.de