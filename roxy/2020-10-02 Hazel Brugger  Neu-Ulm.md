---
id: "2477665785604068"
title: Hazel Brugger * Neu-Ulm
start: 2020-10-02 20:00
end: 2020-10-02 23:00
locationName: ratiopharm arena
address: Europastr. 25, 89231 Neu-Ulm
link: https://www.facebook.com/events/2477665785604068/
image: 72578362_10156859013927756_4330583539935346688_o.jpg
teaser: Tropical  Hazel Brugger ist wieder da, und das ist auch gut so. In ihrem
  zweiten Programm spricht sie über die großen Themen dieser Welt. Welches sind
isCrawled: true
---
Tropical

Hazel Brugger ist wieder da, und das ist auch gut so. In ihrem zweiten Programm spricht sie über die großen Themen dieser Welt. Welches sind die besten Drogen? Was hilft bei Schlafstörungen und warum sind Rechtspopulisten die besseren Liebhaber? Gewohnt trocken und unaufdringlich baut Hazel in „Tropical“ ihr Universum auf und öffnet dem Publikum die Tür in ihr Gehirn. Denn dort, wo es wehtut, fängt das echte Lachen doch erst richtig an.

Einlass: 18:30 H
Veranstalter: ROXY.ulm