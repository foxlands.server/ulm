---
id: "295535511556395"
title: Hannes Wittmer im ROXY Sound Garten
start: 2020-09-05 20:00
end: 2020-09-05 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/295535511556395/
teaser: Eigentlich wär Hannes Wittmer gerade in Kanada. Nachdem er 2018 sein Alter Ego
  „Spaceman Spiff“ abgelegt, mit seinem Album „Das große Spektakel“ dem M
isCrawled: true
---
Eigentlich wär Hannes Wittmer gerade in Kanada. Nachdem er 2018 sein Alter Ego „Spaceman Spiff“ abgelegt, mit seinem Album „Das große Spektakel“ dem Musikbusiness den Rücken gekehrt und anschließend zahlreiche Konzerte auf Pay-What-You-Want-Basis gespielt hat, wollte er auf einem Frachtschiff den Atlantik überqueren, um die intensive Zeit für ein paar Monate hinter sich zu lassen und an neuen Ideen zu arbeiten. Wie bei so vielen anderen, ist durch das Coronavirus alles ganz anders gekommen. In dieser unruhigen Zeit, die Stillstand und Eskalation auf sonderbare Weise miteinander verbindet, ist nun „Das Ende der Geschichte“ entstanden, eine Hand voll Lieder über das Zeitgeschehen. In zwanzig eindringlichen Minuten singt und spricht Hannes über Ängste, Ratlosigkeit, Hoffnung, die menschliche Natur und das Abschiednehmen von Altbekanntem. Statt in Kanada lebt er nun auf einem Campingplatz bei Würzburg. Die Wohnung war für die Reise längst gekündigt und so sind sogar Teile der Aufnahmen mit Strom aus der Solaranlage im selbst ausgebauten Campingbus entstanden. Es sind unsichere Zeiten, aber manchmal bieten genau diese den Raum für neue Ideen.

Der Biergarten ist ab 15:00 H geöffnet.
Eintritt frei