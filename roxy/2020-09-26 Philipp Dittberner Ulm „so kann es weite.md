---
id: "497516297842585"
title: Philipp Dittberner Ulm „so kann es weiter gehen … Tour 2020
start: 2020-09-26 20:00
end: 2020-09-26 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/497516297842585/
image: 86969582_3584299504975474_5241646730205724672_o.jpg
teaser: Philipp Dittberner so kann es weiter gehen … Tour 2020  26.09.2020 Ulm Roxy
  Einlass 19.00 Uhr  Beginn 20.00 Uhr  Tickets unter http://bit.ly/sokannUlm
isCrawled: true
---
Philipp Dittberner
so kann es weiter gehen … Tour 2020

26.09.2020 Ulm Roxy
Einlass 19.00 Uhr 
Beginn 20.00 Uhr 
Tickets unter http://bit.ly/sokannUlm

Philipp Dittberner geht im Herbst 2020 auf „so kann es weiter gehen… Tour“ mit im Gepäck: gefühlvolle Texte, inspiriert mal von verliebten, mal von gebrochenen Herzen, Melancholie im alltäglichen Leben und von jugendlicher Leichtigkeit. Dittberner gelingt es kaum greifbare Gefühle in Worte zu fassen und seinen Fans aus der Seele zu sprechen. Ein Erfolgskonzept, das aufgeht; die neue Single „So kann es weitergehen“ läuft in den großen und kleinen Radioanstalten auf Rotation und auch die über zwei Millionen Streams sprechen für das Songwriting des jungen Berliners. Philipp Dittberner holt einen mit seinen Texten dort ab, wo man gerade steht, mit allem Ballast, aller Sehnsucht, aller Euphorie, aller Melancholie.