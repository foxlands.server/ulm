---
id: "667417654005869"
title: 'Vernissage: Andreas Weidmann "lightmoves"'
start: 2020-06-12 19:00
end: 2020-06-12 21:00
address: ROXY.ulm
link: https://www.facebook.com/events/667417654005869/
image: 88090823_10157283131892756_4124625358282555392_o.jpg
teaser: Im Rahmen von Ulm Moves!  In diesen Werken verstehe und verwende ich die
  Fotografie in ihrem ursprünglichen Sinn von Phos = Licht und grafein = schrei
isCrawled: true
---
Im Rahmen von Ulm Moves!

In diesen Werken verstehe und verwende ich die Fotografie in ihrem ursprünglichen Sinn von Phos = Licht und grafein = schreiben, zeichnen.

Die Fotografie ist die Form, mit der der Fotograf als Lichtbildner, Zeichner mit Licht, seine Gedanken, seine Gefühle, seine Arbeit darstellt. Als Werkzeug dazu verwende ich das Medium „Licht“ und seinen ständigen Begleiter,

den Schatten. Beide sind untrennbar, sogar abhängig. Am Licht hängt das Leben, das Werden ebenso das Vergehen.

Die ursprüngliche Motivation und auch Rechtfertigung der Fotografie ist das Festhalten, das EInfrieren eines meist sehr kurzen Augenblicks. Die Realität wird festgehalten, gefangen.

Das Festhalten dieses Augenblicks möchte etwas provozieren. Wobei provozieren in unserer heutigen Kultur meist negativ belegt ist. Mit etwas provozieren verstehe ich auch „zum Nachdenken anregen“. Beim Betrachter entsteht das Bild im Kopf, so wie es bei der Aufnahme vom Auge, vom Kopf gemacht wird, nicht von der Kamera.

Mit meinen Bildern möchte ich beim Betrachter eine Reaktion, eine Bewegung auslösen, provozieren. Diese Reaktion beim Betrachter kann psychisch und auch physisch stattfinden. Selbst ein Ablehnen (psychisch) als auch ein Abwenden (physisch) ist eine provozierte Reaktion, eine Bewegung - auf das Bild.

Die Fotografie als klassischer Übermittler eines Augenblicks als gnadenloser Darsteller der Realität wird in meiner Arbeit weggeführt zur Darstellung und Ergründung von mentalen Zuständen beim Erschaffen und Betrachten.

Das Ursprüngliche des Moments auflösen, das Materielle, Statische auflösen, die Abkehr des Moments und die Hinwendung zur Bewegung, zur Zeitbewegung (-dehnung) findet in meiner Arbeit bei der Aufnahme statt. Dieser Prozess setzt sich beim Betrachten fort.

Ich möchte Abläufe in Gang setzen und untersuchen: Physisch und psychisch weg vom Augenblick, weg von der Schnelligkeit. Hin zu einer Ablösung vom Augenblick.

Wo und wann man ist, hinterlässt man Spuren. Man ist nie wirklich weg!

Ich dehne diesen Augenblick zu einer unbestimmten Zeit in einen unbestimmten Raum.

Die Augen, die Gedanken folgen, es entstehen neue Gedanken, Emotionen. Aus dem Augenblick wird eine unbestimmte Abfolge von „Zeit“.

Licht ist unsichtbar, virtuell, nicht real. Ich mache es sichtbar mit Tänzern, mit Körpern, die selbst Emotionen und Zeit mit einbringen und eine Geschichte von Sein und Nichtsein, von Leben und Vergehen erzählen.

So entsteht statt des kurzen Augenblicks ein vielleicht endloser Prozess.

****

Sa. 13. Juni während Abendveranstaltungen
So. 14. Juni während Abendeveranstaltung
Mo 15. Juni GESCHLOSSEN
Di. 16. Juni Ausstellung ist ab 14:00 H geöffnet bis nach Abendveranstaltung
Mi 17. Juni 14:00—19:00 H
Do. 18. Juni Ausstellung ist ab 14:00 H geöffnet bis nach Abendveranstaltung
Fr. 19. Juni Ausstellung ist ab 14:00 H geöffnet bis nach Abendveranstaltung

Am 12./13./14./16./19. ist der Fotograf abends vor Ort.