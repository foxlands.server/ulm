---
id: "1159751604382799"
title: ChoreoLab - 249 Choreographic Failures (Berblinger Jubiläum)
start: 2020-08-22 19:00
end: 2020-08-22 20:30
address: ROXY.ulm
link: https://www.facebook.com/events/1159751604382799/
image: 104012049_1092814051113108_1185908187885912504_n.jpg
teaser: Wir leben in einer Gesellschaft, die fortwährend den Erfolg sucht und belohnt.
  Dabei steht Triumph oft am Ende einer endlosen Reihe von Misserfolgen.
isCrawled: true
---
Wir leben in einer Gesellschaft, die fortwährend den Erfolg sucht und belohnt. Dabei steht Triumph oft am Ende einer endlosen Reihe von Misserfolgen. Doch der Misserfolg gilt als Makel und wird nur hinter vorgehaltener Hand thematisiert. Diese Tilgung des Misserfolgs aus unserem Alltag übt einen enormen Druck auf uns alle aus, uns selbst immer nur im besten Licht zu präsentieren. In „249 Choreographic Failures“ (übersetzt: choreografische Misserfolge) werden internationale und lokale Künstlerinnen und Künstler ganz bewusst Misserfolge identifizieren, beschreiben, schaffen, teilen und aufführen – in Form choreografierter Bewegung und Tanz. 

Aktuelle Informationen im Hinblick auf ggf. notwendige Corona-Maßnahmen im Rahmen der Veranstaltungsdurchführung finden Sie unter http://movingrhizomes.com/choreolab2020/

Veranstalter: ROXY gGmbH und Moving Rhizomes e.V.
------
Die Veranstaltung findet im Rahmen des Berblinger Jubiläums der Stadt Ulm statt. Weitere Informationen zum Jubiläum unter www.berblinger.ulm.de
------
We live in a society that constantly seeks and rewards success. Even though triumph is often the end result of an endless string of failures, we regard failure as a blemish. In “249 Choreographic Failures” international and local artists address the issue of failure, carefully identifying, describing, creating, sharing and performing failure in the form of dance.
Please check the website in advance for current information due to Corona restrictions: http://movingrhizomes.com/choreolab2020/