---
id: "1093237004362417"
title: popschorle im Oktober
start: 2020-10-31 20:00
end: 2020-10-31 23:00
address: ROXY.ulm
link: https://www.facebook.com/events/1093237004362417/
image: 119103626_10157746824322201_390477550418636093_o.jpg
teaser: "Die nächste #popschorle findet am 31. Oktober statt —  dieses Mal in der
  deutschsprachigen Version! Die popschorle ist eine gemeinsame Veranstaltungsr"
isCrawled: true
---
Die nächste #popschorle findet am 31. Oktober statt —  dieses Mal in der deutschsprachigen Version!
Die popschorle ist eine gemeinsame Veranstaltungsreihe von popbastion ulm und Club Schili

AK: 5,- € *
*Die AK-Einnahmen gehen komplett an die Bands!
19:00 Uhr: Einlass
20:00 Uhr: Beginn

Mit dabei: 

Maffai | Post Punk | Nürnberg
https://www.youtube.com/watch?v=Toe89veQyYM

Das Gegengift zum Erwachsenwerden trägt einen neuen Namen: Maffai.
Das 2018 in Süddeutschland gegründete Quartett bewegt sich soundtechnisch irgendwo zwischen wütendem Indie und filigranem Post-Punk. Aus Melodie und rauer Oberfläche sticht hier insbesondere die Emocore-Sozialisierung der Bandmitglieder hervor. Spätestens live wird dies omnipräsent hörbar. Kein unnötiger Ballast. Kein Schnickschnack. Drei Minuten reichen, um gezielt auf den Punkt zu kommen.



VOLTKID | Deutsch Rock | Ulm
https://www.youtube.com/watch?v=GzFJBbxNg-w

VOLTKID wie BrzBrz! Die fünf Jungs aus Ulm machen den Sound, der dir in deiner Cloudrap verseuchten Playlist fehlt. Was dem Trap der Rap, sind bei VOLTKID die Gitarren. Angeschlossen, angesteckt und ab geht die wilde Fahrt.
Was sind schon Genres? Abwechslungsreich mit eingängigen Riffs, treibenden Drums, gedankenverlorenen Texten und einer markanten Stimme: Tanzt, singt, lacht, weint – macht was Ihr wollt, solange es voller Emotionen ist. VOLTKID möchte euch ein Stückchen von dem Feeling zurückgeben, welches Ihr in den schlechtbeleuchteten und rauchdurchzogenen Kellerclubs gespürt und liegen gelassen habt. Es muss nicht alles sauber und perfekt sein. Endlich raus aus dem Proberaum und rauf auf die Bühnen Deutschlands. Immer mit dem unbändigen Tatendrang die Menschen zu erreichen.



PlusPol | Alternative | Göppingen
https://www.youtube.com/watch?v=ojkbyPZeRbQ

Pluspol ist eine Band, die definitiv mehr unter Strom steht, als dass sie mit ihm schwimmt. Vier Jungs, die lieber verletzt im nächsten Graben liegen, als nie falsch abzubiegen. Vielleicht ein wenig ungestüm, vielleicht ein wenig naiv, aber vor allem ehrlich. 

Diese Ehrlichkeit spiegelt sich in kraftvoller Gitarrenmusik und ausdruckstarken deutschen Texten wider. Ein Gefühl zwischen Melancholie und Leichtigkeit, hervorgerufen durch eine Synthese aus Gesang und Rap-Elementen, der ein gewollter Popcharakter auch nicht mehr von der Hand zu weisen ist.

***
Die Veranstaltung findet wie geplant statt. Bitte beachtet: Unsere Bestuhlung ist mit ausreichend Abständen aufgebaut. Sucht euch entsprechend eurer Gruppengröße geeignete Sitzgruppen, ggf. muss sich eure Gruppe für die Zeit der Vorstellung trennen. Stühle dürfen nicht verschoben werden.
Tragt eure eigene Mund-Nasen-Bedeckung, die ihr auf eurem Sitzplatz abnehmen könnt. Wir werden eure Kontaktdaten aufnehmen (Telefonnummer oder Adresse), für vier Wochen verwahren und nur im Falle eines Coronafalls an das Gesundheitsamt aushändigen.
Danke für Geduld und Rücksicht auf andere Gäste.