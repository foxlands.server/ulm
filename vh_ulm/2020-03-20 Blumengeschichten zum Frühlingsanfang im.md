---
id: "195966668155514"
title: Blumengeschichten zum Frühlingsanfang im Blumenladen
start: 2020-03-20 18:30
end: 2020-03-20 20:00
address: Blaustein-Herrlingen, die blume, Bahnhofstraße 6
link: https://www.facebook.com/events/195966668155514/
image: 86840878_2921652977911873_2203601355682086912_o.jpg
teaser: Blumengeschichten zum Frühlingsanfang im Blumenladen Tine Mehls,
  Geschichtenerzählerin Freitag, 20. März, 18:30 bis 20 Uhr Eintritt EUR 12,00
  Anmeldun
isCrawled: true
---
Blumengeschichten zum Frühlingsanfang im Blumenladen
Tine Mehls, Geschichtenerzählerin
Freitag, 20. März, 18:30 bis 20 Uhr
Eintritt EUR 12,00
Anmeldung erforderlich
Blaustein-Herrlingen, die blume, Bahnhofstraße 6
Nr. 20F 1504026

Ein Märchenabend der besonderen Art erwartet sie! Tine Mehls erzählt Märchen, Sagen und Geschichten rund um Blumen, Gärtner und die Liebe. Und das Ungewöhnliche daran ist, dass Sie den Geschichten mit allen Sinnen lauschen können, denn zu Gehör kommen die Geschichten in einem Blumenladen, umgeben von den Düften und Farben der Pflanzen dort. Freuen Sie sich auf einen gemütlichen Frühlingsabend mit rätselhaften und fröhlichen Geschichten in einem zauberhaften Ambiente, dem Blumenladen "die blume" in Herrlingen bei Blaustein.