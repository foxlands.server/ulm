---
id: "3409745399139001"
title: "vh inklusiv: China – Vortrag in leicht verständlicher Sprache"
start: 2020-11-10 19:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/3409745399139001/
image: 122118307_3571625459581285_5113255427370091827_o.jpg
teaser: China – vh inklusiv Vortrag in leicht verständlicher Sprache Judith Happ
  Dienstag, 10. November, 19 Uhr Eintritt frei EinsteinHaus, Club Orange Nr. 20
isCrawled: true
---
China – vh inklusiv
Vortrag in leicht verständlicher Sprache
Judith Happ
Dienstag, 10. November, 19 Uhr
Eintritt frei
EinsteinHaus, Club Orange
Nr. 20H0704005

Ist China eine Bedrohung oder eine Herausforderung? In den Nachrichten sorgt China seit Jahrzehnten immer wieder für Schlagzeilen. Was wissen wir über dieses Land? Es ist ein Land mit einer jahrtausendealten Geschichte und faszinierenden Landschaften. Über 1,4 Milliarden Menschen leben in diesem Land. Sie sprechen eine Sprache, die kein Alphabet hat..

In dem Vortrag werden die wichtigsten Daten und Fakten gezeigt. Zahlreiche Fotos und spannende Erfahrungen aus dem Alltagsleben bringen Land und Leute näher.