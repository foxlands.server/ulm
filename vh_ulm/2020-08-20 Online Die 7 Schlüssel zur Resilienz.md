---
id: "933309590431790"
title: "Online: Die 7 Schlüssel zur Resilienz"
start: 2020-08-20 18:45
end: 2020-08-20 20:15
link: https://www.facebook.com/events/933309590431790/
image: 101595886_3168362563240912_8344029645376585728_n.jpg
teaser: "Die 7 Schlüssel zur Resilienz – Ihre persönliche Widerstandsfähigkeit
  gegenüber steigenden Anforderungen und permanentem Zeitdruck Dozentin:
  Gabriele"
isCrawled: true
---
Die 7 Schlüssel zur Resilienz – Ihre persönliche Widerstandsfähigkeit gegenüber steigenden Anforderungen und permanentem Zeitdruck
Dozentin: Gabriele Hagmann
Ihr Erfolgsrezept ist Achtsamkeit, Entspannung und Effektivität. Nutzen Sie Ihr Potenzial und Ihre innere Kraft. Wir alle haben unsere Geschichte und wir haben die Kraft, Schwierigkeiten zu überwinden und dabei an unseren Erfahrungen zu wachsen. Je mehr Resilienz, desto einfacher das Selbstmanagement.

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 20.08.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1407006

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €