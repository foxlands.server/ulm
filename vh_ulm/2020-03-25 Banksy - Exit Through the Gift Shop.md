---
id: "1508710919290902"
title: Banksy - Exit Through the Gift Shop
start: 2020-03-25 18:00
end: 2020-03-25 20:00
locationName: Mephisto Ulm
address: Rosengasse 15, 89073 Ulm
link: https://www.facebook.com/events/1508710919290902/
image: 87160172_2921663551244149_547808192248152064_n.jpg
teaser: Banksy - Exit Through the Gift Shop Dokumentarfilm von Banksy Frankreich 2010,
  83 Minuten In Zusammenarbeit mit dem Mephisto-Kino Ulm Mittwoch, 25. Mä
isCrawled: true
---
Banksy - Exit Through the Gift Shop
Dokumentarfilm von Banksy
Frankreich 2010, 83 Minuten
In Zusammenarbeit mit dem Mephisto-Kino Ulm
Mittwoch, 25. März, 18 Uhr
Eintritt EUR 8,50/5,00
OmU (Eglisch mit deutschen Untertiteln)
Ulm, Mephisto-Kino, Rosengasse 15
Nr. 20F 0200002

Banksy ist ein Phantom. Obwohl seine Kunst Fassaden und Mauern auf der ganzen Welt ziert, weiß niemand, wer hinter dem gefeierten Street Art Künstler eigentlich steckt. Bis sich ein verrückter Franzose und selbsternannter Dokumentarfilmer namens Thierry Guetta zum Ziel gesetzt hat, Banksy aufzuspüren. Was ihm durch gute Kontakte und ein bisschen Zufall sogar gelingt. Aber dann kommt alles anders als geplant: Banksy dreht den Spieß um und richtet die Kamera auf Guetta selbst. Der wiederum startet nun selbst eine überraschende Karriere als Künstler. Egal ob Dokumentarfilm,  Spielfilm oder ein Mockumentary – der Film spielt gekonnt  mit Wahrheit und Fiktion und liefert gleichzeitig eine böse Satire auf den Kunstmarkt.