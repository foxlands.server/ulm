---
id: "203681991011376"
title: Literaturtreff am Abend
start: 2020-03-05 18:00
end: 2020-03-05 21:00
address: Blaustein, Stadtbücherei, Marktplatz 2
link: https://www.facebook.com/events/203681991011376/
teaser: Literaturtreff am Abend Petra Harakaidis-Rohwer, Bibliothekarin Donnerstag, 5.
  März, 18 Uhr Eintritt frei Blaustein, Stadtbücherei, Marktplatz 2 Nr. 2
isCrawled: true
---
Literaturtreff am Abend
Petra Harakaidis-Rohwer, Bibliothekarin
Donnerstag, 5. März, 18 Uhr
Eintritt frei
Blaustein, Stadtbücherei, Marktplatz 2
Nr. 20F 1504049

Haben Sie schon einmal den Wunsch verspürt, sich über Ihre/n Lieblingsautor/in zu unterhalten? Möchten Sie wissen, welche Neuerscheinungen in Ihrer Lieblingsbuchsparte zu erwarten sind? Suchen Sie Literaturtipps für Buchgeschenke? Oder haben Sie spezielle Bücherwünsche? In entspannter Atmosphäre können Sie sich mit der Bibliothekarin Frau Harakaidis-Rohwer über Literatur austauschen.
