---
id: "1213941422270382"
title: "Online: Selbstpräsentation: Was ist meine »Marke«."
start: 2020-09-03 18:45
end: 2020-09-03 20:15
link: https://www.facebook.com/events/1213941422270382/
image: 101437826_3168358409907994_3597892308562870272_n.jpg
teaser: "Online: Selbstpräsentation: Was ist meine »Marke«. Für den ersten Eindruck
  gibt es keine zweite Chance Dozentin: Gabriele Hagmann Werden Sie authentis"
isCrawled: true
---
Online: Selbstpräsentation: Was ist meine »Marke«. Für den ersten Eindruck gibt es keine zweite Chance
Dozentin: Gabriele Hagmann
Werden Sie authentisch sichtbar und erleben Sie mit Ihren Sinnen ein nachhaltiges Ergebnis. Lernen Sie Ihr »Überheblichkeits-Training« und treten im Alltag sprachlich eindrucksstark auf. Lernen Sie in dünner Luft zu atmen. Reagieren Sie majestätisch!

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 03.09.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1405003

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €