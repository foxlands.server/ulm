---
id: "2896839413704307"
title: "Online: Geld und Wissen – Sicher das eigene Geld anlegen"
start: 2020-05-07 18:00
end: 2020-05-07 20:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2896839413704307/
image: 94727018_3085097731567396_7267262828665372672_n.jpg
teaser: "Geld und Wissen – Sicher und souverän das eigene Geld anlegen Vortrag /
  Dozent: Jan Neynaber  In 9 Schritten zur finanziellen Mündigkeit! Der Vortrag"
isCrawled: true
---
Geld und Wissen – Sicher und souverän das eigene Geld anlegen
Vortrag / Dozent: Jan Neynaber

In 9 Schritten zur finanziellen Mündigkeit! Der Vortrag gibt eine Einführung über die Funktionsweise der Finanzmärkte. Folgende Inhalte werden vorgestellt: Beschaffenheit der Finanzmärkte – Beurteilung von Anlagemöglichkeiten – Investieren ohne abgeleitete Finanzprodukte – Aktienfondauswahl entsprechend der eigenen Persönlichkeit – Strukturiert Aktienideen finden – Risiken erkennen und vermeiden.

Jan Neynaber studierte BWL, Politik und Psychologie. Er berät seit 20 Jahren Versicherungen, Pensionskassen und Aktienfonds täglich bei der Aktienanlage.

Datum | Uhrzeit
1 Abend (2,67 Unterrichtsstunden) | Donnerstag
Beginn: 07.05.2020 | 18:00 bis 20:00 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F0805005

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
10 - 30 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/onlineseminar-geld-und-wissen-passend-und-sicher-das-eigene-geld-anlege/20F0805005