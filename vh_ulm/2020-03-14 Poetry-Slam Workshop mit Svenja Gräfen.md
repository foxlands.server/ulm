---
id: "201276944324184"
title: Poetry-Slam Workshop mit Svenja Gräfen
start: 2020-05-29 14:00
end: 2020-05-29 17:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/201276944324184/
image: 87067865_2921642624579575_2020547371330437120_o.jpg
teaser: Poetry-Slam Workshop mit Svenja Gräfen für Jugendliche ab 16 Jahren und
  Erwachsene 1 Vormittag (4 UStd.) Freitag, 29. Mai 14 - 17 Uhr Gebühr EUR 29,00
isCrawled: true
---
Poetry-Slam Workshop mit Svenja Gräfen
für Jugendliche ab 16 Jahren und Erwachsene
1 Vormittag (4 UStd.)
Freitag, 29. Mai 14 - 17 Uhr
Gebühr EUR 29,00
Anmeldung erforderlich (info@vh-ulm.de)
EinsteinHaus, Atelier
Nr. 20F 0910033

Was ist Poetry Slam und wie funktioniert das Schreiben für die Bühne? Mit welchen Techniken kann man gute Texte aus sich herauskitzeln? Wann ist ein Text gut, und gibt es auch schlechte Texte? Wie soll man überhaupt anfangen? Wie kommen die Ideen? All diesen Fragen widmet sich der Workshop. Mit verschiedenen Übungen und Kreativitätstechniken tasten sich die Teilnehmer/innen ganz neu ans Schreiben heran und bekommen hilfreiche Tipps und Tricks wie sie ihre Text gut performen. Es sind keine Vorkenntnisse nötig. Bitte Schreimaterial (Block oder Papier und Stifte) mitbringen. Die 29-jährige Autorin, (Netz-)Feministin und Poetry-Slammerin Svenja Gräfen ist auf unzähligen Poetry Slams und Lesebühnen aufgetreten. 2011 erreichte sie den vierten Platz im Finale der Deutschsprachigen Meisterschaften im Poetry Slam. Am Vorabend liest Gräfen aus ihrem Roman »Freiraum«, siehe Seite Kurs-Nr. 20F0118031.