---
id: "2858633834184055"
title: Albert Einstein und Carl Laemmle
start: 2020-04-21 19:30
end: 2020-04-21 22:30
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2858633834184055/
image: 89825673_2974492655961238_6182448411472560128_n.jpg
teaser: Albert Einstein und Carl Laemmle. Retter von jüdischen Verwandten und Freunden
  Dr. Christof Rieber, Historiker und Autor In Zusammenarbeit mit dem Dok
isCrawled: true
---
Albert Einstein und Carl Laemmle. Retter von jüdischen Verwandten und Freunden
Dr. Christof Rieber, Historiker und Autor
In Zusammenarbeit mit dem Dokumentationszentrum Oberer Kuhberg und dem Albert Einstein Discovery-Center
Dienstag, 21. April, 19:30 Uhr
Eintritt EUR 6,00
EinsteinHaus, Club Orange
Nr. 20F 0108114

Zwei schwäbische Stars treffen einander in den USA und versuchen die US-Einwanderungspolitik zu mildern. Der Laupheimer Carl Laemmle hat die Filmfabrik Hollywood als Pionier maßgeblich geschaffen. Dort trifft ihn Albert Einstein. Der unterhaltsame Vergleich schärft das Bild der beiden prominenten jüdischen Deutschen.