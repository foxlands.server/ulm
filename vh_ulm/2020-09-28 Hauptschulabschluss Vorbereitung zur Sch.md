---
id: "3564750466883158"
title: "Hauptschulabschluss: Vorbereitung zur Schulfremdenprüfung"
start: 2020-09-28 14:00
end: 2020-09-28 17:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/3564750466883158/
teaser: "Vorbereitungskurs zur Schulfremdenprüfung für den Hauptschulabschluss
  Dozenten: Bärbel Schmidt, Wolfram Krug, Claudia Clemens  Der Vorbereitungskurs
  i"
isCrawled: true
---
Vorbereitungskurs zur Schulfremdenprüfung für den Hauptschulabschluss
Dozenten: Bärbel Schmidt, Wolfram Krug, Claudia Clemens

Der Vorbereitungskurs ist für alle diejenigen gedacht, die die Schule nach neun Pflichtschuljahren verlassen, ohne einen anerkannten Abschluss erreicht zu haben. Damit fehlt aber die Grundvoraussetzung für sehr viele Berufswege. Die Vorbereitung findet in den Fächern Deutsch, Mathematik, Gemeinschaftskunde, Geschichte und wahlweise Englisch statt. Weiter wird umfassend auf die Hausarbeit und Präsentation vorbereitet. Der Kurs läuft über 10 Monate und schließt mit der Schulfremdenprüfung an einer staatlichen Ulmer Schule ab.
Außerdem kann über das »9+3-Modell« die sog. »Mittlere Reife« erlangt werden. Dazu muss der Notendurchschnitt aus Hauptschule, Berufsschule und Gesellenbrief mindestens 2,5 betragen. Das »9+3-Modell« hat also auch für diejenigen Bedeutung, die bereits eine Lehre beenden konnten und noch keinen Hauptschulabschluss besitzen.
Kursumfang Elf Unterrichtsstunden (mit Englisch vierzehn) pro Woche.

Anmeldung und nähere Informationen im EinsteinHaus (2. OG).
Bärbel Schmidt, Telefon 0731 1530-26
schmidt@vh-ulm.de

Öffnungszeiten
Montag, Dienstag und Donnerstag
jeweils von 14 bis 17 Uhr
(Schulferien ausgenommen)

Datum | Uhrzeit
141 Termine (611 Unterrichtsstunden) | Montag, Dienstag, Mittwoch, Donnerstag
Beginn: 28.09.2020 | 14:00 bis 17:15 Uhr

Ort
EinsteinHaus, Seminarraum 6

Kurs-Nummer
20H1201101

Beratung zum Kursinhalt
Bärbel Schmidt 
Tel. 0731 1530-26 
schmidt@vh-ulm.de

Plätze
11 - 11 Teilnehmer/innen 
 Persönliche Beratung erforderlich

Preis
290,00 €