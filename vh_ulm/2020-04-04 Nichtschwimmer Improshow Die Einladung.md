---
id: "634783503923551"
title: "Nichtschwimmer Improshow: Die Einladung"
start: 2020-04-04 19:30
end: 2020-04-04 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/634783503923551/
image: 87105276_1062141927501015_9094251966883692544_o.jpg
teaser: Wir freuen uns Euch zu unserer neuen Improtheatershow im Club Orange
  einzuladen. Unser besonderer Gast des Abends ist Jeroen Tessers aus den
  Niederlan
isCrawled: true
---
Wir freuen uns Euch zu unserer neuen Improtheatershow im Club Orange einzuladen. Unser besonderer Gast des Abends ist Jeroen Tessers aus den Niederlanden.

Es ist spät abends. Du öffnest die Haustür und findest einen schlichten Brief auf den Boden. Jemand hat ihn unter der Tür hindurchgeschoben. Auf der Einladung steht, dass Du am 04. April um 19:00 Uhr zum Club Orange kommen sollst. Wirst Du erpresst? Oder ist es einfach nur eine Einladung zu einem lustigen Improtheaterabend? Finde es heraus. Bring Ideen und gute Laune mit oder lass Dich davon anstecken. Einlass ist um 19:00 Uhr! Wir freuen uns auf einen schönen gemeinsamen Abend.

Einlass 19:00 Uhr, Beginn 19:30

Eintritt frei

Jeroen Tessers:
www.troeba.nl

Wir sind die Improtheater Gruppe "Die Nichtschwimmer" des Musischen Zentrum (MUZ) der Universität Ulm. Die MUZ hat viele spannende und kreative Gruppen in welchen auch Du Dich ausleben kannst. Hier noch ein Link zu anderen MUZ Veranstaltungen:
www.uni-ulm.de/einrichtungen/muz/musisches-zentrum/termine-konzerte/

In Kooperation mit der Volkshochschule Ulm.
www.vh-ulm.de/cms/index.php?id=64&kathaupt=20&katid=1007&knr=20F0118113