---
id: "687484685397293"
title: "Online-Veranstaltung: Der Code der Macht"
start: 2020-06-25 19:30
end: 2020-06-25 22:30
link: https://www.facebook.com/events/687484685397293/
image: 96712706_3111301815613654_2276185989451874304_o.jpg
teaser: "Online-Veranstaltung: Der Code der Macht: Wer beherrscht den digitalen Raum?
  Adrian Lobe Donnerstag, 25. Juni, 19:30 Uhr Eintritt frei Webinar Nr. 20F"
isCrawled: true
---
Online-Veranstaltung: Der Code der Macht: Wer beherrscht den digitalen Raum?
Adrian Lobe
Donnerstag, 25. Juni, 19:30 Uhr
Eintritt frei
Webinar
Nr. 20F0109314

Der Journalist und Buchautor Adrian Lobe analysiert in seinem Vortrag diese tektonischen Machtverschiebungen in der digitalen Gesellschaft, er erklärt die Fallstricke smarter Gadgets, die nicht die digitalen Diener sind, als die sie vermarktet werden, und er zeigt Lösungen auf, wie man sich aus dem Klammergriff der Datenkraken befreien kann.

Adrian Lobe studierte in Tübingen, Paris und Heidelberg Politik- und Rechtswissenschaft und arbeitet für verschiedene Zeitungen im deutschsprachigen Raum. 2016 wurde er mit dem Preis des Forschungsnetzwerks Surveillance Studies ausgezeichnet, 2017 erhielt er den ersten Journalistenpreis der Stiftung Datenschutz.

Nach erfolgter Anmeldung erhalten Sie im Vorfeld der Veranstaltung eine E-Mail mit den Zugangsdaten.
