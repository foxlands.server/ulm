---
id: "167690754531813"
title: "Online-Veranstaltung: Homo urbanus – die Zukunft der Städte"
start: 2020-05-05 20:00
end: 2020-05-05 23:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/167690754531813/
image: 94740004_3079060542171115_6562422604781584384_n.jpg
teaser: "Online-Veranstaltung: Homo urbanus – Ein evolutionsbiologischer Blick in die
  Zukunft der Städte Prof. Dr. Elisabeth Oberzaucher  Die Evolutionsgeschic"
isCrawled: true
---
Online-Veranstaltung: Homo urbanus – Ein evolutionsbiologischer Blick in die Zukunft der Städte
Prof. Dr. Elisabeth Oberzaucher

Die Evolutionsgeschichte hat uns Menschen nicht nur hinsichtlich der Anatomie geprägt, sondern auch hinsichtlich der Wahrnehmung, Kognition und des Verhaltens. Da wir den größten Teil unserer Evolution in der Savanne verbracht haben und Städte erst seit kurzer Zeit unsere Lebensbedingungen prägen, passen wir als biologische Wesen nicht so gut in die Stadt. Wie müssen Städte gebaut werden, damit die menschlichen Bedürfnisse, und nicht nur verkehrs- und wirtschaftspolitische Überlegungen berücksichtigt werden? Denn nur lebenswerte Städte funktionieren auch nachhaltig.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben!) und ein entsprechendes Endgerät. Sie können auch ohne Kamera und Mikrofon teilnehmen.

Hinweise zur Verwendung finden Sie unter https://www.vh-ulm.de/digitale-angebote/anleitungen-online-kurse, zum Datenschutz unter https://www.vh-ulm.de/cms/index.php?id=202.

Termin: Dienstag, 05.05.2020 | 20:00 Uhr
Kurs-Nummer 20F0101214

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
5 - 90 Teilnehmer/innen 
Eine Anmeldung ist nicht nötig.

Eintritt frei