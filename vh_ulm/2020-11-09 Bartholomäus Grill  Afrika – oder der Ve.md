---
id: "268571167825821"
title: "Bartholomäus Grill:  Afrika – oder der Versuch, einen Kontinent zu verstehen"
start: 2020-11-09 20:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/268571167825821/
image: 122162249_3571621036248394_5656567587541327219_n.jpg
teaser: "Bartholomäus Grill:  Afrika – oder der Versuch, einen Kontinent zu verstehen
  Bartholomäus Grill Montag, 9. November, 20 Uhr Eintritt 6,00 € EinsteinHa"
isCrawled: true
---
Bartholomäus Grill:  Afrika – oder der Versuch, einen Kontinent zu
verstehen
Bartholomäus Grill
Montag, 9. November, 20 Uhr
Eintritt 6,00 €
EinsteinHaus, Club Orange
Nr. 20H0109308

In Zusammenarbeit mit der Eine-Welt-Regionalpromotorin

Seit 1980 versucht der mehrfach preisgekrönte Afrika-Korrespondent des SPIEGEL einen Kontinent zu verstehen, in dem Europa dreimal Platz finden würde. Ein Kontinent mit 1,3 Milliarden Einwohnern, von denen 60 Prozent jünger als 25 Jahre sind. Ein Kontinent, bestehend aus 55 Staaten, Tausenden von großen Völkern und kleinen Ethnien, Kulturen und Religionen mit über 2000 verschiedenen Sprachen. Ist es eine Anmaßung, Afrika verstehen zu wollen? Dann geht es vielleicht eher um Momentaufnahmen von einem rauen und sanften, brutalen und feinfühligen, niederschmetternden und beglückenden Erdteil.