---
id: "547168089417685"
title: HfG-Sommerkonzert mit Natalia Mateo & Band
start: 2020-07-24 19:30
end: 2020-07-24 22:00
address: Hochschule für Gestaltung Ulm
link: https://www.facebook.com/events/547168089417685/
image: 75369306_3519473608070696_1037104593323950080_o.jpg
teaser: Sommerkonzert auf der HfG-Terrasse, bei schlechtem Wetter innen. Wir setzen
  die erfolgreiche Reihe unserer Sommerkonzerte auf der genialen HfG-Terrass
isCrawled: true
---
Sommerkonzert auf der HfG-Terrasse, bei schlechtem Wetter innen.
Wir setzen die erfolgreiche Reihe unserer Sommerkonzerte auf der genialen HfG-Terrasse fort.
(Einlass: 18:30)

Die Sommerkonzerte an der legendären (und schönsten) Theke Ulms werden langsam Kult. Bisher hatten wir bis auf eine Ausnahme jedes Mal schönes Wetter, so dass man beim Konzert auf der Terrasse den herrlichen Blick übers Donautal genießen konnte.

Heuer ist Natalia Mateo unser Gast!

Verstörend und intim. Schön, wütend, rau. Weich, verletzlich und verletzend. Reif. Zerbrechlich. Betörend.
Natalia Mateo hat Wurzeln und ist wie der Jazz doch gleichzeitig entwurzelt. Die polnische Sängerin singt und säuselt, definiert aber gleichzeitig Dissonanzen, sie liebt den Kitsch genauso wie sein Gegenteil. Eigenkompositionen treffen da auf traditionelle slawische Folksongs, Weltmusik auf Pop und Jazz. -  Von 2014 – 2017 war sie jedes Jahr bei uns zu Gast und hat sich eine treue Fangemeinde erspielt. Dann löste sie leider ihre Band auf. Für unser diesjähriges Sommerkonzert konnten wir sie wieder verpflichten, und sowohl Publikum als auch Sängerin freuen sich aufs Wiedersehen und Wiederhören. Allerdings stand bis Redaktionsschluss des Programmheftes noch nicht fest, welche Begleitmusiker sie mitbringen wird.

Und Achtung: Unsere HfG-Sommerkonzerte sind in der Regel ausverkauft. Es empfiehlt sich, Karten im Vorverkauf zu besorgen:

(Bei schlechtem Wetter findet das Konzert innen statt.)

Eintritt:  18 €, ermäßigt: 15 €, Schüler und Studenten 10 €, im Vorverkauf zuzüglich Gebühr (bis 16 Jahre freier Eintritt).

(Veranstalter: KunstWerk e.V. in Zusammenarbeit mit der vh Ulm und der Stiftung HfG)