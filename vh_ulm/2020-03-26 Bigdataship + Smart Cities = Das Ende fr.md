---
id: "2496870370568784"
title: Bigdataship + Smart Cities = Das Ende freier Räume?
start: 2020-03-26 20:00
end: 2020-03-26 23:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2496870370568784/
image: 87254588_2921667837910387_329625094856900608_o.jpg
teaser: vh Architektur Bigdataship + Smart Cities = Das Ende freier Räume? Wilfried
  Wang Donnerstag, 26. März, 20 Uhr Eintritt EUR 8,00/6,00 Zertifizierte For
isCrawled: true
---
vh Architektur
Bigdataship + Smart Cities = Das Ende freier Räume?
Wilfried Wang
Donnerstag, 26. März, 20 Uhr
Eintritt EUR 8,00/6,00
Zertifizierte Fortbildung AKBW
EinsteinHaus, Club Orange
Nr. 20F 0101303

Welche Veränderungen birgt aus Sicht der Architektur die Digitalisierung der Städte, die auch unter den Begriff „Smart Cities“ breit diskutiert werden. Kann der öffentliche Raum in Zeiten von Digitalisierung noch Freiraum für die Bewohner/innen einer Stadt bieten? Für den Architekten Wilfried Wang vom Büro Hoidn Wang Partner steht fest, dass es durch die Datenpreisgabe im Internet und im öffentlichen Raum, sowie deren Verknüpfung keine individuelle Privatsphäre und damit auch keine freien Räume mehr geben wird. In dem Vortrag wird er auch darauf eingehen welche Entscheidungen und Projekte aus Sicht des Architekten und Städteplaners dieser Entwicklung entgegenwirken können.