---
id: "2928098873918212"
title: Konzertlesung - Hommage an Mercedes Sosa
start: 2020-07-10 19:00
end: 2020-07-10 21:30
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2928098873918212/
image: 89035618_2974496665960837_2135753714357501952_o.jpg
teaser: Konzertlesung - Hommage an Mercedes Sosa Freitag, 10. Juli, 19 Uhr Eintritt
  EUR 10,00/8,00 EinsteinHaus, Club Orange Nr. 20F 0120026  Sie war für viel
isCrawled: true
---
Konzertlesung - Hommage an Mercedes Sosa
Freitag, 10. Juli, 19 Uhr
Eintritt EUR 10,00/8,00
EinsteinHaus, Club Orange
Nr. 20F 0120026

Sie war für viele »die Stimme Lateinamerikas«:  Mercedes Sosa. Die argentinische Vokalistin war eine Ikone, eine Symbolfigur, ein Vorbild des sozialen Engagements der Künstler/innen für die Zukunft ihres Volkes.  Bárbara Moreno und der Gitarrist Frank Sattelberger  präsentieren eine Konzert-Lesung über das Leben und den Gesang von Mercedes Sosa unter dem Motto »Gracias a la vida«. In Erinnerung an eine der wichtigsten lateinamerikanischen Folklore-Interpretinnen, werden sie mit Dias, Musik und Gedichten von dieser herrausragenden Sängerin erzählen.