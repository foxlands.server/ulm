---
id: "253709365733463"
title: "Online: Die drei Schritte Ihrer persönlichen Karrieregestaltung"
start: 2020-07-23 18:45
end: 2020-07-23 20:15
link: https://www.facebook.com/events/253709365733463/
image: 101392426_3168373696573132_6006329545173499904_n.jpg
teaser: "Online: Die drei Schritte Ihrer persönlichen Karrieregestaltung Dozentin:
  Gabriele Hagmann Der Wandel der Arbeitswelt verändert die Berufsbilder, Arbe"
isCrawled: true
---
Online: Die drei Schritte Ihrer persönlichen Karrieregestaltung
Dozentin: Gabriele Hagmann
Der Wandel der Arbeitswelt verändert die Berufsbilder, Arbeitsmärkte und Karriereziele. Dabei bieten die aktuell unsicheren Zeiten auch spannende Chancen. Setzen Sie mit Ihren individuellen Stärken, Fähigkeiten und Kompetenzen neue Optionen für Ihre Karriere. Seien Sie vorbereitet für die Veränderung der Arbeitswelt von morgen!

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 23.07.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1416001

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €