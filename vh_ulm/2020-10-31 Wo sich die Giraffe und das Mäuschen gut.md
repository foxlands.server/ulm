---
id: "635910007049689"
title: Wo sich die Giraffe und das Mäuschen guten Tag sagen
start: 2020-10-31 10:00
end: 2020-10-31 11:30
address: kontiki - Kunstschule für Kinder und Jugendliche
link: https://www.facebook.com/events/635910007049689/
image: 121467734_3552144478196050_6000214159083830356_n.jpg
teaser: "Wo sich die Giraffe und das Mäuschen guten Tag sagen  Dozentin: Sarah
  Percoco  Wir lassen den grosse Giraffenpinsel durch das Gras wandern und
  wollen"
isCrawled: true
---
Wo sich die Giraffe und das Mäuschen guten Tag sagen

Dozentin: Sarah Percoco

Wir lassen den grosse Giraffenpinsel durch das Gras wandern und wollen mal sehen wie die knalligen Fröschchen über den Teich hüpfen. Vielleicht erspüren unsere Fingerlein noch andere bunte Tierchen!

Bei diesem Malspiel findet das Erste kennen lernen diverser Maltormen statt, wobei die Kindern die Spuren ihres eigenen Handelns und die Freude an Farbe und anderen altersgemäßen Materialien entdecken können.

Mit Elternteil.

Datum | Uhrzeit
3-mal (6 Unterrichtsstunden)
Samstag, 17. Oktober, 10:00 - 11:30 Uhr
Samstag, 24. Oktober, 10:00 - 11:30 Uhr
Samstag, 31. Oktober, 10:00 - 11:30 Uhr

Ort
Ulm, kontiki, Ästhetische Frühbildung

Kurs-Nummer
20H0380200

Zielgruppe
2 bis 3 Jahre

Beratung zum Kursinhalt
Mirtan Teichmüller 
Tel. 0731 1530-29 
teichmueller@kontiki.vh-ulm.de

Plätze
6 - 8 Teilnehmer/innen 
 noch freie Plätze

Preis
39,00 € (einschl. Material)