---
id: "1231484457231929"
title: "Philosophie am Abend: Gewissheit"
start: 2020-09-25 20:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/1231484457231929/
teaser: "Philosophie am Abend: Gewissheit Dozent: Dr. Martin Böhnisch In
  Zusammenarbeit mit der Buchhandlung Aegis Ulm  Auf Gewissheiten können wir
  bauen. Sie"
isCrawled: true
---
Philosophie am Abend: Gewissheit
Dozent: Dr. Martin Böhnisch
In Zusammenarbeit mit der Buchhandlung Aegis Ulm

Auf Gewissheiten können wir bauen. Sie dienen uns als Orientierung in unsicheren, aber auch in sicheren Zeiten. Wo soll es lang gehen? Was ist zu tun? Wie sollen wir uns verhalten? Für den, der sich einer Sache gewiss ist, ist dies kein Problem: Dort muss man drehen, hier geht es weiter, das darf man nicht übersehen, bei diesen Dingen sollte man aufpassen. Gewissheit bringt uns zum Handeln, zum Urteilen. Sie lässt uns souverän und mächtig erscheinen.

Auf der anderen Seite kennen wir den Zustand, wenn Gewissheiten ins Wanken kommen. Wir werden mit einer neuen Situation konfrontiert, mit einer entgegengesetzten Haltung, mit Tatsachen, die unseren Annahmen widersprechen oder geraten in eine persönliche Krise, weil das, von dem wir überzeugt waren, sich als Irrtum herausstellte.

Warum eigentlich? Von welcher Art des Wissens sind unsere Gewissheiten und auf welchem Wissen bauen sie auf? Wo beginnt Gewissheit und wo hat der Zweifel ein Ende? Diese philosophischen Fragen haben nicht nur eine existenzielle, sondern auch politische Dimension. Denn Politiker wollen und müssen Bürgern Sicherheit bieten. Im Vortrag wollen wir dem Phänomen der Gewissheit auf den Grund gehen. Eingeladen sind alle philosophisch Interessierten und die, die es noch werden wollen.

Auf Grund der Abstandsregeln ist die Zahl der Sitzplätze begrenzt – wir bitten um Verständnis für diese Maßnahme im Sinne unser aller Gesundheit.

Datum | Uhrzeit
Termin: Freitag, 25.09.2020 | 20:00 Uhr
Ort
EinsteinHaus, Club Orange

Kurs-Nummer
20H0710007

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
 Eine Anmeldung ist nicht nötig.

Preis
Eintritt frei