---
id: "339079887070469"
title: "Online: Akzeptanz – die Kunst Dinge anzunehmen, die Du nicht ändern kannst"
start: 2020-05-28 18:45
end: 2020-05-28 20:15
link: https://www.facebook.com/events/339079887070469/
image: 99353178_3147747225302446_4949859949334233088_n.jpg
teaser: "Online: Akzeptanz – die Kunst Dinge anzunehmen, die Du nicht ändern kannst
  Dozentin: Gabriele Hagmann »Ich hatte einfach nur Glück«. »Ich bin ein Pech"
isCrawled: true
---
Online: Akzeptanz – die Kunst Dinge anzunehmen, die Du nicht ändern kannst
Dozentin: Gabriele Hagmann
»Ich hatte einfach nur Glück«. »Ich bin ein Pechvogel«. Kennen Sie Ihren inneren Monolog? Setzen Sie in Ihrem Leben auf Glück oder Zufall? Setzen Sie Ihre Ziele mit Leidenschaft um, finden Sie Ihre innere Kraftquelle und nutzen Sie Ihre eigenen Talente.

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 28.05.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1407004

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
Gebührenfrei