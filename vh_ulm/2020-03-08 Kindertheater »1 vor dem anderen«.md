---
id: "506951883522645"
title: Kindertheater »1 vor dem anderen«
start: 2020-03-08 15:00
end: 2020-03-08 17:00
address: Langenau, Pfleghofsaal
link: https://www.facebook.com/events/506951883522645/
teaser: ab 4 Jahre Kindertheater »1 vor dem anderen« Compagnie nik Sonntag, 8. März,
  15 Uhr Eintritt EUR 6,00 Vorverkauf StadtBücherei Langenau Langenau, Pfle
isCrawled: true
---
ab 4 Jahre
Kindertheater »1 vor dem anderen«
Compagnie nik
Sonntag, 8. März, 15 Uhr
Eintritt EUR 6,00
Vorverkauf StadtBücherei Langenau
Langenau, Pfleghofsaal
Nr. 20F 1508020

Valentin und Waldemar haben eine Geschichten-Schatzkiste gefunden. Stück für Stück spuckt sie auf Papierstreifen eine Geschichte aus und verspricht demjenigen, der sie spielt, am Ende einen Schatz. Das Märchen »Das hässliche Entlein« von Andersen scheint es zu werden.
Mit clownesker Spiellust und Freude an schräger Kostümierung stürzen sich die beiden Freunde in die Erzählung. Doch die Ungerechtigkeiten in der Geschichte stört sie dabei immer mehr, und das normale Ende der Geschichte wollen sie wirklich nicht übernehmen!
Auf der Suche nach dem passenden Ende des Märchens machen sie eine wichtige Entdeckung. Es gibt einen S(ch)atz, der uns alle verbindet: »Die Würde des Menschen ist unantastbar«.
Eine Erfahrung, hier von Niels Klaunick und Dominik Burki so leicht und komödiantisch gespielt, dass sie schon für die Kleinsten nach zu vollziehen ist.