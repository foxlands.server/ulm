---
id: "827861384709724"
title: Die Sinfonisierung der Opernmusik
start: 2020-11-14 16:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/827861384709724/
image: 121968447_3565585340185297_2850967008936135506_n.jpg
teaser: Die Sinfonisierung der Opernmusik Vortrag von Roland Bauer In Zusammenarbeit
  mit dem Richard-Wagner-Verband  Von Beethovens sinfonischem Werk zu Richa
isCrawled: true
---
Die Sinfonisierung der Opernmusik
Vortrag von Roland Bauer In Zusammenarbeit mit dem Richard-Wagner-Verband

Von Beethovens sinfonischem Werk zu Richard Wagners Tristan und Isolde - Die Sinfonisierung der Opernmusik. Die Sinfonie wurde von Beethoven zur Vollendung geführt. Daher belegte Wagner sie mit dem Urteil vom »Ende« und sah die Weiterentwicklung nur über eine Sinfonisierung der Opernmusik im Musikdrama als möglich. Mit »Tristan und Isolde« hat Wagner diesen Entwicklungsprozess selbst zu einem absoluten Höhepunkt - vielleicht sogar zu einem erneuten »Ende« - geführt. Von Beethoven ausgehend soll die Sinfonisierung der Opernmusik in »Tristan und Isolde« dargestellt werden, wobei der Halbtonschritt dafür exemplarisch steht. Roland Bauer referiert seit vielen Jahren in unterschiedlichsten Einrichtungen, bei Richard Wagner Verbänden und während der Festspiele im Kunstmusem Bayreuth. Der Schwerpunkt seiner Vortragstätigkeit bildet das gesamte Werk Wagners und dabei insbesondere die musiktheoretische Analyse.

Mitglieder, Student/innen, Schüler/innen EUR 5,00

Datum | Uhrzeit
Termin: Samstag, 14.11.2020 | 16:00 Uhr
Ort
EinsteinHaus, Club Orange

Kurs-Nummer
20H0120191

Informationen zur Veranstaltung
Tanja Leuthe 
Tel. 0731 1530-34 
leuthe@vh-ulm.de

Plätze
 Keine Online-Anmeldung möglich/nötig

Preis
Eintritt 10,00 €