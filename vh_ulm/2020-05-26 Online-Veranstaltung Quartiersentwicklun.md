---
id: "246672000110605"
title: "Online-Veranstaltung: Quartiersentwicklung in Ulm"
start: 2020-05-26 20:00
link: https://www.facebook.com/events/246672000110605/
image: 99408242_3147737988636703_3853079323758034944_n.jpg
teaser: "Online-Veranstaltung: Quartiersentwicklung in Ulm In Zusammenarbeit mit der
  Zukunftsstadt Ulm  Es wird an diesem Abend besonders um die sozialen Aspek"
isCrawled: true
---
Online-Veranstaltung: Quartiersentwicklung in Ulm
In Zusammenarbeit mit der Zukunftsstadt Ulm

Es wird an diesem Abend besonders um die sozialen Aspekte der Stadtentwicklung und die Frage gehen, inwieweit Bauen, architektonische Planung und digitale Weiterentwicklung von Quartieren sozialen und gesellschaftlichen Zielen folgen oder folgen sollte. Nach einer allgemeine Einführung blicken wir auf die Stadt Ulm.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben!) und ein entsprechendes Endgerät. Sie können auch ohne Kamera und Mikrofon teilnehmen.

Hinweise zur Verwendung finden Sie unter https://www.vh-ulm.de/digitale-angebote/anleitungen-online-kurse, zum Datenschutz unter https://www.vh-ulm.de/cms/index.php?id=202.

Datum | Uhrzeit
Termin: Dienstag, 26.05.2020 | 20:00 Uhr
Ort
Webinar

Kurs-Nummer
20F0101220

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
1 - 80 Teilnehmer/innen 
 Eine Anmeldung ist nicht nötig.

Preis
Eintritt frei