---
id: "288553422153268"
title: Online-Kurs Fit an der Nähmaschine
start: 2020-05-19 18:00
end: 2020-05-19 20:00
link: https://www.facebook.com/events/288553422153268/
image: 96821335_3119004921510010_6514993182147608576_o.jpg
teaser: "Online-Kurs Fit an der Nähmaschine Nähmaschinen-Check Dozentin: Petra
  Mache  Sie haben vor Jahren schon einmal genäht und wieder Lust was
  Individuelle"
isCrawled: true
---
Online-Kurs Fit an der Nähmaschine
Nähmaschinen-Check
Dozentin: Petra Mache

Sie haben vor Jahren schon einmal genäht und wieder Lust was Individuelles anzufertigen? An diesem Abend lernen Sie die Funktionen einer Nähmaschine kennen:
Faden richtig einfädeln, unterschiedliche Nutzstiche einstellen, Schnittteile aneinander nähen und Stoffe absteppen. Dazu kommt eine kleine Stoffkunde: Welcher Stoff eignet sich für mein Wunschprojekt? Was ist beim Zuschnitt zu beachten? Wie ist das mit dem Fadenlauf?
Nach kurzen Nähübungen fertigen wir ein kleines Wärmekissen mit Applikation.

Bitte für Webinar bereitstellen: Nähmaschine mit Ersatznadeln, Stecknadeln, Schere, Maßband, Stoffkreide oder Bleistift, Baumwollstoffe zum Probenähen und für kleines Projekt.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse, ein Endgerät mit Kamera und Mikrofon (Rechner, Laptop, Tablet, Smartphone ...) und eine Internetverbindung.
Sie erhalten für den Kurstermin von uns eine Einladung per E-Mail für den Unterricht per »Zoom«.

Rechtliche Bestimmungen und Datenschutz Zoom:
https://zoom.us/docs/de-de/privacy-and-legal.html

Datum | Uhrzeit
1 Abend (2,67 Unterrichtsstunden) | Dienstag
Beginn: 19.05.2020 | 18:00 bis 20:00 Uhr
Ort
Webinar

Kurs-Nummer
20F1508160

Beratung zum Kursinhalt
Monya Jabri 
Tel. 0731 1530-12 
jabri@vh-ulm.de

Plätze
4 - 6 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €