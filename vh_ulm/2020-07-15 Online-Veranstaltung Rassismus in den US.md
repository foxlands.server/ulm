---
id: "2366161457025088"
title: "Online-Veranstaltung: Rassismus in den USA und in Deutschland"
start: 2020-07-15 20:00
end: 2020-07-15 22:00
link: https://www.facebook.com/events/2366161457025088/
image: 107932845_3274692032607964_8650487904323132007_n.jpg
teaser: "Online-Veranstaltung: Von George Floyd zu Black Lives Matter zu »all cops are
  berufsunfähig« (taz): Rassismus in den USA und in Deutschland  Dozentinn"
isCrawled: true
---
Online-Veranstaltung: Von George Floyd zu Black Lives Matter zu »all cops are berufsunfähig« (taz): Rassismus in den USA und in Deutschland

Dozentinnen: Dr. Elisabeth Engel (DHI Washington), Dr. Maria Alexopoulou (Universität Mannheim)

Am 25. Mai 2020 erstickte der Afroamerikaner George Floyd, weil ein weißer Polizist ihm bei seiner Festnahme sein Knie in den Nacken drückte. Seitdem formieren sich Proteste im Namen der Black Lives Matter Bewegung, die weit über die USA hinausgehen. Sie alle sehen in George Floyd ein Sinnbild für systemischen Rassismus, der sich nicht allein als Polizeigewalt äußert, sondern viele weitere Facetten hat. 

Auch in Deutschland griffen von Rassismus Betroffene die weltweite Empörung über diesen polizeilichen Gewaltakt auf, um auch auf ihre Situation und Erfahrungen hinzuweisen. Dieser Vortrag beleuchtet die historischen Dimensionen der gegenwärtigen Proteste und ihrer Repräsentation in den USA und in Deutschland.

Dieser Kurs wird über die Online-Plattform »Zoom« angeboten. Zur Teilnahme benötigen Sie eine E-Mail-Adresse (bitte bei der Anmeldung angeben!) und ein entsprechendes Endgerät. Sie können auch ohne Kamera und Mikrofon teilnehmen. 

Hinweise zur Verwendung finden Sie unter https://www.vh-ulm.de/digitale-angebote/anleitungen-online-kurse, zum Datenschutz unter https://www.vh-ulm.de/cms/index.php?id=202.

Nach erfolgter Anmeldung erhalten Sie ca. eine Stunde vor Beginn die Zugangsdaten per E-Mail.

Datum | Uhrzeit
Termin: Mittwoch, 15.07.2020 | 20:00 Uhr
Ort
online-Seminar

Kurs-Nummer
20F0109315

Informationen zur Veranstaltung
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
10 - 80 Teilnehmer/innen 
 noch freie Plätze

Preis
Eintritt frei