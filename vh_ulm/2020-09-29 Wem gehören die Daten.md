---
id: "994260851002426"
title: Wem gehören die Daten
start: 2020-09-29 20:00
end: 2020-09-29 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/994260851002426/
teaser: Wem gehören die Daten Christian Lenk Dienstag, 29. September, 20 Uhr  Eintritt
  frei Anmeldung über das ZAWiW Universität Ulm unter 0731-50 26601 notwe
isCrawled: true
---
Wem gehören die Daten
Christian Lenk
Dienstag, 29. September, 20 Uhr

Eintritt frei
Anmeldung über das ZAWiW Universität Ulm unter 0731-50 26601 notwendig

Ort: EinsteinHaus, Club Orange
Nr. 20H0109301

Im Rahmen des Projekts gesundaltern@bw.de