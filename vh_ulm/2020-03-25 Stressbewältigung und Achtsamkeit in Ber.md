---
id: "797366910776074"
title: Stressbewältigung und Achtsamkeit in Beruf und Alltag
start: 2020-03-25 19:00
end: 2020-03-25 22:00
address: Blaustein-Pfaffenhau, Bürgerzentrum
link: https://www.facebook.com/events/797366910776074/
image: 87054874_2921666227910548_7565393379312271360_o.jpg
teaser: Stressbewältigung und Achtsamkeit in Beruf und Alltag Ein Vortrag von Miriam
  Sander Mittwoch, 25. März, 19 Uhr Eintritt EUR 5,00 Blaustein-Pfaffenhau,
isCrawled: true
---
Stressbewältigung und Achtsamkeit in Beruf und Alltag
Ein Vortrag von Miriam Sander
Mittwoch, 25. März, 19 Uhr
Eintritt EUR 5,00
Blaustein-Pfaffenhau, Bürgerzentrum, Erhard-Grözinger-Str. 55, Kleiner Saal
Nr. 20F 1504036

Dieser Vortrag beschäftigt sich mit Stress und seinen Auswirkungen auf den Menschen. Zum einen werden wissenswerte Grundlagen über Stress vermittelt, zum anderen gibt die Referentin Anregungen, wie Stress im Alltag erfolgreich bewältigt werden kann.