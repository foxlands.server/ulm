---
id: "908515012884936"
title: »Bagage« Lesung mit Monika Helfer
start: 2020-03-17 20:00
end: 2020-03-17 23:00
address: Langenau, Pfleghofsaal
link: https://www.facebook.com/events/908515012884936/
image: 87210897_2921647464579091_8542319304656289792_n.jpg
teaser: »Bagage« Lesung mit Monika Helfer Dienstag, 17. März, 20 Uhr Eintritt EUR
  10,00/6,00 Vorverkauf Buchhandlung Mahr Langenau, Pfleghofsaal Nr. 20F 15080
isCrawled: true
---
»Bagage«
Lesung mit Monika Helfer
Dienstag, 17. März, 20 Uhr
Eintritt EUR 10,00/6,00
Vorverkauf Buchhandlung Mahr
Langenau, Pfleghofsaal
Nr. 20F 1508017

Josef und Maria Moosbrugger leben mit ihren Kindern am Rand eines Bergdorfes. Sie sind die Abseitigen, die Armen, die Bagage. Es ist die Zeit des ersten Weltkriegs und Josef wird zur Armee eingezogen. Die Zeit, in der Maria und die Kinder allein zurückbleiben und abhängig werden vom Schutz des Bürgermeisters. Die Zeit, in der Georg aus Hannover in die Gegend kommt, der nicht nur hochdeutsch spricht und wunderschön ist, sondern eines Tages auch an die Tür der Bagage klopft. Und es ist die Zeit, in der Maria schwanger wird mit Grete, dem Kind der Familie, mit dem Josef nie ein Wort sprechen wird: der Mutter der Autorin. Mit großer Wucht erzählt Monika Helfer die Geschichte ihrer eigenen Herkunft.