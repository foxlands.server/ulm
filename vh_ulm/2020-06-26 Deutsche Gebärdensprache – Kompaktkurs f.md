---
id: "317929875873824"
title: Deutsche Gebärdensprache – Kompaktkurs für Anfänger und Fortgeschrittene I
  (DGS 1 und DGS 2)
start: 2020-06-26 18:00
end: 2020-06-27 13:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/317929875873824/
image: 104200928_3203221933088308_6893051143559976312_n.jpg
teaser: "Deutsche Gebärdensprache – Kompaktkurs für Anfänger und Fortgeschrittene I
  (DGS 1 und DGS 2)  Dozentin: Elke Wendt-Wagner Datum | Uhrzeit 4 Wochenende"
isCrawled: true
---
Deutsche Gebärdensprache – Kompaktkurs für Anfänger und Fortgeschrittene I
(DGS 1 und DGS 2)

Dozentin: Elke Wendt-Wagner
Datum | Uhrzeit
4 Wochenenden (32 Unterrichtsstunden) | Freitag, Samstag
Beginn: 26.06.2020 | 18:00 bis 13:00 Uhr
Termine
Ort
EinsteinHaus, Seminarraum 18

Kurs-Nummer
20F1101013

Beratung zum Kursinhalt
Dr. Markus Stadtrecher 
Tel. 0731 1530-24 
stadtrecher@vh-ulm.de

Plätze
8 - 11 Teilnehmer/innen 
 noch freie Plätze

Preis
129,00 €