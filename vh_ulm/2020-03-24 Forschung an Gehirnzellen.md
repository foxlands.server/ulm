---
id: "208394903880164"
title: Forschung an Gehirnzellen
start: 2020-03-24 20:00
end: 2020-03-24 23:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/208394903880164/
image: 86853642_2921658014578036_7219351403326603264_o.jpg
teaser: "Forschung an Gehirnzellen: Faszination und medizinische Perspektiven Prof.
  Dr. Bernd Knöll, Universität Ulm Dienstag, 24. März, 20 Uhr Eintritt EUR 6,"
isCrawled: true
---
Forschung an Gehirnzellen:
Faszination und medizinische Perspektiven
Prof. Dr. Bernd Knöll, Universität Ulm
Dienstag, 24. März, 20 Uhr
Eintritt EUR 6,00
EinsteinHaus, Club Orange
Nr. 20F 0109604

Unser Gehirn besteht aus Milliarden von Nervenzellen, die auf komplexeste Art und Weise in neuronalen Schaltkreisen miteinander verknüpft sind. Im Rahmen des Vortrages werden die Grundlagen der Biologie von Nervenzellen vorgestellt. Dabei wird auf Besonderheiten einzelner Nervenzellen eingegangen und darüberhinaus aufgezeigt, wie sich Neurone in der dreidimensionalen Umgebung des Gehirns organisieren. Neben dem gesunden Gehirn wird auch darauf eingegangen, welche Veränderungen im Gehirn bei verschiedenen Verletzungen sowie Erkrankungen auftreten. Insgesamt soll der Vortrag die Faszination von Grundlagenforschung an Nervenzellen vorstellen, gleichzeitig aber auch die Brücke hin zur medizinischen Forschung am verletzten Gehirn schlagen.