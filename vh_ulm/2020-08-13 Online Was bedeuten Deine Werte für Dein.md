---
id: "572791370039875"
title: "Online: Was bedeuten Deine Werte für Dein Leben?"
start: 2020-08-13 18:45
end: 2020-08-13 20:15
link: https://www.facebook.com/events/572791370039875/
image: 101483350_3168365359907299_930723729485332480_n.jpg
teaser: "Online: Was bedeuten Deine Werte für Dein Leben? Dozentin: Gabriele Hagmann
  Unsere Werte tragen uns durch unser Leben. Sie gestalten meine Gedanken, m"
isCrawled: true
---
Online: Was bedeuten Deine Werte für Dein Leben?
Dozentin: Gabriele Hagmann
Unsere Werte tragen uns durch unser Leben. Sie gestalten meine Gedanken, meine Worte und meine Haltung. Ich bin der »Werte-Regisseur« meines Lebens im »Hause der Veränderungen«. Ich bin mir auf der Spur vom Keller bis zur Bühne und betrachte dabei die Wirklichkeit immer wieder aus einer anderen Perspektive.

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 13.08.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1407007

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €