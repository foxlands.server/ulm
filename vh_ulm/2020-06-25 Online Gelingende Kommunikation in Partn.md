---
id: "569936530329401"
title: "Online: Gelingende Kommunikation in Partnerschaft und Beruf"
start: 2020-06-25 18:45
end: 2020-06-25 20:15
link: https://www.facebook.com/events/569936530329401/
image: 82229221_3168381226572379_5020480769437466624_n.jpg
teaser: "Online: Gelingende Kommunikation in Partnerschaft und Beruf Dozentin:
  Gabriele Hagmann Geht es Ihnen auch so, ich wünsche mir eine gelingende
  Kommunik"
isCrawled: true
---
Online: Gelingende Kommunikation in Partnerschaft und Beruf
Dozentin: Gabriele Hagmann
Geht es Ihnen auch so, ich wünsche mir eine gelingende Kommunikation, doch immer wieder kommen da die Fettnäpfchen. Wie setze ich gekonnt und authentisch Kommunikationsmodelle im Alltag ein und erreiche meine Gegenüber? Worte haben Macht. Wählen Sie ihre Wort bewusst, um das auszulösen, was Ihr Ziel ist.

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 25.06.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1405001

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €