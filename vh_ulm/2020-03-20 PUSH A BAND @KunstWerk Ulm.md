---
id: "548125662639006"
title: PUSH A BAND @KunstWerk Ulm
start: 2020-03-20 21:00
end: 2020-03-20 23:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/548125662639006/
image: 74448957_3519420111409379_5200423458005057536_o.jpg
teaser: Ihr wolltet schon immer mal wissen, wie wir eigentlich zu unseren Acts kommen?
  Kein Problem, heute plaudern wir aus dem Nähkästchen. Mitglieder unsere
isCrawled: true
---
Ihr wolltet schon immer mal wissen, wie wir eigentlich zu unseren Acts kommen? Kein Problem, heute plaudern wir aus dem Nähkästchen. Mitglieder unserer Programmgruppe erzählen von ihrer Arbeit, und der Booker gibt Einblick in seinen Part.
Und: Wir stellen euch per Videos eine Reihe von Bands vor, die möglichweise in unser Programm kommen. Wir diskutieren mit euch darüber, und ihr entscheidet mit, welche Bands wir auswählen!
Vielleicht hast du Glück und deine Lieblingsband steht demnächst bei uns auf der Bühne.
Eintritt frei  
(Veranstalter: KunstWerk e.V. in Zusammenarbeit mit der vh Ulm)




