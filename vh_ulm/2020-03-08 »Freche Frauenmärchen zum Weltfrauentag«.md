---
id: "1030384124004534"
title: »Freche Frauenmärchen zum Weltfrauentag«
start: 2020-03-08 18:00
end: 2020-03-08 21:00
address: Langenau, Helferhaus, Raum 1
link: https://www.facebook.com/events/1030384124004534/
teaser: »Freche Frauenmärchen zum Weltfrauentag« Tine Mehls, Geschichtenerzählerin
  Sonntag, 8. März, 18 Uhr Eintritt EUR 10,00 (einschl. einem Glas Sekt) Anme
isCrawled: true
---
»Freche Frauenmärchen zum Weltfrauentag«
Tine Mehls, Geschichtenerzählerin
Sonntag, 8. März, 18 Uhr
Eintritt EUR 10,00
(einschl. einem Glas Sekt)
Anmeldung erforderlich
Langenau, Helferhaus, Raum 1
Nr. 20F 1508001

Fast 400 Märchen und Geschichten aus aller Welt hat die Geschichtenerzählerin Tine Mehls im Repertoire und einige davon erzählt sie an diesem Abend. Frauenmärchen der verschiedensten Kulturen, in denen die Frauen nicht nur die bösen Stiefmütter oder Hexen sind oder sehnsuchtsvoll und passiv auf ihren Prinzen warten. Frauen, die frech und selbstbewusst die Herausforderung meistern, die ihnen das Leben bietet. Manchmal führen sie die Männer an der Nase herum, manchmal schlagen sie sogar dem Teufel ein Schnippchen. Auf jeden Fall wird dieser Abend kurzweilig und fröhlich und es dürfen auch gerne Männer zum Zuhören dazu kommen.