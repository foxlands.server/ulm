---
id: "330100164683504"
title: vh-Podiumsdiskussion „Und jetzt! Perspektiven für Ulm“
start: 2020-07-25 18:00
end: 2020-07-25 22:00
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/330100164683504/
teaser: vh-Podiumsdiskussion „Und jetzt! Perspektiven für Ulm“   Die vh hat für das
  Frühjahr 2020 das Semesterthema »Stadträume - Lebensräume - Freiräume« gew
isCrawled: true
---
vh-Podiumsdiskussion „Und jetzt! Perspektiven für Ulm“
 
Die vh hat für das Frühjahr 2020 das Semesterthema »Stadträume - Lebensräume - Freiräume« gewählt. Dies möchten wir zum Anlass nehmen, mit VertreterInnnen unserer Stadt-Gesellschaft ins Gespräch zu kommen, was Corona mit uns gemacht hat uns welche Veränderungen wir uns wünschen. 

Auf dem Podium - im Freien auf dem Kornhausplatz, bei Regen in der vh - sind der Oberbürgermeister der Stadt Ulm Gunter Czisch, der Vertreter des Stadtjugendringes Tobias "Bobbes" Schmidt, Maria Eichenhofer-Fröscher für den Ulmer Seniorenrat, die 21-jährige Wintana Berhe für Fridays for Future und Greenpeace und Dr. Christoph Hantel, seit einem Jahr Leiter der Ulmer Volkshochschule.
 
Die Corona – Pandemie hat weltweit vieles gesellschaftlich, sozial und kulturell auf den Kopf gestellt. In uns wächst die Sehnsucht, dass bald ein Impfstoff gefunden ist und alles wieder so wird wie vorher. Aber ist das überhaupt möglich und wünschenswert? Kann die Menschheit sich anlässlich Corona verändern? Sind wir in der Lage, trotz der vielen Toten, Kranken und der gravierenden Auswirkungen für die Wirtschaft das Virus zum Anlass zu nehmen, unsere Lebensweise, die Strukturen unseres Zusammenlebens und unsere gesellschaftlichen Spielregeln auf den Prüfstand zu stellen?

Die Veranstaltung ist kostenfrei. Die Veranstalter bitten um Anmeldung auf der Homepage der vh Ulm.

https://www.vh-ulm.de/vh-programm/kurs-finder/detail/kurs/und-jetzt-perspektiven-fuer-ulm/20F0000037

Datum | Uhrzeit
Termin: Samstag, 25.07.2020 | 18:00 Uhr vh Ulm (Kornhausplatz)