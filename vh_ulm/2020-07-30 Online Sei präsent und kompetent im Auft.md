---
id: "3222954747764582"
title: "Online: Sei präsent und kompetent im Auftreten!"
start: 2020-07-30 18:45
end: 2020-07-30 20:15
link: https://www.facebook.com/events/3222954747764582/
image: 101529812_3168370559906779_6332920516218716160_n.jpg
teaser: "Online: Sei präsent und kompetent im Auftreten! Dozentin: Gabriele Hagmann
  Werden Sie authentisch sichtbar und erleben Sie mit Ihren Sinnen ein nachha"
isCrawled: true
---
Online: Sei präsent und kompetent im Auftreten!
Dozentin: Gabriele Hagmann
Werden Sie authentisch sichtbar und erleben Sie mit Ihren Sinnen ein nachhaltiges Ergebnis. Lernen Sie Ihr »Überheblichkeits-Training« und treten im Alltag sprachlich eindrucksstark auf. Lernen Sie in dünner Luft zu atmen. Reagieren Sie majestätisch!

Datum | Uhrzeit
1 Abend (2 Unterrichtsstunden) | Donnerstag
Beginn: 30.07.2020 | 18:45 bis 20:15 Uhr
Ort
Onlineseminar Beruf

Kurs-Nummer
20F1405002

Beratung zum Kursinhalt
Norbert Herre 
Tel. 0731 1530-16 
herre@vh-ulm.de

Plätze
5 - 25 Teilnehmer/innen 
 noch freie Plätze

Preis
15,00 €