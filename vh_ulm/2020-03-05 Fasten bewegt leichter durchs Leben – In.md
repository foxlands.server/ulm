---
id: "2288480858112485"
title: "Fasten bewegt: leichter durchs Leben – Infoabend"
start: 2020-03-05 19:00
end: 2020-03-05 20:30
address: Erbach, KaffCafé, Auf der Wühre 13
link: https://www.facebook.com/events/2288480858112485/
teaser: Fasten bewegt ... leichter durchs Leben – Infoabend Wilma Bäuerle,
  Fastenleiterin (dfa), Gesundheits- und Mentaltrainerin Donnerstag, 5. März, 19
  bis
isCrawled: true
---
Fasten bewegt ... leichter durchs Leben – Infoabend
Wilma Bäuerle, Fastenleiterin (dfa), Gesundheits- und Mentaltrainerin
Donnerstag, 5. März, 19 bis 20:30 Uhr
Eintritt frei
Erbach, KaffCafé, Auf der Wühre 13
Nr. 20F 1505028

So wie der Frühjahrsputz in Haus und Wohnung die Reste des Winters vertreibt, ist Fasten eine natürliche Möglichkeit, den Körper zu reinigen und von alten, abgelagerten Stoffwechselresten zu befreien. Es ist eine ganzheitliche Maßnahme zur Gesundheitsprävention und wirkt in drei Dimensionen: medizinisch-körperlich, mitmenschlich-sozial, spirituell-religiös. Anhand detaillierter Beschreibungen lernen Sie die Einzelheiten der Fastenzeit kennen, beginnend bei den Entlastungstagen und der Darmreinigung über die reinen Fastentage bis hin zum Fastenbrechen und dem stufenweisen Kostaufbau. Die »Reset-Taste« drücken: Fasten kann von den Zwängen des Alltags befreien, eine Rückbesinnung auf das Wesentliche ermöglichen und zu innerer Freiheit und Erkenntnis führen.

Dieser Abend dient als Vorbereitungstreffen für die nachfolgende Fastenwoche unter 20F1505309 und ist Voraussetzung für deren Teilnahme.