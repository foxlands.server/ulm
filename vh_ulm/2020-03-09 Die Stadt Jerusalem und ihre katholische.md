---
id: "2496413987279708"
title: Die Stadt Jerusalem und ihre katholischen Christen
start: 2020-03-09 20:00
end: 2020-03-09 21:30
locationName: Ulmer Volkshochschule
address: Kornhausplatz 5, 89073 Ulm
link: https://www.facebook.com/events/2496413987279708/
image: 87298696_2921635257913645_8261451065090637824_n.jpg
teaser: Die Stadt Jerusalem und ihre katholischen Christen Pater Nikodemus Schnabel
  Montag, 9. März, 20 bis 21:30 Uhr Eintritt EUR 6,00 EinsteinHaus, Club Ora
isCrawled: true
---
Die Stadt Jerusalem und ihre katholischen Christen
Pater Nikodemus Schnabel
Montag, 9. März, 20 bis 21:30 Uhr
Eintritt EUR 6,00
EinsteinHaus, Club Orange
Nr. 20F 0101525

Pater Nikodemus, den ZDF-Fernsehzuschauern mit der Reihe »Ein guter Grund zum Feiern« ist ein kluger und eloquenter Arbeiter im Weinberg des Herrn. Seit vielen Jahren lebt und arbeitet er in Jerusalem, in der Dormitio-Abtei auf dem Zion-Berg. In dem aufgeheizten Klima dieser Stadt, in der Juden, Christen und Muslime gleichermaßen religiöse Wurzeln haben, ist ein normales Leben nicht möglich. Wie erlebt er seine Rolle als Seelsorger für die deutschsprachigen katholischen Christen in Israel? Warum heißt sein Buch zu dem Thema »Zuhause um Niemandsland«? Pater Nikodemus wird berichten und im Anschluss Bücher signieren.