---
id: "572712693430190"
title: Vernissage Friede, Freude, Eierkuchen ("Summer in the City")
start: 2020-09-06 17:00
end: 2020-09-06 19:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/572712693430190/
teaser: KUNSTPOOL. Galerie am Ehinger Tor forderte in Zusammenarbeit mit dem Verein
  für Friedensarbeit Künstler*innen  auf, Arbeiten für die Ausstellung einzu
isCrawled: true
---
KUNSTPOOL. Galerie am Ehinger Tor forderte in Zusammenarbeit mit dem Verein für Friedensarbeit Künstler*innen  auf, Arbeiten für die Ausstellung einzureichen. Eine Jury wählte 20 Bewerber aus, die aus ganz Deutschland kommen.
Die Vernissage findet im überdachten Außenbereich statt, damit nicht alle Vernissagebesucher gleichzeitig in den Ausstellungsräumen sein müssen.
Diese Veranstaltung ist Teil unserer Reihe "Summer in the City".
Vielen Dank an das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg für die Unterstützung.

Teilnehmende Künstler*innen:

Myrah Adams (Neu-Ulm) 
Winfried Becker (Kempten)
Maren Dietrich (Georgensgmünd)
Josef Feistle (Weißenhorn)
Dorothea Grathwohl (Ulm)
Dietmar Herzog (Neu-Ulm)
Gisela Hoßfeld-Weber (Senden)
Petra Hübel (Otterbach)
Ruth Knecht (Blaubeuren)
Reinhard Köhler (Ulm)
Hans Liebl (Neu-Ulm)
Martin Lorenz (Landau)
Monika Meinold (Warstein)
Hans-Günther Obermeier (Köln)
Dietmar Paetzold (Köln)
Caritt Reichl (Ulm)
Kerstin Römhild (Lohr)
Harald Schmidt (Aachen)
Renate Vetter (Reutlingen)
Amei Gerlinde Wöllmer (Ulm)



