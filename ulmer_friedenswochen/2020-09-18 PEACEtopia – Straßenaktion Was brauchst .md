---
id: "1792216174250673"
title: "PEACEtopia – Straßenaktion: Was brauchst du für Frieden?"
start: 2020-09-18 17:00
end: 2020-09-18 19:00
address: Münsterplatz/ Ecke Hirschstraße Ulm
link: https://www.facebook.com/events/1792216174250673/
teaser: Wir wollen bei Lounge-Atmosphäre die Phantasie anregen, was es für ein
  friedliches Zusammenleben braucht. Wie müssen wir aufeinander zugehen, damit
  PE
isCrawled: true
---
Wir wollen bei Lounge-Atmosphäre die Phantasie anregen, was es für ein friedliches Zusammenleben braucht. Wie müssen wir aufeinander zugehen, damit PEACEtopia realisierbar hier bei uns wird? Kommt und bringt euch kreativ ein, damit wir viele Ideen für Frieden in unserer Gesellschaft, Politik und im täglichen Miteinander sammeln und für alle sichtbar gestalten können.

Münsterplatz / Ecke Hirschstraße (Fußgängerzone), 89073 Ulm
Eintritt frei
Veranstalter: Bund der Deutschen Katholischen Jugend (BDJK), Dekanat Ehingen- Ulm

