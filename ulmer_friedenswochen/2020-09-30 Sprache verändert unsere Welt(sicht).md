---
id: "743753629502029"
title: Sprache verändert unsere Welt(sicht)
start: 2020-09-30 19:30
end: 2020-09-30 22:30
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/743753629502029/
teaser: Gezielte Propaganda, verfälschte Nachrichten, manipulative Sprachregelungen –
  alle diese Methoden zur Beeinflussung der öffentlichen Meinung und damit
isCrawled: true
---
Gezielte Propaganda, verfälschte Nachrichten, manipulative Sprachregelungen – alle diese Methoden zur Beeinflussung der öffentlichen Meinung und damit auch des Handelns politischer Akteure gab es schon lange vor Beginn des Internetzeitalters. Heute wird es allerdings immer unüberschaubarer, wer, (von) wo und in wessen Auftrag das „Framing“ politischer Inhalte und die Steuerung der öffentlichen Meinung betreibt. Welche Gegenstrategien gibt es?
Referent: Andreas Zumach, Genf

Eintritt frei, Spenden erbeten
Veranstalter: Verein Ulmer Weltladen e. V., Ulmer Netz für eine andere Welt e. V.
