---
id: "2842677475841749"
title: "Stadtrundfahrt zum Nachdenken: Militär und Rüstung in Ulm"
start: 2020-09-04 18:00
address: Parkplatz Westbad, Moltkestr. 33, 89077 Ulm
link: https://www.facebook.com/events/2842677475841749/
teaser: Im Bewusstsein der Ulmer ist es nicht unbedingt, welche herausragende Rolle
  Militär und Rüstung hier spielen. Manchmal bekommt man häppchenweise Infor
isCrawled: true
---
Im Bewusstsein der Ulmer ist es nicht unbedingt, welche herausragende Rolle Militär und Rüstung hier spielen. Manchmal bekommt man häppchenweise Informationen. Heute machen wir uns im Reisebus auf den Weg, schauen uns die meisten dieser Stellen im Stadtbild an, manche auffällig, manche fast nicht erkennbar. Natürlich gibt es auch sachkundige Informationen. Wer Lust hat, setzt sich zu einer Unterhaltung anschließend noch mit uns auf ein Kaltgetränk zusammen
Treffpunkt 18:00 Uhr: Parkplatz Westbad, Moltkestr. 33, 89077 Ulm
Eintritt und Fahrt: frei, Dauer ca. 2 Std.
Veranstalter: Verein für Friedensarbeit e. V. und Ulmer Ärzteinitiative/IPPNW 