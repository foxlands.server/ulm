---
id: "1392261310958152"
title: Quo vadis, Europa?
start: 2020-09-01 18:00
locationName: Haus der Gewerkschaften, Ulm
address: Ulm
link: https://www.facebook.com/events/1392261310958152/
teaser: Immer wieder wird beschworen, dass die europäische Staatengemeinschaft sich
  nach dem 2. Weltkrieg als großartiges Friedensprojekt gegründet hat. Was i
isCrawled: true
---
Immer wieder wird beschworen, dass die europäische Staatengemeinschaft sich
nach dem 2. Weltkrieg als großartiges Friedensprojekt gegründet hat. Was ist von
diesem Anspruch geblieben, nachdem durch den Druck der NATO und der USA und
nicht zuletzt durch das Agieren der verantwortlichen Politiker in Europa
Sicherheitspolitik zunehmend als Militärpolitik verstanden wird?
Wie ist der aktuelle Stand der Dinge, was müsste geschehen, um friedenspolitische
Lösungen wieder in den Vordergrund zu rücken? Dies wollen wir anlässlich des
Antikriegstags 2020 diskutieren.
Referentin: Claudia Haydt (Tübingen, seit vielen Jahren bei der Informationsstelle
Militarisierung e. V. engagiert)
Haus der Gewerkschaften, Weinhof 23, 89073 Ulm
Eintritt frei
Veranstalter: DGB-Kreisverband Ulm/Alb-Donau
