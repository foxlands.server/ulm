---
name: Ulmer Friedenswochen
website: http://www.friedenswochen-ulm.de
email: friedensarbeit@web.de
scrape:
  source: facebook
  options:
    page_id: ulmerfriedenswochen
---
Du und ich - wir sind eins. Ich kann dir nicht wehtun, ohne mich zu verletzen. (Mahatma Gandhi)
Organisiert vom Arbeitskreis Friedenswoche Ulm/Neu-Ulm fanden die Ulmer Friedenswochen erstmals im Jahr 1977 statt. Der Verein für Friedensarbeit Ulm, Radio free FM und mehrere Gruppen und Initiativen bringen nach 40 Jahren die Ulmer Friedenswochen wieder zurück in die Stadt!
Projektziel
Angesichts der politischen Zeitenwende, wollen wir der Unsicherheit vieler mit der Neuorganisation des lokalen Friedensnetzwerkes aktiv begegnen. Die Themen dieses Netzwerkes sollen auch über die Friedenswochen hinaus in Form einer Sendung und weiteren Veranstaltungen zusammengefasst und verbreitet werden. In den Räumen von Radio free FM sollen die Aktivitäten gebündelt werden.
Projektaufbau
Anhand der schriftlich erhaltenen Dokumentationen der vier Ulmer Friedenswochen (1977, 1978, 1979, 1980) erarbeiten wir uns die Grundlage zur Realisierung der Ulmer Friedenswochen im Jahr 2017. Im Zeitraum vom 1. September (Antikriegstag) bis zum 17. Oktober werden die Ulmer Friedenswochen 2017 stattfinden. Alle teilnehmenden Gruppen und Organisationen identifizieren sich mit der sogenannten "Plattform der Koordinationsgruppe Ulmer Friedenswochen" und den damit verbundenen Zielen.
