---
id: "900207167167713"
title: Auschwitz als Steinbruch – Was von den NS-Verbrechen bleibt
start: 2020-09-10 19:30
locationName: Haus der Gewerkschaften, Ulm
address: Ulm
link: https://www.facebook.com/events/900207167167713/
teaser: Wie die Verbrechen des NS-Regimes vergegenwärtigt werden, ist zunehmend einem
  ökonomischen und ideologischen Markt überlassen. Dies hat international
isCrawled: true
---
Wie die Verbrechen des NS-Regimes vergegenwärtigt werden, ist zunehmend einem
ökonomischen und ideologischen Markt überlassen. Dies hat international Einfluss
auf das Geschichtsbild.
Inhalt des Vortrags: Was bleibt von NS-Verbrechen? Welche Aspekte der
Erinnerungen von Zeitzeugen wurden von Anfang an ignoriert? Welche
Missverständnisse bestimmen die Vorstellungen über Konzentrationslager?
Referent: Thomas Willms (Geschäftsführer der Bundesvereinigung VVN-BdA)
Haus der Gewerkschaften, Weinhof 23, 89073 Ulm
Eintritt frei
Veranstalter: Kooperation VVN-BdA, Kreisvereinigung Ulm, DZOK e. V.