---
id: "287318622369669"
title: "Zwischen Paradies und Hölle. Gedichte und Musik aus einem Land im Krieg:
  Syrien"
start: 2020-09-16 19:30
locationName: Kath. Kirchengemeinde St. Georg Ulm
address: Beethovenstr. 1, 89073 Ulm
link: https://www.facebook.com/events/287318622369669/
teaser: Wir hören Gedichte aus Syrien zuerst in Arabisch, der Originalsprache, und
  dann in deutscher Übersetzung. Dazu jeweils kurze Gedanken bzw. Information
isCrawled: true
---
Wir hören Gedichte aus Syrien zuerst in Arabisch, der Originalsprache, und dann in deutscher Übersetzung. Dazu jeweils kurze Gedanken bzw. Informationen über Gedicht und Autor/in. Zwischen den Gedichten Musik eines syrischen Duos. In einer Schlussrunde gibt es die Möglichkeit zum Gespräch.
Gemeindehaus der Katholischen Kirchengemeinde St. Georg, Beethovenstr. 1,
89073 Ulm
Eintritt frei
Veranstalter: Lyrikkreis Ulm/Neu-Ulm