---
id: "3030089970439834"
title: Frieden braucht Bewegung (Vernissage zur Ausstellung)
start: 2020-09-07 19:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/3030089970439834/
teaser: Eine Geschichte der Friedensbewegung in Plakaten und Bildern. „Frieden braucht
  Bewegung“ hieß es damals, und es kam tatsächlich zu einer Bewegung. Ein
isCrawled: true
---
Eine Geschichte der Friedensbewegung in Plakaten und Bildern.
„Frieden braucht Bewegung“ hieß es damals, und es kam tatsächlich zu einer
Bewegung. Einige haben als Aktivisten mitgemacht und sehr viele beteiligten sich.
Heute ist das Friedensengagement wichtiger denn je. Die Ausstellung sensibilisiert,
liefert Denkanstöße und setzt ein Zeichen für den Frieden auf Erden.
Programm: Redebeiträge, Musikbeiträge, kostenloses Buffet.
EinsteinHaus, Kornhausplatz 5, 89073 Ulm
Eintritt frei 
Veranstalter: Freundschaft, Kultur und Jugend e. V. Ulm (DIDF-Ulm)
(weitere Informationen zur Ausstellung am Anfang dieses Programmheftes)