---
id: "224002695377113"
title: Wenn Parolen Unterschiede machen. Rechtspopulismus in der Gesellschaft.
start: 2020-09-15 20:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/224002695377113/
teaser: "Was Worte und Parolen bewirken können, wurde in der jüngsten deutschen
  Geschichte deutlich: „Wir sind das Volk“ hat 1989 viele bewegt und Stärke
  bewie"
isCrawled: true
---
Was Worte und Parolen bewirken können, wurde in der jüngsten deutschen Geschichte deutlich: „Wir sind das Volk“ hat 1989 viele bewegt und Stärke bewiesen. 2015, 2016 und 2017 steckt etwas anderes dahinter. Welche Haltungen und Einstellungen heute bewegen, ist Thema dieser Veranstaltung. Wohin entwickelt sich unsere Gesellschaft und wie viel Rechtspopulismus verträgt die Demokratie?
Referentin: Angelika Vogt (Demokratiezentrum Baden-Württemberg und Jugendstiftung Baden-Württemberg)
EinsteinHaus, Kornhausplatz 5, 89073 Ulm
Eintritt frei
Veranstalter: Verein für Friedensarbeit, Ulmer Volkshochschule, Ulmer Ärzteinitiative/ IPPNW