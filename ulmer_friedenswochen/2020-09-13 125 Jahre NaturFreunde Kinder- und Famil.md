---
id: "206754880700138"
title: 125 Jahre NaturFreunde Kinder- und Familienfest der Ulmer NaturFreunde
start: 2020-09-13 16:00
locationName: Naturfreundehaus Spatzennest
address: 89134 Weidach
link: https://www.facebook.com/events/206754880700138/
teaser: Programm ab 16 Uhr, geöffnet ab 13 Uhr "Mizzi übt Zaubern" Vorsicht! Mizzi übt
  zaubern! Abrakodabra ... äh oder war es Kobradobraba .... Oh,nein, wie
isCrawled: true
---
Programm ab 16 Uhr, geöffnet ab 13 Uhr
"Mizzi übt Zaubern"
Vorsicht! Mizzi übt zaubern! Abrakodabra ... äh oder war es Kobradobraba .... Oh,nein, wie war noch mal der Zauberspruch? Fröhliches Mitmachtheater mit Musik und tollen Dingen für Kinder zwischen 2,5 und 8 Jahren. Ein Kindertheater von Elke Kastner.
 
Die neuen Spielgeräte (Wippe und Schaukel) auf dem Gelände können ausprobiert, die naturnahe Umgebung erkundet  und am Lagerfeuer können Würste gebraten werden.

Naturfreundehaus Spatzennest, 89134 Blaustein-Weidach
Eintritt frei
Veranstalter: NaturFreunde Ulm e. V.