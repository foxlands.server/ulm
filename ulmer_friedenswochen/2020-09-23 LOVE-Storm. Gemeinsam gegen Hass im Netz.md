---
id: "760114494794667"
title: LOVE-Storm. Gemeinsam gegen Hass im Netz
start: 2020-09-23 20:00
end: 2020-09-23 22:00
locationName: Einsteinhaus Ulm
address: Kornhausplatz 5, D-89073  Germany, 89073 Ulm
link: https://www.facebook.com/events/760114494794667/
teaser: Mit LOVE-Storm kann man Gegenrede gegen Hass trainieren und an Aktionen gegen
  Hass teilnehmen. Im Netz kann man Hasskommentare melden und sich mit and
isCrawled: true
---
Mit LOVE-Storm kann man Gegenrede gegen Hass trainieren und an Aktionen gegen Hass teilnehmen. Im Netz kann man Hasskommentare melden und sich mit anderen Aktiven austauschen. Die Angegriffenen werden geschützt, Zuschauende zu Zivilcourage ermutigt und den Angreifenden werden gewaltfrei Grenzen gesetzt. Gemeinsam stoppen wir den Hass im Netz!
Referentin: Ines Hensch

Eintritt frei

Veranstalter: Verein für Friedensarbeit, Ulmer Volkshochschule und Ulmer Ärzteinitiative/IPPNW
(Gefördert von der Partnerschaft für Demokratie in Ulm mit Fokus Wiblingen)
