---
id: "284923762717210"
title: 50 Jahre Fairer Handel – Was haben wir gelernt?
start: 2020-09-14 19:00
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/284923762717210/
teaser: Er hat einiges erreicht, der „Faire Handel“. Und die Akteure haben viel
  gelernt. Zum Beispiel, dass man die Welt verändern kann. Aber auch, dass es zu
isCrawled: true
---
Er hat einiges erreicht, der „Faire Handel“. Und die Akteure haben viel gelernt. Zum
Beispiel, dass man die Welt verändern kann. Aber auch, dass es zur substanziellen,
„nachhaltigen“ Veränderung noch mehr bedarf als des traditionellen „Fairen
Handels“. Dass es nämlich ein Ende der „imperialen Lebensweise“ (Ulrich Brand) im
Globalen Norden braucht, um den Menschen überall auf dieser Welt ein gutes,
friedliches, kurz: ein erfülltes Leben zu ermöglichen.
Referent: Gerd Nickoleit, Wuppertal
Bürgerhaus Mitte, Schaffnerstraße 17, 89073 Ulm
Eintritt frei
Veranstalter: Verein Ulmer Weltladen e. V., Ulmer Netz für eine andere Welt e. V.,
Eine Welt-Regionalpromotorin 