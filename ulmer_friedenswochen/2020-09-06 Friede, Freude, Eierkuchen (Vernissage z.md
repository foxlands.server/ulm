---
id: "608011400144256"
title: Friede, Freude, Eierkuchen (Vernissage zur Ausstellung)
start: 2020-09-06 17:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/608011400144256/
teaser: KUNSTPOOL. Galerie am Ehinger Tor hat Künstler aus der Region aufgefordert,
  Arbeiten für die Ausstellung einzureichen, die vom 06.09.2020 bis 27.09.20
isCrawled: true
---
KUNSTPOOL. Galerie am Ehinger Tor hat Künstler aus der Region aufgefordert,
Arbeiten für die Ausstellung einzureichen, die vom 06.09.2020 bis 27.09.2020 in der
Galerie zu sehen sein wird. Zur Eröffnung wird auch u. a. Reinhold Thiel einführende
Worte sprechen. Bis Redaktionsschluss des Programmhefts war noch nicht bekannt,
von wem die musikalische Umrahmung stammen wird. Infos gibt es bei
www.friedenswochen-ulm.de und bei Facebook.
KUNSTPOOL. Galerie am Ehinger Tor (Haltestellengelände), 89077 Ulm
Eintritt frei
Veranstalter: KUNSTPOOL. Galerie am Ehinger Tor und Verein für Friedensarbeit e.
V.
(weitere Informationen zur Ausstellung am Anfang dieses Programmheftes)