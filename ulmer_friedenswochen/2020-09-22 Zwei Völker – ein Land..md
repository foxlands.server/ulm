---
id: "290199748858375"
title: Zwei Völker – ein Land.
start: 2020-09-22 19:30
end: 2020-09-22 22:30
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/290199748858375/
teaser: Eine biblische Vision für Frieden zwischen Israel und Palästina  Seit
  Jahrzehnten kommt der Nahe Osten nicht zur Ruhe. Es ist ein Konflikt, der mit
  de
isCrawled: true
---
Eine biblische Vision für Frieden zwischen Israel und Palästina

Seit Jahrzehnten kommt der Nahe Osten nicht zur Ruhe. Es ist ein Konflikt, der mit dem europäischen Antisemitismus begann und der nur gelöst werden kann, wenn eines klar ist: „Beide Seiten müssen international mit dem gleichen Maßstab beurteilt werden: Gleiches Recht auf Sicherheit, Selbstbestimmung, Freizügigkeit, gleiche Menschenwürde. Es darf keine doppelten Standards in der Bewertung der Situation in Israel/Palästina geben.“ (Pax Christi) 
Referent: Bischof em. Hans-Jürgen Abromeit, Greifswald

Eintritt frei, Spenden erbeten
Veranstalter: Verein UImer Weltladen e. V.
