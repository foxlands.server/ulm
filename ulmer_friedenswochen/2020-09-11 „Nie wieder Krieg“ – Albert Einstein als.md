---
id: "714939312397799"
title: „Nie wieder Krieg“ – Albert Einstein als Pazifist
start: 2020-09-11 19:30
address: Stadtbibliothek Vestgasse 1 89073 Ulm
link: https://www.facebook.com/events/714939312397799/
teaser: Albert Einstein, 1879 in Ulm geboren, Weltbürger und Pazifist. Stets ein
  unbequemer Bürger, frei von nationalem Stolz und Pathos, den militärischer Dr
isCrawled: true
---
Albert Einstein, 1879 in Ulm geboren, Weltbürger und Pazifist. Stets ein unbequemer Bürger, frei von nationalem Stolz und Pathos, den militärischer Drill befremdete und verstörte. Zeit seines Lebens Mahner und Kämpfer für den Frieden. „Ich bin entschiedener, aber nicht absoluter Pazifist. Das heißt: Ich bin in allen Fällen gegen Gewaltanwendung, außer in dem Fall, dass der Gegner Vernichtung des Lebens als Selbstzweck beabsichtigt.“ (Einstein 1953)
Referent: Lothar Heusohn
Stadtbibliothek, Vestgasse 1, 89073 Ulm
Eintritt frei