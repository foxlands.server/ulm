---
id: "624735058160846"
title: Russland und Deutschland – zwischen Faszination und Feindbild
start: 2020-09-21 19:30
end: 2020-09-21 22:30
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/624735058160846/
teaser: Ein Abend anlässlich des 75. Jahrestags der bedingungslosen Kapitulation der
  deutschen Wehrmacht  Die zentralen Ereignisse zwischen Russland und Deuts
isCrawled: true
---
Ein Abend anlässlich des 75. Jahrestags der bedingungslosen Kapitulation der deutschen Wehrmacht  Die zentralen Ereignisse zwischen Russland und Deutschland im 20. Jahrhundert waren der deutsche Überfall auf die Sowjetunion und deren entscheidender Beitrag zum Sieg über Nazideutschland. Das Verhältnis beider Länder ist allerdings viel differenzierter. So skizziert der Vortrag einige der wesentlichen historischen und kulturellen Hintergründe, bezieht aber auch die aktuellen Entwicklungen in den Beziehungen und in der Erinnerungspolitik an den Zweiten Weltkrieg ein.
Referent: Reinhard Lauterbach
Eintritt frei, Spenden erbeten 
Veranstalter: Ulmer Netz für eine andere Welt e.V., Verein für Friedensarbeit, Ulmer Ärzteinitiative/IPPNW
