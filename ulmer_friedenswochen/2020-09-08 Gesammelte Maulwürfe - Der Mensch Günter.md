---
id: "284850426177338"
title: Summer in the City:Gesammelte Maulwürfe - Der Mensch Günter Eich
start: 2020-09-08 20:00
address: Kunstpool, Galerie am Ehinger Tor, Ulm
link: https://www.facebook.com/events/284850426177338/
teaser: Der Schriftsteller Günter Eich, Mitglied der berühmten Gruppe 47, war ein
  Meister des poetischen Hörspiels. In seinen Gedichten ist er oft subtil obri
isCrawled: true
---
Der Schriftsteller Günter Eich, Mitglied der berühmten Gruppe 47, war ein Meister des poetischen Hörspiels. In seinen Gedichten ist er oft subtil obrigkeitskritisch und skurril. Er stand öffentlich zu seinen Fehlern und sagte: „Ich habe dem Nationalsozialismus keinen aktiven Widerstand entgegengesetzt. Jetzt so zu tun als ob, liegt mir nicht.“ Dorothea Grathwohl kommt Eich auf die Spur und liest seine komischen und phantastischen Texte fast szenisch.
Eintritt frei

Diese Veranstaltung findet im Rahmen unserer Reihe "Summer in the City" statt.
Vielen Dank an das Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg für die Unterstützung.