---
id: "2649049795198737"
title: Humanität und Menschenrechte außer Kraft
start: 2020-09-29 19:30
end: 2020-09-29 22:30
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/2649049795198737/
teaser: Anmerkungen zur deutschen und europäischen Flüchtlingspolitik  Deutschland
  2020, Europa 2020 – ein Land, ein ganzer Kontinent im „Shutdown“ der „Coron
isCrawled: true
---
Anmerkungen zur deutschen und europäischen Flüchtlingspolitik

Deutschland 2020, Europa 2020 – ein Land, ein ganzer Kontinent im „Shutdown“ der „Corona-Krise“. Ein Land, ein Kontinent im „Sicherheitsabstand“. Und an den Außengrenzen, im Mittelmeer, in den Lagern an der Peripherie tausende von Menschen gewaltsam aufgehalten, zusammengepfercht, ertrinkend und ertrunken, buchstäblich „in der Hölle“. Deutsche und europäische Flüchtlingspolitik 2020: ein Akt „beispielloser Inhumanität“ (Frankfurter Rundschau). 
Referent: Andreas Zumach, Genf

Eintritt frei, Spenden erbeten
Veranstalter: Forum Asyl und Menschenrechte, Evangelischer Diakonieverband Ulm/Alb-Donau, Förderverein des Behandlungszentrums für Folteropfer Ulm e. V., Flüchtlingsrat Ulm/Alb-Donaukreis e. V., Seebrücke Ulm, Stiftung Menschenrechtsbildung, Ulmer Netz für eine andere Welt e. V., Verein Ulmer Weltladen e. V., Eine Welt-Regionalpromotorin
