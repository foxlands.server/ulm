---
id: "971405803307860"
title: Friedensfest
start: 2020-09-26 14:00
end: 2020-09-26 18:30
address: Unterer Kuhberg 16, 89077 Ulm
link: https://www.facebook.com/events/971405803307860/
teaser: '"Gemeinsam leben – gemeinsam feiern – gemeinsam handeln"  ACHTUNG: Wegen der
  Wettervorhersage ist die Programmdauer auf 14 - 18.30 Uhr gekürzt. Die Ba'
isCrawled: true
---
"Gemeinsam leben – gemeinsam feiern – gemeinsam handeln"

ACHTUNG: Wegen der Wettervorhersage ist die Programmdauer auf 14 - 18.30 Uhr gekürzt. Die Band Timura tritt nicht auf.
Das Kinderprogramm findet indoor statt, das Bühnenprogramm im Zelt.

Das Friedensfest zum Ende der Ulmer Friedenswochen ist ein Tag der Begegnung vieler Nationalitäten, deren Kinder und ihrer Familien, Friedensbewegter und Freunde engagierter Musik.
Wir setzen ein Zeichen für ein Leben in Frieden, für eine Welt ohne Kriege und für eine Welt in sozialer Gerechtigkeit. Nur im gegenseitigen Kennenlernen können Vorurteile abgebaut werden. 29 Ulmer Gruppierungen und Organisationen haben die Ulmer Friedenswochen mit ihren Veranstaltungen geprägt – lasst uns heute zusammen feiern.
Ab 12 Uhr Essensangebote aus verschiedenen Ländern, Kaffee und Kuchen sowie Informationsstände der teilnehmenden Gruppen.

Kinderprogramm ab 14 Uhr
- Clown Mizzi
– Kinderflohmarkt
  
Bühnenprogramm ab 16 Uhr
Internationale Folklore sowie folgende Bands:
– Feschtagsmusik
– Izlem
– Unter Wilden

Eintritt frei 
Veranstalter: NaturFreunde Ulm e. V.,  Freundschaft, Kultur und Jugend e. V. Ulm (DIDF Ulm)
