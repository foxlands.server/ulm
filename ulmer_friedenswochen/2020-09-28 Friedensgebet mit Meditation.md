---
id: "3143924455655809"
title: Friedensgebet mit Meditation
start: 2020-09-28 18:30
end: 2020-09-28 19:30
locationName: Haus der Begegnung Ulm
address: Grüner Hof 7, 89073 Ulm
link: https://www.facebook.com/events/3143924455655809/
teaser: Wir laden ein, gemeinsam für den Frieden zu beten. Musik, vorgetragene Texte,
  Meditation und Stille prägen dieses Abendgebet.  Eintritt frei Veranstal
isCrawled: true
---
Wir laden ein, gemeinsam für den Frieden zu beten.
Musik, vorgetragene Texte, Meditation und Stille prägen dieses Abendgebet.

Eintritt frei
Veranstalter: Tibet Initiative Deutschland e. V., Regionalgruppe Ulm/Neu-Ulm
