---
id: "954457181682047"
title: Friedenssong Marathon
start: 2020-09-11 10:00
end: 2020-09-11 17:00
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/954457181682047/
teaser: „I’d love to change the world but I don’t know what to do.“ Ten Years After
  lieferten einen Hit für Generationen. Es gibt aber noch wesentlich mehr So
isCrawled: true
---
„I’d love to change the world but I don’t know what to do.“
Ten Years After lieferten einen Hit für Generationen. Es gibt aber noch wesentlich
mehr Songs über das Thema Frieden. Und dazu brauchen wir Ihre Hilfe!
Schicken Sie uns Ihre Songvorschläge und Friedensbotschaft per Whatsapp ins
Studio unter der 0731/9 38 62 99 oder per Message an unser Facebook und
Instagram (Radio free FM). Bestimmen Sie mit, was an diesem Tag gespielt wird!
UKW: 102,6 Mhz ; Stream: freefm.de.
Veranstalter: Radio free FM Ulm