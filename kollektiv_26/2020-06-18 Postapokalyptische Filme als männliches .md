---
id: "525506924842183"
title: Postapokalyptische Filme als männliches Narrativ
start: 2020-06-18 19:00
end: 2020-06-18 20:30
link: https://www.facebook.com/events/525506924842183/
image: 100757104_2952676041434597_2926945711721283584_o.jpg
teaser: "Auf der Fury Road in den Feminismus - Postapokalyptische Filme als männliches
  Narrativ?  Veronika Kracher: Online/Live über BigBlueButtom (bis zu 40 L"
isCrawled: true
---
Auf der Fury Road in den Feminismus - Postapokalyptische Filme als männliches Narrativ?

Veronika Kracher: Online/Live über BigBlueButtom (bis zu 40 Leute) und Facebook.

Erzählungen – Filme, Literatur, Narrative generell- entstehen nicht in luftleerem Raum, sondern sind in der Regel Reaktionen auf die herrschenden Verhältnisse, sei es nun affirmativ, ablehnend, oder schlicht kommentierend. So können Bilder, Dramen, Bücher, Filme Zeugnis liefern über all das, was die Menschheit beschäftigt, und natürlich: was sie unbewusst beschäftigt und was nur in sublimierter Form ans Tageslicht gelangen darf. Was sagt es also über eine Gesellschaft aus, in der in Filmen, Serien, Comics und Videospielen immer und immer wieder das Leben nach dem Zusammenbruch der Zivilisation verhandelt wird?

Die Faszination mit dem Ende der Welt ist schon längst aus der Kulturindustrie ins reale Leben übergeschwappt: Gruppen wie „Prepper“ rüsten sich für den kommenden Weltuntergang, einige fiebern ihm sogar begeistert entgegen. Und die meisten von ihnen sind Männer; auch kulturindustriell wird deutlich gemacht, dass die Welt nach dem Zivilisationsbruch eine „Men’s World“ sein wird.

Wie die Mitglieder der Fight Clubs in Chuck Palahniuks gleichnamiger Satire auf toxische Männlichkeit als Ausbruch aus der kapitalistischen Langeweile sehnt man sich nach einer Gesellschaft, in der man wieder richtig Mann sein kann, kein einfacher Wurm mehr im Kompost! Endlich sich und allen anderen Überlebenden täglich beweisen dass man mit Härte und Disziplin dem Chaos die Stirn bieten kann.

Die Journalistin Veronika Kracher analysiert in ihrem Vortrag, inwieweit und warum der postapokalyptische Film sich sowohl visuell als auch narrativ als männliches Genre etabliert hat, was die ideologischen Hintergründe dieser Faszination mit dem Ende der Zivilisation sind, und inwieweit langsam damit gebrochen wird.

Veronika Kracher ist freie Journalistin für die Konkret, Jungle World und taz. Ihr Fokus liegt auf marxistisch-feministischer Gesellschafts- und Kulturtheorie.