---
id: "377638183448847"
title: Reichsbürger - Die unterschätzte Gefahr
start: 2020-11-26 18:00
end: 2020-11-26 19:30
link: https://www.facebook.com/events/377638183448847/
image: 126844898_3474138419288354_4771459100481330832_n.jpg
teaser: "Eine Veranstaltung des Bündnis gegen Rechts Ulm: In Deutschland werden mehr
  als 12000 Menschen zu den sogenannten Reichsbürgern gerechnet. Für sie ist"
isCrawled: true
---
Eine Veranstaltung des Bündnis gegen Rechts Ulm:
In Deutschland werden mehr als 12000 Menschen zu den sogenannten Reichsbürgern gerechnet. Für sie ist die Bundesrepublik kein souveränes Staatsgebilde, sondern bestehen die Deutschen Reiche aus der Zeit vor 1945 fort. Gegenwärtig würden fremde Mächte im Hintergrund die Fäden ziehen. Manche Reichsbürger gründen auch eigene Reiche, wie etwa der König von Deutschland in Wittenberg, stellen eigene Pässe und Führerscheine aus. Viele erkennen die deutschen Behörden nicht an, verweigern Bußgeldzahlungen und Steuern. Lange Zeit hielt der deutsche Staat die Angehörigen dieser Szene für Spinner und tat sie als ungefährlich ab - bis im Oktober 2016 ein Polizist in Franken von einem Reichsbürger erschossen wurde.

Meeting-ID: 865 2498 2569
Passwort/Kenncode: 945181