---
id: "319662292516215"
title: Was ist Kritische Theorie?
start: 2020-09-17 19:00
end: 2020-09-17 20:00
link: https://www.facebook.com/events/319662292516215/
teaser: Was ist Kritische Theorie?  "Mitmachen wollte ich nie. Genau das Negative war
  das Positive, dieses Bewusstsein des Nichtmitmachens, des Verweigerns",
isCrawled: true
---
Was ist Kritische Theorie?

"Mitmachen wollte ich nie. Genau das Negative war das Positive, dieses Bewusstsein des Nichtmitmachens, des Verweigerns", so charaktierisiert Leo Löwenthal den Kern der Kritischen Theorie.  Nicht Mitmachen im falschen Leben - das war das Programm der Kritischen Theorie der Frankfurter Schule. Adorno, Horkheimer, Marcuse, Löwenthal, Benjamin und andere - sie analysierten mit kritischem Blick, zeigten unerbittlich Widersprüche auf und demaskieren die moderne Gesellschaft in all ihren Facetten. Ob Psychologie, Pädagogik, Literatur, Ökonomie, Musik, Popkultur, Philosophie - überall finden sich die Spuren der Kritischen Theorie. Trotz oder gerade aufgrund ihrer kritischen Verweigerung gaben sie jedoch nie die Utopie einer besseren Gesellschaft, die Hoffnung auf das gute Leben, auf.

Es wird zudem einen Bezug auf die aktuelle Zeit geben.

17.09. 19 Uhr Online auf Facebook (bitte nach Möglichkeit diese Option nutzen): https://www.facebook.com/kollektiv.26/live/

Möglichkeit zur Teilnahme besteht auch (zahlenmäßig begrenzt) über BigBlueButtom: https://webinar.rosa-reutlingen.de/b/kol-lb9-sju