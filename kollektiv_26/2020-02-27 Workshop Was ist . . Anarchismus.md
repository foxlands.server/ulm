---
id: "2693572120732094"
title: "Workshop: Was ist . . Anarchismus"
start: 2020-02-27 18:00
end: 2020-02-27 21:00
address: Anarres, Mähringerweg 75, 89075 Ulm
link: https://www.facebook.com/events/2693572120732094/
image: 79818306_2604202816281923_6954981060857823232_n.jpg
teaser: Neben der kommunistischen Bewegung stellt der Anarchismus einen zentralen
  Bezugspunkt der zeitgenössischen Linken dar. Die Beziehung der beiden 'verfe
isCrawled: true
---
Neben der kommunistischen Bewegung stellt der Anarchismus einen zentralen Bezugspunkt der zeitgenössischen Linken dar. Die Beziehung der beiden 'verfeindeten Brüder' sorgt nach wie vor für hitzige Debatten und Zerwürfnisse. Doch wie lässt sich der Anarchismus überhaupt definieren? Staatsfeindlich, radikal, gegen Autoritäten?
In der Veranstaltung zum Anarchismus, im Rahmen der Input "Was ist ...?"-Reihe versuchen wir nach einem kurzen Input eine zeitgemäße Definition dieser nicht zu unterschätzenden politischen Strömung zu entwerfen. Außerdem lohnt es sich zu diskutieren, was Kommunismus und Anarchismus voneinander lernen können.

"Was ist..." ist eine Veranstaltungsreihe von INPUT-Ulm/Neu-Ulm zu linken Grundbegriffen. Ab Februar 2020 jeden zweiten Monat (soweit möglich) der letzte Samstag.