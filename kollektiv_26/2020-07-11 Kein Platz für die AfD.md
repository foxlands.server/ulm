---
id: "287135912403341"
title: Kein Platz für die AfD
start: 2020-07-11 14:00
end: 2020-07-11 15:00
link: https://www.facebook.com/events/287135912403341/
image: 107459797_3071163222919211_4364496683769489920_o.jpg
teaser: Wir stellen uns der AfD und ihrer rassistischen Hetze entgegen. Sexualisierte
  Gewalt darf nicht instrumentalisiert werden.
isCrawled: true
---
Wir stellen uns der AfD und ihrer rassistischen Hetze entgegen. Sexualisierte Gewalt darf nicht instrumentalisiert werden.