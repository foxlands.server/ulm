---
id: "588954631916422"
title: Besetzt mal Wieder! - Gentrifizierung, Häuserkampf, Eigentum
start: 2020-03-28 18:00
end: 2020-03-28 21:00
address: Anarres, Fort Unterer Eselsberg, Mähringer Weg 75
link: https://www.facebook.com/events/588954631916422/
image: 81264520_2604218172947054_8424363658573774848_n.jpg
teaser: 'Seit Ende 2018 ist auch #besetzen in Freiburg im Breisgau angekommen und im
  Rahmen der "Wohraum-Gestalten"-Kampagne kommt es immer wieder zu Hausbeset'
isCrawled: true
---
Seit Ende 2018 ist auch #besetzen in Freiburg im Breisgau angekommen und im Rahmen der "Wohraum-Gestalten"-Kampagne kommt es immer wieder zu Hausbesetzungen. Ende 2018 wurde in Freiburg eine Besetzungskampagne mit dem Titel -Die WG- (Wohnraum Gestalten) gestartet in Zuge deren 9 verschiedene Häuser besetzt wurden. Anlass sind horrende Mietpreise, mangelnde Wohnungen und teure Neubauprojekte. So sollen alte Arbeiterhäuser neu konzipierten modernen Wohnkomplexen weichen und wieder einmal ein neues Stadtviertel entstehen. Der Vortrag möchte Mechanismen solcher Verdrängung von Menschen mit geringerem Einkommen (Gentrifizierung) aufzeigen und die Rolle von Privateigentum im System „Wohnungsmarkt“ zeigen. Dabei geht es darum, warum überhaupt Häuser besetzt werden, welche Arten von Besetzungen es gibt und wie das dann auch in der Praxis aussieht und welche anderen Widerstandsformen beim Kampf um die Stadt existieren. Dabei dient die Kulisse Freiburgs exemplarisch für Entwicklungen in verschiedenen Städten und soll politische Handlungsmöglichkeiten veranschaulichen. Auf Grundlage des Vortrags soll dabei Raum entstehen eine Vision einer linken Politik für Wohnraum zu diskutieren.