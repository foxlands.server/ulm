---
name: Critical Mass Ulm
website: https://criticalmassulmneuulm.wordpress.com/
email: 
scrape:
  source: facebook
  options:
    page_id: criticalmassulm
---
Die Critical Mass feiert das Fahrrad: Wir sind die Gegenbewegung zur alltäglichen Blechlawine in unserer Stadt. Du bist herzlich eingeladen, mitzuradeln: wir füllen Ulms Straßen mit so vielen Radfahrer\_innen, dass wir nicht mehr zu übersehen sind! Das Fahrrad ist das überlegene Fortbewegungsmittel. Es verursacht keine Emissionen, keinen Lärm, es ist günstig, und es spart Platz. Wir werben für einen Umstieg. Weg vom Auto, weg vom immensen Flächen- und Energieverbrauch, weg von Lärm, Dreck und Umweltverschmutzung — hin zu umweltfreundlicher Forbewegung, hin zu einer fahrradfreundlichen Stadt Ulm!
