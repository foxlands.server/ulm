---
id: "507385876591963"
title: The Broken Fest 2020
start: 2020-08-28 20:00
end: 2020-08-29 23:59
locationName: Club Action
address: Beim Alten Fritz 3, 89075 Ulm
link: https://www.facebook.com/events/507385876591963/
image: 90849252_783731702151484_2685559203837247488_o.jpg
teaser: "Broken Stage proudly presents:  THE BROKEN FEST  Line-Up: tba   NEU: U18
  Eintritt frei!!! Open Air im Club Action Beim Alten Fritz 3, Ulm (bei schlech"
isCrawled: true
---
Broken Stage proudly presents:

THE BROKEN FEST

Line-Up:
tba


NEU: U18 Eintritt frei!!!
Open Air im Club Action
Beim Alten Fritz 3, Ulm
(bei schlechtem Wetter findet die Veranstaltung drinnen statt)

INFOS:
Covid-19 (Corona):
Aktuell ist schwer abzuschätzen, wie sich der Verlauf der Pandemie weiterentwickeln wird und davon abhängig auch, ob unser Festival stattfinden kann. Doch wir sind sehr optimistisch und gehen davon aus, dass The Broken Fest ganz normal stattfinden wird. Sollte es diesbezüglich Änderungen oder Neuigkeiten geben, lassen wir es euch natürlich sofort wissen.


Essen & Getränke:
Damit ihr nicht während eurer Lieblingsband zum Dönerstand in die Stadt rennen müsst, weil euch der Magen in den Kniekehlen hängt, wird es auch dieses Jahr wieder einen Essensstand geben. Im Club Action an der Bar gibt es außerdem die gewohnten alkoholischen und anti-alkoholischen Getränke.

Anfahrt & Parken:
Bus: Der Club Action ist nur etwa 15 Geh-Minuten von Innenstadt und Hauptbahnhof entfernt. Wer trotzdem mit dem Bus fahren will, die Linie 2 und Linie 5 hält direkt davor. Vom Hbf aus Richtung "Wissenschaftsstadt" oder "Science Park II", Haltestelle "Lehrer Tal" aussteigen.
Auto: Von der Kienlesbergstraße aus in die Straße "Beim Alten Fritz" abbiegen und der Straße bergauf folgen. Nach etwa 200 Metern kommt, nach der Kurve, auf der linken Seite ein Tor. Das ist die Einfahrt zum Parkplatz. Vom Parkplatz aus dann einfach dem Feldweg entlang der Festungsmauer zum Konzertgelände folgen.

Sponsoren:
Bei der Auswahl unserer Sponsoren, legen wir viel Wert darauf, Unternehmen auszuwählen, welche wir unterstützenswert finden und bei welchen wir selbst gerne konsumieren. Bei unseren Sponsoren handelt es sich ausnahmslos um die Betriebe von Freunden.
Wir danken allen unseren Sponsoren herzlich für die Zusammenarbeit. Ohne unsere Sponsoren wäre das Festival nicht möglich.
Wir werden unterstützt von:

Hemperium

No Pain No Gain Tätowierungen

Schreinerei Necknig

Turbine Powered

Außerdem wird das Festival präsentiert von:
AWAY FROM LIFE
Fragen & Anregungen:
Wenn ihr Fragen oder Vorschläge zum Festival habt, dann könnt ihr uns auf verschiedene Wege kontaktieren: In die Veranstaltung posten, Nachricht an unsere Facebook-Seite, oder eine E-Mail an info@thebrokenfest.com

LOVE MUSIC, HATE FASCISM