---
name: Christopher Street Day Ulm, Neu-Ulm
website: http://www.csdulm.de
email:
scrape:
  source: facebook
  options:
    page_id: CSDUlm.NeuUlm
---
Der CSD ist eine Demonstration und Feier gegen Diskriminierung und für Gleichstellung und Akzeptanz.
