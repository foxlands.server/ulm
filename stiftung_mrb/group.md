---
name: Stiftung Menschenrechtsbildung
website: http://www.stiftung-mrb.de
email: kontakt@stiftung-menschenrechtsbildung.de
scrape:
  source: facebook
  options:
    page_id: stiftungmrb
---
Die Stiftung Menschenrechtsbildung verleiht den Ulmer Menschenrechtspreis und trägt zur Aufklärung über Menschenrechtsthemen durch Veranstaltungen bei.
