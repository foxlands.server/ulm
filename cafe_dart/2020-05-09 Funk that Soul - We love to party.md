---
id: "465281307445068"
title: Funk that Soul - We love to party
start: 2020-05-09 20:00
end: 2020-05-09 23:30
locationName: Café D'Art
address: Augsburgerstraße 35, 89231 Neu-Ulm
link: https://www.facebook.com/events/465281307445068/
image: 87485397_1630939433723664_8684721904587636736_n.jpg
teaser: Cafe D'Art die Dritte. Nach dem erfolgreichen Konzert mit vollem Haus im
  Novemeber 2019 sehen wir uns im Jahr 2020 wieder ;-)
isCrawled: true
---
Cafe D'Art die Dritte. Nach dem erfolgreichen Konzert mit vollem Haus im Novemeber 2019 sehen wir uns im Jahr 2020 wieder ;-)