---
name: Showbuddies
website: http://www.showbuddies.de
email: info@showbuddies.de
scrape:
  source: facebook
  options:
    page_id: Showbuddies
---
Beim Improvisationstheater entstehen alle Szenen spontan nach den Vorgaben des Publikums. Erleben Sie einen Abend voller witziger, charmanter Uraufführungen. Die Showbuddies stellen sich dem Unbekannten und zaubern eine spontane Show für Sie.
Lassen Sie sich überraschen und mischen Sie mit!
