---
name: Fashion Revolution Ulm / Neu-Ulm
website: http://www.fischerins-kleid.de
email: info@fischerins-kleid.de
scrape:
  source: facebook
  options:
    page_id: fashionrevolutionulmneuulm
---
Wir (Fischerins Kleid & Ulm UnUsual) wollen die Fashion Revolution nach Ulm und Neu-Ulm tragen und damit möglichst viele Menschen erreichen.
