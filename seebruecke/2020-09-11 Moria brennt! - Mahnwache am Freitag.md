---
id: "326544441992072"
title: Moria brennt! - Mahnwache am Freitag
start: 2020-09-11 19:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/326544441992072/
teaser: 🔥Moria brennt!🔥  🔸Mahnwache, Freitag 19:00, Münsterplatz 🔸  Spätestens
  jetzt müsste die EU erkannt haben, dass Moria kein Ort zum Leben ist - erst
isCrawled: true
---
🔥Moria brennt!🔥 
🔸Mahnwache, Freitag 19:00, Münsterplatz 🔸

Spätestens jetzt müsste die EU erkannt haben, dass Moria kein Ort zum Leben ist - erst recht nicht in einer Pandemie, nicht nach einem Großbrand, einfach nie und für niemanden! Deshalb veranstalten wir eine Mahnwache, um zu zeigen, dass Ulm Platz hat, um Geflüchtete aufzunehmen. Freitag, 19:00, Münsterplatz. 

Sagts euren Freund*innen und kommt zahlreich! #evacuatenow #leavenoonebehind #wirhabenplatz