---
id: "606891443431184"
title: "Bass+Coma: All-Starz"
start: 2020-03-14 23:00
end: 2020-03-15 05:00
locationName: Bass+coma
address: Stuttgarter Straße 15, 89075 Ulm
link: https://www.facebook.com/events/606891443431184/
image: 88142331_2584671521799825_8825995696919281664_o.jpg
teaser: "+Deep Space Bass Celebration+ +Residents n' Friends+  DJs:  +MRC BP Crew+
  (Bassportation)  +Dubdron+ (Bass+Coma)  +Half Ace+ (Deep Within Recordings |"
isCrawled: true
---
+Deep Space Bass Celebration+
+Residents n' Friends+

DJs:

+MRC BP Crew+
(Bassportation)

+Dubdron+
(Bass+Coma)

+Half Ace+
(Deep Within Recordings | Bassportation | Bass+Coma)

+Drop Cake+
(Bass+Coma)

+Smuggah+
(Bass+Coma)

MCs:

Silance
(TCFTC)

Rucky Dee
(Phantom Dub Digital)


+No Drugs No Violence+
+No ID no Entry+ 