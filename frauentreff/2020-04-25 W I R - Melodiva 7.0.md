---
id: "2404178879812863"
title: W I R - Melodiva 7.0
start: 2020-04-25 19:30
end: 2020-04-25 22:00
locationName: Altes Theater Ulm
address: Wagnerstraße 1, 89077 Ulm
link: https://www.facebook.com/events/2404178879812863/
image: 66647801_2953751241363069_6203782686557863936_o.jpg
teaser: "Endlich ist es wieder soweit! Und vermutlich gerade noch rechtzeitig: Google,
  Facebook und Co. haben längst aufgegeben - W.i.R. hat die Welt nun fest"
isCrawled: true
---
Endlich ist es wieder soweit! Und vermutlich gerade noch rechtzeitig: Google, Facebook und Co. haben längst aufgegeben - W.i.R. hat die Welt nun fest im digitalen Würgegriff des Web 7.0.

Kann Melodiva uns vor dem digitalen Supergau noch retten? So ganz analog?
Erlebt den Münchner Lesbenchor mit seinen neuen Mitgliedern Siri und Alexa bei der musikalischen Datenverarbeitung aus Schlager, Pop, Rock und einer spannenden Geschichte mit algorhythmischem Ohrenschmaus!