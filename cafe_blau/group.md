---
name: Café Blau
website: http://www.agwest.de
scrape:
  source: facebook
  options:
    page_id: CafeBlauUlm
---
Unser attraktives Café im Ulmer Dichterviertel wird von vielen Ehrenamtlichen mitgetragen. Es lebt vom gemeinschaftlichen Gestalten ohne finanziellen Profit.
