---
id: "1765422183597229"
title: Champions League Finale 2020 Live
start: 2020-08-23 20:00
end: 2020-08-24 01:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/1765422183597229/
teaser: Wir übertragen das Champions League Finale 2020, Paris Saint-Germain gegen den
  FC Bayern München, Sonntagabend Live im Eden Kino. Für kühles Bier ist
isCrawled: true
---
Wir übertragen das Champions League Finale 2020, Paris Saint-Germain gegen den FC Bayern München, Sonntagabend Live im Eden Kino. Für kühles Bier ist gesorgt. Kommt vorbei, denn alleine schauen ist doof. 