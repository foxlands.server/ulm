---
id: "773742793420685"
title: "Plattentest: 5 Gänge Vinyl mit DJ Alkapulko Roy"
start: 2020-10-01 20:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/773742793420685/
teaser: Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das
  Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs
isCrawled: true
---
Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs mit getragen haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge. 
Zum Anpfiff drinnen stellt sich Alkapulko Roy an die Teller. - Wer kennt Sie nicht. Azzeasy und Alkapulko Roy, die Old Kidz on the Block. Gestartet im Su Casa, später im Eden, entwickelte sich Ihre Partyreihe mit zur heißesten Show in Ulm. Wo die Old Kidz waren, da war immer mächtig was los. Und als ehemaliger Betreiber des Edens ist ein Abend mit Roy schon längst überfällig. Lieblingsplatten hat er genug, darüber müssen wir nicht reden. Er freut sich schon auf's Eden, seiner alte Wirkungsstätte, und wir, wir tun das auch!