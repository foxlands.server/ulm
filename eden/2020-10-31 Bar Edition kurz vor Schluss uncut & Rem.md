---
id: "425600782018956"
title: "Last Match: Reminder & Un:Cut"
start: 2020-10-31 17:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/425600782018956/
image: 123074601_3696318980413315_6304440130989365719_o.jpg
teaser: Reminder und Un:Cut (deleted user) kennt man mittlerweile als Urgesteine der
  Ulmer Drum´n´Bass-Szene und unter anderem auch durch die Dubstep Event-Re
isCrawled: true
---
Reminder und Un:Cut (deleted user) kennt man mittlerweile als Urgesteine der Ulmer Drum´n´Bass-Szene und unter anderem auch durch die Dubstep Event-Reihe subt0ne, welche sie mit gegründet haben. Wer ab und an Radio FreeFM hört, kennt als geneigter dnb head sicherlich die StepUp Show und Sets von den beiden.
Zum Last Match vor der Zwangspause spielen die 2 Liquid dnb-Vinyl von 2005 bis heute.  

Eintritt zwei Euro (Dj-Zwickel), Zutritt maximal 76 Personen

Anfang Dezember startet das Eden dann wieder mit neuen Vinyl-Liebhabern und deren heißen Scheiben in die Winter-Saison - watch out!