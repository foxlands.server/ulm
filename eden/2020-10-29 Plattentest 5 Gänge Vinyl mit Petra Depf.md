---
id: "449755039341452"
title: "Plattentest: 5 Gänge Vinyl mit Petra Depfenhart"
start: 2020-10-29 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/449755039341452/
image: 122865867_3684381441607069_1538294180951338153_o.jpg
teaser: Es ist soweit! Endlich legt auch eine Frau beim Plattentest auf, und dann
  gleich Petra, die in Sachen Musik enorm was zu bieten hat. Als "Disco-Petra"
isCrawled: true
---
Es ist soweit! Endlich legt auch eine Frau beim Plattentest auf, und dann gleich Petra, die in Sachen Musik enorm was zu bieten hat. Als "Disco-Petra" normalerweise auf der Tanze, ist sie heute beim 5-Gänge-Vinyl hinterm Plattenteller zu finden. Wer eintönigen Discostampf oder faden Musikeintopf erwartet, ist falsch gewickelt. Eine Prise Elektro hier, ein Schuss Synthiepop da, fertig ist die vollwertige Musik-Kost. Wenn Du nett fragst, bekommst Du schon auch mal 'nen Nachschlag. 