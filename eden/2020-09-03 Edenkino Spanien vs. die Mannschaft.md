---
id: "1636607556501593"
title: "Edenkino: Spanien vs. die Mannschaft"
start: 2020-09-03 20:45
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/1636607556501593/
teaser: Während DJ Bore im Lustgarten seine Lieblingsplatten präsentiert, läuft
  Donnerstagabend im Edenkino das Spiel Spanien gegen die Mannschaft. Spätsommer
isCrawled: true
---
Während DJ Bore im Lustgarten seine Lieblingsplatten präsentiert, läuft Donnerstagabend im Edenkino das Spiel Spanien gegen die Mannschaft. Spätsommermärchen!