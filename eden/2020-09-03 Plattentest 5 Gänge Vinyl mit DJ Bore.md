---
id: "2740438726275886"
title: "Plattentest: 5 Gänge Vinyl mit DJ Bore"
start: 2020-09-03 19:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/2740438726275886/
teaser: Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das
  Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten. Ungem
isCrawled: true
---
Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge.

Unter dem Namen DJ Bore verbirgt sich Martin vom Plattenladen Soundcircus, und der hat zweifellos eine besondere Wahrnehmung für Schallplatten, hatte bestimmt schon Millionen von Scheiben in seinen Händen. Früher, als es in Ulm noch keinen Plattenladen gab, ist er extra nach Berlin gefahren, um den neuen heißen Scheiß für Ulm zu kaufen. Da war schon klar, dass er mal ein erfolgreicher DJ wird. Angefangen hat sein Auflegen im Violet, später im Mahatma, und mit der Partyreihe Soul-a-Go-Go noch viele Jahre im Eden, um nur mal einige seiner Stationen zu nennen. Wir im Lustgarten sind jetzt natürlich besonders gespannt, fast schon nervös, welche fünf Platten Martin für uns spielen wird. Bei Bier und chilligen Drinks lassen wir uns überraschen ....