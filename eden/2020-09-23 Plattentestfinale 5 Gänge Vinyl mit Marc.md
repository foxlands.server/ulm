---
id: "360279715110482"
title: "Plattentestfinale: 5 Gänge Vinyl mit Marco & Klaus"
start: 2020-09-23 18:00
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/360279715110482/
teaser: Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das
  Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs
isCrawled: true
---
Beim Fünf-Gänge-Menü präsentieren euch DJs, die in den vergangenen Jahren das Cabaret Eden mit Musik bespielt haben oder maßgeblich die Idee des Clubs mit getragen haben, ihre fünf Lieblingsplatten. Ungemixt in voller Länge. 
Zum letzten Plattentest dieser Saison - der Sommer ist bald rum - kommt die Musik direkt aus der heißen Küche. Marco aus der Pinocchio Pizza-Werkstatt und Klaus Eden mit seiner Pommesbraterei werden mit einer delikaten Plattenauswahl für gute Stimmung sorgen - ein echtes Finale also!