---
id: "1202457423459409"
title: fremde früchtchen - strange fruit
start: 2020-09-19 18:00
end: 2020-09-19 23:45
locationName: Cabaret Eden
address: Karlstraße 71, 89073 Ulm
link: https://www.facebook.com/events/1202457423459409/
teaser: Im Rahmen der Kulturnacht Ulm/Neu-Ulm 2020 re-inszeniert das Künstlerduo
  schöne haut im Eden den Sündenfall mit fremden Früchtchen. Billie Holidays ti
isCrawled: true
---
Im Rahmen der Kulturnacht Ulm/Neu-Ulm 2020 re-inszeniert das Künstlerduo schöne haut im Eden den Sündenfall mit fremden Früchtchen. Billie Holidays titelgebender Song von 1939 und sein verstörender Text dient als Folie, um unsere Rolle als weiße Deutsche und die damit verbundenen Machtstrukturen zu hinterfragen. – Der Fall George Floyd ist einen Ozean von uns entfernt und die Verhältnisse in den USA sind kaum übertragbar, oder gibt es doch Ärger im Paradies? Und falls ja, sollten wir den nicht besser unter den Teppich kehren? - Mixed Media Installation von Cora Schönemann und Marc Hautmann. 

Beginn stündlich ab 18 Uhr. Für süße Säfte wird gesorgt.

Co-Veranstalter: Kulturnacht Ulm/ Neu-Ulm

Herzlichen Dank auch an die Kunstarkaden Kempten https://www.facebook.com/search/top/?q=kunstarkaden%20kempten für die Bereitstellung des Drehortes Im Artis-in-Residence-Programm!
