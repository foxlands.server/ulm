---
name: Cabaret Eden
website: http://www.cabareteden.de
email: info@cabareteden.de
scrape:
  source: facebook
  options:
    page_id: cabaret.eden
---
Veranstaltungsort für Live-Musik, Tanz- und Nachtclub
