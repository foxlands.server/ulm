---
id: "2699997733595940"
title: "Wohnzimmerkonzert: Rosa Blut ,- Rock'n Roll & Liebe"
start: 2020-10-30 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/2699997733595940/
image: 122245707_3587382874638153_4358414539046766714_o.jpg
teaser: Hallo liebe Ulmer, wir sind Rosa Blut aus München.  Nachdem wir vor etwa zwei
  Monaten einen unfassbaren Tag in Ulm verbracht haben um unsere Bandfoto
isCrawled: true
---

Hallo liebe Ulmer, wir sind Rosa Blut aus München.

Nachdem wir vor etwa zwei Monaten einen unfassbaren Tag in Ulm verbracht haben um unsere Bandfotos schießen zu lassen (von der hervorragenden Fotografin Janine Juliane Grüber) sind wir am Freitag den 30.10. wieder bei Euch!
Diesmal um ein Konzert zu spielen.
Und zwar in Judith's wunderschöner Apotheke!

Wie wir klingen? Ziehts Euch rein:

https://www.youtube.com/watch?v=isIuQEY2iHY&feature=share

Alle Songs die wir für Euch im Gepäck haben sind sozusagen frisch aus dem Backofen und warten nur darauf verspeist zu werden!

Rosa Blut

https://www.instagram.com/rosablutmusik/

https://m.facebook.com/Rosablutmusik/