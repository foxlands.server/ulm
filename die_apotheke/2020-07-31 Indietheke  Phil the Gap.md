---
id: "312158439907191"
title: Indietheke / Phil the Gap
start: 2020-07-31 21:00
end: 2020-08-01 02:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/312158439907191/
teaser: Heute gibt es in Eurer Lieblings - Apotheke nicht nur leckere Drinks, sondern
  auch wundertolle Musik auf die Ohren.  Serviert werden Klassiker von T
isCrawled: true
---
Heute gibt es in Eurer Lieblings - Apotheke nicht nur leckere Drinks, sondern auch wundertolle Musik auf die Ohren.

Serviert werden Klassiker von The Cure oder The Smiths, Dauerbrenner von Franz Ferdinand, Kraftclub oder The Killers, aber auch viel Genreverwandtes rund um Bilderbuch, Daft Punk oder Jan Delay! Hauptsache es macht gute Laune.

Get on your dancing shoes and let´s dance to Joy Division! ;)

DJ´s Phil the Gap ( Eden rocks Indie, Indiekranz)