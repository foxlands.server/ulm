---
id: "341922623849328"
title: Süper sound
start: 2020-09-05 21:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/341922623849328/
teaser: Anatolian rock, psychedelic, disko, folk... süper sound mit Songül🌹
isCrawled: true
---

Anatolian rock, psychedelic, disko, folk...
süper sound mit Songül🌹