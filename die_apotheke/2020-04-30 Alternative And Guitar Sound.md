---
id: "2786143351464729"
title: Alternative And Guitar Sound
start: 2020-04-30 21:00
end: 2020-05-01 01:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/2786143351464729/
image: 88434821_2960613433981770_3165326464052625408_n.jpg
teaser: Tanz in den Mai, geht dir am Allerwertesten vorbei? ;) Dann gibt`s heute bei
  uns die Alternative...  Wir werden mit alternative und legendärem guitar
isCrawled: true
---
Tanz in den Mai, geht dir am Allerwertesten vorbei? ;) Dann gibt`s heute bei uns die Alternative...

Wir werden mit alternative und legendärem guitar sound ins lange Wochenende starten... na, wenn das kein Grund zu Feiern ist!

Ein Hoch auf Nirvana, H-Blockx, Incubus und Foo Fighters... heute wirds laut! Und wir sind heiß!

Eintritt wie immer frei!