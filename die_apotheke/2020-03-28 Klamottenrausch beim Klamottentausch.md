---
id: "168550594451648"
title: Klamottenrausch beim Klamottentausch
start: 2020-03-28 10:00
end: 2020-03-28 18:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/168550594451648/
image: 83943688_2885564498153331_3603852675197698048_n.jpg
teaser: Hi Mädels! Habt ihr auch zu viel Klamotten im Schrank? Es ist Zeit mal wieder
  Auszumisten? Ballast loszuwerden? Und zum Wegwerfen sind die Klamotten d
isCrawled: true
---
Hi Mädels! Habt ihr auch zu viel Klamotten im Schrank? Es ist Zeit mal wieder Auszumisten? Ballast loszuwerden? Und zum Wegwerfen sind die Klamotten doch eh viel zu Schade... na, dann nutzt die Möglichkeit bei uns und tauscht was das Zeug hält...

Habt ihr Lust in gemütlicher Atmosphäre über den Tag verteilt nach Klamottenschätzen zu stöbern? Dann bringt ab 20. März eure Klamotten (bis 20 Teile) vorbei und nehmt euch an dem Tag des Klamottentauschs maximal so viel neue schicke Fummel wieder mit!

Verbindet den Tausch doch mit einem leckeren Frühstück, einem Glas Sekt oder gönnt euch Kaffee und Kuchen... wie auch immer, habt Spaß, schöne Stunden und fühlt euch mit euren Funden reich beschenkt!

(Nicht getauschte Klamotten können gerne am 28.03.20 wieder abgeholt werden - bitte dann mit Namen versehen - oder ihr spendet die Kleidung für einen guten Zweck und lasst sie da!)

Wir freuen uns auf euch und den Tag! 

Der Klamottentausch ist natürlich kostenfrei! :)