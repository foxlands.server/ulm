---
id: "317512702666644"
title: Miniflohmarkt
start: 2020-10-24 10:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/317512702666644/
image: 119168671_3470750206301421_5224994150088141821_o.jpg
teaser: Miniflohmarkt in der Apotheke? Kein morgens um 4 Aufstehen, kein Frieren, kein
  Regen... Wer Lust hat mitzumachen meldet sich vorher bei uns an. Gerne
isCrawled: true
---
Miniflohmarkt in der Apotheke?
Kein morgens um 4 Aufstehen, kein Frieren, kein Regen...
Wer Lust hat mitzumachen meldet sich vorher bei uns an. Gerne auch per mail: kulturapotheke@ gmx.de (wer zuerst kommt malt zuerst😊) Wir reservieren Euch einen Tisch an dem Ihr dann Euch Sachen verscherbeln könnt.
Die Standgebühr beträgt zwischen 10 und 15 Euro.
Ab 9.00 Uhr kann aufgebaut werden.
Selbstverständlich gibt es Kaffee & Kuchen und vielleicht auch ein Gläschen Sekt🥂
Wir freuen uns auf Euch und Euren Krempel🥰