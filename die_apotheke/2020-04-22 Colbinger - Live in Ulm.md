---
id: "460120931366927"
title: Colbinger - Live in Ulm
start: 2020-04-22 20:00
end: 2020-04-22 23:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/460120931366927/
image: 81549898_10157333883348702_1358367335315931136_o.jpg
teaser: Colbinger - Stimme, Songwriter, Lyriker & Gitarrist    TOURING TOURING TOURING
  - Konzerte & Termine 2020       www.colbinger.com  Colbinger - Stimme,
isCrawled: true
---
Colbinger - Stimme, Songwriter, Lyriker & Gitarrist    TOURING TOURING TOURING - Konzerte & Termine 2020       www.colbinger.com

Colbinger - Stimme, Songwriter, Gitarrist und Weltenbummler aus Süddeutschland. Er kennt das Licht und die Schatten, die Freuden und die Zweifel und vor allem die Hoffnung, die einen weitergehen lässt und nennt seine Songs Erinnerer und Gefährten. Seine charismatische Stimme, von rauh bis zart, von laut bis leise und sein sehr persönlicher Stil verweben Akustikrock mit Country und Folk, gewürzt mit Leichtigkeit des Funks und der Melancholie des Blues. Colbinger hat eine klare Botschaft: "Die Gewissheit um die Schaffenskraft des Guten in allem." Er nimmt uns mit auf eine Reise, geprägt von der unbändigen Lust am Leben und sich den eigenen Irrungen und Wirrungen zu stellen, um vielleicht ein Augenzwinkern später eine Erkenntnis reicher zu sein. Alles beginnt mit uns im Hier und Jetzt und nicht anderswo. Es ist seine natürliche Ausdruckskraft und das Gespür für den Augenblick, die poetisch berührenden Texte und die musikalische Freude, die die Herzen seines Publikums erreichen und begeistern.
So auch sein Leitsatz: "Machen wir uns auf und bleiben wir dran."

Colbinger ist in 2020 weiter auf  "Machen wir uns auf und bleiben wir dran" - Tour. Diese startete in 2018 und umfasst seither ca. 280 Konzerte in Deutschland, Österreich und auch der Schweiz. Auf seinem gesamten Weg bis hier, teilte er beispielsweise schon die Bühne mit Tito & Tarantula, Alvin Lee, Eb Davis, Jesper Munk, Sophie Hunger oder Oneida James Rebeccu, der langjährigen Bassistin von Joe Cocker. Die Reise geht weiter. www.colbinger.com

Der neue Tag - Oberpfälzischer Kurier - Bayern 12/2019
"Charismatischer Meister der Poesie"

PNP - Passauer Neue Presse 09/2019
"Colbinger hatte einen beeindruckenden Auftritt in Simbach und begeisterte mit seinen Texten seine Zuhörerschaft, die sichtlich zufrieden den Heimweg antrat."

Frizz Leipzig - Monatsmagazin 04/2019
"...er macht Station, um positive Ideen in die Menge zu singen. Ein seltener Schatz."

Oberpfälzer Zeitungen - Onetz.de 04/2019
"Ein leidenschaftlicher Musikabend mit kraftvollem und mitreißendem Gitarrensound. Colbingers Stil war ein mit Country, Folk und bluesiger Melancholie verwobener Akustik-Rock. Die deutschen Texte interpretierte er überzeugend. Denn der Texter war ein Verfechter seiner Botschaften. Das Gespür für den Augenblick, gepaart mit musikalischer Freude und Lebenserfahrung, zeigten ganz große Liedkunst. 

Traunsteiner Tagblatt - Südostbayern - Konzertrezension 12/2017
"Er begeisterte durch unbeschreibliche Authentizität und einem sehr kraftvollen, mitreißenden Sound, Elemente aus Rock, Country und Folk gemischt mit seiner scharfen Stimme prägen seinen Stil. Er ist ein Songpoet und streut immer wieder Geschichten ein und verriet Träume sind keine Orte, sondern ein Weg. Das Konzert beendete er mit mehreren Zugaben vor bewegt begeistertem Publikum und versprach wiederzukommen."


Schallmagazin Berlin 1/2016
"...er macht aus Lebenserfahrung und alten Wunden große Liedkunst...und wird mit seinem neuen Album viele neue Fans dazugewinnen. Ganz so wie er im Finalen Song "King Without A Crown" singt: "In uns allen steckt ein König ohne Krone, erinnern wir uns gemeinsam daran."

Folker Magazin - ByteFM 1/2016
"Colbinger gelingt mit seinem Solowerk das Kunststück ein Album zu produzieren, das auch nach vielfachem Hören viel Freude bereitet...da bekommt das ausgelutschte Sprichwort „er macht sein Ding“ neue Authentizität." 

Er-em online.de 12/2015
"Colbinger interpretiert die Texte mit Überzeugung, der Mann steht hinter seinen Botschaften, und
liefert ein Album, das zum konzentrierten Zuhören einlädt und viel Stoff für eigene Gedankengänge
liefert." 

Landeszeitung Lüneburg 11/2015
"Colbinger hat dieses leidenschaftlich-sehnsüchtige Timbre auf den Stimmbändern, die einen
dazu bringt, mitzufühlen. Übrigens: eine Stimme und eine Gitarre – mehr braucht es nicht, um
Herzen zu erobern."