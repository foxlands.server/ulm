---
name: Die Apotheke
website: https://www.facebook.com/pg/Die-Apotheke-1697727243603735
email: kulturapotheke@gmx.de
scrape:
  source: facebook
  options:
    page_id: Die-Apotheke-1697727243603735
---
Die Apotheke ist ein Café in der Ulmer Oststadt. Der ehemaligen Zundeltor-Apotheke wird mit Kaffee und allerlei kulturellen Veranstaltungen neues Leben eingehaucht.
