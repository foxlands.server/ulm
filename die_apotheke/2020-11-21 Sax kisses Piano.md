---
id: "794129008015809"
title: Sax kisses Piano
start: 2020-11-21 20:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/794129008015809/
image: 120909686_3545913515451756_3160390363900295368_n.jpg
teaser: Sax kisses Piano - Silvia Bleicher (sax) und Henning Dampel (piano) verzaubern
  mit ihrem gekonnten Wechselspiel zwischen unterhaltsamen Jazz-Klassiker
isCrawled: true
---
Sax kisses Piano - Silvia Bleicher (sax) und Henning Dampel (piano) verzaubern mit ihrem gekonnten Wechselspiel zwischen unterhaltsamen Jazz-Klassikern, speziell arrangierten Pop-Songs und stimmungsvollen Eigenkompositionen.