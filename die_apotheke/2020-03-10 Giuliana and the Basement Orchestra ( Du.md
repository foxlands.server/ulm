---
id: "187750155785165"
title: Giuliana and the Basement Orchestra ( Duo ) live
start: 2020-03-10 20:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/187750155785165/
image: 86932357_551582778900679_6870432422022873088_n.jpg
teaser: Gatbo kommen nach Ulm!  Zumindest ein Teil von uns...    Giuliana begeistert
  mit ihrer relaxten, tiefen Stimme und eingängigen Rhythmen auf der Gitarr
isCrawled: true
---
Gatbo kommen nach Ulm! 
Zumindest ein Teil von uns...
 

Giuliana begeistert mit ihrer relaxten, tiefen Stimme und eingängigen Rhythmen auf der Gitarre. Mit verschiedenen Einflüssen aus dem Jazz, Soul und Punk kreiert sie einen ganz eigenen, aussergewöhnlichen Sound. Genau diese Mischung machen ihre Songs, Marke Eigenbau, auch so authentisch und das nicht erst seit gestern! Schon seit einigen Jahren bespielt sie kleinere Locations, arbeitete bereits bei Kunstprojekten mit und musiziert nach wie vor mit Leidenschaft in den Fußgängerzonen etlicher Städte wie Potsdam, Ulm und Hamburg. 
Mal ehrlich, fragil, mal ironisch und gewitzt. 
Eine einzigartige Stimme mit großen Zielen.

An ihrer Seite findet sich an diesem Abend Ramon Walter - Gitarrist und Multitalent. 
Mit seiner rauhen, kraftvollen Stimme und seinen ausgeprägten Fertigkeiten an den Saiten bildet er gemeinsam mit Giuliana ein mehr als dynamisches Duo. 

Beginn: 20 Uhr 
Der Hut wird rumgereicht und auch an diesem Abend: Es ist Scheinwerfertag! 
Wir freuen uns! :)

