---
id: "197367818004774"
title: Moonshine Pop - DJ Abend
start: 2020-03-27 21:00
end: 2020-03-28 01:00
locationName: Die Apotheke
address: Olgastraße 143, 89073 Ulm
link: https://www.facebook.com/events/197367818004774/
image: 86474398_2910713402305107_2090934407773814784_o.jpg
teaser: Ihr habt Bock auf einen gemütlichen DJ-Abend mit feiner Mucke? Dann seid ihr
  heute bei uns genau richtig!  Matze Edel von free.fm bespielt die Apothek
isCrawled: true
---
Ihr habt Bock auf einen gemütlichen DJ-Abend mit feiner Mucke? Dann seid ihr heute bei uns genau richtig!

Matze Edel von free.fm bespielt die Apotheke mit gepflegter Konversationsmusik. Auf den virtuellen Plattentellern drehen sich Indietronica von der Insel, DDR-Soul, Twee aus den Highlands oder japanischer Folk.

Eintritt frei. UND IHR SEID DABEI!