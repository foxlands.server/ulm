---
id: "1"
title: KUNDGEBUNG - Nein zu Rassismus! Gemeinsam sind wir stark!
start: 2020-06-13 15:30
end: 2020-06-13 18:00
address: Münsterplatz, Ulm
link: https://www.facebook.com/events/1198314567186726/
image: 2020-06-13-demo.jpg
teaser: "Redebeiträge, Flashmobs, Plakataktionen. Gemeinsam sind wir stark gegen Rassismus!"
---

**KUNDGEBUNG**

**Wann:** Samstag, 13.06.2020, 15:30 Uhr

**Wo:** Münsterplatz Ulm

**Was:** Redebeiträge, Flashmobs, Plakataktionen.

