---
id: "678572049442822"
title: Philosphie am Abend - Macht
start: 2020-11-20 20:00
address: vh ulm - club orange
link: https://www.facebook.com/events/678572049442822/
image: 120189529_1249085348763347_5411202550777998570_n.jpg
teaser: Philosophie am Abend - mit Dr. Martin Böhnisch  Wer Macht ausübt, scheint
  hinlänglich klar. Diese Erfahrung haben wir bereits als Kinder machen können
isCrawled: true
---
Philosophie am Abend - mit Dr. Martin Böhnisch 
Wer Macht ausübt, scheint hinlänglich klar. Diese Erfahrung haben wir bereits als Kinder machen können. Hier mein Wille, dort der Wille des Anderen. Hier ein Sieger, dort ein Verlierer. Was Macht jedoch ist, ist hingegen weniger leicht zu bestimmen. Ist es eine Art Tätigkeit einer Person, eine Gesinnung vieler, eine eigentümliche Kraft oder eine Art des Herrschens? Für den berühmten Soziologen Max Weber war und ist der Begriff der Macht schon immer „amorph“ gewesen.

In der Veranstaltung „Philosophie am Abend“ wollen wir der Macht etwas auf den Grund gehen. Dazu werden wir uns nicht nur auf der Makroebene bewegen. Auch die Meso- und Mikroebene soll in den Blick genommen werden. Eingeladen sind alle philosophisch Interessierten und die es noch werden wollen. 