---
name: Aegis Literatur Buchhandlung
website: https://www.genialokal.de/buchhandlung/ulm/aegis-literatur
email: info@aegis-literatur.de
scrape:
  source: facebook
  options:
    page_id: aegisbuchhandlung
---
Aegis Literatur, einer der ältesten Buchhandlungen in Ulm, in einer der schönsten Gassen von Ulm, direkt hinter dem Ulmer Münster. Moderne Literatur, Klassiker, Kinderbücher,Lesungen,Veranstaltungen, Buchvorstellungen, alles was das Leserherz begehrt.
