---
id: "195211108479102"
title: "Literaturwoche Donau 2020: Lesung Ivna Zic: Die Nachkommende"
start: 2020-04-19 19:30
end: 2020-04-19 21:00
locationName: adk ulm
address: Zinglerstrasse 35, 89077 Ulm
link: https://www.facebook.com/events/195211108479102/
image: 88097076_2798395873539512_2698443830847340544_o.jpg
teaser: „Wie funktioniert Erzählen überhaupt? Wie können wir Dinge nicht erzählen und
  damit trotzdem ganz viel erzählen?“ (Ivna Zic) Die Autorin und Regisseur
isCrawled: true
---
„Wie funktioniert Erzählen überhaupt? Wie können wir Dinge nicht erzählen und damit trotzdem ganz viel erzählen?“ (Ivna Zic)
Die Autorin und Regisseurin Ivna Zic hat mit „Die Nachkommende“ ihr Romandebüt vorgelegt, über den Verlust von Heimat, über den Verlust eines geliebten Menschen und – haltlose Trauer. Die Erzählerin reist per Zug von Paris nach Kroatien, wo, wie jeden Sommer, die Familie auf der Großmutterinsel wartet. Sie denkt an den Mann, mit dem sie ein Jahr lang eine Beziehung führte: Ein Maler, der nicht mehr malte. Im fahrenden Zug setzt sich der tote Großvater zu ihr, ebenfalls ein Maler, der aufgehört hatte zu malen. Die zwei abwesenden, imaginierten Männer werden zu ihren Begleitern auf einer Reise in die Vergangenheit. 