---
id: "218894259480233"
title: "Literatur unter Bäumen: Beats. Maro Verlag, Augsburg"
start: 2020-08-27 19:00
end: 2020-08-27 20:30
locationName: Donau Insel Neu-Ulm
address: Insel, 89231 Neu-Ulm
link: https://www.facebook.com/events/218894259480233/
teaser: Literatur unter Bäumen widmet sich in seiner vierten Auflage den
  Verlegerinnen. War die Buchbranche früher eine männliche Domäne, ist die
  unabhängige
isCrawled: true
---
Literatur unter Bäumen widmet sich in seiner vierten Auflage den Verlegerinnen. War die Buchbranche früher eine männliche Domäne, ist die unabhängige Verlagsszene der Gegenwart geprägt durch zahlreiche Verlegerinnen, die für neue, unbekannte, übersehene oder unterschätzte Bereiche der Literatur eintreten. Das wichtigste Kapital der eingeladenen Verlegerinnen ihre Begeisterung, ihre Überzeugung von der absoluten Notwendigkeit ihrer Arbeit und ihre Liebe zum Wort.

Seit 1969 ist MARO der Verlag für Gewagtes und Gewitztes, Hausautoren sind u. a. Charles Bukowski, Jörg Fauser und John Fante. Mit Sarah Käsmayr ist nun die nächste Generation in die Leitung dieses Verlags für Unerwartetes und Unbeirrtes eingestiegen.
Autorin Susanne Neuffer  stellt ihre bei Maro erschienenen Bücher vor, die Reales durch ein magisches Brennglas betrachten.

Susanne Neuffer, Jahrgang 1951, publiziert seit 1999 im Augsburger Maro-Verlag, der seine Gründungsenergie aus den Achtundsechzigern zog und seit 1970 zu den interessantesten unabhängigen Verlagen gehört (nicht nur, weil er sich Bukowski, Fante und anderen modernen Klassikern  widmet). "Im Schuppen ein Mann" versammelt Erzählungen über Geschiedene, Insolvente, Verlassene, Fast-Helden eines subtil ins Absurde und Komische verschobenen Alltags. Ist der Nachbar womöglich Jesus oder warum verwandelt er Wasser in Wein? Ist die Rivalin ein menschliches Wesen oder ein besonders attraktives Exemplar künstlicher Intelligenz? Weiß die Hausbesitzerin, was sich im Schuppen tut? Kann man die Welt durch ein Radioprogramm retten? Wieso fallen Ballkleider vom Himmel und wer spielt auf beim Untergang Europas?

"Mit sicherem Blick: Susanne Neuffer erzählt von Menschen, die gerade so durchkommen" (FAZ)

Eintritt: 8 € inkl. eines nichtalkoholischen Getränks
Tickets an der Abendkasse, Einlass 18:30 Uhr

Schlechtwetter-Regelung: Bei schlechter Witterung sind wir in der Stadtbücherei 
Neu-Ulm, Heiner-Metzger-Platz 1. Dort sind aufgrund der Corona-Regeln nur 25 Plätze möglich.
Wetter-Telefon ab 16 Uhr am jeweiligen Veranstaltungstag: 0731/ 7050-2121
www.literaturbaeume.neu-ulm.de
www.literatursalon.net
facebook.com/literaturbaeume/ 
instagram: stadt.neuulm