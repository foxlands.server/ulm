---
id: "3219567894798147"
title: Switch Sommerfest - House Open-Air
start: 2020-08-30 15:00
end: 2020-08-30 21:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/3219567894798147/
teaser: Switch Sommerfest mit Serdar, Jonas, AD!  Serdar Dogan (bleepgeeks) Jonas Rall
  (Elektroofen) AD! the DJ (Switch)  House/Disco/Balearic   __________
isCrawled: true
---
Switch Sommerfest mit Serdar, Jonas, AD!

Serdar Dogan (bleepgeeks)
Jonas Rall (Elektroofen)
AD! the DJ (Switch)

House/Disco/Balearic


__________


Liederkranz Öffnungszeiten: 
Dienstag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆