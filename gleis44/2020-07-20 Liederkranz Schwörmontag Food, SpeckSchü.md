---
id: "379591389781681"
title: Liederkranz Schwörmontag Food, SpeckSchüttel DJs & Chillen! :)
start: 2020-07-20 13:00
end: 2020-07-20 23:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/379591389781681/
teaser: "Ein entspannter Schwörmontag mit leckerem Essen, chilligen DJs von Schüttel
  dein Speck und Hochsommerwetter! : )   Ihr könnt euch bei uns auch gerne T"
isCrawled: true
---
Ein entspannter Schwörmontag mit leckerem Essen, chilligen DJs von Schüttel dein Speck und Hochsommerwetter! : ) 

Ihr könnt euch bei uns auch gerne To-Go versorgen und den Abend ganz gechillt auf der Wiese mit euren Liebsten verbringen. 

Die Veranstaltung wird ermöglich durch: 
SWU Stadtwerke Ulm/Neu-Ulm GmbH
Indauna e.V 
Gleis 44

_______


Der Abend findet im Rahmen von TASTE Street Food to go Picknick in der Au wir Schwören. statt, für Kulinarische Untermalung ist also gesorgt! : ))) 

____________________
Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren.
Hier für euch kurz und bündig alle Änderungen:

- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu.
- Leider können wir keine Stehplätze an der Theke anbieten.

____________________

Liederkranz Öffnungszeiten:
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr
Wir freuen uns auf euch! 🐇🦊🦆
