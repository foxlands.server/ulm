---
id: "249170303034245"
title: Kollektiv Schillerstraße (im Liederkranz)
start: 2020-06-12 17:00
end: 2020-06-12 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/249170303034245/
image: 101673120_858799557950257_6451005556780957696_o.jpg
teaser: "Techno und House im Sitzen und mit ganz viel Liebe! :
  )   ____________________   Damit sich alle gut und sicher fühlen haben wir ein
  Hygiene-Konzept e"
isCrawled: true
---
Techno und House im Sitzen und mit ganz viel Liebe! : ) 

____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 12 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆