---
id: "704655826777285"
title: Kollektiv Schillerstraße w/ Fabi Haas (im Liederkranz)
start: 2020-07-25 17:00
end: 2020-07-25 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/704655826777285/
image: 107437728_883953792101500_1759115755607622792_o.jpg
teaser: "Techno und House im Sitzen und mit ganz viel Liebe! : )   Unser Resident Ekko
  hat Geburstag und nach jährlicher Tradition haben wir für die Fete Fabi"
isCrawled: true
---
Techno und House im Sitzen und mit ganz viel Liebe! : ) 

Unser Resident Ekko hat Geburstag und nach jährlicher Tradition haben wir für die Fete Fabi Haas aus Stuttgart eingeladen. Liebe! 

x Fabi Haas
(Verspielt Verspult / Stuttgart)
x Ekko

____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆