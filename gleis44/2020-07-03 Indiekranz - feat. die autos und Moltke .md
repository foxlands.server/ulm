---
id: "264621041290406"
title: Indiekranz - feat. die autos und Moltke & Mörike
start: 2020-07-03 17:00
end: 2020-07-03 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/264621041290406/
teaser: Indie meet´s Liederkranz. Biergarten meet´s Gitarrensound!   Mit von der
  Partie sind die beiden wundervollen Bands die autos und Moltke & Mörike. Davo
isCrawled: true
---
Indie meet´s Liederkranz. Biergarten meet´s Gitarrensound! 

Mit von der Partie sind die beiden wundervollen Bands die autos und Moltke & Mörike. Davor und danach begleiten Euch die besten Indiehymnen von Artic Monkey, The Strokes, Franz Ferdinand & co., elektronisch angehauchten Beats álla MGMT oder Bilderbuch sowie alles andere was gute Vibes verbreitet durch einen Stimmungsvollen Abend mit toller Musik.

Das Ganze ist Kostenlos :)
____________________

DJ´s:
DJ Traver (Gleis 44)
SAN_FRANCESCO
Phil the Gap (Eden Rocks Indie)

____________________
Moltke & Mörike- ca. 19:00 Uhr

Auf dem Musikkompass liegt Moltke&Mörike irgendwo zwischen Indie und Punk. Wer glaubt, es handle sich dabei nur um einfältige Bum-Bum-Musik, hat vollkommen Recht und irrt sich zugleich gewaltig.

Im November 2019 gewannen die drei Ulmer den „Förderpreis Junge Ulmer Kunst“ in der Kategorie „Populäre Musik“. Dabei überzeugten sie sowohl mit ihrer dynamischchaotischen Spielweise als auch mit eindringlichen deutschen Texten mit eindeutiger Botschaft. Inhaltlich wird sich klar gegen jede Form rechten 
Gedankenguts positioniert. Zudem spricht die Band gesellschaftliche Schieflagen und persönliche Probleme offen an.

____________________
die autos - ca. 20 Uhr 

Eine Band zwischen Aufbruch und Abbruch, Hingabe und Aufgabe, Exzess und Zärtlichkeit. Seit nunmehr 16 Jahren kümmern sich Simon Schöfisch (Gitarre und Gesang), Niki Pertl (Gitarre und Gesang), Martin Görz (Gitarre und Gesang), Andreas Neubert (Bass) und Andreas Schmid (Schlagzeug) um den Rock ‘n‘ Roll in der Provinz.

Weitab von Trends der hippen Metropolen spielen die autos eine Musik, die ihre Energie aus dem Glück und der Verzweiflung am Alltäglichen, aus Natur und Mystik saugt. Es gibt aus dem Punk geborenen Folkrock, Melodien süß und schräg, Schleier und Klarheit. Ein Leben in der Musik. Eine Musik für‘s Leben.

____________________
Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren.
Hier für euch kurz und bündig alle Änderungen:

- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu.
- Leider können wir keine Stehplätze an der Theke anbieten.

____________________

Liederkranz Öffnungszeiten:
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr
Wir freuen uns auf euch! 🐇🦊🦆
