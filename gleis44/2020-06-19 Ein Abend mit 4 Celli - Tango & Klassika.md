---
id: "1148560688838313"
title: Ein Abend mit 4 Celli - Tango & Klassikabend
start: 2020-06-19 19:00
end: 2020-06-19 21:30
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/1148560688838313/
image: 103320516_126688165716125_7335835142222643240_o.jpg
teaser: Kein Instrument vermag es den Facettenreichtum der Melancholie in solchen
  Höhen zu schaukeln und wieder in Tiefen zu stürzen, wie das Cello. Das Quart
isCrawled: true
---
Kein Instrument vermag es den Facettenreichtum der Melancholie in solchen Höhen zu schaukeln und wieder in Tiefen zu stürzen, wie das Cello. Das Quartett mit Ulmer Cellist Mathis Merkle schöpfen dieses Spektrum aus, in dem sie mal berührend mal exotisch Tangos, Auszüge aus Oper oder Ouvertüren, Schlager aus den 1930ern oder Klassiker der Klassik spielen. Ein musikalisch hochwertiges, wie auch berührendes Erlebnis. 

Bei schlechtem Wetter findet die Veranstaltung im Musiksaal der Teutonia statt.

____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

Gestaltung: Buck et Baumgärtel