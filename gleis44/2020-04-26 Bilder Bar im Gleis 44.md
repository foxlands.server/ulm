---
id: "195419624860854"
title: Bilder Bar im Gleis 44/ Eröffnung Surfartgallery
start: 2020-05-21 14:00
end: 2020-05-21 18:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/195419624860854/
image: 98443869_817690038722201_4927842344952135680_n.jpg
teaser: "Plan Änderung !  Morgen am Vatertag eröffnen wir die Surf Art Gallery im
  Gleis 44 :)  14-18 Uhr  Künstler:  Lorenz Bee  Moritz Reulein   Die Corona Re"
isCrawled: true
---
Plan Änderung ! 
Morgen am Vatertag eröffnen wir die Surf Art Gallery im Gleis 44 :) 
14-18 Uhr 
Künstler: 
Lorenz Bee 
Moritz Reulein 

Die Corona Regeln werden selbstverständlich eingehalten. 
