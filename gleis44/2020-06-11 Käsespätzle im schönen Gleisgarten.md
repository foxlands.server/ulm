---
id: "260575518585858"
title: Käsespätzle im schönen Gleisgarten
start: 2020-06-11 13:00
end: 2020-06-11 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/260575518585858/
image: 101540497_858053718024841_8583113690012712960_o.jpg
teaser: "Donnerstag gibt es selbstgemachte Käsespätzle und ganz viel Liebe! :
  )  ____________________  Damit sich alle gut und sicher fühlen haben wir ein
  Hygi"
isCrawled: true
---
Donnerstag gibt es selbstgemachte Käsespätzle und ganz viel Liebe! : ) 
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

