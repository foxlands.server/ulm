---
id: "280145213340296"
title: Bordel Nouveau (im Liederkranz)
start: 2020-07-17 17:00
end: 2020-07-17 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/280145213340296/
image: 106913956_745893252894354_8864891938379241606_n.jpg
teaser: ENDLICH DÜRFEN WIR WIEDER MUSIK FÜR EUCH MACHEN!! <3  Die Bordel Nouveau -
  Crew rund um Harry Ken, Hans Pech und unserem Gast PAT MORITA wird am FREIT
isCrawled: true
---
ENDLICH DÜRFEN WIR WIEDER MUSIK FÜR EUCH MACHEN!! <3

Die Bordel Nouveau - Crew rund um Harry Ken, Hans Pech und unserem Gast PAT MORITA wird am FREITAG den 17.7.2020 im Liederkranz Kulturbiergarten feinste elektronische Wohlfühlmusik in Eure Gehörgänge zaubern und zusammen machen wir uns wunderschöne Stunden!

Wir freuen uns so sehr, Eure zauberhaften Gesichter mal wieder live zu sehen und haben richtig Lust auf gute Vibes! 

Seid dabei und leitet mit uns stilvoll das Wochenende ein!

DER EINTRITT IST FREI! :)

Line-Up:

Harry Ken
Hans Pech
PAT MORITA

Sounds:

House, Deephouse, Tech-House, Melodic-Techno, Afrohouse - so wie ihr es von uns kennt ;)

 ____________________
Damit sich alle gut und sicher fühlen, wurde natürlich ein Hygiene-Konzept erarbeitet und es werden alle Tische, etc. sehr regelmäßig desinfiziert. 

Hier für euch kurz und bündig alle Änderungen: 

- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________
Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! <3