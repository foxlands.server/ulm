---
id: "242522246818003"
title: Vatertag im Gleisgarten
start: 2020-05-21 13:00
end: 2020-05-21 23:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/242522246818003/
image: 96676316_843360912827455_4013960414890033152_o.jpg
teaser: Am Vatertag öffnen wir den Biergarten natürlich schon ab dem Mittag! Es gibt
  Pizza, Flammkuchen, Vesperplatten, frisch gezapftes Bier, DJ-Sets und Son
isCrawled: true
---
Am Vatertag öffnen wir den Biergarten natürlich schon ab dem Mittag! Es gibt Pizza, Flammkuchen, Vesperplatten, frisch gezapftes Bier, DJ-Sets und Sonne. Außerdem ist ab dem Vatertag die Foto-Ausstellung "Surf Art Gallery" von den Jungs von der Surfwerkstatt zu sehen. Rein darf immer nur eine Gruppe. Sommervibezz sind aber garantiert! :) 

Musik machen die Diskjockeys Samuel (Leander & die Liebe) und Paul (Van Kost) für euch. Es gibt eine bunte Mischung aus Sommermusik, Elektro, Jazz, Indie, Reggae und Trash! 

____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Gleisgarten Öffnungszeiten: 
Montag - Sonntag
17 - 23 Uhr (SO - DO)
17 - 24 Uhr (FR & SA)

Wir freuen uns auf euch! 🐇🦊🦆