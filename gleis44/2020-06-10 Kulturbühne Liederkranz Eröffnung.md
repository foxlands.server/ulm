---
id: "195666001612680"
title: Kulturbühne Liederkranz Eröffnung
start: 2020-06-10 17:00
end: 2020-06-10 23:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/195666001612680/
image: 101680856_858789014617978_3765927913266872320_o.jpg
teaser: Der Pop-up Kulturbiergarten Liederkranz lädt zur feierlichen Eröffnung mit
  Oberbürgermeister Gunter Czisch und INDAUNA Vereinsvorstand Thomas Kienle e
isCrawled: true
---
Der Pop-up Kulturbiergarten Liederkranz lädt zur feierlichen Eröffnung mit Oberbürgermeister Gunter Czisch und INDAUNA Vereinsvorstand Thomas Kienle ein. Im Anschluss das Eröffnungskonzert des Jazz-Trios mit Joe Fessler am Klavier, Sängerin Lea Knudsen und Saxophonist Norbert Streit. Begleitet werden die Musiker*innen von Gunter Czisch am Schlagzeug. Den Abend ausklingen lässt die Impro-Band von Künstler Mark Klavikowski, mit ihrem interaktiven Projekt „Songlotterie“. 

TIMETABLE:
- ab 17 Uhr: Offen
- 18 Uhr: Vernissage & Rede
- 18-20 Uhr: Jazz vom Quartett Loungecats
- 20-22 Uhr: Songlotterie LIVE
- 22-23 Uhr: Ausklang

____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 12 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆