---
id: "2409681039336880"
title: Indiekranz#2 - feat. KOSMO, Schöfisch & Rueß und WE SAY SO
start: 2020-08-08 16:00
end: 2020-08-08 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/2409681039336880/
teaser: Der Indiekranz rockt die zweite Runde!  Indie meet´s Liederkranz. Biergarten
  meet´s Gitarrensound! Diesmal können wir gleich drei wundervolle Bands pr
isCrawled: true
---
Der Indiekranz rockt die zweite Runde!

Indie meet´s Liederkranz. Biergarten meet´s Gitarrensound!
Diesmal können wir gleich drei wundervolle Bands präsentieren. Mit dabei sind KOSMO, Schöfisch & Rueß und WE SAY SO.
 
Davor, dazwischen und danach gibt es für Euch die besten Indiehymnen von The Artic Monkeys, The Strokes, Franz Ferdinand & co., elektronisch Beats álla MGMT oder Bilderbuch sowie alles andere was gute Vibes verbreitet auf die Ohren.
 
Das Ganze ist wie immer Kostenlos 🙂
 ____________________
DJ´s - davor, dazwischen und danach ab ca. 16:00 Uhr
 
DJ Traver (Gleis 44)
Phil the Gap (Eden Rocks Indie)
 ____________________
 WE SAY SO - ca. 18:00 Uhr
 
Außergewöhnliche Umstände erfordern außergewöhnliche Ausdrucksarten, so sagt man insgeheim. Aber: WE SAY SO! 
Das Pop 'n' Rock-Trio WE SAY SO aus Bellenberg mischt die deutschsprachige Musiklandschaft auf.
 
 ____________________
 Schöfisch & Rueß ca. 18:45 Uhr
 
Schöfisch & Rueß spielen Lo-Fi Blues und Garagenrock, Sixties-Pop und Kinderlieder. Doch in Wahrheit sind es Popsongs in schmutzigem Gewand und Melodien, die keine sein wollen. Ein Rock-Duo zu dem man die Hüften kreisen und die Seele baumeln lassen sollte, während das Tanzbein schwingt. 
 
 ____________________
 KOSMO ca. 20:00 Uhr
 
KOSMO klingt irgendwie so, als würde Rio Reiser über Bluetooth-Boxen Songs von den Black Keys hören. Oder als hätten die vier Musiker ein paar durchzechte Nächte mit Udo Lindenberg in den 70ern durchlebt. In ihren Songs verzichten sie auf den gängigen Schnick-Schnack und bleiben dabei trotzdem vielseitig. Die Band macht deutschsprachigen Rock, ohne dabei in gängige Klischees zu verfallen.
 
 ____________________
Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren.
Hier für euch kurz und bündig alle Änderungen:
 - ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
 - wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
 - Bitte wartet am Eingang, unser Team weist euch Plätze zu.
 - Leider können wir keine Stehplätze an der Theke anbieten.
 ____________________
 Liederkranz Öffnungszeiten:
 Montag - Freitag ab 17 Uhr
 Samstag, Sonntag & Feiertags ab 12 Uhr
 Wir freuen uns auf euch! 🐇🦊🦆