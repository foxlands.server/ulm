---
id: "369189350791121"
title: Indie-Gleis
start: 2020-11-20 21:00
end: 2020-11-21 04:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/369189350791121/
image: 120354875_947687605728118_7115638590806542543_o.jpg
teaser: "INDIE! Konzert + Bar-Abend.   - > VERSCHOBEN auf den 20.11 wegen Krankheit! :
  ) _________________________      THE 5th SEASON! Unser Herbst/Winter-Pro"
isCrawled: true
---
INDIE! Konzert + Bar-Abend. 

- > VERSCHOBEN auf den 20.11 wegen Krankheit! : )
_________________________
  
  THE 5th SEASON! Unser Herbst/Winter-Programm in der Gleis-Bar. Die Corona-Zeit verlangt von uns allen einiges ab, lasst uns gemeinsam das tollste draus machen! Mittwoch bis Samstag ab 21 Uhr. 
  
  Regeln: 
  - Maskenpflicht. Bitte nur am Tisch / festen Platz in der Gruppe die Maske abnehmen. 
  - Leider ist kein Tanzen möglich, ihr könnt aber gerne auf euren Plätzen mitfeiern und Raven! 
  - Um ggf. die Infektionskette nachzuvollziehen müssen wir am Einlass eure Kontaktdaten aufnehmen. 
  
  Um unseren Künstlern, Produzenten und DJs trotz der Pandemie faire Gagen zahlen zu können, müssen wir am Einlass einen kleinen Kulturbeitrag auf Spendenbasis in Höhe von 1 bis 5 € erheben. Jeder soll so zahlen wie er kann :) 
  
  _________________________
  
  
  Die Veranstaltung wird unterstützt vom Ministerium für Wissenschaft, Forschung und Kunst Baden-Württemberg.