---
id: "287729615791391"
title: Dance With Us AIR w/ Patrik Berg
start: 2020-08-15 17:00
end: 2020-08-15 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/287729615791391/
teaser: Dance With Us AIR  Free Entry  15.08.2020 17.00 - 22.00 Uhr  Liederkranz
  ULM  Patrik Berg Oliver Loud  LOKKE Sülow  Love Music and Dance (or Sit) with
isCrawled: true
---
Dance With Us AIR

Free Entry 
15.08.2020
17.00 - 22.00 Uhr

Liederkranz ULM

Patrik Berg
Oliver Loud 
LOKKE
Sülow

Love
Music
and Dance (or Sit) with us 
🖤🎶