---
id: "660074831391657"
title: Fotografie Grundlagen Workshop
start: 2020-06-20 12:00
end: 2020-06-20 17:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/660074831391657/
image: 99415917_3666273976722621_726279884581634048_o.jpg
teaser: Gleis 44s >hauseigener< Fotograf Moritz, verantwortlich für alle von uns
  geposteten Fotos, freut sich, sein know-how bei uns zusammen mit seinem Kolle
isCrawled: true
---
Gleis 44s >hauseigener< Fotograf Moritz, verantwortlich für alle von uns geposteten Fotos, freut sich, sein know-how bei uns zusammen mit seinem Kollegen Dominik an euch weiter zu geben. 

In unserem Grundlagen Fotografie Workshop bieten wir euch einen 3 stündigen Theorieteil und danach einen zwei stündigen Praxis Teil inklusive Fotowalk durch Ulm. Dabei werden wir die gelernten Techniken direkt ausprobieren. Der Workshop geht insgesamt ca. 5 Stunden. Wir starten um 12 Uhr in unserem Seminarraum direkt am Gleis 44. Im Theorieteil werden wir Themen durchnehmen wie: 
-	Kamera 
-	Objektive 
-	Funktion der Kameras (System/Spiegel) 
-	Verschiedenen Funktionen 
-	Iso/Blende/Belichtungszeit/Weißabgleich
-	Fokus 
-	Belichtung 
-	Dateitypen 
-	Bildgestaltung (Perspektiven / Goldener Schnitt / Drittelregel) 


Der Preis liegt bei 80 € pro Person. 




Die Teilnehmerzahl ist auf 12 Personen begrenzt. 



Anmeldungen bitte per Mail an mo@gleis44.de 