---
id: "742025823305735"
title: Elektroofen House Open Air im Liederkranz
start: 2020-07-11 17:00
end: 2020-07-11 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/742025823305735/
image: 107516068_2619104551675552_6821605398144170482_o.jpg
teaser: House, Techno und Disco im Sitzen mit den DJs der Elektroofen Radioshow und
  Leander & die Liebe B2B Van Kost vom Kollektiv Schillerstraße.  __________
isCrawled: true
---
House, Techno und Disco im Sitzen mit den DJs der Elektroofen Radioshow und Leander & die Liebe B2B Van Kost vom Kollektiv Schillerstraße.

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆