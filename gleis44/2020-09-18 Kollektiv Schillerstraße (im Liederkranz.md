---
id: "1756521124495708"
title: Kollektiv Schillerstraße (im Liederkranz)
start: 2020-09-18 17:00
end: 2020-09-18 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/1756521124495708/
teaser: "Techno und House im Sitzen und mit ganz viel Liebe! : )   x Tim Boxx x
  Blumenpanzer x Hans Pech  ____________________   Damit sich alle gut und
  sicher"
isCrawled: true
---
Techno und House im Sitzen und mit ganz viel Liebe! : ) 

x Tim Boxx
x Blumenpanzer
x Hans Pech

____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆