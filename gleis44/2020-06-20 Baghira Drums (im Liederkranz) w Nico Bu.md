---
id: "573330156955466"
title: Baghira Drums (im Liederkranz) w/ Nico Bulla
start: 2020-06-20 15:00
end: 2020-06-20 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/573330156955466/
image: 104308098_867425487087664_6191084776937882643_o.jpg
teaser: "Techno, Downtempo, Deep- und Afro-House im Sitzen und mit ganz viel Liebe. :
  ) Ohne Tanzen, aber dafür mit strahlenden Gesichtern!   Leander & die Lie"
isCrawled: true
---
Techno, Downtempo, Deep- und Afro-House im Sitzen und mit ganz viel Liebe. : ) Ohne Tanzen, aber dafür mit strahlenden Gesichtern! 

Leander & die Liebe
Blumenpanzer
Ekko
(Kollektiv Schillerstraße)

Nico Bulla 
(YOUNION / Eins Tiefer)
____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆