---
id: "602711803825125"
title: Second Hand Surf/Skate/Streetstyle Flohmarkt 2021
start: 2021-03-27 16:00
end: 2021-03-27 21:00
locationName: Surf Repair Ulm
address: Schillerstrasße 44, Ulm
link: https://www.facebook.com/events/602711803825125/
image: 82448068_3308387929177879_7870968469787770880_o.jpg
teaser: Nach dem großen Erfolg der letzten Surf Flohmärkten geht es in die nächste
  Runde. Der Sommer steht vor der Tür und wir haben jede Menge gebrauchte sow
isCrawled: true
---
Nach dem großen Erfolg der letzten Surf Flohmärkten geht es in die nächste Runde.
Der Sommer steht vor der Tür und wir haben jede Menge gebrauchte sowie neue Surfbretter für euch am Start. 
Ausserdem gibt es günstige Finnen, Leashes oder Boardbags. 
Kommt vorbei und sichert euch noch ein Schnäppchen vor dem nächsten Surf Urlaub.
Am 27.März ab 16 Uhr in der Schillerstraße 44 :) 

Surfbretter für Meer und Fluss
Surfbrett Blanks 
Alaia's 
Neos 
Bags 
Accessoires
Klamotten 
Repair Stuff

Mit dabei BG Surfboards aus Reutlingen :) 


An diesem Tag findet auf dem gesamten Gelände ein großer Flohmarkt statt das heißt es ist für alle etwas geboten :) 


Ihr habt gebrauchten Surf Stuff den ihr los werden wollt ? 
Mail an mo@gleis44.de 
