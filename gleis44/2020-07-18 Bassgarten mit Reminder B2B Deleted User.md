---
id: "611418916158588"
title: Bassgarten mit Reminder B2B Deleted User
start: 2020-07-18 17:30
end: 2020-07-18 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/611418916158588/
teaser: Drums, Bass und Vinyl-Schallplatten. Freut euch auf sommerlichen Drum and
  Bass, Jungle und Liquid!  ____________________  Damit sich alle gut und sich
isCrawled: true
---
Drums, Bass und Vinyl-Schallplatten. Freut euch auf sommerlichen Drum and Bass, Jungle und Liquid! 
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 20 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

