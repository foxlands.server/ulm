---
id: "330488681616115"
title: Gleis-Bar Samstag
start: 2020-09-26 21:00
end: 2020-09-27 04:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/330488681616115/
teaser: "Samstag in der schönen Gleis-Bar! <3 Wir, die DJs und die guten Geister des
  Hauses freuen uns auf einen schönen Abend zusammen mit euch : )   An den P"
isCrawled: true
---
Samstag in der schönen Gleis-Bar! <3 Wir, die DJs und die guten Geister des Hauses freuen uns auf einen schönen Abend zusammen mit euch : ) 

An den Plattentellern: 
- Frederic Stunkel (Minimal Deep Tech) 
- MIRA (Downtempo)
- Lenn Reich (Tech-House) 

Bis Samstag! Davor spielt übrigens die Band Malaka Hostel ein kleines Wohnzimmerkonzert, Tickets gibt es zu gewinnen bei Radio free FM. 

-> Radio-/Wohnzimmerkonzert: Malaka Hostel - 25 Jahre free FM