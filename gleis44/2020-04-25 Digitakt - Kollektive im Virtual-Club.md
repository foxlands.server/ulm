---
id: "679283429290005"
title: Digitakt - Kollektive im Virtual-Club
start: 2020-04-25 18:30
end: 2020-04-26 01:30
address: https://www.virtual-clubbing.com/
link: https://www.facebook.com/events/679283429290005/
image: 94120093_2493261300987000_7246878368417710080_o.jpg
teaser: Liebe Homeofficerinnen und Homeofficer,  wir wissen, dass feiern ein
  menschliches Bedürfnis ist. Wir möchten dieses Bedürfnis soweit es geht auch
  weit
isCrawled: true
---
Liebe Homeofficerinnen und Homeofficer, 
wir wissen, dass feiern ein menschliches Bedürfnis ist. Wir möchten dieses Bedürfnis soweit es geht auch weiterhin in dieser Lockdown-Situation befriedigen. 



Zur Anpassung an die derzeitige Situation haben wir vergangenen Samstag unseren AUFTAKT 2020 über unsere schnieke kleine Website www.Virtual-Clubbing.com gefeiert und wollen euch nun zur nächste Runde am 25.04.2020 herzlich einladen! 
Nutzt die Gelegenheit, deckt euch mit Spirituosen ein und dreht Zuhause die Boxen auf! 
Zu Sehen und vorallem Hören  gibt es einen bunten Mix an DJs und  Kollektiven aus unterschiedlichen Ecken Deutschlands darunter

Peace Peter
Shani Shanti
Panopticum
Kerfuffle.Sounds
Villa TuNichtGut
Kollektiv Schillerstraße / Gleis 44
&
WhildStage.

Das Lineup wird in den kommenden Tagen veröffentlicht. 
Bis dahin, stay healthy and stay home… aber kommt online!