---
id: "227837708479088"
title: Dämser im Gleisgarten
start: 2020-08-19 17:30
end: 2020-08-19 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/227837708479088/
teaser: "Tech-House    ____________________    Gleisgarten Öffnungszeiten:   Sonntag -
  Donnerstag 17 - 23 Uhr   Freitag - Samstag 17 - 24 Uhr   Wir freuen uns"
isCrawled: true
---
Tech-House


 ____________________


 Gleisgarten Öffnungszeiten: 
 Sonntag - Donnerstag 17 - 23 Uhr 
 Freitag - Samstag 17 - 24 Uhr 
 Wir freuen uns auf euch! 🐇🦊🦆