---
id: "754929268651276"
title: Blumenpanzer Gleisgarten DJ-Set
start: 2020-07-17 18:00
end: 2020-07-17 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/754929268651276/
teaser: Afro-House, Tech-House, Yoga-Techno.  ____________________  Damit sich alle
  gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden
isCrawled: true
---
Afro-House, Tech-House, Yoga-Techno. 
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

