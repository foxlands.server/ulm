---
id: "1054872758281189"
title: Polaroid & Celluloid - Vernissage // Kulturnacht
start: 2020-09-19 19:00
end: 2020-09-20 03:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/1054872758281189/
teaser: RE-Edition // Austellung + Vernissage + Kulturnacht Polaroid &
  Celluloid   Digital im Web & analog auf Alu Dibond - ab dem 19.09 in der
  Gleis-Bar!  __
isCrawled: true
---
RE-Edition // Austellung + Vernissage + Kulturnacht
Polaroid & Celluloid 

Digital im Web & analog auf Alu Dibond - ab dem 19.09 in der Gleis-Bar! 
______________


RE-Edition // Polaroid & Celluloid zeigt Fotografien aus über 30 Jahren Agentur- und Werbegeschichte in modernem Gewand. 

Die Dias und Polaroids stammen aus kommerziellen Mode- oder Wäsche-Fotostrecken und wurden unter anderem an der Côte d’Azur, in der Bretagne, auf Ibiza und in Südamerika aufgenommen. Die „RE-Edition“ besteht darin, die über 10000 Arbeiten zu digitalisieren und daraus ausgewählte Motive in aufwändigen Bildmontagen in ein neues, teils gar abstraktes, Licht zu stellen. Die manuellen Ergänzungen durch Schrift und Farbe individualisieren jedes einzelne Werk und verbinden die digitale und analoge Welt zu einer Einheit. „Die Fotos werden als Großformate auf Alu-Dibond Verbundplatten gedruckt und danach mit Acryl Farben von Hand nachbearbeitet. Jedes Exponat wird so zu einem Unikat“, sagt Künstler und Agenturchef Nestor Aksiuk.

"Wir wollen den Charme und Charakter der analogen Fotos mit digitalen Stilmitteln neu interpretieren und deren Bezug zur heutigen Zeit verdeutlichen.“ Deshalb sind viele der Bilder auch mit lyrischen Sprüchen oder Zitaten aus Songtexten versehen. „Es ist spannend wie viel Popkultur gerade im Design und der Kunst auch über die Jahrzehnte hinweg funktioniert. Obwohl das Arbeiten aus unseren frühen Schaffensjahren sind können wir erstaunlich viele Motive heute noch weiterverwenden.“

Die Exponate werden auch nach der Vernissage an der Kulturnacht auch auf art.aksis.de zu sehen sein und noch für einige Wochen in der Gleis-Bar ausgestellt.


Die Ausstellung wird durch das Ministerium für Forschung, Wissenschaft und Kunst ermöglicht.
