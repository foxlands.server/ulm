---
id: "949245618927402"
title: Electronic Flow Yoga | Liederkranz Kulturbiergarten
start: 2020-09-19 15:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/949245618927402/
teaser: Electrify your karma!   Nach drei erfolgreichen Sessions im gemütlichen
  Bahngarten des Bahnwärter Thiels sind wir zum ersten Mal auch in Ulm am Start!
isCrawled: true
---
Electrify your karma! 

Nach drei erfolgreichen Sessions im gemütlichen Bahngarten des Bahnwärter Thiels sind wir zum ersten Mal auch in Ulm am Start!

Corona hatte uns alle das letzte halbe Jahr fest im Griff, ein Grund mehr diese Monate Revue passieren lassen und alle negativen Gedanken durch ein 90-minütigen Vinyasa Flow angeleitet von Jenni aus dem Körper herauszuflowen.
Abschließend gibt es noch eine 15-minütige angeleitete Meditation um Kraft zu schöpfen und sich neue Ziele für das Jahr zu setzen.

Begleitet wird der Flow von Jan Taubmann mit sphärischem Downtempo, der euch ein bisschen ins Träumen versetzt. Dieser meditative Zustand hebt euch energetisch auf ein völlig neues Level, sodass ihr tiefenentspannt und total gelassen der zweiten Jahreshälfte entgegen flown könnt! 

Start ist um 15 Uhr.
Bitte bringt eure eigenen Yogamatten mit.
Wir freuen uns auf euch!

Aufgrund der beschränkten Plätze empfehlen wir den VVK:
www.electronicflowyoga.ticket.io
Bitte nehmt den Infektionsschutz ernst. D.h. Masken dürfen bitte nur auf der Yogamatte abgenommen werden. Bitte haltet zu jederzeit den gebotenen Abstand ein. 
