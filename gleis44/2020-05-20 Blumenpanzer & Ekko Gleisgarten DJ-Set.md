---
id: "2582162985371843"
title: Blumenpanzer & Ekko Gleisgarten DJ-Set
start: 2020-05-20 17:00
end: 2020-05-20 23:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/2582162985371843/
image: 96708682_843466636150216_131646721921908736_o.jpg
teaser: "Schöne Musik, Techno & House, für euch im Gleisgarten! :
  )   ____________________  Damit sich alle gut und sicher fühlen haben wir ein
  Hygiene-Konzept"
isCrawled: true
---
Schöne Musik, Techno & House, für euch im Gleisgarten! : ) 

____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Sonntag
17 - 23 Uhr (SO - DO)
17 - 24 Uhr (FR & SA)

Wir freuen uns auf euch! 🐇🦊🦆

