---
id: "638972596802477"
title: Bildbearbeitung- Grundlagen / RAW Entwicklung Workshop Gleis 44
start: 2020-11-21 12:00
end: 2020-11-21 17:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/638972596802477/
image: 120560966_4072046492812032_2588083802331080897_o.jpg
teaser: Gleis 44s >hauseigener< Fotograf Moritz, verantwortlich für alle von uns
  geposteten Fotos, freut sich, sein know-how bei uns zusammen mit seinem Kolle
isCrawled: true
---
Gleis 44s >hauseigener< Fotograf Moritz, verantwortlich für alle von uns geposteten Fotos, freut sich, sein know-how bei uns zusammen mit seinem Kollegen Dominik an euch weiter zu geben. 

Diesmal bieten wir euch einen Grundlagen Kurs in der Bildbearbeitung an. Wir werden mit euch zusammen Fotos machen rund ums Gleis 44, danach lernt ihr wie ihr eure RAW Dateien speichert und organisiert. 
Im Kurs lernen wir die Grundlagen des Raw-Konverters in Photoshop sowie in Lightroom. Ausserdem werden wir euch zeigen wie ihr in Photoshop aus mehreren Bildern eine Komposition erstellen könnt. 
Wir zeigen euch wie ihr eure eigenen Presets erstellen und Anwenden könnt und sogar per Lightroom Handy Version auf die eigenen Handybilder legen könnt.
Ihr benötigt dazu eine Kamera sowie ein Laptop mit Photoshop & Lightroom Lizenz. 

Adobe bietet zu beiden Programmen einen kostenlose Probemonat an. 

Der Workshop ist auf 10 Personen limitiert also meldet euch schnell an. 

Anmeldungen bitte an: 

hallo @ moritz-reulein.de

Der Preis liegt bei 80 € pro Person.