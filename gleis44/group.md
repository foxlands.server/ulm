---
name: Gleis 44
website: http://gleis44.de
email: info@gleis44.de
scrape:
  source: facebook
  options:
    page_id: gleis44
---
Biergarten, Club, Kunst und Werkstatt in der Schillerstraße.
