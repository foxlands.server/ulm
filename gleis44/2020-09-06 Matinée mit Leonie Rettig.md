---
id: "621709845212466"
title: Matinée mit Leonie Rettig
start: 2020-09-06 11:00
end: 2020-09-06 12:30
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/621709845212466/
teaser: Klaviersonaten von Ludwig van Beethoven  Sonate in cis-moll, op. 27 Nr. 2
  „Mondschein“ Sonate in C-Dur, op. 53 „Waldstein“ -- Sonate in c-moll, op. 13
isCrawled: true
---
Klaviersonaten von Ludwig van Beethoven

Sonate in cis-moll, op. 27 Nr. 2 „Mondschein“
Sonate in C-Dur, op. 53 „Waldstein“
--
Sonate in c-moll, op. 13 „Pathétique“
Sonate in f-moll, op. 57 „Appassionata“

--

Eine »unerhörte Musikalität« bescheinigte Daniel Barenboim der jungen Pianistin Leonie Rettig, als sie ihm im Alter von 14 Jahren vorspielte, – heute gehört die Musikerin zu den »großen Nachwuchshoffnungen am Klavier« (NDR Kulturredaktion).

1991 in Stuttgart geboren, wusste Leonie Rettig bereits als Kind genau, was sie wollte. In einem Interview mit IDAGIO beschreibt sie dies sehr eindrücklich: »Ich glaube nicht, dass ich jemals entschieden habe, Pianistin zu werden […] Vielmehr begleitete mich immer diese innere Sicherheit, und ohne sie jemals zu hinterfragen, war es für mich vollkommen selbstverständlich, diesen Weg zu gehen.«

Leonie Rettig studierte in Hannover bei Vladimir Krainev, Schüler des legendären russischen Musikpädagogen Heinrich Neuhaus. 

2016 organisierte sie unter dem Eindruck der Flüchtlingskrise 2015/16 ein Benefizkonzert zugunsten der UNICEF im Kammermusiksaal der Berliner Philharmonie, für das sie als Mitwirkende nicht nur die Schauspielerin Katja Riemann gewinnen konnte, sondern auch die Schirmherrschaft unter Frau Prof. Monika Grütters, Staatsministerin für Kultur und Medien. Sie spielte mehrfach für die Aktion 100.000 in Ulm und engagierte sich darüber hinaus selbst aktiv in der Flüchtlingshilfe.

Leonie Rettig studierte in Hannover bei Vladimir Krainev, Schüler des legendären russischen Musikpädagogen Heinrich Neuhaus. Einladungen führen die Pianistin nicht nur zu den bedeutendsten Festivals und Konzertreihen in Europa sondern auch auf Tourneen als Solistin sowie mit Orchester z.B. durch Kanada oder China. 

Beginn 11 Uhr. Eintritt frei. 
