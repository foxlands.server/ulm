---
id: "635558240392339"
title: Käsespätzle & DJ-Sound im schönen Gleisgarten
start: 2020-07-23 17:00
end: 2020-07-23 22:30
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/635558240392339/
image: 106457123_878342715995941_4715645635837597529_o.jpg
teaser: "Donnerstag gibt es selbstgemachte Käsespätzle, DJ-Musik von den RhythmFellows
  und ganz viel Liebe! : )  ____________________  Damit sich alle gut und"
isCrawled: true
---
Donnerstag gibt es selbstgemachte Käsespätzle, DJ-Musik von den RhythmFellows und ganz viel Liebe! : ) 
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft in Gruppen von maximal 10 Personen kommen und gemeinsam an einem Tisch Sitzen.
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

