---
id: "578314916395735"
title: Phil The Gap Gleisgarten DJ-Set
start: 2020-05-29 18:00
end: 2020-05-29 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/578314916395735/
image: 99418362_851580232005523_7919441805697875968_o.jpg
teaser: "Phil The Gap vom Eden-Rocks-Indie-Team präsentiert Alternative, Indie, Indie
  Pop und artenverwantes für einen schniecken Freitagabend! : )  __________"
isCrawled: true
---
Phil The Gap vom Eden-Rocks-Indie-Team präsentiert Alternative, Indie, Indie Pop und artenverwantes für einen schniecken Freitagabend! : ) 
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

