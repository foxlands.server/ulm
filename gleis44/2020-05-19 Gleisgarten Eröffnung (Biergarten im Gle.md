---
id: "179043340012043"
title: Gleisgarten Eröffnung (Biergarten im Gleis 44)
start: 2020-05-19 17:00
end: 2020-05-19 23:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/179043340012043/
image: 96580603_843446596152220_8341197011365658624_o.jpg
teaser: Das Gleis 44 eröffnet am 19. Mai endlich die Pforten des Gleisgartens für alle
  und jeden! Dass wir jetzt wieder Eröffnen dürfen, ist echt unglaublich
isCrawled: true
---
Das Gleis 44 eröffnet am 19. Mai endlich die Pforten des Gleisgartens für alle und jeden! Dass wir jetzt wieder Eröffnen dürfen, ist echt unglaublich schön. Wir freuen uns auf viele gute Abende, endlich mal wieder Live DJ-Sets und das Zusammenkommen mit Freunden und Geliebten. 

Die letzten Wochen haben wir fleißig für euch am Gleisgarten gewerkelt, die dritte Saison und der dritte Sommer im Gleis wird wieder besonders! Wir haben jetzt auch eigene Flammkuchen, Vesperplatten, neue Deko UND gezapftes Bier vom Fass! <3 

____________________


Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________


Gleisgarten Öffnungszeiten: 
Montag - Sonntag
17 - 23 Uhr (SO - DO)
17 - 24 Uhr (FR & SA)

Wir freuen uns auf euch! 🐇🦊🦆