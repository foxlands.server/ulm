---
id: "257154708715011"
title: Nico Bulla Gleisgarten DJ-Set
start: 2020-06-18 18:00
end: 2020-06-18 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/257154708715011/
image: 101573022_858054978024715_319135204172103680_o.jpg
teaser: Nico Bulla spielt schöne Musik zwischen Techno, House und Hip-Hop! :)
  ____________________  Damit sich alle gut und sicher fühlen haben wir ein
  Hygien
isCrawled: true
---
Nico Bulla spielt schöne Musik zwischen Techno, House und Hip-Hop! :)
____________________

Damit sich alle gut und sicher fühlen haben wir ein Hygiene-Konzept erarbeitet und werden alle Tische etc. sehr regelmäßig Desinfizieren. 
Hier für euch kurz und bündig alle Änderungen: 
- ihr dürft nur in Gruppen die aus 2 Haushalten bestehen kommen! 
- wir müssen eure Namen und Kontaktdaten aufnehmen und 4 Wochen speichern.
- Bitte wartet am Eingang, unser Team weist euch Plätze zu. 
- Leider können wir keine Stehplätze an der Theke anbieten. 

____________________

Gleisgarten Öffnungszeiten: 
Montag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 
Sonntag 13 - 23 Uhr

Wir freuen uns auf euch! 🐇🦊🦆

