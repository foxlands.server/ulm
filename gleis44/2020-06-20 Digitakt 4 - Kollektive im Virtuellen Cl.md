---
id: "702503637229846"
title: Digitakt 4 - Kollektive im Virtuellen Club
start: 2020-06-20 15:00
end: 2020-06-20 21:00
link: https://www.facebook.com/events/702503637229846/
image: 79122053_2539050463074750_8367749977559896717_o.jpg
teaser: Es wird gemunkelt, die Virtual-Clubbing-Phase neigt sich dem Ende zu aber das
  entscheidet natürlich immernoch Bill Gates und Greta. Und wir entscheide
isCrawled: true
---
Es wird gemunkelt, die Virtual-Clubbing-Phase neigt sich dem Ende zu aber das entscheidet natürlich immernoch Bill Gates und Greta. Und wir entscheiden gemeinsam das Beste draus zu machen und haben wieder exquisite Künstler und Künstlerinnen für euch am Start! Am 20.06. geht es ab 15:00 los. 
Mit vereinfachter Bedienung der Website, dem virtuellen Dancefloor und Live-Workshops!  
Zum Schluss noch für die Belesenen unter euch ein kleiner Witz: Was bestellt der Dalai Lama im Dönerladen? 
Eins mit allem.

Lineup kommt noch - stay spontan boy* ;)
*and girl