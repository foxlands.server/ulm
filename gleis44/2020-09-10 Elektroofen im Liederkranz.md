---
id: "1946127625511613"
title: Elektroofen im Liederkranz
start: 2020-09-10 17:00
end: 2020-09-10 22:00
locationName: Liederkranz Kulturbiergarten
address: Friedrichsau 9, 89073 Ulm
link: https://www.facebook.com/events/1946127625511613/
teaser: House, Deep House, Nu Disco w/ Julian Scheiffele & Jonas Rall (Elektroofen,
  Radio free FM) — all evening long  ____________________   Liederkranz Öffn
isCrawled: true
---
House, Deep House, Nu Disco
w/ Julian Scheiffele & Jonas Rall (Elektroofen, Radio free FM)
— all evening long

____________________


Liederkranz Öffnungszeiten: 
Montag - Freitag ab 17 Uhr
Samstag, Sonntag & Feiertags ab 12 Uhr

Wir freuen uns auf euch! 🐇🦊🦆