---
id: "587572141919605"
title: Käsespätzle & Funk
start: 2020-08-20 17:00
end: 2020-08-20 22:00
locationName: Gleis 44
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/587572141919605/
teaser: "Donnerstag gibt es selbstgemachte Käsespätzle und Funk von den Rythm
  Fellows!   ____________________  Gleisgarten Öffnungszeiten:  Sonntag -
  Donnersta"
isCrawled: true
---
Donnerstag gibt es selbstgemachte Käsespätzle und Funk von den Rythm Fellows! 

____________________

Gleisgarten Öffnungszeiten: 
Sonntag - Donnerstag 17 - 23 Uhr 
Freitag - Samstag 17 - 24 Uhr 

Wir freuen uns auf euch! 🐇🦊🦆

