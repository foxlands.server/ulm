---
id: "2807053562872385"
title: Kleidertausch und Vortrag Textilindustrie
start: 2020-10-01 17:30
address: Klimacamp Ulm
link: https://www.facebook.com/events/2807053562872385/
teaser: Um 17:30 Uhr startet die Kleidertauschparty, hierfür gerne den eigenen
  Kleiderschrank ausmisten! Im Anschluss um 19 Uhr hält Annemarie Brückner von
  Fi
isCrawled: true
---
Um 17:30 Uhr startet die Kleidertauschparty, hierfür gerne den eigenen Kleiderschrank ausmisten! Im Anschluss um 19 Uhr hält Annemarie Brückner von Fischerins Kleid einen Vortrag zur fairen Textilindustrie.