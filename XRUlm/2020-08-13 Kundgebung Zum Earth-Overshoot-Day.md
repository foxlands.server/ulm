---
id: "702109197035767"
title: Kundgebung Zum Earth-Overshoot-Day
start: 2020-08-13 16:00
end: 2020-08-13 18:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/702109197035767/
teaser: Ohne eine bewohnbare Umwelt, die genug Ressourcen hat für alle, gibt es keine
  Zukunft. Jugend aktiv in Ulm, Fridays For Future Ulm, sowie Extinction R
isCrawled: true
---
Ohne eine bewohnbare Umwelt, die genug Ressourcen hat für alle, gibt es keine Zukunft. Jugend aktiv in Ulm, Fridays For Future Ulm, sowie Extinction Rebellion Ulm macht auf die Mißstände aufmerksam und bietet Lösungsvorschläge an. Zeigen wir Ulm und der Welt, was wir und unsere Stadt für unsere Zukunft tun können!