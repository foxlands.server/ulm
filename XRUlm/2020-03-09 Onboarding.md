---
id: "855362198245709"
title: Onboarding
start: 2020-03-09 19:00
end: 2020-03-09 22:00
address: Heidenheimerstr. 82, Ulm
link: https://www.facebook.com/events/855362198245709/
image: 88237091_1042675879448441_644475862992814080_o.jpg
teaser: Ihr habt Interesse uns und unsere Bewegung näher kennenzulernen oder bei uns
  mitzumachen? Dann ist das Onboarding die richtige Veranstaltung. :)   Die
isCrawled: true
---
Ihr habt Interesse uns und unsere Bewegung näher kennenzulernen oder bei uns mitzumachen? Dann ist das Onboarding die richtige Veranstaltung. :) 

Dieser Abend ist Interessierten gewidmet. Hier erfahrt ihr mehr über unsere Ziele, Werte und Strukturen. Außerdem werden alle Fragen geklärt - bspw. hinsichtlich unserer Forderungen, unserer Aktionen, wie man sich beteiligen kann, etc. 

Das Treffen wird im Bischof-Sproll-Haus Studentenwohnheim (Heidenheimerstr. 82, Ulm) statt finden. Kommt zum Eingang - wir werden euch dort abholen und/oder den Weg zum Raum ausschildern.

Kommt vorbei, bringt gerne Freunde und Bekannte mit.
Wir freuen uns auf Euch! :)

Love and Rage,
XR Ulm
