---
id: "1293243281065096"
title: Plenum Treffen
start: 2020-03-02 19:00
end: 2020-03-02 22:00
locationName: Bürgerhaus Mitte
address: Schaffner Straße, 89073 Ulm
link: https://www.facebook.com/events/1293243281065096/
image: 83946527_1013528262363203_7601611623043694592_o.jpg
teaser: Hallo liebe Rebels,   Wir treffen uns jeden 1. & 3. Montag im Monat im
  Bürgerhaus Mitte zum Plenum. Bei diesen Treffen besprechen wir wichtige
  organis
isCrawled: true
---
Hallo liebe Rebels, 

Wir treffen uns jeden 1. & 3. Montag im Monat im Bürgerhaus Mitte zum Plenum. Bei diesen Treffen besprechen wir wichtige organisatorische Themen.

Für alle, die noch nie bei uns mitgemacht haben: Es wird auch in Zukunft regelmäßig Einführungstreffen geben, die speziell Interessierten gewidmet sind. Die nächste Gelegenheit hierfür wird im März sein (Termin kommt noch!). 

Ansonsten seid ihr aber natürlich auch sehr gerne beim Plenum gesehen. Kommt gerne vorbei!

Unser Treffpunkt: Bürgerhaus Mitte, Schaffnerstraße 17 . 
Wann? 19 Uhr, jeden 1. und 3. Montag im Monat

Wir freuen uns über jeden der kommt! :)