---
id: "2926357144146698"
title: Mahnwache - Eine Mobilprämie für Alle! Kein Geld für Klimasünder
start: 2020-05-29 17:30
end: 2020-05-29 18:30
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/2926357144146698/
image: 100089065_146419063624886_5689797116952576000_n.jpg
teaser: "#MobilPrämieFürAlle  Lobbyisten, Vorstände der Autoindustrie und einige
  Bundesländer fordern eine Kaufprämie für Neuwagen von mehreren Tausend Euro –"
isCrawled: true
---
#MobilPrämieFürAlle

Lobbyisten, Vorstände der Autoindustrie und einige Bundesländer fordern eine Kaufprämie für Neuwagen von mehreren Tausend Euro – finanziert aus Steuergeldern. Damit würde der Rohstoffverbrauch angekurbelt und der Verkauf klimaschädlicher Benzin- und Dieselautos gefördert.
Das darf nicht passieren! Staatliche Hilfen müssen Klima- und Umweltschutz fördern, sozial gerecht und transparent sein und dürfen nicht auf Umwegen der Bereicherung von ohnehin eher besser gestellten Gesellschaftsschichten dienen. Deshalb sind die Staatshilfen zu Gunsten der Wirtschaft an entsprechende Auflagen und Anreize zu knüpfen. Damit werden die Weichen für mehr Gemeinwohl, Nachhaltigkeit und Gerechtigkeit zu gestellt.
Wir fordern eine Mobilprämie für alle, zur Unterstützung nachhaltiger Mobilitätslösungen und aus Gründen sozialer Gerechtigkeit. Sie kann zum Beispiel in Form von Gutscheinen pro Kopf verteilt werden, die für den Kauf von E-Bikes, Leichtelektromobile, für ÖPNV-Jahreskarten oder Carsharing ausgegeben werden können. 
Darüber hinaus sollen direkte Staatshilfen nur Unternehmen erhalten, die einen verbindlichen Klimaschutzplan vorlegen, keine Boni und Dividenden an ihre Vorstände und AktionärInnen auszahlen und keine Gewinne in Steueroasen verlagern. 

Unterstützt unsere Forderungen bei der Mahnwache am Freitag, den 29. Mai von 17.30 bis 18.30 Uhr auf dem Münsterplatz. 

Bitte Mund-Nasen-Schutz verwenden und mindestens 1,50 m Abstand untereinander halten, 5 m zu PassantInnen. Bringt nach Möglichkeit Eure Fahrräder mit, als Signal für nachhaltige Mobilität und um das Abstandhalten zu erleichtern.
Personen, die sich krank fühlen oder krank sind, bleiben bitte zuhause. Damit beim Auftreten einer Corona-Infektion die Teilnehmenden benachrichtigt werden können, notieren wir auf freiwilliger Basis Kontaktdaten der Teilnehmenden. Bitte wendet Euch dafür an einen der Ordner vor Ort.


Unterstützer: ADFC Ulm/Alb-Donau, BUND Ulm, Bündnis für eine agrogentechnikfreie Region (um) Ulm, Extinction Rebellion Ulm, Fridays For Future Ulm/Neu-Ulm, Gemeinwohl-Ökonomie Ulm, Greenpeace Ulm/Neu-Ulm, lokale agenda ulm 21, Parents For Future Ulm/Neu-Ulm/Alb-Donau, Solidarische Landwirtschaft Ulm/Neu-Ulm, Ulmer Netz für eine andere Welt e.V, Umweltgewerkschaft Gruppe Ulm/Neu-Ulm.



Kein Geld für Steinzeittechnologien, Steuertrickser und Klimasünder!

Hier geht´s zur Online-Petition. Bisher haben über 225.000 Menschen unterschrieben!

https://aktion.campact.de/abwrackpraemie-20/appell/teilnehmen


Grafik © ADFC

BUND Donau-Iller, Ulm, Extinction Rebellion Ulm, Fridays For Future Ulm, Parents for Future Ulm, Greenpeace Ulm/Neu-Ulm, Lokale Agenda Ulm 21, #ADFCUlm, #BündnisfüreineagrogentechnikfreieRegionUlm, #SolidarischeLandwirtschaftUlm, #UmweltgewerkschaftUlm
#UlmerNetzfüreineandereWelt