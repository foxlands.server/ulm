---
name: Ulmer Weltladen
website: http://www.ulmer-weltladen.de
email: info@ulmer-weltladen.de
scrape:
  source: facebook
  options:
    page_id: UlmerWeltladen
---
Mit Deiner Entscheidung, faire Produkte zu kaufen, trägst Du dazu bei, die Welt ein wenig gerechter zu gestalten.
Bei uns kannst Du bei schöner Musik aus dem Süden durch die Regale stöbern oder einfach nur ein leckeres Getränk an der Kaffeebar genießen.
