---
name: Kulturbuchhandlung Jastram
website: http://www.jastram-buecher.de
email: 
scrape:
  source: facebook
  options:
    page_id: jastrambuecher
---
Kulturbuchhandlung Jastram. Am Judenhof, dem schönsten Platz in Ulm. Moderne Literatur, Klassiker, Kinderbücher, Musik-CDs, Lesungen, Veranstaltungen, Buchvorstellungen, Bio-Wein und Mirabellen-Schnaps aus der Region.
