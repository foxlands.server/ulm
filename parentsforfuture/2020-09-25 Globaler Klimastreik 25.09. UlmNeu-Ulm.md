---
id: "1617317485094201"
title: Globaler Klimastreik 25.09. Ulm/Neu-Ulm
start: 2020-09-25 15:00
locationName: Ulmer Münster
address: Münsterplatz, 89073 Ulm
link: https://www.facebook.com/events/1617317485094201/
teaser: "Die Erde brennt und trotzdem passiert Nichts! Deshalb heißt es diesen Freitag
  zum globalen Klimastreik: #KeinGradWeiter  Wir haben zu Beginn der Coron"
isCrawled: true
---
Die Erde brennt und trotzdem passiert Nichts!
Deshalb heißt es diesen Freitag zum globalen Klimastreik:
#KeinGradWeiter

Wir haben zu Beginn der Corona-Krise gesehen, dass die Politik entschlossen handeln kann, wenn sie wirklich will. Auf die genauso aktuelle und dringliche Klimakrise wird aber viel zu wenig und viel zu langsam reagiert.

Und deshalb gehen wir wieder auf die Straße:
- Für unsere Zukunft
- Für das Einhalten des Pariser Klimaabkommens
- Für eine evidenzbasierte Politik

Dieses Mal starten wir auf dem Münsterplatz!

Wir wollen beide Krisen ernstnehmen. Das Coronavirus ist real und die Fallzahlen in Ulm und Neu-Ulm sind besorgniserregend hoch.
Deshalb: tragt euren MNS bei der Demo und haltet Abstand!
