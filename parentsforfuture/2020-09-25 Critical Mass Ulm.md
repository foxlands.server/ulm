---
id: "189673625771047"
title: Critical Mass Ulm
start: 2020-09-25 18:30
end: 2020-09-25 20:30
locationName: Stadthaus Ulm
address: Münsterplatz 50, 89073 Ulm
link: https://www.facebook.com/events/189673625771047/
teaser: "Critical Mass Ulm & Neu-Ulm  jeden letzten Freitag im Monat um 18:30
  (Achtung, je nach Jahreszeit evtl. abweichende Startzeiten)  Treffpunkt:
  Stadthau"
isCrawled: true
---
Critical Mass Ulm & Neu-Ulm

jeden letzten Freitag im Monat um 18:30
(Achtung, je nach Jahreszeit evtl. abweichende Startzeiten)

Treffpunkt: Stadthaus Münsterplatz

https://criticalmassulmneuulm.wordpress.com