---
id: "189673635771046"
title: Critical Mass Ulm
start: 2021-01-29 18:30
end: 2021-01-29 20:30
locationName: Stadthaus Ulm
address: Münsterplatz 50, 89073 Ulm
link: https://www.facebook.com/events/189673635771046/
image: 83706683_2828779367180805_8719465256549613568_o.jpg
teaser: "Critical Mass Ulm & Neu-Ulm  jeden letzten Freitag im Monat um 18:30
  (Achtung, je nach Jahreszeit evtl. abweichende Startzeiten)  Treffpunkt:
  Stadthau"
isCrawled: true
---
Critical Mass Ulm & Neu-Ulm

jeden letzten Freitag im Monat um 18:30
(Achtung, je nach Jahreszeit evtl. abweichende Startzeiten)

Treffpunkt: Stadthaus Münsterplatz

https://criticalmassulmneuulm.wordpress.com