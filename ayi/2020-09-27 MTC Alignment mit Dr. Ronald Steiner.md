---
id: "3691018674259966"
title: MTC Alignment mit Dr. Ronald Steiner
start: 2020-09-27 07:00
end: 2020-09-27 15:30
locationName: Ashtanga Yoga Bodensee
address: Braunegger Straße 34a, 78462 Konstanz
link: https://www.facebook.com/events/3691018674259966/
teaser: Don't miss!!! Im Sep dürfen wir wieder... Schon zum zweiten Mal findet eine
  anatomische Fortbildung für Yogalehrer, Physiotherapeuten, Ärzte mit Dr. R
isCrawled: true
---
Don't miss!!! Im Sep dürfen wir wieder...
Schon zum zweiten Mal findet eine anatomische Fortbildung für Yogalehrer, Physiotherapeuten, Ärzte mit Dr. Ronald Steiner statt.

MTC (Modular Therapy Course) Thema:  "Alignment" 
Ausrichtung auf das Lebendige

mit Dr. Ronald Steiner

in Konstanz

Sichere Dir jetzt einen Platz unter dem Link unten...

Nähere Infos findest Du unter ashtangayoga.info/ausbildungen
