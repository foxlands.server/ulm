---
id: "190117145562894"
title: Shiva Dance
start: 2020-02-21 21:00
end: 2020-02-22 00:30
locationName: Shiva Dance
address: Frauenstraße 124, 89073 Ulm
link: https://www.facebook.com/events/190117145562894/
teaser: "Shiva Dance #2/2020. Ich  lade wieder herzlich zu einer neuen Runde barfuß
  tanzen ins AYI ein. Wer organische Beats, Downtempo, melodiösen Techno und"
isCrawled: true
---
Shiva Dance #2/2020. Ich  lade wieder herzlich zu einer neuen Runde barfuß tanzen ins AYI ein. Wer organische Beats, Downtempo, melodiösen Techno und Ethnogrooves mag, ist willkommen, sich tänzerisch bei Kerzenschein im Yogastudio auszutoben. 
Wir freuen uns auf dich. Wenn möglich komm pünktlich, damit wir gemeinsam beginnen können.
Wie immer ohne Eintritt, Schuhe, Alkohol und Drogen. 
Spenden für unsere Unkosten sind willkommen...