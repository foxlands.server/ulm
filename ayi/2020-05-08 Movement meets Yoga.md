---
id: "830327804046223"
title: Movement meets Yoga
start: 2020-08-14 17:00
end: 2020-08-14 20:00
locationName: Element Yoga
address: Pfuelstrasse 5, 10997 Berlin
link: https://www.facebook.com/events/830327804046223/
image: 86348445_2687195534691985_658979993321209856_n.jpg
teaser: "Movement meets Yoga  Mit Dr. Ronald Steiner  Freitag, 14. August 2020, 18-21
  Uhr  Ashtanga Yoga: die bewegte Meditation. Erlebe, wie sich Atem und Bew"
isCrawled: true
---
Movement meets Yoga

Mit Dr. Ronald Steiner

Freitag, 14. August 2020, 18-21 Uhr

Ashtanga Yoga: die bewegte Meditation. Erlebe, wie sich Atem und Bewegung auf faszinierende Weise verbinden – und was animal moves mit Yoga zu tun haben.

Vorbeugen, Rückbeugen, Rotationen – all diese Bewegungen dürften Dir als Yogi vertraut sein. Doch welche Bewegungsmuster kann unser Körper noch ausführen?

Im ersten Teil des Workshops erwartet Dich ein spannendes Experiment:

Wir verlassen gewohnte Muster und loten auf spielerische doch zugleich herausfordernde Art die Möglichkeiten unseres Bewegungsspektrums aus. Das ist nicht nur für unseren gesamten Bewegungsapparat sehr gesund, sondern führt garantiert zu einem frischen Geist. – Du fühlst Dich ein wenig wie ein Kind das Laufen lernt.

Nach einer kurzen Pause (ca. 30-40 min.) geht es weiter mit einer eher traditionellen Yogapraxis. Dennoch wirst Du überrascht sein, wie die neuen Elemente sich in das bekannte auf ungeahnte Weise integrieren und ergänzen. Erlebe die Faszination der bewegten Meditation und lasse Dich von Deinem Atem durch die einzelnen Haltungen tragen.

Für wen?

Der Workshop ist praktisch veranlagt und offen für interessierte Yogaschüler, aber auch für Yogalehrer und Schüler, die sich in der Yogalehrer Ausbildung befinden.

Sei offen für Neues! Erlebe deine Bewegungsspielräume bei neuen Übungen und klassischen Asanas auf spannende Weise anders.

Preis

59€

Buchung & Infos
Onlinebuchung auf der Ashtangayoga.info Seite: Movement meets Yoga mit Dr. Ronald Steiner im Element Yoga Studio Berlin
Infos über yoga@elementyoga.de
Mehr Informationen zu Dr. Ronald Steiner: AshtangaYoga.info
Unsere Empfehlung für Dich

Ergänzend zu diesem Workshop ‚Movement meets Yoga‘, empfehlen wir Dir das Intensiv Workshop Wochenende mit Dr. Ronald Steiner von Samstag bis Sonntag, 9.-10. Mai 2020: Anatomie der Bewegung – Grundlagen für Yogalehrende.

Photography by Wari Om – www.wari.cat