---
id: "1000386393720353"
title: Learning to Fly - Workshop mit Dr. Ronald Steiner
start: 2020-10-30 16:00
end: 2020-10-30 20:00
locationName: Yogaschule SOLIS
address: Gliesmaroder Straße 1, 38106 Braunschweig
link: https://www.facebook.com/events/1000386393720353/
image: 120141798_3444103755610149_974038086164478833_n.jpg
teaser: Nur fliegen ist schöner??? Bei diesem Workshop lernst Du, wie Du scheinbar
  mühelos abheben kannst und Deine Ashtanga Praxis auf diese Weise eine neue,
isCrawled: true
---
Nur fliegen ist schöner??? Bei diesem Workshop lernst Du, wie Du scheinbar mühelos abheben kannst und Deine Ashtanga Praxis auf diese Weise eine neue, spielerische Leichtigkeit gewinnt.

Armbalancen, Handstände, Durchspringen.... Der Ashtanga Yoga bietet jede Menge Gelgenheiten, um zumindest kurzfristig einmal abzuheben und die Leichtigkeit des Fliegens zu erleben. Lerne, wie Du die ersten Schritte machst oder arbeite mit speziellen Technik-Tipps an einer bereits fortgeschrittenen Praxis. Egal ob Anfänger*in oder Profi-Flieger*in: Bei diesem Workshop ist für jede*n etwas dabei!

Dr. Ronald Steiner steht für traditionelles und zugleich innovatives Ashtanga Yoga. Als fortgeschrittener Praktiker ist er fest in der Tradition des Ashtanga verwurzelt und gehört zu den wenigen von Sri K. Pattabhi  Jois autorisierten und BNS Iyengar zertifizierten Lehrern. Ausgehend von seinem Hintergrund als Arzt legt er viel Wert auf genaue Ausrichtung für gesundes und heilsames Üben. Er hilft Dir Deine individuelle Praxis zu entwickeln. Weitere Informationen unter: AshtangaYoga.info

Zeitplan:
Freitag
16:00 bis 20:00 Uhr (inklusive einer ca. 30-45 minütigen Pause)

Veranstaltungsort:
Yogaschule SOLIS
Gliesmaroder Straße 1
38106 Braunschweig

Kosten: 85,- EUR

Wir freuen uns auf Dich :-)!