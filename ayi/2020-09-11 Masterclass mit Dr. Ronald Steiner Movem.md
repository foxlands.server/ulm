---
id: "526634571252354"
title: "Masterclass mit Dr. Ronald Steiner: Movement Yoga"
start: 2020-09-11 17:00
end: 2020-09-11 21:00
locationName: Yogability
address: Mühlenstraße 9, 58313 Herdecke
link: https://www.facebook.com/events/526634571252354/
image: 72639702_2556111354448944_3041170195159711744_n.jpg
teaser: "MASTERCLASS MIT DR. RONALD STEINER:  MOVEMENT YOGA   Vorbeugen, Rückbeugen,
  Rotationen - all diese Bewegungen dürften Dir als Yogi vertraut sein. Doch"
isCrawled: true
---
MASTERCLASS MIT DR. RONALD STEINER: 
MOVEMENT YOGA
 
Vorbeugen, Rückbeugen, Rotationen - all diese Bewegungen dürften Dir als Yogi vertraut sein. Doch welche Bewegungsmuster kann unser Körper noch ausführen? Im ersten Teil des Workshops erwartet Dich ein spannendes Experiment: Wir verlassen gewohnte Muster und loten auf spielerische doch zugleich herausfordernde Art die Möglichkeiten unseres Bewegungsspektrums aus. Das ist nicht nur für unseren gesamten Bewegungsapparat sehr gesund, sondern führt garantiert zu einem frischen Geist. - Du fühlst Dich ein wenig wie ein Kind das Laufen lernt. 

Nach einer kurzen Pause geht es weiter mit einer eher traditionellen Yogapraxis. Dennoch wirst Du überrascht sein, wie die neuen Elemente sich in das bekannte auf ungeahnte Weise integrieren und ergänzen. Erlebe an die Faszination der bewegten Meditation und lasse Dich von Deinem Atem durch die einzelnen Haltungen tragen.
 
DR. RONALD STEINER
ist einer der wenigen Yogalehrer, die ganz traditionell von den indischen Meistern Sri K. Pattabhi Jois und BNS Iyengar autorisiert wurden. Er ist zudem Arzt, Wissenschaftler und Sportmediziner mit Forschungen im Bereich Prävention und Rehabilitation u.a. am Universitätsklinikum Ulm. Die von ihm begründete AYInnovation®-Methode baut eine Brücke zwischen uralter Tradition und fundierter Praxis, zwischen angewandter Anatomie und lebendiger Philosophie, zwischen präziser Technik und praktischer Erfahrung. Dadurch entsteht eine für den Einzelnen maßgeschneiderte, sehr persönliche Yogapraxis – von therapeutisch-präventiv bis hin zu sportlich-akrobatisch. Die modularen und deutschlandweit multizentrischen AYI® Yogalehrer Aus- und Weiterbildungen legen einen besonderen Schwerpunkt auf Prävention, Alignment und Yogatherapie.
 
Mehr Infos zu Ronald unter www.ashtangayoga.info


****

D A T U M  / U H R Z E I T 

Freitag, 28. September 2020
17:00 - 21:00 Uhr (diesmal 4 Stunden)

****

O R T

YOGABILITY
STUDIO QUARTIER RUHR AUE
Mühlenstr. 9
58313 Herdecke

--------

P R E I S

EUR 85.00

--------

I N F O R M A T I O N   &.  A N M E L D U N G

https://www.yogability.de/workshops/dr-ronald-steiner-masterclass-movement-yoga/

Du kannst Dich auch telefonisch unter 02330.8918776, über unsere App oder einfach direkt in unserem Studio anmelden.

www.yogability.de