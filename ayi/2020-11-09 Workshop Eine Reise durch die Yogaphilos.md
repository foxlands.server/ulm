---
id: "2398674086900795"
title: "Workshop: Eine Reise durch die Yogaphilosophie mit Anna Trökes"
start: 2020-11-09 09:15
end: 2020-11-09 12:30
locationName: AshtangaYoga.info
address: Frauenstr. 124, Ulm
link: https://www.facebook.com/events/2398674086900795/
image: 82440772_3600260916665862_1136299199487279104_o.jpg
teaser: Erfahre die Hintergründe des Yoga anhand von jahrtausendalten Quelltexten und
  mithilfe von Annas jahrzehntelanger Erfahrung im Studieren dieser Philos
isCrawled: true
---
Erfahre die Hintergründe des Yoga anhand von jahrtausendalten Quelltexten und mithilfe von Annas jahrzehntelanger Erfahrung im Studieren dieser Philosophie.

Das erwartet dich in diesem Workshop 
Die Methoden des Yoga gründen in den Konzepten der Yoga - Philosophie. Wenn wir sie kennen, dann geben sie zum einen unserer Yoga - Praxis eine ungeahnte Ausrichtung und Tiefe. Gleichzeitig sprechen diese Konzepte viele Probleme an, die uns im Alltag begegnen und zeigen uns neue Sichtweise und Lösungsmöglichkeiten auf. Wir werden uns Textpassagen aus der Bhagavadgita, aus den Upanishaden und aus den Hatha - Yoga - Texten anschauen.

"Die Arbeit mit den Quellentexten des Yoga hilft uns immer wieder, Sammlung und Klarheit für unseren Geist zu erlangen und unserer Übungspraxis Inspiration, Ausrichtung und Tiefe zu geben." 
Anna Trökes

Tagesplan
Montag	9:15 - 12:30 Uhr
Dienstag	9:15 - 12:30 Uhr
Mittwoch	9:15 - 12:30 Uhr

Kosten
145 Euro bzw. 130,50 Euro für AYI Mitglieder

Veranstaltungsort
AYInstitute
Frauenstr. 124
89073 Ulm