---
name: Greenpeace Ulm/Neu-Ulm
website: http://www.ulm.greenpeace.de
email: info@ulm.greenpeace.de
scrape:
  source: facebook
  options:
    page_id: greenpeaceulm
---
Greenpeace ist eine internationale Umweltorganisation, die mit gewaltfreien Aktionen für den Schutz der Lebensgrundlagen kämpft. Unser Ziel ist es, Umweltzerstörung zu verhindern, Verhaltensweisen zu ändern und Lösungen durchzusetzen. Greenpeace ist überparteilich und völlig unabhängig von Politik, Parteien und Industrie. Mehr als eine halbe Million Menschen in Deutschland spenden an Greenpeace und gewährleisten damit unsere tägliche Arbeit zum Schutz der Umwelt.

Greenpeace Ulm/Neu-Ulm ist eine von ca.100 ehrenamtlich arbeitenden Gruppen in Deutschland. Wir stehen als Anlaufstelle für alle Fragen über Greenpeace zur Verfügung. Durch regionale Öffentlichkeitsarbeit versuchen wir, die internationalen Kampagnenthemen in Ulm und um Ulm herum bekannt zu machen. Um dieses Ziel zu erreichen, führen wir Info-Stände in Fußgängerzonen, bei Stadtfesten, auf Konzerten etc. durch; wir halten Vorträge bei Vereinen, Schulen etc. und machen lokale Pressearbeit. Wir beteiligen uns an Podiumsdiskussionen oder Umwelttagen, wir organisieren Ausstellungen und gestalten Projekttage an Schulen. Wir unterstützen Greenpeace-Recherchen vor Ort, beteiligen uns an bundesweiten Aktionstagen von Greenpeace. In verschiedenen Arbeitsgruppen bereiten wir die Information für die Öffentlichkeitsarbeit auf und erarbeiten Vorträge, die von allen Interessierten kostenlos angefordert, bzw. besucht werden können.
