---
id: "1000474493739084"
title: Leben nach Corona - Eure Ideen auf einem Regenbogen
start: 2020-07-18 13:00
end: 2020-07-18 16:00
address: Hans-und-Sophie-Scholl-Platz, 89073 Ulm
link: https://www.facebook.com/events/1000474493739084/
teaser: Wie sieht für euch ein nachhaltigeres Leben in Zukunft aus? 🌈  Kommt am
  Samstag zum Hans-und-Sophie-Scholl-Platz und schreibt eure bunten Ideen und W
isCrawled: true
---
Wie sieht für euch ein nachhaltigeres Leben in Zukunft aus? 🌈 
Kommt am Samstag zum Hans-und-Sophie-Scholl-Platz und schreibt eure bunten Ideen und Wünsche auf den Regenbogen. - Der wandert dann sogar bis nach Berlin!

Wir freuen uns auf ein friedliches, buntes Miteinander! 🤗 🌈 

#lifeaftercorona #buildbackbetter 