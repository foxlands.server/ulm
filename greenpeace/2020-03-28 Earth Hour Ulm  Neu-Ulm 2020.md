---
id: "926048328219232"
title: Earth Hour Ulm / Neu-Ulm 2020
start: 2020-03-28 20:30
end: 2020-03-28 21:30
address: Ulm,/Neu-Ulm
link: https://www.facebook.com/events/926048328219232/
image: 83499234_2546159768826530_6064170359375003648_o.jpg
teaser: "In Ulm und Neu-Ulm gehen wieder die Lichter aus! Setzt ein Zeichen mit uns -
  für Klimaschutz und Energiewende.   Mitmachen kann jedeR: Schaltet am 28."
isCrawled: true
---
In Ulm und Neu-Ulm gehen wieder die Lichter aus! Setzt ein Zeichen mit uns - für Klimaschutz und Energiewende. 

Mitmachen kann jedeR: Schaltet am 28. März für eine Stunde die Lichter, Computer, Fernseher und andere verzichtbare Geräte aus. Postet hier ein Foto, was Ihr in der Stunde für die Erde unternommen habt.