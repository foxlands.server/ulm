---
id: "620572592172682"
title: Podcast "Queer oder Lsbttiq*- was heisst das eigentlich?"
start: 2020-07-22 15:00
end: 2020-07-22 15:30
locationName: Radio free FM
address: Platzgasse 18, 89073 Ulm
link: https://www.facebook.com/events/620572592172682/
image: 106022352_1165436123843148_5091543805308610254_o.jpg
teaser: Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen
  abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. D
isCrawled: true
---
Das Projekt "Vielfalt Leben in Ulm in der einen Welt" möchte Stereotypen abbauen und für Vielfalt und gegen Rassismus und Diskriminierung eintreten. Die Podcast-Reihe behandelt Themen wie Diversity, Menschenrechte, Demokratie oder Extremismus. 

Heute zu Gast sind Julia Heinrich und Sandra Nickel von Young and Queer Ulm e.V. Sie erklären uns, was queer und LSBTTIQ* überhaupt bedeutet. Warum ist eine geschlechtergerechte Sprache wichtig? Sie erklären auch die Aspekte der diversen Geschlechter, wie trans* und nicht-binär, sowie die Intersexualität. 
Live zu hören bei Radio free FM, 102.6 MHz, am 22.07.2020 um 15.00 Uhr oder danach als Podcast bei www.freefm.de