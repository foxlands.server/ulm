---
id: "136085164119987"
title: Gemütliches Zusammensitzen
start: 2020-02-05 19:00
end: 2020-02-05 22:00
address: Henrys Coffee World
link: https://www.facebook.com/events/136085164119987/
image: 84256087_1465237633655098_4607452065436794880_o.jpg
teaser: Hallo zusammen,   unser nächstes Treffen wird ein gemütliches Zusammensitzen
  im Henry's Coffee World in Ulm sein. Wir möchten dabei auch gleich gemein
isCrawled: true
---
Hallo zusammen, 

unser nächstes Treffen wird ein gemütliches Zusammensitzen im Henry's Coffee World in Ulm sein. Wir möchten dabei auch gleich gemeinsam mit euch schon ein paar erste Veranstaltungsideen für das kommende Halbjahr sammeln, also kommt am besten zahlreich und bringt gerne Ideen mit! :-)
Wir treffen uns am kommenden Mittwoch (05.02.) um 19 Uhr direkt vorm Henry's (Hirschstraße 5). Es wäre lieb, wenn ihr bis Sonntag verbindlich Bescheid geben könntet, ob ihr kommt, damit wir ggf. reservieren können.

Wir freuen uns auf euch! :-)