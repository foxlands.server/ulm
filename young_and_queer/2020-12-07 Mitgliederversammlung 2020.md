---
id: "821325742037893"
title: Mitgliederversammlung 2020
start: 2020-12-07 19:00
end: 2020-12-07 21:00
link: https://www.facebook.com/events/821325742037893/
image: 125389897_1726135667565292_5570429440390905647_n.jpg
teaser: Liebe Unterstützer*innen,  Liebe Freund*innen,  Liebe Befürworter*innen,  wir
  laden fristgerecht zur diesjährigen Mitgliederversammlung von Young and
isCrawled: true
---
Liebe Unterstützer*innen, 
Liebe Freund*innen, 
Liebe Befürworter*innen,

wir laden fristgerecht zur diesjährigen Mitgliederversammlung von Young and Queer Ulm e.V. ein.

Liebe Grüße
Sandra, Julia und Julia  