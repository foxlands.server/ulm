---
id: "756694634861335"
title: Mitgliederversammlung
start: 2020-04-27 19:00
end: 2020-04-27 21:00
address: Mähringer Weg 75, 89075 Ulm
link: https://www.facebook.com/events/756694634861335/
image: 89567943_1504309333081261_6865372375022567424_n.jpg
teaser: EINLADUNG ZUR MITGLIEDERVERSAMMLUNG   Liebe Vereinsmitglieder, liebe
  Gruppenbesucher*innen,   wir laden alle Menschen, die in irgendeiner Form mit
  uns
isCrawled: true
---
EINLADUNG ZUR MITGLIEDERVERSAMMLUNG 

Liebe Vereinsmitglieder, liebe Gruppenbesucher*innen, 

wir laden alle Menschen, die in irgendeiner Form mit unserem Verein zu tun haben, zur Mitgliedervsammlung am 27.04.2020 um 19 Uhr in unsere Räumen ein. Die Tagesordnungspunkte können dem Bild oben in der Veranstaltung entnommen werden. 

Aufgrund der momentan noch nicht absehbaren Entwicklungen der Sicherheitsmaßnahmen um COVID-19 behalten wir uns vor, die Veranstaltung kurzfristig zu verschieben. 

Wir freuen uns über alle Teilnehmenden! 

