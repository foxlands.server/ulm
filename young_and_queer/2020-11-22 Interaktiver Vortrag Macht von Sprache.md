---
id: "3428629523840409"
title: "Interaktiver Vortrag: Macht von Sprache"
start: 2020-11-22 11:00
end: 2020-11-22 13:00
link: https://www.facebook.com/events/3428629523840409/
image: 125402163_1726133907565468_5262189144568948715_n.jpg
teaser: In Kooperation mit dem Frauentreff Ulm veranstalten wir am Sonntag, 22.11.2020
  ab 11 Uhr einen online Vortrag via Zoom zum Thema "Macht von Sprache" �
isCrawled: true
---
In Kooperation mit dem Frauentreff Ulm veranstalten wir am Sonntag, 22.11.2020 ab 11 Uhr einen online Vortrag via Zoom zum Thema "Macht von Sprache" 🗯️💬✊
Im Vortrag möchten wir erklären, warum eine geschlechtergerechte Sprache wichtig ist. Dabei zeigen wir auf, inwiefern eine genderexklusive Sprache nach wie vor genutzt wird, um heteronormative und sexistische Machtstrukturen auf einer gesellschaftlichen Ebene zu reproduzieren und damit beispielsweise queere Lebensrealitäten explizit auszuschließen. Zudem möchten wir konkrete Handlungsimpulse geben, wie diese Ungleichverhältnisse stattdessen durch eine geschlechtergerechte Sprache dekonstruiert werden können.

Referentinnen: Julia Heinrich, Sandra Nickel, Julia Gann 

Wir freuen uns auf rege Diskussionen! 💬🗯️🏳️‍🌈