---
id: "580005849327341"
title: Workshop zum Thema „Vielfalt von Geschlecht und sexueller  Orientierung“
start: 2020-07-29 19:00
end: 2020-07-29 21:00
address: Unterer Kuhberg 16, 89077 Ulm
link: https://www.facebook.com/events/580005849327341/
teaser: Wir laden euch in Kooperation mit Young and Queer Ulm e.V. zu einem spannenden
  Workshop zum Thema „Vielfalt von Geschlecht und sexueller Orientierung“
isCrawled: true
---
Wir laden euch in Kooperation mit Young and Queer Ulm e.V. zu einem spannenden Workshop zum Thema „Vielfalt von Geschlecht und sexueller Orientierung“ ein. Da Menschen aus der LGBTTIQA*-Community sowohl im privaten als auch im öffentlichen Raum tagtäglich vielen Diskriminierungsformen ausgesetzt sind, zielt der Workshop darauf ab, ein breiteres Verständnis von sexueller und geschlechtlicher Vielfalt zu schaffen und aufzuzeigen, inwiefern Lebensformen außerhalb heteronormativer Vorstellungen auch heute noch gesellschaftlich sanktioniert werden. Doch LGBTTIQA* - was ist das eigentlich? Diesen und weiteren Fragen werden sich die drei Referentinnen Julia Heinrich, Julia Gann und Sandra Nickel vom Verein Young and Queer Ulm in ihrem Workshop annähern. Im ersten Teil des Workshops werden die Teilnehmer*innen interaktiv für sexuelle, amouröse und geschlechtliche Vielfaltsdimensionen sensibilisiert. Dazu werden sowohl die unterschiedlichen Ebenen von Geschlecht vermittelt als auch grundlegende Begrifflichkeiten innerhalb der verschiedenen Vielfaltsdimensionen erklärt und voneinander abgegrenzt.  Mit diesem Hintergrundwissen werden sich die Teilnehmer*innen im weiteren Verlauf des Workshops den Einfluss und die Macht von Sprache im Zusammenhang mit Sexualität und Sexismus erarbeiten. Im letzten Teil des Workshops wird ein Raum für (anonyme) Fragen und Diskussion gegeben sein, in dem die Teilnehmer*innen mit den Referentinnen über Workshopinhalte sowie persönliche (Diskriminierungs-)Erfahrungen ins Gespräch kommen können. 

Aufgrund der aktuellen Lage ist die Teilnehmerzahl begrenzt. Wir bitten um eine Voranmeldung (per Email: c.can@hdb-ul-nu.de, telefonisch: 0731 / 403 10 123).

Das Ganze findet statt am Mittwoch, den 29.07.2020 um 19 Uhr im HDB Ulm / Neu-Ulm e.V. (Unterer Kuhberg 16, 89077 Ulm) statt. 
