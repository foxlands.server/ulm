---
id: "278312420184654"
title: Vielfalt feiern, Pride leben - Queere Kundgebung
start: 2020-07-12 15:00
end: 2020-07-12 17:00
locationName: Münsterplatz Ulm
address: Münsterplatz 21, 89073 Ulm
link: https://www.facebook.com/events/278312420184654/
image: 107098269_1603590379819822_6172260702908148545_o.jpg
teaser: Save the Date! Vielfalt feiern, Pride leben - Queere Kundgebung zur
  Sichtbarmachung queerer Themen und Lebensrealitäten am 12.07.2020 ab 15 Uhr
  auf de
isCrawled: true
---
Save the Date! Vielfalt feiern, Pride leben - Queere Kundgebung zur Sichtbarmachung queerer Themen und Lebensrealitäten am 12.07.2020 ab 15 Uhr auf dem Münsterplatz Ulm 🏳️‍🌈🏳️‍🌈🏳️‍🌈

Wir freuen uns riesig, diese Veranstaltung in Kooperation mit Jugend aktiv in Ulm und der Ulm - Internationale Stadt im Rahmen des Projekts "Demokratie leben" auf die Beine gestellt zu haben! Danke an alle Kooperationspartner*innen!❤️ Webung gibt's auch über Radio free FM

🏳️‍🌈 Pride-Merch mitbringen

⚠️1,5 Meter Mindestabstand einhalten

⚠️Mund-Nasen-Schutz tragen

Wir freuen uns auf euch! 🏳️‍🌈