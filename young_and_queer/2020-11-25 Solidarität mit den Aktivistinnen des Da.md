---
id: "159855759083227"
title: Solidarität mit den Aktivist*innen des Dannenröder Forstes
start: 2020-11-25 18:30
locationName: Münsterplatz
address: Ulm
link: https://www.facebook.com/events/159855759083227/
image: 126160234_3466585303376999_9096449571920227677_n.jpg
teaser: Anlässlich der am 10.11.2020 begonnenen Räumung des Dannenröder Forstes
  organisieren wir im Rahmen eines solidarischen Zusammenschlusses diverser Orga
isCrawled: true
---
Anlässlich der am 10.11.2020 begonnenen Räumung des Dannenröder Forstes organisieren wir im Rahmen eines solidarischen Zusammenschlusses diverser Organisationen eine Kundgebung. In diesem Kontext wollen wir Stellung zu den aktuellen Entwicklungen nehmen und unsere politischen Forderungen anbringen. 
Im Hinblick auf die bevorstehende Klimakatastrophe liegt es in unserer Verantwortung, uns aktiv gegen solche irrationale Vorhaben zu wehren.