---
id: "642118726666556"
title: Kultur und Kuchen zum Sonntag - Die Jamsession am Ende der Welt
start: 2020-11-01 15:00
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/642118726666556/
image: 122972030_3562487713789238_6854048085259427726_o.jpg
teaser: Ab Montag müssen wir aufgrund der verstärkten Maßnahmen unsere Pforten für
  mindestens einen Monat schließen. Daher haben wir uns für euch etwas ganz S
isCrawled: true
---
Ab Montag müssen wir aufgrund der verstärkten Maßnahmen unsere Pforten für mindestens einen Monat schließen. Daher haben wir uns für euch etwas ganz Spezielles überlegt.

Die Jamsession am Ende der Welt.

Wir möchten euch im Rahmen einer Jamsession die Möglichkeit bieten, euch diesen Sonntag ab 15 Uhr mit euren Besten treffen zu können. Dies ist aber nur möglich, wenn sich jeder Einzelne von uns verantwortlich fühlt. Wir nehmen die bevorstehenden Maßnahmen überaus ernst und möchten euch bitten, dies ebenfalls zu tun. 
Denkt an die Einhaltung der Maskenpflicht und haltet Abstand, wann immer es möglich ist.
Uns allen machen diese Umstände zu schaffen. Doch nur, wenn wir mit gutem Beispiel voran gehen, können wir ein Zeichen setzen, welches FÜR uns Kulturschaffende und unsere Gäste spricht. 

Lasst euch von der Musik entführen, genießt die Gesellschaft eurer Mitmenschen und erfreut euch an dem ein oder anderen tiefgreifenden Gespräch. 
Als kleines Dankeschön wird es Kaffee und Kuchen auf Spendenbasis geben. 
Wir freuen uns auf euer kommen!