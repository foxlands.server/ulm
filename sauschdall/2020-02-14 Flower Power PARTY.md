---
id: "2675295202570661"
title: Flower Power PARTY
start: 2020-02-14 21:00
end: 2020-02-15 05:00
locationName: Jazzkeller Sauschdall
address: Prittwitzstr. 36, 89075 Ulm
link: https://www.facebook.com/events/2675295202570661/
image: 82125430_1528913253930596_949295009159446528_n.jpg
teaser: "DIE Flower Power Party im Sauschdall mit Musik der Sixties :: Hippie Hymnen,
  Flower Power, Rock, Beat, Kraut, Psychedelic Mit den DJ´s: Frank, Invader"
isCrawled: true
---
DIE Flower Power Party im Sauschdall mit Musik der Sixties ::
Hippie Hymnen, Flower Power, Rock, Beat, Kraut, Psychedelic
Mit den DJ´s: Frank, Invader Zzzipp
Mit Ausstellung von MOY-A ab  21.00
PEACE FOREVER
Powered by Radio Free fm