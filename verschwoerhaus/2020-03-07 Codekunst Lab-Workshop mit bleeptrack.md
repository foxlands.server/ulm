---
id: 467gl8qgualqq83n9f67t40nfe
title: Codekunst Lab-Workshop mit bleeptrack
start: 2020-03-07 13:00
end: 2020-03-07 17:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Mit Code lässt sich nicht nur die Welt verändern, sondern auch Kunst
  erstellen. Der Computer bekomm
isCrawled: true
---


Mit Code lässt sich nicht nur die Welt verändern, sondern auch Kunst erstellen. Der Computer bekommt bestimmte Code-Parameter und den Rest erledigt er von selbst: ob mit Hilfe eines Schreibplotters, einer Fräsmaschine oder einer Projektion – alles ist möglich. Man nennt es Codekunst oder generative art. bleeptrack, aka Sabine Wieluch zeigt euch wie es geht 🙂



Wer sich davor Inspirationen holen möchte, schaut ins Ulmer Museum, in dem derzeitig bleeptracks Codekunst-Ausstellung bis zum 23. Feburar 2020 zu bestaunen ist. Sabine gewann jüngst den Förderpreis Junge Ulmer Kunst in der Sparte Bildende Kunst.



Samstag, 7. März 2020; 13:00 – 17.00 Uhr im Verschwörhaus



(Kostenlos und ohne Anmeldung, für alle Jugendlichen und jungen Erwachsenen)



🖥Kostenloser Jugend hackt Lab Codekunst Workshopo

📍Salon

🏢Jugend hackt Lab Ulm

🙂bleeptrack