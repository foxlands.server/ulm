---
id: ohtn003jet66qjunq5gglcb1fo_R20201019T180000
title: ++ fällt aus ++ Maker-Monday
start: 2020-10-19 20:00
end: 2020-10-19 22:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Aufgrund der Covid-19-Auflagen ist der Betrieb nur eingeschraenkt moeglich und
  vornehmlich fuer Mens
isCrawled: true
---
Aufgrund der Covid-19-Auflagen ist der Betrieb nur eingeschraenkt moeglich und vornehmlich fuer Menschen, die bereits Geraeteeinweisungen haben. Neueinweisungen sind ggf. nicht moeglich. Wer neu dazustossen will, bitte unbedingt vorher anmelden, es sind derzeit leider _keine_ Drop-In-einfach-mal-reinkommen-Treffen möglich!



Für die Anmeldung gerne eine Mail an makermonday@verschwoerhaus.de - oder für Leute, die bereits im Slack sind, in #makermonday bescheidgeben.



🖥 Von 3D-Druck bis zur Holzwerkstatt

📍 Werkstätten

🙂 Sabine Wieluch

🙂 Henning Schütz