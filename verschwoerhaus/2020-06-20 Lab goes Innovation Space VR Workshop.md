---
id: 59C339E8-2A80-4CE5-8D0C-990921DC81B1
title: "+++fällt aus +++ Lab goes Innovation Space: VR Workshop"
start: 2020-06-20 09:00
end: 2020-06-20 10:00
address: Online
teaser: Der Workshop muss leider ausfallen. Als Ersatzprogramm gibt es am 18. Juli, 13
  Uhr einen Online-Work
isCrawled: true
---
Der Workshop muss leider ausfallen. Als Ersatzprogramm gibt es am 18. Juli, 13 Uhr einen Online-Workshop zusammen mit dem Innovation Space. 