---
id: 33ul5imar9k4tt89c9420b3e0i
title: "Lab-Talk mit Svenja Gräfen: Netzfeminismus"
start: 2020-10-10 11:00
end: 2020-10-10 14:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: "Zum Jugend hackt Jahresthema ID_entity: Lab im Salon mit Svenja Gräfen: Talk
  und Diskussion zum Them"
isCrawled: true
---
Zum Jugend hackt Jahresthema ID_entity: Lab im Salon mit Svenja Gräfen: Talk und Diskussion zum Thema Netzfeminismus. Kostenlos und ohne Voranmeldung. Weitere Infos folgen.