---
id: 7jv7p67rd0ucvm7v52kdankpd9
title: "+++ fällt aus +++ Wikipedia: Offenes Editieren und Wikidata"
start: 2020-03-21 14:00
end: 2020-03-21 17:00
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: <br><br>Wikidata ist eine freie und offene Wissensdatenbank, die sowohl von
  Menschen als auch von Ma
isCrawled: true
---
<br><br>Wikidata ist eine freie und offene Wissensdatenbank, die sowohl von Menschen als auch von Maschinen gelesen und bearbeitet werden kann.<br><br>Wikidata dient als zentraler Speicher für strukturierte Daten von Wikimedia-Schwesterprojekten wie Wikipedia, Wikivoyage, Wiktionary, Wikisource und anderen.&nbsp;<br><br>Am 21.03. findet zwischen 14:00 und 17:00 ein offenes Editieren im Verschwörhaus statt.&nbsp;<br>Dabei kann auf Wunsch eine kleine Einführung in Wikidata gegeben werden.&nbsp;<br><br>Speziell sollen die Daten der Landtagsabgeordneten Baden-Württembergs in Wikidata untersucht und vervollständigt werden.&nbsp;<br><br>Weitere Infos:&nbsp;<br><a href="https://de.wikipedia.org/wiki/Wikipedia%3AUlm%2FNeu-Ulm">https://de.wikipedia.org/wiki/Wikipedia%3AUlm%2FNeu-Ulm</a><br>🖥 An Wikipedia arbeiten und über Wikidata lernen<br>📍Salon<br>🙂 Looniverse<br>🏢 Wikidata