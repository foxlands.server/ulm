---
id: 6gr6ss8mnnfhdjkkdfr02lskfp
title: "Lab-Online-Workshop: Grafikprogrammierung mit GLSL-Shadern"
start: 2020-05-16 13:00
end: 2020-05-16 15:00
teaser: Kreative Grafik erstellen mit shadern. Wie das geht zeigt euch blinry.Damit
  wir uns während des La
isCrawled: true
---
Kreative Grafik erstellen mit shadern. Wie das geht zeigt euch blinry.



Damit wir uns während des Lab-Workshops unterhalten können, braucht ihr ein Mikro, oder idealerweise ein Headset. Wir treffen uns am Samstag, 16. Mai 2020 um 13 Uhr online auf BBB  https://bbb.ulm.dev/b/glsl-workshop  



Installieren müsst ihr nichts. Wir werden Tools verwenden, die einfach im Browser laufen. Probiert am besten vorher schon einmal aus, ob diese Seite bei euch flüssig läuft: https://www.shadertoy.com/view/ts33R2



Unser Jugend hackt Lab Online Workshop ist kostenlos und offen für alle Jugendlichen und jungen Menschen.

🖥 Online-Lab-Workshop mit blinry

📍 Online auf https://bbb.ulm.dev/b/glsl-workshop

🏢 Jugend hackt Lab und Mentor blinry