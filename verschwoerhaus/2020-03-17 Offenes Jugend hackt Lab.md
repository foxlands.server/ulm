---
id: 3bedn8584nhug2eeqdabg0eh5a
title: +++ fällt aus +++ Offenes Jugend hackt Lab
start: 2020-03-17 17:00
end: 2020-03-17 19:30
address: Verschwörhaus, Weinhof 9, 89073 Ulm, Deutschland
teaser: Beschreibung:Unter dem Motto „Offenes Coden, Tüfteln und Weltverbessern“
  eröffnet euch das Jugend ha
isCrawled: true
---
Beschreibung:Unter dem Motto „Offenes Coden, Tüfteln und Weltverbessern“ eröffnet euch das Jugend hackt Lab eigenen Space zum Experimentieren. Kommt mit euren Ideen ins Verschwörhaus und stellt sie im Lab vor. Habt ihr kein konkretes Projekt und seid auf der Suche nach Inspiration? Don’t worry! Lasst euch von den anderen inspirieren oder fragt im Lab nach kleinen Herausforderungen. Wie bei den Workshops begleiten euch auch im Lab unsere tollen Mentor*innen. 





🖥 Eigene Ideen verwirklichen, Inspiration suchen und mit den Mentor*innen experimentieren 📍 Ganzes Haus



🏢 Jugend Hackt Lab Ulm 



🏢 Jugend Hackt Mentor*innen 

🏢 Tom Novy