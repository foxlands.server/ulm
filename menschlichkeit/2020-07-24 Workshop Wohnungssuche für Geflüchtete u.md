---
id: "1639644082879135"
title: Workshop Wohnungssuche für Geflüchtete und Ehrenamtliche
start: 2020-07-24 17:00
end: 2020-07-24 20:00
locationName: Menschlichkeit
address: Schillerstraße 44, 89077 Ulm
link: https://www.facebook.com/events/1639644082879135/
teaser: Wie finde ich eine Wohnung? Wo suche ich? Was muss ich schreiben?  Was mache
  ich wenn ich eine WG finden möchte?   Diese Fragen werden wir euch beantw
isCrawled: true
---
Wie finde ich eine Wohnung?
Wo suche ich?
Was muss ich schreiben? 
Was mache ich wenn ich eine WG finden möchte? 

Diese Fragen werden wir euch beantworten und viele Tipps geben, wie ihr eine Wohnung finden könnt. 

Bitte anmelden! 
rilli@migration-diakonie.de


بالتعاون مع الدياكونيه و الكاريتاز سوف نقدم يوم 24 الشهر السابع من الساعة الخامسة وحتى الثامنة مساءً ورشة عمل عن "طريقة البحث عن سكن في ألمانيا" للاجئين و المتطوعين في مجال مساعدة اللاجئين. هذه الورشة من المفترض أن تكون بدايةً للإجابة عن الأسئلة التالية:  أين أستطيع أن أبحث عن سكن؟ كيف أكتب رسالة مناسبة من أجل الحصول على فرصة في مقابلة مؤجر البيت؟ ما هو الفرق ما بين الإعلان  لبيت و الإعلان لسكن مشترك (في غي) ؟      يجب المعرفة مسبقاً أن هذه الورشة لن تكون موعد من أجل البحث عن سكن و إنما للتعريف بالطرق و الإمكانيات عن كيفية الحصول و البحث عن سكن في ألمانيا.  في الشهر الثامن سوف نقوم بإجراء ورشة عمل أخرى عن موضوع "عقد  أجار السكن" و عن كافة المعلومات الضرورية حول هذا الموضوع، الإعلان عن هذه الورشة سيتم لاحقاً.

للتسجيل الرجاء كتابة إيميل على العنوان التالي:

rilli@migration-diakonie.de