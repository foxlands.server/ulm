---
id: "524195428228988"
title: "Für Sama: Sondervorstellung mit anschließender Gesprächsrunde"
start: 2020-03-03 19:00
end: 2020-03-03 22:00
locationName: Mephisto Ulm
address: Rosengasse 15, 89073 Ulm
link: https://www.facebook.com/events/524195428228988/
teaser: FOR SAMA ist ein intimer, persönlicher und zutiefst ergreifender Film während
  des syrischen Bürgerkriegs aus der Sicht einer jungen Frau und Mutter. F
isCrawled: true
---
FOR SAMA ist ein intimer, persönlicher und zutiefst ergreifender Film während des syrischen Bürgerkriegs aus der Sicht einer jungen Frau und Mutter.
FOR SAMA ist eine Liebeserklärung einer jungen Mutter an ihre Tochter. Der Film erzählt die Geschichte von Waad al-Kateabs Leben in Aleppo: angefangen von den Aufständen in der syrischen Stadt über die Liebe zu ihrem Mann, ihre Hochzeit bis hin zur Geburt ihrer Tochter SAMA - und all das inmitten eines katastrophalen Krieges. Mit ihrer Kamera liefert Waad al-Kateab unfassbare Bilder über Verlust, Freude und Überleben - und über allem schwebt die Frage: soll sie aus der Stadt fliehen, um das Leben ihrer Tochter zu beschützen? Auch wenn das Verlassen des geliebten Heimatlandes bedeuten würde, den Kampf für die Freiheit aufzugeben, für den sie so viel geopfert hat... (Quelle: Verleih)