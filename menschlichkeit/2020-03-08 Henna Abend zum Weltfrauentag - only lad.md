---
id: "874129696370603"
title: Henna Abend zum Weltfrauentag - only ladies! <3
start: 2020-03-08 16:00
end: 2020-03-08 19:00
address: Cafe Alma, Ehrenäcker 18, Ulm Wiblingen
link: https://www.facebook.com/events/874129696370603/
teaser: Im Namen von menschlichkeit-Ulm e.V. und Ebru e.V., laden wir Sie herzlich zu
  unserem Henna-Abend am 08.März um 16 Uhr bis 19 Uhr im Café Alma, Erenäc
isCrawled: true
---
Im Namen von menschlichkeit-Ulm e.V. und Ebru e.V., laden wir Sie herzlich zu unserem Henna-Abend am 08.März um 16 Uhr bis 19 Uhr im Café Alma, Erenäcker 18, ein. 
Es wird eine Vorführung des traditionell türkischen Henna mit Tanz und Henna Bemalungen geben, dazu gibt es auch eine Kleinigkeit zu essen.

Bitte melden Sie sich an:
menschlichkeit-ulm@outlook.de

Wir freuen uns auf Euch! <3
